from cutsDefinition import *
lstMCMBFullSelection    = [L0MUHlt1MBias, nSPDHits, nLongTr, muonsEta, muonsPT, muonsIsMu, diMuonMass, diMuonPT  ] # full selection for signal extraction
lstMCMBNonResonant      = [L0MUHlt1MBias, nSPDHits, nLongTr, muonsEta, muonsPT, muonsIsMu, NonResMass] # Hlt1MinBias, low Mass
lstMCMBAllJpsi          = lstMCMBFullSelection[:-1] # (almost) full: No dimu_pt lt 1 GeV cut (both oherent and incoherent contributions)
lstMCMBFullSelPhi       = lstMCMBFullSelection + [deltaPhi] # high delta phi
lstMCMBAllJpsiPhi       = lstMCMBAllJpsi    + [deltaPhi] # Dphi > 0.8
lstMCMBEnrIncJpsi       = lstMCMBAllJpsi    + [hidiMuPT] # selects large pt dimuons
lstMCMBEnrIncJpsiPhi    = lstMCMBEnrIncJpsi + [deltaPhi] # selects large pt dimuons and Dphi > 0.8
lstMCMBNonResPhi        = lstMCMBNonResonant + [deltaPhi] # Dphi > 0.8
lstMCMBEnrNonRes        = lstMCMBNonResonant + [hidiMuPT] # selects low pt dimuons
lstMCMBEnrNonResPhi     = lstMCMBEnrNonRes   + [deltaPhi] # selects low pt dimuons and Dphi > 0.8
# Joining cuts and saving in strings
cutMCMBFullSelection = " && ".join([x for x in lstMCMBFullSelection])
cutMCMBNonResonant   = " && ".join([x for x in lstMCMBNonResonant  ])
cutMCMBAllJpsi       = " && ".join([x for x in lstMCMBAllJpsi      ])
cutMCMBFullSelPhi    = " && ".join([x for x in lstMCMBFullSelPhi   ])
cutMCMBAllJpsiPhi    = " && ".join([x for x in lstMCMBAllJpsiPhi   ])
cutMCMBEnrIncJpsi    = " && ".join([x for x in lstMCMBEnrIncJpsi   ])
cutMCMBEnrIncJpsiPhi = " && ".join([x for x in lstMCMBEnrIncJpsiPhi])
cutMCMBNonResPhi     = " && ".join([x for x in lstMCMBNonResPhi    ])
cutMCMBEnrNonRes     = " && ".join([x for x in lstMCMBEnrNonRes    ])
cutMCMBEnrNonResPhi  = " && ".join([x for x in lstMCMBEnrNonResPhi ])

# ALL STRINGS LIST #
lstMinbiasTriggerMC = [\
cutMCMBFullSelection,\
cutMCMBNonResonant,\
cutMCMBAllJpsi,\
cutMCMBFullSelPhi,\
cutMCMBAllJpsiPhi,\
cutMCMBEnrIncJpsi,\
cutMCMBEnrIncJpsiPhi,\
cutMCMBNonResPhi,\
cutMCMBEnrNonRes,\
cutMCMBEnrNonResPhi]
