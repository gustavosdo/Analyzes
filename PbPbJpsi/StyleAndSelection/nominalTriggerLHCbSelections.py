from cutsDefinition import *
# main selections
lstFullSelection     = [runNumbers, triggerDimuon, nSPDHits, nLongTr, muonsEta, muonsPT, diMuonMass, diMuonPT  ] # full selection for signal extraction
lstNonResonant       = [runNumbers, L0MUHlt1MBias, nSPDHits, nLongTr, muonsEta, muonsPT, NonResMass] # Hlt1MinBias, low Mass
lstAllJpsi           = lstFullSelection[:-1] # (almost) full: No dimu_pt lt 1 GeV cut (both coherent and incoherent contributions)
# Full Selection hierarchy
lstFullSelHRC        = lstFullSelection + [HRCselec] # full selection applied for the signal + HRC nominal cut
lstFullSelHRCb       = lstFullSelection + [bHRCselec] # full selection applied for the signal + HRCb nominal cut
lstFullSelHRCf       = lstFullSelection + [fHRCselec] # full selection applied for the signal + HRCf nominal cut
lstFullSelPhi        = lstFullSelection + [deltaPhi] # high delta phi
lstFullSelHRCPhi     = lstFullSelection + [deltaPhi, HRCselec] # high delta phi and HRC < 7
lstFullSelHRCbPhi    = lstFullSelection + [deltaPhi, bHRCselec] # high delta phi and HRCb < 7
lstFullSelHRCfPhi    = lstFullSelection + [deltaPhi, fHRCselec] # high delta phi and HRCf < 7
# Coh.+Incoh. Jpsi Hierarchy
lstAllJpsiHRC        = lstAllJpsi    + [HRCselec] # HRC < 7
lstAllJpsiHRCb       = lstAllJpsi    + [bHRCselec] # HRCb < 7
lstAllJpsiHRCf       = lstAllJpsi    + [fHRCselec] # HRCf < 7
lstAllJpsiPhi        = lstAllJpsi    + [deltaPhi] # Dphi > 0.8
lstAllJpsiHRCPhi     = lstAllJpsi    + [deltaPhi, HRCselec] # Dphi > 0.8, HRC < 7
lstAllJpsiHRCbPhi    = lstAllJpsi    + [deltaPhi, bHRCselec] # Dphi > 0.8, HRCb < 7
lstAllJpsiHRCfPhi    = lstAllJpsi    + [deltaPhi, fHRCselec] # Dphi > 0.8, HRCf < 7
# ENRICHED INCOH JPSI
lstEnrIncJpsi        = lstAllJpsi    + [hidiMuPT] # selects large pt dimuons
lstEnrIncJpsiHRC     = lstEnrIncJpsi + [HRCselec] # selects large pt dimuons and HRC < 7
lstEnrIncJpsiHRCb    = lstEnrIncJpsi + [bHRCselec] # selects large pt dimuons and HRCb < 7
lstEnrIncJpsiHRCf    = lstEnrIncJpsi + [fHRCselec] # selects large pt dimuons and HRCf < 7
lstEnrIncJpsiPhi     = lstEnrIncJpsi + [deltaPhi] # selects large pt dimuons and Dphi > 0.8
lstEnrIncJpsiHRCPhi  = lstEnrIncJpsi + [deltaPhi, HRCselec] # selects large pt dimuons and Dphi > 0.8, HRC < 7
lstEnrIncJpsiHRCbPhi = lstEnrIncJpsi + [deltaPhi, bHRCselec] # selects large pt dimuons and Dphi > 0.8, HRCb < 7
lstEnrIncJpsiHRCfPhi = lstEnrIncJpsi + [deltaPhi, fHRCselec] # selects large pt dimuons and Dphi > 0.8, HRCf < 7
# NonResonant Hierarchy
lstNonResHRC         = lstNonResonant + [HRCselec] # HRC < 7
lstNonResHRCb        = lstNonResonant + [bHRCselec] # HRC < 7
lstNonResHRCf        = lstNonResonant + [fHRCselec] # HRC < 7
lstNonResPhi         = lstNonResonant + [deltaPhi] # Dphi > 0.8
lstNonResHRCPhi      = lstNonResonant + [deltaPhi, HRCselec] # Dphi > 0.8, HRC < 7
lstNonResHRCbPhi     = lstNonResonant + [deltaPhi, bHRCselec] # Dphi > 0.8, HRCb < 7
lstNonResHRCfPhi     = lstNonResonant + [deltaPhi, fHRCselec] # Dphi > 0.8, HRCf < 7
# ENRICHED NONRES
lstEnrNonRes         = lstNonResonant + [lowdiMuPT] # selects low pt dimuons
lstEnrNonResHRC      = lstEnrNonRes   + [HRCselec] # selects low pt dimuons and HRC < 7
lstEnrNonResHRCb     = lstEnrNonRes   + [bHRCselec] # selects low pt dimuons and HRCb < 7
lstEnrNonResHRCf     = lstEnrNonRes   + [fHRCselec] # selects low pt dimuons and HRCf < 7
lstEnrNonResPhi      = lstEnrNonRes   + [deltaPhi] # selects low pt dimuons and Dphi > 0.8
lstEnrNonResHRCPhi   = lstEnrNonRes   + [deltaPhi, HRCselec] # selects low pt dimuons and Dphi > 0.8, HRC < 7
lstEnrNonResHRCbPhi  = lstEnrNonRes   + [deltaPhi, bHRCselec] # selects low pt dimuons and Dphi > 0.8, HRCb < 7
lstEnrNonResHRCfPhi  = lstEnrNonRes   + [deltaPhi, fHRCselec] # selects low pt dimuons and Dphi > 0.8, HRCf < 7
# Inverted HRC 
lstFullSelInvHRC        = lstFullSelection + [InvHRCsel] # full selection applied for the signal + InvHRC nominal cut
lstFullSelInvHRCb       = lstFullSelection + [InvbHRCsel] # full selection applied for the signal + InvHRCb nominal cut
lstFullSelInvHRCf       = lstFullSelection + [InvfHRCsel] # full selection applied for the signal + InvHRCf nominal cut
lstFullSelInvHRCPhi     = lstFullSelection + [deltaPhi, InvHRCsel] # high delta phi and InvHRC 
lstFullSelInvHRCbPhi    = lstFullSelection + [deltaPhi, InvbHRCsel] # high delta phi and InvHRCb 
lstFullSelInvHRCfPhi    = lstFullSelection + [deltaPhi, InvfHRCsel] # high delta phi and InvHRCf 
lstAllJpsiInvHRC        = lstAllJpsi    + [InvHRCsel] # InvHRC 
lstAllJpsiInvHRCb       = lstAllJpsi    + [InvbHRCsel] # InvHRCb 
lstAllJpsiInvHRCf       = lstAllJpsi    + [InvfHRCsel] # InvHRCf 
lstAllJpsiInvHRCPhi     = lstAllJpsi    + [deltaPhi, InvHRCsel] # Dphi > 0.8, InvHRC 
lstAllJpsiInvHRCbPhi    = lstAllJpsi    + [deltaPhi, InvbHRCsel] # Dphi > 0.8, InvHRCb 
lstAllJpsiInvHRCfPhi    = lstAllJpsi    + [deltaPhi, InvfHRCsel] # Dphi > 0.8, InvHRCf 
lstEnrIncJpsiInvHRC     = lstEnrIncJpsi + [InvHRCsel] # selects large pt dimuons and InvHRC 
lstEnrIncJpsiInvHRCb    = lstEnrIncJpsi + [InvbHRCsel] # selects large pt dimuons and InvHRCb 
lstEnrIncJpsiInvHRCf    = lstEnrIncJpsi + [InvfHRCsel] # selects large pt dimuons and InvHRCf 
lstEnrIncJpsiInvHRCPhi  = lstEnrIncJpsi + [deltaPhi, InvHRCsel] # selects large pt dimuons and Dphi > 0.8, InvHRC 
lstEnrIncJpsiInvHRCbPhi = lstEnrIncJpsi + [deltaPhi, InvbHRCsel] # selects large pt dimuons and Dphi > 0.8, InvHRCb 
lstEnrIncJpsiInvHRCfPhi = lstEnrIncJpsi + [deltaPhi, InvfHRCsel] # selects large pt dimuons and Dphi > 0.8, InvHRCf 
lstNonResInvHRC         = lstNonResonant + [InvHRCsel] # InvHRC 
lstNonResInvHRCb        = lstNonResonant + [InvbHRCsel] # InvHRC 
lstNonResInvHRCf        = lstNonResonant + [InvfHRCsel] # InvHRC 
lstNonResInvHRCPhi      = lstNonResonant + [deltaPhi, InvHRCsel] # Dphi > 0.8, InvHRC 
lstNonResInvHRCbPhi     = lstNonResonant + [deltaPhi, InvbHRCsel] # Dphi > 0.8, InvHRCb 
lstNonResInvHRCfPhi     = lstNonResonant + [deltaPhi, InvfHRCsel] # Dphi > 0.8, InvHRCf 
lstEnrNonResInvHRC      = lstEnrNonRes   + [InvHRCsel] # selects low pt dimuons and InvHRC 
lstEnrNonResInvHRCb     = lstEnrNonRes   + [InvbHRCsel] # selects low pt dimuons and InvHRCb 
lstEnrNonResInvHRCf     = lstEnrNonRes   + [InvfHRCsel] # selects low pt dimuons and InvHRCf 
lstEnrNonResInvHRCPhi   = lstEnrNonRes   + [deltaPhi, InvHRCsel] # selects low pt dimuons and Dphi > 0.8, InvHRC 
lstEnrNonResInvHRCbPhi  = lstEnrNonRes   + [deltaPhi, InvbHRCsel] # selects low pt dimuons and Dphi > 0.8, InvHRCb 
lstEnrNonResInvHRCfPhi  = lstEnrNonRes   + [deltaPhi, InvfHRCsel] # selects low pt dimuons and Dphi > 0.8, InvHRCf 
# Joining cuts and saving in strings
cutFullSelection     = " && ".join([x for x in lstFullSelection    ]) 
cutNonResonant       = " && ".join([x for x in lstNonResonant      ])
cutAllJpsi           = " && ".join([x for x in lstAllJpsi          ])
cutFullSelHRC        = " && ".join([x for x in lstFullSelHRC       ])
cutFullSelHRCb       = " && ".join([x for x in lstFullSelHRCb      ])
cutFullSelHRCf       = " && ".join([x for x in lstFullSelHRCf      ])
cutFullSelPhi        = " && ".join([x for x in lstFullSelPhi       ])
cutFullSelHRCPhi     = " && ".join([x for x in lstFullSelHRCPhi    ])
cutFullSelHRCbPhi    = " && ".join([x for x in lstFullSelHRCbPhi   ])
cutFullSelHRCfPhi    = " && ".join([x for x in lstFullSelHRCfPhi   ])
cutAllJpsiHRC        = " && ".join([x for x in lstAllJpsiHRC       ])
cutAllJpsiHRCb       = " && ".join([x for x in lstAllJpsiHRCb      ])
cutAllJpsiHRCf       = " && ".join([x for x in lstAllJpsiHRCf      ])
cutAllJpsiPhi        = " && ".join([x for x in lstAllJpsiPhi       ])
cutAllJpsiHRCPhi     = " && ".join([x for x in lstAllJpsiHRCPhi    ])
cutAllJpsiHRCbPhi    = " && ".join([x for x in lstAllJpsiHRCbPhi   ])
cutAllJpsiHRCfPhi    = " && ".join([x for x in lstAllJpsiHRCfPhi   ])
cutEnrIncJpsi        = " && ".join([x for x in lstEnrIncJpsi       ])
cutEnrIncJpsiHRC     = " && ".join([x for x in lstEnrIncJpsiHRC    ])
cutEnrIncJpsiHRCb    = " && ".join([x for x in lstEnrIncJpsiHRCb   ])
cutEnrIncJpsiHRCf    = " && ".join([x for x in lstEnrIncJpsiHRCf   ])
cutEnrIncJpsiPhi     = " && ".join([x for x in lstEnrIncJpsiPhi    ])
cutEnrIncJpsiHRCbPhi = " && ".join([x for x in lstEnrIncJpsiHRCbPhi])
cutEnrIncJpsiHRCfPhi = " && ".join([x for x in lstEnrIncJpsiHRCfPhi])
cutEnrIncJpsiHRCPhi  = " && ".join([x for x in lstEnrIncJpsiHRCPhi ])
cutNonResHRC         = " && ".join([x for x in lstNonResHRC        ])
cutNonResHRCb        = " && ".join([x for x in lstNonResHRCb       ])
cutNonResHRCf        = " && ".join([x for x in lstNonResHRCf       ])
cutNonResPhi         = " && ".join([x for x in lstNonResPhi        ])
cutNonResHRCPhi      = " && ".join([x for x in lstNonResHRCPhi     ])
cutNonResHRCbPhi     = " && ".join([x for x in lstNonResHRCbPhi    ])
cutNonResHRCfPhi     = " && ".join([x for x in lstNonResHRCfPhi    ])
cutEnrNonRes         = " && ".join([x for x in lstEnrNonRes        ])
cutEnrNonResHRC      = " && ".join([x for x in lstEnrNonResHRC     ])
cutEnrNonResHRCb     = " && ".join([x for x in lstEnrNonResHRCb    ])
cutEnrNonResHRCf     = " && ".join([x for x in lstEnrNonResHRCf    ])
cutEnrNonResPhi      = " && ".join([x for x in lstEnrNonResPhi     ])
cutEnrNonResHRCPhi   = " && ".join([x for x in lstEnrNonResHRCPhi  ])
cutEnrNonResHRCbPhi  = " && ".join([x for x in lstEnrNonResHRCbPhi ])
cutEnrNonResHRCfPhi  = " && ".join([x for x in lstEnrNonResHRCfPhi ])
# Inverted HRC
cutFullSelInvHRC        = " && ".join([x for x in lstFullSelInvHRC       ]) 
cutFullSelInvHRCb       = " && ".join([x for x in lstFullSelInvHRCb      ]) 
cutFullSelInvHRCf       = " && ".join([x for x in lstFullSelInvHRCf      ]) 
cutFullSelInvHRCPhi     = " && ".join([x for x in lstFullSelInvHRCPhi    ]) 
cutFullSelInvHRCbPhi    = " && ".join([x for x in lstFullSelInvHRCbPhi   ]) 
cutFullSelInvHRCfPhi    = " && ".join([x for x in lstFullSelInvHRCfPhi   ]) 
cutAllJpsiInvHRC        = " && ".join([x for x in lstAllJpsiInvHRC       ]) 
cutAllJpsiInvHRCb       = " && ".join([x for x in lstAllJpsiInvHRCb      ]) 
cutAllJpsiInvHRCf       = " && ".join([x for x in lstAllJpsiInvHRCf      ]) 
cutAllJpsiInvHRCPhi     = " && ".join([x for x in lstAllJpsiInvHRCPhi    ]) 
cutAllJpsiInvHRCbPhi    = " && ".join([x for x in lstAllJpsiInvHRCbPhi   ]) 
cutAllJpsiInvHRCfPhi    = " && ".join([x for x in lstAllJpsiInvHRCfPhi   ]) 
cutEnrIncJpsiInvHRC     = " && ".join([x for x in lstEnrIncJpsiInvHRC    ]) 
cutEnrIncJpsiInvHRCb    = " && ".join([x for x in lstEnrIncJpsiInvHRCb   ]) 
cutEnrIncJpsiInvHRCf    = " && ".join([x for x in lstEnrIncJpsiInvHRCf   ]) 
cutEnrIncJpsiInvHRCPhi  = " && ".join([x for x in lstEnrIncJpsiInvHRCPhi ]) 
cutEnrIncJpsiInvHRCbPhi = " && ".join([x for x in lstEnrIncJpsiInvHRCbPhi]) 
cutEnrIncJpsiInvHRCfPhi = " && ".join([x for x in lstEnrIncJpsiInvHRCfPhi]) 
cutNonResInvHRC         = " && ".join([x for x in lstNonResInvHRC        ]) 
cutNonResInvHRCb        = " && ".join([x for x in lstNonResInvHRCb       ]) 
cutNonResInvHRCf        = " && ".join([x for x in lstNonResInvHRCf       ]) 
cutNonResInvHRCPhi      = " && ".join([x for x in lstNonResInvHRCPhi     ]) 
cutNonResInvHRCbPhi     = " && ".join([x for x in lstNonResInvHRCbPhi    ]) 
cutNonResInvHRCfPhi     = " && ".join([x for x in lstNonResInvHRCfPhi    ]) 
cutEnrNonResInvHRC      = " && ".join([x for x in lstEnrNonResInvHRC     ]) 
cutEnrNonResInvHRCb     = " && ".join([x for x in lstEnrNonResInvHRCb    ]) 
cutEnrNonResInvHRCf     = " && ".join([x for x in lstEnrNonResInvHRCf    ]) 
cutEnrNonResInvHRCPhi   = " && ".join([x for x in lstEnrNonResInvHRCPhi  ]) 
cutEnrNonResInvHRCbPhi  = " && ".join([x for x in lstEnrNonResInvHRCbPhi ]) 
cutEnrNonResInvHRCfPhi  = " && ".join([x for x in lstEnrNonResInvHRCfPhi ]) 

# For mass fit
lstFullSelNoMass            = [runNumbers, triggerDimuon, nSPDHits, nLongTr, muonsEta, muonsPT, diMuonPT  ] # full selection except for the mass cut
lstFullSelNoMassHRCdeltaPhi = lstFullSelNoMass + [deltaPhi, HRCselec]
cutFullSelNoMassHRCdeltaPhi = " && ".join([x for x in lstFullSelNoMassHRCdeltaPhi])

# ALL STRINGS LIST #
lstNominalTriggerLHCb = [\
cutFullSelection,\
cutNonResonant,\
cutAllJpsi,\
cutFullSelHRC,\
cutFullSelHRCb,\
cutFullSelHRCf,\
cutFullSelPhi,\
cutFullSelHRCPhi,\
cutFullSelHRCbPhi,\
cutFullSelHRCfPhi,\
cutAllJpsiHRC,\
cutAllJpsiHRCb,\
cutAllJpsiHRCf,\
cutAllJpsiPhi,\
cutAllJpsiHRCPhi,\
cutAllJpsiHRCbPhi,\
cutAllJpsiHRCfPhi,\
cutEnrIncJpsi,\
cutEnrIncJpsiHRC,\
cutEnrIncJpsiHRCb,\
cutEnrIncJpsiHRCf,\
cutEnrIncJpsiPhi,\
cutEnrIncJpsiHRCbPhi,\
cutEnrIncJpsiHRCfPhi,\
cutEnrIncJpsiHRCPhi,\
cutNonResHRC,\
cutNonResHRCb,\
cutNonResHRCf,\
cutNonResPhi,\
cutNonResHRCPhi,\
cutNonResHRCbPhi,\
cutNonResHRCfPhi,\
cutEnrNonRes,\
cutEnrNonResHRC,\
cutEnrNonResHRCb,\
cutEnrNonResHRCf,\
cutEnrNonResPhi,\
cutEnrNonResHRCPhi,\
cutEnrNonResHRCbPhi,\
cutEnrNonResHRCfPhi,\
cutFullSelInvHRC,\
cutFullSelInvHRCb,\
cutFullSelInvHRCf,\
cutFullSelInvHRCPhi,\
cutFullSelInvHRCbPhi,\
cutFullSelInvHRCfPhi,\
cutAllJpsiInvHRC,\
cutAllJpsiInvHRCb,\
cutAllJpsiInvHRCf,\
cutAllJpsiInvHRCPhi,\
cutAllJpsiInvHRCbPhi,\
cutAllJpsiInvHRCfPhi,\
cutEnrIncJpsiInvHRC,\
cutEnrIncJpsiInvHRCb,\
cutEnrIncJpsiInvHRCf,\
cutEnrIncJpsiInvHRCPhi,\
cutEnrIncJpsiInvHRCbPhi,\
cutEnrIncJpsiInvHRCfPhi,\
cutNonResInvHRC,\
cutNonResInvHRCb,\
cutNonResInvHRCf,\
cutNonResInvHRCPhi,\
cutNonResInvHRCbPhi,\
cutNonResInvHRCfPhi,\
cutEnrNonResInvHRC,\
cutEnrNonResInvHRCb,\
cutEnrNonResInvHRCf,\
cutEnrNonResInvHRCPhi,\
cutEnrNonResInvHRCbPhi,\
cutEnrNonResInvHRCfPhi,\
cutFullSelNoMassHRCdeltaPhi] 
