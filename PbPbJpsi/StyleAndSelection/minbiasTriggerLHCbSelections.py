from cutsDefinition import *
# main selections
lstMBFullSelection     = [runNumbers, L0MUHlt1MBTCKS, nSPDHits, nLongTr, muonsEta, muonsPT, diMuonMass, diMuonPT  ] # full selection for signal extraction
lstMBNonResonant       = [runNumbers, L0MUHlt1MBTCKS, nSPDHits, nLongTr, muonsEta, muonsPT, NonResMass] # Hlt1MinBias, low Mass
lstMBAllJpsi           = lstMBFullSelection[:-1] # (almost) full: No dimu_pt lt 1 GeV cut (both oherent and incoherent contributions)
# Full Selection hierarchy
lstMBFullSelHRC        = lstMBFullSelection + [HRCselec] # full selection applied for the signal + HRC nominal cut
lstMBFullSelHRCb       = lstMBFullSelection + [bHRCselec] # full selection applied for the signal + HRCb nominal cut
lstMBFullSelHRCf       = lstMBFullSelection + [fHRCselec] # full selection applied for the signal + HRCf nominal cut
lstMBFullSelPhi        = lstMBFullSelection + [deltaPhi] # high delta phi
lstMBFullSelHRCPhi     = lstMBFullSelection + [deltaPhi, HRCselec] # high delta phi and HRC < 7
lstMBFullSelHRCbPhi    = lstMBFullSelection + [deltaPhi, bHRCselec] # high delta phi and HRCb < 7
lstMBFullSelHRCfPhi    = lstMBFullSelection + [deltaPhi, fHRCselec] # high delta phi and HRCf < 7
# Coh.+Incoh. Jpsi Hierarchy
lstMBAllJpsiHRC        = lstMBAllJpsi    + [HRCselec] # HRC < 7
lstMBAllJpsiHRCb       = lstMBAllJpsi    + [bHRCselec] # HRCb < 7
lstMBAllJpsiHRCf       = lstMBAllJpsi    + [fHRCselec] # HRCf < 7
lstMBAllJpsiPhi        = lstMBAllJpsi    + [deltaPhi] # Dphi > 0.8
lstMBAllJpsiHRCPhi     = lstMBAllJpsi    + [deltaPhi, HRCselec] # Dphi > 0.8, HRC < 7
lstMBAllJpsiHRCbPhi    = lstMBAllJpsi    + [deltaPhi, bHRCselec] # Dphi > 0.8, HRCb < 7
lstMBAllJpsiHRCfPhi    = lstMBAllJpsi    + [deltaPhi, fHRCselec] # Dphi > 0.8, HRCf < 7
# ENRICHED INCOH JPSI
lstMBEnrIncJpsi        = lstMBAllJpsi    + [hidiMuPT] # selects large pt dimuons
lstMBEnrIncJpsiHRC     = lstMBEnrIncJpsi + [HRCselec] # selects large pt dimuons and HRC < 7
lstMBEnrIncJpsiHRCb    = lstMBEnrIncJpsi + [bHRCselec] # selects large pt dimuons and HRCb < 7
lstMBEnrIncJpsiHRCf    = lstMBEnrIncJpsi + [fHRCselec] # selects large pt dimuons and HRCf < 7
lstMBEnrIncJpsiPhi     = lstMBEnrIncJpsi + [deltaPhi] # selects large pt dimuons and Dphi > 0.8
lstMBEnrIncJpsiHRCPhi  = lstMBEnrIncJpsi + [deltaPhi, HRCselec] # selects large pt dimuons and Dphi > 0.8, HRC < 7
lstMBEnrIncJpsiHRCbPhi = lstMBEnrIncJpsi + [deltaPhi, bHRCselec] # selects large pt dimuons and Dphi > 0.8, HRCb < 7
lstMBEnrIncJpsiHRCfPhi = lstMBEnrIncJpsi + [deltaPhi, fHRCselec] # selects large pt dimuons and Dphi > 0.8, HRCf < 7
# NonResonant Hierarchy
lstMBNonResHRC         = lstMBNonResonant + [HRCselec] # HRC < 7
lstMBNonResHRCb        = lstMBNonResonant + [bHRCselec] # HRC < 7
lstMBNonResHRCf        = lstMBNonResonant + [fHRCselec] # HRC < 7
lstMBNonResPhi         = lstMBNonResonant + [deltaPhi] # Dphi > 0.8
lstMBNonResHRCPhi      = lstMBNonResonant + [deltaPhi, HRCselec] # Dphi > 0.8, HRC < 7
lstMBNonResHRCbPhi     = lstMBNonResonant + [deltaPhi, bHRCselec] # Dphi > 0.8, HRCb < 7
lstMBNonResHRCfPhi     = lstMBNonResonant + [deltaPhi, fHRCselec] # Dphi > 0.8, HRCf < 7
# ENRICHED NONRES
lstMBEnrNonRes         = lstMBNonResonant + [hidiMuPT] # selects low pt dimuons
lstMBEnrNonResHRC      = lstMBEnrNonRes   + [HRCselec] # selects low pt dimuons and HRC < 7
lstMBEnrNonResHRCb     = lstMBEnrNonRes   + [bHRCselec] # selects low pt dimuons and HRCb < 7
lstMBEnrNonResHRCf     = lstMBEnrNonRes   + [fHRCselec] # selects low pt dimuons and HRCf < 7
lstMBEnrNonResPhi      = lstMBEnrNonRes   + [deltaPhi] # selects low pt dimuons and Dphi > 0.8
lstMBEnrNonResHRCPhi   = lstMBEnrNonRes   + [deltaPhi, HRCselec] # selects low pt dimuons and Dphi > 0.8, HRC < 7
lstMBEnrNonResHRCbPhi  = lstMBEnrNonRes   + [deltaPhi, bHRCselec] # selects low pt dimuons and Dphi > 0.8, HRCb < 7
lstMBEnrNonResHRCfPhi  = lstMBEnrNonRes   + [deltaPhi, fHRCselec] # selects low pt dimuons and Dphi > 0.8, HRCf < 7
# Inverted HRC selections
lstMBFullSelInvHRC        = lstMBFullSelection + [InvHRCsel] # full selection applied for the signal + InvHRC nominal cut
lstMBFullSelInvHRCb       = lstMBFullSelection + [InvbHRCsel] # full selection applied for the signal + InvHRCb nominal cut
lstMBFullSelInvHRCf       = lstMBFullSelection + [InvfHRCsel] # full selection applied for the signal + InvHRCf nominal cut
lstMBFullSelInvHRCPhi     = lstMBFullSelection + [deltaPhi, InvHRCsel] # high delta phi and InvHRC 
lstMBFullSelInvHRCbPhi    = lstMBFullSelection + [deltaPhi, InvbHRCsel] # high delta phi and InvHRCb 
lstMBFullSelInvHRCfPhi    = lstMBFullSelection + [deltaPhi, InvfHRCsel] # high delta phi and InvHRCf 
lstMBAllJpsiInvHRC        = lstMBAllJpsi    + [InvHRCsel] # InvHRC 
lstMBAllJpsiInvHRCb       = lstMBAllJpsi    + [InvbHRCsel] # InvHRCb 
lstMBAllJpsiInvHRCf       = lstMBAllJpsi    + [InvfHRCsel] # InvHRCf 
lstMBAllJpsiInvHRCPhi     = lstMBAllJpsi    + [deltaPhi, InvHRCsel] # Dphi > 0.8, InvHRC 
lstMBAllJpsiInvHRCbPhi    = lstMBAllJpsi    + [deltaPhi, InvbHRCsel] # Dphi > 0.8, InvHRCb 
lstMBAllJpsiInvHRCfPhi    = lstMBAllJpsi    + [deltaPhi, InvfHRCsel] # Dphi > 0.8, InvHRCf 
lstMBEnrIncJpsiInvHRC     = lstMBEnrIncJpsi + [InvHRCsel] # selects large pt dimuons and InvHRC 
lstMBEnrIncJpsiInvHRCb    = lstMBEnrIncJpsi + [InvbHRCsel] # selects large pt dimuons and InvHRCb 
lstMBEnrIncJpsiInvHRCf    = lstMBEnrIncJpsi + [InvfHRCsel] # selects large pt dimuons and InvHRCf 
lstMBEnrIncJpsiInvHRCPhi  = lstMBEnrIncJpsi + [deltaPhi, InvHRCsel] # selects large pt dimuons and Dphi > 0.8, InvHRC 
lstMBEnrIncJpsiInvHRCbPhi = lstMBEnrIncJpsi + [deltaPhi, InvbHRCsel] # selects large pt dimuons and Dphi > 0.8, InvHRCb 
lstMBEnrIncJpsiInvHRCfPhi = lstMBEnrIncJpsi + [deltaPhi, InvfHRCsel] # selects large pt dimuons and Dphi > 0.8, InvHRCf 
lstMBNonResInvHRC         = lstMBNonResonant + [InvHRCsel] # InvHRC 
lstMBNonResInvHRCb        = lstMBNonResonant + [InvbHRCsel] # InvHRC 
lstMBNonResInvHRCf        = lstMBNonResonant + [InvfHRCsel] # InvHRC 
lstMBNonResInvHRCPhi      = lstMBNonResonant + [deltaPhi, InvHRCsel] # Dphi > 0.8, InvHRC 
lstMBNonResInvHRCbPhi     = lstMBNonResonant + [deltaPhi, InvbHRCsel] # Dphi > 0.8, InvHRCb 
lstMBNonResInvHRCfPhi     = lstMBNonResonant + [deltaPhi, InvfHRCsel] # Dphi > 0.8, InvHRCf 
lstMBEnrNonResInvHRC      = lstMBEnrNonRes   + [InvHRCsel] # selects low pt dimuons and InvHRC 
lstMBEnrNonResInvHRCb     = lstMBEnrNonRes   + [InvbHRCsel] # selects low pt dimuons and InvHRCb 
lstMBEnrNonResInvHRCf     = lstMBEnrNonRes   + [InvfHRCsel] # selects low pt dimuons and InvHRCf 
lstMBEnrNonResInvHRCPhi   = lstMBEnrNonRes   + [deltaPhi, InvHRCsel] # selects low pt dimuons and Dphi > 0.8, InvHRC 
lstMBEnrNonResInvHRCbPhi  = lstMBEnrNonRes   + [deltaPhi, InvbHRCsel] # selects low pt dimuons and Dphi > 0.8, InvHRCb 
lstMBEnrNonResInvHRCfPhi  = lstMBEnrNonRes   + [deltaPhi, InvfHRCsel] # selects low pt dimuons and Dphi > 0.8, InvHRCf 
# Joining cuts and saving in strings
cutMBFullSelection     = " && ".join([x for x in lstMBFullSelection    ]) 
cutMBNonResonant       = " && ".join([x for x in lstMBNonResonant      ])
cutMBAllJpsi           = " && ".join([x for x in lstMBAllJpsi          ])
cutMBFullSelHRC        = " && ".join([x for x in lstMBFullSelHRC       ])
cutMBFullSelHRCb       = " && ".join([x for x in lstMBFullSelHRCb      ])
cutMBFullSelHRCf       = " && ".join([x for x in lstMBFullSelHRCf      ])
cutMBFullSelPhi        = " && ".join([x for x in lstMBFullSelPhi       ])
cutMBFullSelHRCPhi     = " && ".join([x for x in lstMBFullSelHRCPhi    ])
cutMBFullSelHRCbPhi    = " && ".join([x for x in lstMBFullSelHRCbPhi   ])
cutMBFullSelHRCfPhi    = " && ".join([x for x in lstMBFullSelHRCfPhi   ])
cutMBAllJpsiHRC        = " && ".join([x for x in lstMBAllJpsiHRC       ])
cutMBAllJpsiHRCb       = " && ".join([x for x in lstMBAllJpsiHRCb      ])
cutMBAllJpsiHRCf       = " && ".join([x for x in lstMBAllJpsiHRCf      ])
cutMBAllJpsiPhi        = " && ".join([x for x in lstMBAllJpsiPhi       ])
cutMBAllJpsiHRCPhi     = " && ".join([x for x in lstMBAllJpsiHRCPhi    ])
cutMBAllJpsiHRCbPhi    = " && ".join([x for x in lstMBAllJpsiHRCbPhi   ])
cutMBAllJpsiHRCfPhi    = " && ".join([x for x in lstMBAllJpsiHRCfPhi   ])
cutMBEnrIncJpsi        = " && ".join([x for x in lstMBEnrIncJpsi       ])
cutMBEnrIncJpsiHRC     = " && ".join([x for x in lstMBEnrIncJpsiHRC    ])
cutMBEnrIncJpsiHRCb    = " && ".join([x for x in lstMBEnrIncJpsiHRCb   ])
cutMBEnrIncJpsiHRCf    = " && ".join([x for x in lstMBEnrIncJpsiHRCf   ])
cutMBEnrIncJpsiPhi     = " && ".join([x for x in lstMBEnrIncJpsiPhi    ])
cutMBEnrIncJpsiHRCbPhi = " && ".join([x for x in lstMBEnrIncJpsiHRCbPhi])
cutMBEnrIncJpsiHRCfPhi = " && ".join([x for x in lstMBEnrIncJpsiHRCfPhi])
cutMBEnrIncJpsiHRCPhi  = " && ".join([x for x in lstMBEnrIncJpsiHRCPhi ])
cutMBNonResHRC         = " && ".join([x for x in lstMBNonResHRC        ])
cutMBNonResHRCb        = " && ".join([x for x in lstMBNonResHRCb       ])
cutMBNonResHRCf        = " && ".join([x for x in lstMBNonResHRCf       ])
cutMBNonResPhi         = " && ".join([x for x in lstMBNonResPhi        ])
cutMBNonResHRCPhi      = " && ".join([x for x in lstMBNonResHRCPhi     ])
cutMBNonResHRCbPhi     = " && ".join([x for x in lstMBNonResHRCbPhi    ])
cutMBNonResHRCfPhi     = " && ".join([x for x in lstMBNonResHRCfPhi    ])
cutMBEnrNonRes         = " && ".join([x for x in lstMBEnrNonRes        ])
cutMBEnrNonResHRC      = " && ".join([x for x in lstMBEnrNonResHRC     ])
cutMBEnrNonResHRCb     = " && ".join([x for x in lstMBEnrNonResHRCb    ])
cutMBEnrNonResHRCf     = " && ".join([x for x in lstMBEnrNonResHRCf    ])
cutMBEnrNonResPhi      = " && ".join([x for x in lstMBEnrNonResPhi     ])
cutMBEnrNonResHRCPhi   = " && ".join([x for x in lstMBEnrNonResHRCPhi  ])
cutMBEnrNonResHRCbPhi  = " && ".join([x for x in lstMBEnrNonResHRCbPhi ])
cutMBEnrNonResHRCfPhi  = " && ".join([x for x in lstMBEnrNonResHRCfPhi ])
# Inverted HRC
cutMBFullSelInvHRC        = " && ".join([x for x in lstMBFullSelInvHRC       ]) 
cutMBFullSelInvHRCb       = " && ".join([x for x in lstMBFullSelInvHRCb      ]) 
cutMBFullSelInvHRCf       = " && ".join([x for x in lstMBFullSelInvHRCf      ]) 
cutMBFullSelInvHRCPhi     = " && ".join([x for x in lstMBFullSelInvHRCPhi    ]) 
cutMBFullSelInvHRCbPhi    = " && ".join([x for x in lstMBFullSelInvHRCbPhi   ]) 
cutMBFullSelInvHRCfPhi    = " && ".join([x for x in lstMBFullSelInvHRCfPhi   ]) 
cutMBAllJpsiInvHRC        = " && ".join([x for x in lstMBAllJpsiInvHRC       ]) 
cutMBAllJpsiInvHRCb       = " && ".join([x for x in lstMBAllJpsiInvHRCb      ]) 
cutMBAllJpsiInvHRCf       = " && ".join([x for x in lstMBAllJpsiInvHRCf      ]) 
cutMBAllJpsiInvHRCPhi     = " && ".join([x for x in lstMBAllJpsiInvHRCPhi    ]) 
cutMBAllJpsiInvHRCbPhi    = " && ".join([x for x in lstMBAllJpsiInvHRCbPhi   ]) 
cutMBAllJpsiInvHRCfPhi    = " && ".join([x for x in lstMBAllJpsiInvHRCfPhi   ]) 
cutMBEnrIncJpsiInvHRC     = " && ".join([x for x in lstMBEnrIncJpsiInvHRC    ]) 
cutMBEnrIncJpsiInvHRCb    = " && ".join([x for x in lstMBEnrIncJpsiInvHRCb   ]) 
cutMBEnrIncJpsiInvHRCf    = " && ".join([x for x in lstMBEnrIncJpsiInvHRCf   ]) 
cutMBEnrIncJpsiInvHRCPhi  = " && ".join([x for x in lstMBEnrIncJpsiInvHRCPhi ]) 
cutMBEnrIncJpsiInvHRCbPhi = " && ".join([x for x in lstMBEnrIncJpsiInvHRCbPhi]) 
cutMBEnrIncJpsiInvHRCfPhi = " && ".join([x for x in lstMBEnrIncJpsiInvHRCfPhi]) 
cutMBNonResInvHRC         = " && ".join([x for x in lstMBNonResInvHRC        ]) 
cutMBNonResInvHRCb        = " && ".join([x for x in lstMBNonResInvHRCb       ]) 
cutMBNonResInvHRCf        = " && ".join([x for x in lstMBNonResInvHRCf       ]) 
cutMBNonResInvHRCPhi      = " && ".join([x for x in lstMBNonResInvHRCPhi     ]) 
cutMBNonResInvHRCbPhi     = " && ".join([x for x in lstMBNonResInvHRCbPhi    ]) 
cutMBNonResInvHRCfPhi     = " && ".join([x for x in lstMBNonResInvHRCfPhi    ]) 
cutMBEnrNonResInvHRC      = " && ".join([x for x in lstMBEnrNonResInvHRC     ]) 
cutMBEnrNonResInvHRCb     = " && ".join([x for x in lstMBEnrNonResInvHRCb    ]) 
cutMBEnrNonResInvHRCf     = " && ".join([x for x in lstMBEnrNonResInvHRCf    ]) 
cutMBEnrNonResInvHRCPhi   = " && ".join([x for x in lstMBEnrNonResInvHRCPhi  ]) 
cutMBEnrNonResInvHRCbPhi  = " && ".join([x for x in lstMBEnrNonResInvHRCbPhi ]) 
cutMBEnrNonResInvHRCfPhi  = " && ".join([x for x in lstMBEnrNonResInvHRCfPhi ]) 

# LIST WITH ALL STRINGS #
lstMinbiasTriggerLHCb = [\
cutMBFullSelection,\
cutMBNonResonant,\
cutMBAllJpsi,\
cutMBFullSelHRC,\
cutMBFullSelHRCb,\
cutMBFullSelHRCf,\
cutMBFullSelPhi,\
cutMBFullSelHRCPhi,\
cutMBFullSelHRCbPhi,\
cutMBFullSelHRCfPhi,\
cutMBAllJpsiHRC,\
cutMBAllJpsiHRCb,\
cutMBAllJpsiHRCf,\
cutMBAllJpsiPhi,\
cutMBAllJpsiHRCPhi,\
cutMBAllJpsiHRCbPhi,\
cutMBAllJpsiHRCfPhi,\
cutMBEnrIncJpsi,\
cutMBEnrIncJpsiHRC,\
cutMBEnrIncJpsiHRCb,\
cutMBEnrIncJpsiHRCf,\
cutMBEnrIncJpsiPhi,\
cutMBEnrIncJpsiHRCbPhi,\
cutMBEnrIncJpsiHRCfPhi,\
cutMBEnrIncJpsiHRCPhi,\
cutMBNonResHRC,\
cutMBNonResHRCb,\
cutMBNonResHRCf,\
cutMBNonResPhi,\
cutMBNonResHRCPhi,\
cutMBNonResHRCbPhi,\
cutMBNonResHRCfPhi,\
cutMBEnrNonRes,\
cutMBEnrNonResHRC,\
cutMBEnrNonResHRCb,\
cutMBEnrNonResHRCf,\
cutMBEnrNonResPhi,\
cutMBEnrNonResHRCPhi,\
cutMBEnrNonResHRCbPhi,\
cutMBEnrNonResHRCfPhi,\
cutMBFullSelInvHRC,\
cutMBFullSelInvHRCb,\
cutMBFullSelInvHRCf,\
cutMBFullSelInvHRCPhi,\
cutMBFullSelInvHRCbPhi,\
cutMBFullSelInvHRCfPhi,\
cutMBAllJpsiInvHRC,\
cutMBAllJpsiInvHRCb,\
cutMBAllJpsiInvHRCf,\
cutMBAllJpsiInvHRCPhi,\
cutMBAllJpsiInvHRCbPhi,\
cutMBAllJpsiInvHRCfPhi,\
cutMBEnrIncJpsiInvHRC,\
cutMBEnrIncJpsiInvHRCb,\
cutMBEnrIncJpsiInvHRCf,\
cutMBEnrIncJpsiInvHRCPhi,\
cutMBEnrIncJpsiInvHRCbPhi,\
cutMBEnrIncJpsiInvHRCfPhi,\
cutMBNonResInvHRC,\
cutMBNonResInvHRCb,\
cutMBNonResInvHRCf,\
cutMBNonResInvHRCPhi,\
cutMBNonResInvHRCbPhi,\
cutMBNonResInvHRCfPhi,\
cutMBEnrNonResInvHRC,\
cutMBEnrNonResInvHRCb,\
cutMBEnrNonResInvHRCf,\
cutMBEnrNonResInvHRCPhi,\
cutMBEnrNonResInvHRCbPhi,\
cutMBEnrNonResInvHRCfPhi]
