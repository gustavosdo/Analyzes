from cutsDefinition import *
import ROOT
from ROOT import *


lstMCNonResonantNotIsMu = [L0MUHlt1MBias, nSPDHits, nLongTr, muonsEta, muonsPT, NonResMass]

cutMCNonResonantNotIsMu = " && ".join([x for x in lstMCNonResonantNotIsMu])

MC_diMu_PbPb_2015 = TChain('',''); MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree')

test =  TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NominalNonResonant_noIsMu_test.root", "recreate")

test.cd()

treeOut = MC_diMu_PbPb_2015.CopyTree(cutMCNonResonantNotIsMu)

treeOut.Write()

test.Close()
