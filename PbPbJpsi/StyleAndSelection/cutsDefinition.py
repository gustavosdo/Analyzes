# * * * * * #
# Jpsi mass by Particle Data Group
M_Jpsi = 3096.900
# Jpsi mass windows for this analysis: 65 MeV around Jpsi
lowestMass = M_Jpsi - 65.
highestMass = M_Jpsi + 65.

# * * * * * #
# individual cuts
HLT1TCK    = "((HLT1TCK == 19042883) || (HLT1TCK == 19071555))" # Hlt1MBiasVelo runs on L0Muon triggered events
L0MUON     = "(J_psi_1S_L0MUONDecision_TOS)" # at least one muon pt gt(greater than) 900 mev
L0SPD      = "(J_psi_1S_L0SPDDecision_Dec)" # nSPDHits > 2 && HCAL E_T > 240 MeV
Hlt1MBias  = "(J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec)" # nVeloTracks geq 1
Hlt1HiMas  = "(J_psi_1S_Hlt1DiMuonHighMassDecision_TOS)" # dimuon mass gt 2.7 GeV
runNumbers = "!((runNumber > 169024) && (runNumber < 169091))" # not bad lumi runs
nSPDHits   = "(nSPDHits < 20)" # CEP-like event
nLongTr    = "(nLongTracks == 2)"
muonsEta   = "((muplus_eta > 2) && (muplus_eta < 4.5) && (muminus_eta > 2) && (muminus_eta < 4.5))" # lhcb acceptance
muonsPT    = "((muplus_PT > 500) && (muminus_PT > 500))"
muonsIsMu  = "((muplus_isMuon == 1) && (muminus_isMuon == 1))"
diMuonPT   = "(J_psi_1S_PT < 1000)" # CEP event: product have low pt
diMuonMass = "((J_psi_1S_M >"+str(lowestMass)+") && (J_psi_1S_M <"+str(highestMass)+"))" # tight mass window
NonResMass = "(J_psi_1S_M < 2700)" # nonresonant mass
lowdiMuPT  = "(J_psi_1S_LNPT2 < -5)" # enrich NonResonant contribution
hidiMuPT   = "(J_psi_1S_LNPT2 > -2)" # enrich inelastic Jpsi (is equal to pt gt 0.37 GeV)
HRCselec   = "(log_hrc_fom_v2 < 7)" # HERSCHEL selection
InvHRCsel  = "(log_hrc_fom_v2 > 7)" # Inverted HERSCHEL selection
bHRCselec  = "(log_hrc_fom_B_v2 < 7)" # HERSCHEL selection (only on B)
fHRCselec  = "(log_hrc_fom_F_v2 < 7)" # HERSCHEL selection (only on F)
InvbHRCsel = "(log_hrc_fom_B_v2 > 7)" # Inverted HERSCHEL selection (only on B)
InvfHRCsel = "(log_hrc_fom_F_v2 > 7)" # Inverted HERSCHEL selection (only on F)
deltaPhi   = "(deltaPhi_muplus_muminus > 0.9)"

# * * * * * #
# Trigger Paths #
triggerDimuon  = L0MUON+" && "+Hlt1HiMas
triggerMinBias = L0SPD+" && "+Hlt1MBias
L0MUHlt1MBias  = L0MUON+" && "+Hlt1MBias
L0MUHlt1MBTCKS = L0MUON+" && "+Hlt1MBias+" && "+HLT1TCK
