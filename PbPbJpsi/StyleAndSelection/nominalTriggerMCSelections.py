from cutsDefinition import *
lstMCFullSelection    = [triggerDimuon, nSPDHits, nLongTr, muonsEta, muonsPT, muonsIsMu, diMuonMass, diMuonPT  ] # full selection for signal extraction
lstMCFullSelNoTrigger = lstMCFullSelection[1:] # almost full selection for signal extraction (no trigger)
lstMCNonResonant      = [L0MUHlt1MBias, nSPDHits, nLongTr, muonsEta, muonsPT, muonsIsMu, NonResMass] # Hlt1MinBias, low Mass
lstMCAllJpsi          = lstMCFullSelection[:-1] # (almost) full: No dimu_pt lt 1 GeV cut (both oherent and incoherent contributions)
lstMCFullSelPhi       = lstMCFullSelection + [deltaPhi] # high delta phi
lstMCFullSelNoTrigPhi = lstMCFullSelNoTrigger + [deltaPhi] # high delta phi
lstMCAllJpsiPhi       = lstMCAllJpsi    + [deltaPhi] # Dphi > 0.8
lstMCEnrIncJpsi       = lstMCAllJpsi    + [hidiMuPT] # selects large pt dimuons
lstMCEnrIncJpsiPhi    = lstMCEnrIncJpsi + [deltaPhi] # selects large pt dimuons and Dphi > 0.8
lstMCNonResPhi        = lstMCNonResonant + [deltaPhi] # Dphi > 0.8
lstMCEnrNonRes        = lstMCNonResonant + [hidiMuPT] # selects low pt dimuons
lstMCEnrNonResPhi     = lstMCEnrNonRes   + [deltaPhi] # selects low pt dimuons and Dphi > 0.8
# Joining cuts and saving in strings
cutMCFullSelection    = " && ".join([x for x in lstMCFullSelection]) 
cutMCFullSelNoTrigger = " && ".join([x for x in lstMCFullSelNoTrigger])
cutMCNonResonant      = " && ".join([x for x in lstMCNonResonant  ])
cutMCAllJpsi          = " && ".join([x for x in lstMCAllJpsi      ])
cutMCFullSelPhi       = " && ".join([x for x in lstMCFullSelPhi   ])
cutMCFullSelNoTrigPhi = " && ".join([x for x in lstMCFullSelNoTrigPhi])
cutMCAllJpsiPhi       = " && ".join([x for x in lstMCAllJpsiPhi   ])
cutMCEnrIncJpsi       = " && ".join([x for x in lstMCEnrIncJpsi   ])
cutMCEnrIncJpsiPhi    = " && ".join([x for x in lstMCEnrIncJpsiPhi])
cutMCNonResPhi        = " && ".join([x for x in lstMCNonResPhi    ])
cutMCEnrNonRes        = " && ".join([x for x in lstMCEnrNonRes    ])
cutMCEnrNonResPhi     = " && ".join([x for x in lstMCEnrNonResPhi ])

# For mass plot/fit
lstMCFullSelNoMassHRCdeltaPhi = [triggerDimuon, nSPDHits, nLongTr, muonsEta, muonsPT, muonsIsMu, diMuonPT, deltaPhi] # full selection except for the mass cut
cutMCFullSelNoMassHRCdeltaPhi = " && ".join([x for x in lstMCFullSelNoMassHRCdeltaPhi])
# ALL STRINGS LIST #
lstNominalTriggerMC = [\
cutMCFullSelNoTrigger,\
cutMCFullSelection,\
cutMCNonResonant,\
cutMCAllJpsi,\
cutMCFullSelPhi,\
cutMCFullSelNoTrigPhi,\
cutMCAllJpsiPhi,\
cutMCEnrIncJpsi,\
cutMCEnrIncJpsiPhi,\
cutMCNonResPhi,\
cutMCEnrNonRes,\
cutMCEnrNonResPhi,\
\
cutMCFullSelNoMassHRCdeltaPhi]
