//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Oct  8 17:31:37 2018 by ROOT version 6.14/04
// from TChain DecayTree/
//////////////////////////////////////////////////////////

#ifndef makeNewTuple_h
#define makeNewTuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class makeNewTuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxJ_psi_1S_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxJ_psi_1S_OWNPV_COV = 1;
   static constexpr Int_t kMaxmuplus_OWNPV_COV = 1;
   static constexpr Int_t kMaxmuplus_ORIVX_COV = 1;
   static constexpr Int_t kMaxmuminus_OWNPV_COV = 1;
   static constexpr Int_t kMaxmuminus_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        J_psi_1S_BPVLTIME;
   Double_t        J_psi_1S_DOCA;
   Double_t        J_psi_1S_ETA;
   Double_t        J_psi_1S_ITClusters;
   Double_t        J_psi_1S_OTClusters;
   Double_t        J_psi_1S_PERR2;
   Double_t        J_psi_1S_TRCHI2DOF;
   Double_t        J_psi_1S_TTClusters;
   Double_t        J_psi_1S_TightPhi;
   Double_t        J_psi_1S_VCHI2_NDOF;
   Double_t        J_psi_1S_VeloClusters;
   Double_t        J_psi_1S_Y;
   Double_t        J_psi_1S_eta;
   Double_t        J_psi_1S_phi;
   Double_t        J_psi_1S_ENDVERTEX_X;
   Double_t        J_psi_1S_ENDVERTEX_Y;
   Double_t        J_psi_1S_ENDVERTEX_Z;
   Double_t        J_psi_1S_ENDVERTEX_XERR;
   Double_t        J_psi_1S_ENDVERTEX_YERR;
   Double_t        J_psi_1S_ENDVERTEX_ZERR;
   Double_t        J_psi_1S_ENDVERTEX_CHI2;
   Int_t           J_psi_1S_ENDVERTEX_NDOF;
   Float_t         J_psi_1S_ENDVERTEX_COV_[3][3];
   Double_t        J_psi_1S_OWNPV_X;
   Double_t        J_psi_1S_OWNPV_Y;
   Double_t        J_psi_1S_OWNPV_Z;
   Double_t        J_psi_1S_OWNPV_XERR;
   Double_t        J_psi_1S_OWNPV_YERR;
   Double_t        J_psi_1S_OWNPV_ZERR;
   Double_t        J_psi_1S_OWNPV_CHI2;
   Int_t           J_psi_1S_OWNPV_NDOF;
   Float_t         J_psi_1S_OWNPV_COV_[3][3];
   Double_t        J_psi_1S_IP_OWNPV;
   Double_t        J_psi_1S_IPCHI2_OWNPV;
   Double_t        J_psi_1S_FD_OWNPV;
   Double_t        J_psi_1S_FDCHI2_OWNPV;
   Double_t        J_psi_1S_DIRA_OWNPV;
   Double_t        J_psi_1S_P;
   Double_t        J_psi_1S_PT;
   Double_t        J_psi_1S_PE;
   Double_t        J_psi_1S_PX;
   Double_t        J_psi_1S_PY;
   Double_t        J_psi_1S_PZ;
   Double_t        J_psi_1S_MM;
   Double_t        J_psi_1S_MMERR;
   Double_t        J_psi_1S_M;
   Int_t           J_psi_1S_ID;
   Bool_t          J_psi_1S_L0Global_Dec;
   Bool_t          J_psi_1S_L0Global_TIS;
   Bool_t          J_psi_1S_L0Global_TOS;
   Bool_t          J_psi_1S_Hlt1Global_Dec;
   Bool_t          J_psi_1S_Hlt1Global_TIS;
   Bool_t          J_psi_1S_Hlt1Global_TOS;
   Bool_t          J_psi_1S_Hlt1Phys_Dec;
   Bool_t          J_psi_1S_Hlt1Phys_TIS;
   Bool_t          J_psi_1S_Hlt1Phys_TOS;
   Bool_t          J_psi_1S_Hlt2Global_Dec;
   Bool_t          J_psi_1S_Hlt2Global_TIS;
   Bool_t          J_psi_1S_Hlt2Global_TOS;
   Bool_t          J_psi_1S_Hlt2Phys_Dec;
   Bool_t          J_psi_1S_Hlt2Phys_TIS;
   Bool_t          J_psi_1S_Hlt2Phys_TOS;
   Bool_t          J_psi_1S_L0MUONDecision_Dec;
   Bool_t          J_psi_1S_L0MUONDecision_TIS;
   Bool_t          J_psi_1S_L0MUONDecision_TOS;
   Bool_t          J_psi_1S_L0SPDDecision_Dec;
   Bool_t          J_psi_1S_L0SPDDecision_TIS;
   Bool_t          J_psi_1S_L0SPDDecision_TOS;
   Bool_t          J_psi_1S_L0PUDecision_Dec;
   Bool_t          J_psi_1S_L0PUDecision_TIS;
   Bool_t          J_psi_1S_L0PUDecision_TOS;
   Bool_t          J_psi_1S_L0SPDLowMultDecision_Dec;
   Bool_t          J_psi_1S_L0SPDLowMultDecision_TIS;
   Bool_t          J_psi_1S_L0SPDLowMultDecision_TOS;
   Bool_t          J_psi_1S_L0CALODecision_Dec;
   Bool_t          J_psi_1S_L0CALODecision_TIS;
   Bool_t          J_psi_1S_L0CALODecision_TOS;
   Bool_t          J_psi_1S_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          J_psi_1S_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          J_psi_1S_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec;
   Bool_t          J_psi_1S_Hlt1BBMicroBiasVeloDecision_TIS;
   Bool_t          J_psi_1S_Hlt1BBMicroBiasVeloDecision_TOS;
   Bool_t          J_psi_1S_Hlt1LumiLowBeamCrossingDecision_Dec;
   Bool_t          J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TIS;
   Bool_t          J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TOS;
   Bool_t          J_psi_1S_Hlt1VeloClosingMicroBiasDecision_Dec;
   Bool_t          J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TIS;
   Bool_t          J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TOS;
   Bool_t          J_psi_1S_Hlt2BBPassThroughDecision_Dec;
   Bool_t          J_psi_1S_Hlt2BBPassThroughDecision_TIS;
   Bool_t          J_psi_1S_Hlt2BBPassThroughDecision_TOS;
   Double_t        muplus_BPVLTIME;
   Double_t        muplus_DOCA;
   Double_t        muplus_ETA;
   Double_t        muplus_ITClusters;
   Double_t        muplus_OTClusters;
   Double_t        muplus_PERR2;
   Double_t        muplus_TRCHI2DOF;
   Double_t        muplus_TTClusters;
   Double_t        muplus_TightPhi;
   Double_t        muplus_VCHI2_NDOF;
   Double_t        muplus_VeloClusters;
   Double_t        muplus_Y;
   Double_t        muplus_eta;
   Double_t        muplus_phi;
   Double_t        muplus_OWNPV_X;
   Double_t        muplus_OWNPV_Y;
   Double_t        muplus_OWNPV_Z;
   Double_t        muplus_OWNPV_XERR;
   Double_t        muplus_OWNPV_YERR;
   Double_t        muplus_OWNPV_ZERR;
   Double_t        muplus_OWNPV_CHI2;
   Int_t           muplus_OWNPV_NDOF;
   Float_t         muplus_OWNPV_COV_[3][3];
   Double_t        muplus_IP_OWNPV;
   Double_t        muplus_IPCHI2_OWNPV;
   Double_t        muplus_ORIVX_X;
   Double_t        muplus_ORIVX_Y;
   Double_t        muplus_ORIVX_Z;
   Double_t        muplus_ORIVX_XERR;
   Double_t        muplus_ORIVX_YERR;
   Double_t        muplus_ORIVX_ZERR;
   Double_t        muplus_ORIVX_CHI2;
   Int_t           muplus_ORIVX_NDOF;
   Float_t         muplus_ORIVX_COV_[3][3];
   Double_t        muplus_P;
   Double_t        muplus_PT;
   Double_t        muplus_PE;
   Double_t        muplus_PX;
   Double_t        muplus_PY;
   Double_t        muplus_PZ;
   Double_t        muplus_M;
   Int_t           muplus_ID;
   Double_t        muplus_PIDe;
   Double_t        muplus_PIDmu;
   Double_t        muplus_PIDK;
   Double_t        muplus_PIDp;
   Double_t        muplus_ProbNNe;
   Double_t        muplus_ProbNNk;
   Double_t        muplus_ProbNNp;
   Double_t        muplus_ProbNNpi;
   Double_t        muplus_ProbNNmu;
   Double_t        muplus_ProbNNghost;
   Bool_t          muplus_hasMuon;
   Bool_t          muplus_isMuon;
   Bool_t          muplus_hasRich;
   Bool_t          muplus_UsedRichAerogel;
   Bool_t          muplus_UsedRich1Gas;
   Bool_t          muplus_UsedRich2Gas;
   Bool_t          muplus_RichAboveElThres;
   Bool_t          muplus_RichAboveMuThres;
   Bool_t          muplus_RichAbovePiThres;
   Bool_t          muplus_RichAboveKaThres;
   Bool_t          muplus_RichAbovePrThres;
   Bool_t          muplus_hasCalo;
   Bool_t          muplus_L0Global_Dec;
   Bool_t          muplus_L0Global_TIS;
   Bool_t          muplus_L0Global_TOS;
   Bool_t          muplus_Hlt1Global_Dec;
   Bool_t          muplus_Hlt1Global_TIS;
   Bool_t          muplus_Hlt1Global_TOS;
   Bool_t          muplus_Hlt1Phys_Dec;
   Bool_t          muplus_Hlt1Phys_TIS;
   Bool_t          muplus_Hlt1Phys_TOS;
   Bool_t          muplus_Hlt2Global_Dec;
   Bool_t          muplus_Hlt2Global_TIS;
   Bool_t          muplus_Hlt2Global_TOS;
   Bool_t          muplus_Hlt2Phys_Dec;
   Bool_t          muplus_Hlt2Phys_TIS;
   Bool_t          muplus_Hlt2Phys_TOS;
   Bool_t          muplus_L0MUONDecision_Dec;
   Bool_t          muplus_L0MUONDecision_TIS;
   Bool_t          muplus_L0MUONDecision_TOS;
   Bool_t          muplus_L0SPDDecision_Dec;
   Bool_t          muplus_L0SPDDecision_TIS;
   Bool_t          muplus_L0SPDDecision_TOS;
   Bool_t          muplus_L0PUDecision_Dec;
   Bool_t          muplus_L0PUDecision_TIS;
   Bool_t          muplus_L0PUDecision_TOS;
   Bool_t          muplus_L0SPDLowMultDecision_Dec;
   Bool_t          muplus_L0SPDLowMultDecision_TIS;
   Bool_t          muplus_L0SPDLowMultDecision_TOS;
   Bool_t          muplus_L0CALODecision_Dec;
   Bool_t          muplus_L0CALODecision_TIS;
   Bool_t          muplus_L0CALODecision_TOS;
   Bool_t          muplus_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          muplus_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          muplus_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          muplus_Hlt1BBMicroBiasVeloDecision_Dec;
   Bool_t          muplus_Hlt1BBMicroBiasVeloDecision_TIS;
   Bool_t          muplus_Hlt1BBMicroBiasVeloDecision_TOS;
   Bool_t          muplus_Hlt1LumiLowBeamCrossingDecision_Dec;
   Bool_t          muplus_Hlt1LumiLowBeamCrossingDecision_TIS;
   Bool_t          muplus_Hlt1LumiLowBeamCrossingDecision_TOS;
   Bool_t          muplus_Hlt1VeloClosingMicroBiasDecision_Dec;
   Bool_t          muplus_Hlt1VeloClosingMicroBiasDecision_TIS;
   Bool_t          muplus_Hlt1VeloClosingMicroBiasDecision_TOS;
   Bool_t          muplus_Hlt2BBPassThroughDecision_Dec;
   Bool_t          muplus_Hlt2BBPassThroughDecision_TIS;
   Bool_t          muplus_Hlt2BBPassThroughDecision_TOS;
   Int_t           muplus_TRACK_Type;
   Int_t           muplus_TRACK_Key;
   Double_t        muplus_TRACK_CHI2NDOF;
   Double_t        muplus_TRACK_PCHI2;
   Double_t        muplus_TRACK_MatchCHI2;
   Double_t        muplus_TRACK_GhostProb;
   Double_t        muplus_TRACK_CloneDist;
   Double_t        muplus_TRACK_Likelihood;
   Double_t        muminus_BPVLTIME;
   Double_t        muminus_DOCA;
   Double_t        muminus_ETA;
   Double_t        muminus_ITClusters;
   Double_t        muminus_OTClusters;
   Double_t        muminus_PERR2;
   Double_t        muminus_TRCHI2DOF;
   Double_t        muminus_TTClusters;
   Double_t        muminus_TightPhi;
   Double_t        muminus_VCHI2_NDOF;
   Double_t        muminus_VeloClusters;
   Double_t        muminus_Y;
   Double_t        muminus_eta;
   Double_t        muminus_phi;
   Double_t        muminus_OWNPV_X;
   Double_t        muminus_OWNPV_Y;
   Double_t        muminus_OWNPV_Z;
   Double_t        muminus_OWNPV_XERR;
   Double_t        muminus_OWNPV_YERR;
   Double_t        muminus_OWNPV_ZERR;
   Double_t        muminus_OWNPV_CHI2;
   Int_t           muminus_OWNPV_NDOF;
   Float_t         muminus_OWNPV_COV_[3][3];
   Double_t        muminus_IP_OWNPV;
   Double_t        muminus_IPCHI2_OWNPV;
   Double_t        muminus_ORIVX_X;
   Double_t        muminus_ORIVX_Y;
   Double_t        muminus_ORIVX_Z;
   Double_t        muminus_ORIVX_XERR;
   Double_t        muminus_ORIVX_YERR;
   Double_t        muminus_ORIVX_ZERR;
   Double_t        muminus_ORIVX_CHI2;
   Int_t           muminus_ORIVX_NDOF;
   Float_t         muminus_ORIVX_COV_[3][3];
   Double_t        muminus_P;
   Double_t        muminus_PT;
   Double_t        muminus_PE;
   Double_t        muminus_PX;
   Double_t        muminus_PY;
   Double_t        muminus_PZ;
   Double_t        muminus_M;
   Int_t           muminus_ID;
   Double_t        muminus_PIDe;
   Double_t        muminus_PIDmu;
   Double_t        muminus_PIDK;
   Double_t        muminus_PIDp;
   Double_t        muminus_ProbNNe;
   Double_t        muminus_ProbNNk;
   Double_t        muminus_ProbNNp;
   Double_t        muminus_ProbNNpi;
   Double_t        muminus_ProbNNmu;
   Double_t        muminus_ProbNNghost;
   Bool_t          muminus_hasMuon;
   Bool_t          muminus_isMuon;
   Bool_t          muminus_hasRich;
   Bool_t          muminus_UsedRichAerogel;
   Bool_t          muminus_UsedRich1Gas;
   Bool_t          muminus_UsedRich2Gas;
   Bool_t          muminus_RichAboveElThres;
   Bool_t          muminus_RichAboveMuThres;
   Bool_t          muminus_RichAbovePiThres;
   Bool_t          muminus_RichAboveKaThres;
   Bool_t          muminus_RichAbovePrThres;
   Bool_t          muminus_hasCalo;
   Bool_t          muminus_L0Global_Dec;
   Bool_t          muminus_L0Global_TIS;
   Bool_t          muminus_L0Global_TOS;
   Bool_t          muminus_Hlt1Global_Dec;
   Bool_t          muminus_Hlt1Global_TIS;
   Bool_t          muminus_Hlt1Global_TOS;
   Bool_t          muminus_Hlt1Phys_Dec;
   Bool_t          muminus_Hlt1Phys_TIS;
   Bool_t          muminus_Hlt1Phys_TOS;
   Bool_t          muminus_Hlt2Global_Dec;
   Bool_t          muminus_Hlt2Global_TIS;
   Bool_t          muminus_Hlt2Global_TOS;
   Bool_t          muminus_Hlt2Phys_Dec;
   Bool_t          muminus_Hlt2Phys_TIS;
   Bool_t          muminus_Hlt2Phys_TOS;
   Bool_t          muminus_L0MUONDecision_Dec;
   Bool_t          muminus_L0MUONDecision_TIS;
   Bool_t          muminus_L0MUONDecision_TOS;
   Bool_t          muminus_L0SPDDecision_Dec;
   Bool_t          muminus_L0SPDDecision_TIS;
   Bool_t          muminus_L0SPDDecision_TOS;
   Bool_t          muminus_L0PUDecision_Dec;
   Bool_t          muminus_L0PUDecision_TIS;
   Bool_t          muminus_L0PUDecision_TOS;
   Bool_t          muminus_L0SPDLowMultDecision_Dec;
   Bool_t          muminus_L0SPDLowMultDecision_TIS;
   Bool_t          muminus_L0SPDLowMultDecision_TOS;
   Bool_t          muminus_L0CALODecision_Dec;
   Bool_t          muminus_L0CALODecision_TIS;
   Bool_t          muminus_L0CALODecision_TOS;
   Bool_t          muminus_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          muminus_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          muminus_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          muminus_Hlt1BBMicroBiasVeloDecision_Dec;
   Bool_t          muminus_Hlt1BBMicroBiasVeloDecision_TIS;
   Bool_t          muminus_Hlt1BBMicroBiasVeloDecision_TOS;
   Bool_t          muminus_Hlt1LumiLowBeamCrossingDecision_Dec;
   Bool_t          muminus_Hlt1LumiLowBeamCrossingDecision_TIS;
   Bool_t          muminus_Hlt1LumiLowBeamCrossingDecision_TOS;
   Bool_t          muminus_Hlt1VeloClosingMicroBiasDecision_Dec;
   Bool_t          muminus_Hlt1VeloClosingMicroBiasDecision_TIS;
   Bool_t          muminus_Hlt1VeloClosingMicroBiasDecision_TOS;
   Bool_t          muminus_Hlt2BBPassThroughDecision_Dec;
   Bool_t          muminus_Hlt2BBPassThroughDecision_TIS;
   Bool_t          muminus_Hlt2BBPassThroughDecision_TOS;
   Int_t           muminus_TRACK_Type;
   Int_t           muminus_TRACK_Key;
   Double_t        muminus_TRACK_CHI2NDOF;
   Double_t        muminus_TRACK_PCHI2;
   Double_t        muminus_TRACK_MatchCHI2;
   Double_t        muminus_TRACK_GhostProb;
   Double_t        muminus_TRACK_CloneDist;
   Double_t        muminus_TRACK_Likelihood;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           B00;
   Int_t           B01;
   Int_t           B02;
   Int_t           B03;
   Int_t           B10;
   Int_t           B11;
   Int_t           B12;
   Int_t           B13;
   Int_t           B20;
   Int_t           B21;
   Int_t           B22;
   Int_t           B23;
   Int_t           F10;
   Int_t           F11;
   Int_t           F12;
   Int_t           F13;
   Int_t           F20;
   Int_t           F21;
   Int_t           F22;
   Int_t           F23;
   Double_t        log_hrc_fom_v2;
   Double_t        log_hrc_fom_B_v2;
   Double_t        log_hrc_fom_F_v2;
   Int_t           nchB;
   Float_t         adc_B[1000];   //[nchB]
   Int_t           nchF;
   Float_t         adc_F[1000];   //[nchF]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;

   // List of branches
   TBranch        *b_J_psi_1S_BPVLTIME;   //!
   TBranch        *b_J_psi_1S_DOCA;   //!
   TBranch        *b_J_psi_1S_ETA;   //!
   TBranch        *b_J_psi_1S_ITClusters;   //!
   TBranch        *b_J_psi_1S_OTClusters;   //!
   TBranch        *b_J_psi_1S_PERR2;   //!
   TBranch        *b_J_psi_1S_TRCHI2DOF;   //!
   TBranch        *b_J_psi_1S_TTClusters;   //!
   TBranch        *b_J_psi_1S_TightPhi;   //!
   TBranch        *b_J_psi_1S_VCHI2_NDOF;   //!
   TBranch        *b_J_psi_1S_VeloClusters;   //!
   TBranch        *b_J_psi_1S_Y;   //!
   TBranch        *b_J_psi_1S_eta;   //!
   TBranch        *b_J_psi_1S_phi;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_X;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_Y;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_Z;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_XERR;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_YERR;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_ZERR;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_CHI2;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_NDOF;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_COV_;   //!
   TBranch        *b_J_psi_1S_OWNPV_X;   //!
   TBranch        *b_J_psi_1S_OWNPV_Y;   //!
   TBranch        *b_J_psi_1S_OWNPV_Z;   //!
   TBranch        *b_J_psi_1S_OWNPV_XERR;   //!
   TBranch        *b_J_psi_1S_OWNPV_YERR;   //!
   TBranch        *b_J_psi_1S_OWNPV_ZERR;   //!
   TBranch        *b_J_psi_1S_OWNPV_CHI2;   //!
   TBranch        *b_J_psi_1S_OWNPV_NDOF;   //!
   TBranch        *b_J_psi_1S_OWNPV_COV_;   //!
   TBranch        *b_J_psi_1S_IP_OWNPV;   //!
   TBranch        *b_J_psi_1S_IPCHI2_OWNPV;   //!
   TBranch        *b_J_psi_1S_FD_OWNPV;   //!
   TBranch        *b_J_psi_1S_FDCHI2_OWNPV;   //!
   TBranch        *b_J_psi_1S_DIRA_OWNPV;   //!
   TBranch        *b_J_psi_1S_P;   //!
   TBranch        *b_J_psi_1S_PT;   //!
   TBranch        *b_J_psi_1S_PE;   //!
   TBranch        *b_J_psi_1S_PX;   //!
   TBranch        *b_J_psi_1S_PY;   //!
   TBranch        *b_J_psi_1S_PZ;   //!
   TBranch        *b_J_psi_1S_MM;   //!
   TBranch        *b_J_psi_1S_MMERR;   //!
   TBranch        *b_J_psi_1S_M;   //!
   TBranch        *b_J_psi_1S_ID;   //!
   TBranch        *b_J_psi_1S_L0Global_Dec;   //!
   TBranch        *b_J_psi_1S_L0Global_TIS;   //!
   TBranch        *b_J_psi_1S_L0Global_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt1Global_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt1Global_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt1Global_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt1Phys_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt1Phys_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt1Phys_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt2Global_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt2Global_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt2Global_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt2Phys_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt2Phys_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt2Phys_TOS;   //!
   TBranch        *b_J_psi_1S_L0MUONDecision_Dec;   //!
   TBranch        *b_J_psi_1S_L0MUONDecision_TIS;   //!
   TBranch        *b_J_psi_1S_L0MUONDecision_TOS;   //!
   TBranch        *b_J_psi_1S_L0SPDDecision_Dec;   //!
   TBranch        *b_J_psi_1S_L0SPDDecision_TIS;   //!
   TBranch        *b_J_psi_1S_L0SPDDecision_TOS;   //!
   TBranch        *b_J_psi_1S_L0PUDecision_Dec;   //!
   TBranch        *b_J_psi_1S_L0PUDecision_TIS;   //!
   TBranch        *b_J_psi_1S_L0PUDecision_TOS;   //!
   TBranch        *b_J_psi_1S_L0SPDLowMultDecision_Dec;   //!
   TBranch        *b_J_psi_1S_L0SPDLowMultDecision_TIS;   //!
   TBranch        *b_J_psi_1S_L0SPDLowMultDecision_TOS;   //!
   TBranch        *b_J_psi_1S_L0CALODecision_Dec;   //!
   TBranch        *b_J_psi_1S_L0CALODecision_TIS;   //!
   TBranch        *b_J_psi_1S_L0CALODecision_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt1BBMicroBiasVeloDecision_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt1BBMicroBiasVeloDecision_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt1LumiLowBeamCrossingDecision_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt1VeloClosingMicroBiasDecision_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TOS;   //!
   TBranch        *b_J_psi_1S_Hlt2BBPassThroughDecision_Dec;   //!
   TBranch        *b_J_psi_1S_Hlt2BBPassThroughDecision_TIS;   //!
   TBranch        *b_J_psi_1S_Hlt2BBPassThroughDecision_TOS;   //!
   TBranch        *b_muplus_BPVLTIME;   //!
   TBranch        *b_muplus_DOCA;   //!
   TBranch        *b_muplus_ETA;   //!
   TBranch        *b_muplus_ITClusters;   //!
   TBranch        *b_muplus_OTClusters;   //!
   TBranch        *b_muplus_PERR2;   //!
   TBranch        *b_muplus_TRCHI2DOF;   //!
   TBranch        *b_muplus_TTClusters;   //!
   TBranch        *b_muplus_TightPhi;   //!
   TBranch        *b_muplus_VCHI2_NDOF;   //!
   TBranch        *b_muplus_VeloClusters;   //!
   TBranch        *b_muplus_Y;   //!
   TBranch        *b_muplus_eta;   //!
   TBranch        *b_muplus_phi;   //!
   TBranch        *b_muplus_OWNPV_X;   //!
   TBranch        *b_muplus_OWNPV_Y;   //!
   TBranch        *b_muplus_OWNPV_Z;   //!
   TBranch        *b_muplus_OWNPV_XERR;   //!
   TBranch        *b_muplus_OWNPV_YERR;   //!
   TBranch        *b_muplus_OWNPV_ZERR;   //!
   TBranch        *b_muplus_OWNPV_CHI2;   //!
   TBranch        *b_muplus_OWNPV_NDOF;   //!
   TBranch        *b_muplus_OWNPV_COV_;   //!
   TBranch        *b_muplus_IP_OWNPV;   //!
   TBranch        *b_muplus_IPCHI2_OWNPV;   //!
   TBranch        *b_muplus_ORIVX_X;   //!
   TBranch        *b_muplus_ORIVX_Y;   //!
   TBranch        *b_muplus_ORIVX_Z;   //!
   TBranch        *b_muplus_ORIVX_XERR;   //!
   TBranch        *b_muplus_ORIVX_YERR;   //!
   TBranch        *b_muplus_ORIVX_ZERR;   //!
   TBranch        *b_muplus_ORIVX_CHI2;   //!
   TBranch        *b_muplus_ORIVX_NDOF;   //!
   TBranch        *b_muplus_ORIVX_COV_;   //!
   TBranch        *b_muplus_P;   //!
   TBranch        *b_muplus_PT;   //!
   TBranch        *b_muplus_PE;   //!
   TBranch        *b_muplus_PX;   //!
   TBranch        *b_muplus_PY;   //!
   TBranch        *b_muplus_PZ;   //!
   TBranch        *b_muplus_M;   //!
   TBranch        *b_muplus_ID;   //!
   TBranch        *b_muplus_PIDe;   //!
   TBranch        *b_muplus_PIDmu;   //!
   TBranch        *b_muplus_PIDK;   //!
   TBranch        *b_muplus_PIDp;   //!
   TBranch        *b_muplus_ProbNNe;   //!
   TBranch        *b_muplus_ProbNNk;   //!
   TBranch        *b_muplus_ProbNNp;   //!
   TBranch        *b_muplus_ProbNNpi;   //!
   TBranch        *b_muplus_ProbNNmu;   //!
   TBranch        *b_muplus_ProbNNghost;   //!
   TBranch        *b_muplus_hasMuon;   //!
   TBranch        *b_muplus_isMuon;   //!
   TBranch        *b_muplus_hasRich;   //!
   TBranch        *b_muplus_UsedRichAerogel;   //!
   TBranch        *b_muplus_UsedRich1Gas;   //!
   TBranch        *b_muplus_UsedRich2Gas;   //!
   TBranch        *b_muplus_RichAboveElThres;   //!
   TBranch        *b_muplus_RichAboveMuThres;   //!
   TBranch        *b_muplus_RichAbovePiThres;   //!
   TBranch        *b_muplus_RichAboveKaThres;   //!
   TBranch        *b_muplus_RichAbovePrThres;   //!
   TBranch        *b_muplus_hasCalo;   //!
   TBranch        *b_muplus_L0Global_Dec;   //!
   TBranch        *b_muplus_L0Global_TIS;   //!
   TBranch        *b_muplus_L0Global_TOS;   //!
   TBranch        *b_muplus_Hlt1Global_Dec;   //!
   TBranch        *b_muplus_Hlt1Global_TIS;   //!
   TBranch        *b_muplus_Hlt1Global_TOS;   //!
   TBranch        *b_muplus_Hlt1Phys_Dec;   //!
   TBranch        *b_muplus_Hlt1Phys_TIS;   //!
   TBranch        *b_muplus_Hlt1Phys_TOS;   //!
   TBranch        *b_muplus_Hlt2Global_Dec;   //!
   TBranch        *b_muplus_Hlt2Global_TIS;   //!
   TBranch        *b_muplus_Hlt2Global_TOS;   //!
   TBranch        *b_muplus_Hlt2Phys_Dec;   //!
   TBranch        *b_muplus_Hlt2Phys_TIS;   //!
   TBranch        *b_muplus_Hlt2Phys_TOS;   //!
   TBranch        *b_muplus_L0MUONDecision_Dec;   //!
   TBranch        *b_muplus_L0MUONDecision_TIS;   //!
   TBranch        *b_muplus_L0MUONDecision_TOS;   //!
   TBranch        *b_muplus_L0SPDDecision_Dec;   //!
   TBranch        *b_muplus_L0SPDDecision_TIS;   //!
   TBranch        *b_muplus_L0SPDDecision_TOS;   //!
   TBranch        *b_muplus_L0PUDecision_Dec;   //!
   TBranch        *b_muplus_L0PUDecision_TIS;   //!
   TBranch        *b_muplus_L0PUDecision_TOS;   //!
   TBranch        *b_muplus_L0SPDLowMultDecision_Dec;   //!
   TBranch        *b_muplus_L0SPDLowMultDecision_TIS;   //!
   TBranch        *b_muplus_L0SPDLowMultDecision_TOS;   //!
   TBranch        *b_muplus_L0CALODecision_Dec;   //!
   TBranch        *b_muplus_L0CALODecision_TIS;   //!
   TBranch        *b_muplus_L0CALODecision_TOS;   //!
   TBranch        *b_muplus_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_muplus_Hlt1BBMicroBiasVeloDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1BBMicroBiasVeloDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1BBMicroBiasVeloDecision_TOS;   //!
   TBranch        *b_muplus_Hlt1LumiLowBeamCrossingDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1LumiLowBeamCrossingDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1LumiLowBeamCrossingDecision_TOS;   //!
   TBranch        *b_muplus_Hlt1VeloClosingMicroBiasDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1VeloClosingMicroBiasDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1VeloClosingMicroBiasDecision_TOS;   //!
   TBranch        *b_muplus_Hlt2BBPassThroughDecision_Dec;   //!
   TBranch        *b_muplus_Hlt2BBPassThroughDecision_TIS;   //!
   TBranch        *b_muplus_Hlt2BBPassThroughDecision_TOS;   //!
   TBranch        *b_muplus_TRACK_Type;   //!
   TBranch        *b_muplus_TRACK_Key;   //!
   TBranch        *b_muplus_TRACK_CHI2NDOF;   //!
   TBranch        *b_muplus_TRACK_PCHI2;   //!
   TBranch        *b_muplus_TRACK_MatchCHI2;   //!
   TBranch        *b_muplus_TRACK_GhostProb;   //!
   TBranch        *b_muplus_TRACK_CloneDist;   //!
   TBranch        *b_muplus_TRACK_Likelihood;   //!
   TBranch        *b_muminus_BPVLTIME;   //!
   TBranch        *b_muminus_DOCA;   //!
   TBranch        *b_muminus_ETA;   //!
   TBranch        *b_muminus_ITClusters;   //!
   TBranch        *b_muminus_OTClusters;   //!
   TBranch        *b_muminus_PERR2;   //!
   TBranch        *b_muminus_TRCHI2DOF;   //!
   TBranch        *b_muminus_TTClusters;   //!
   TBranch        *b_muminus_TightPhi;   //!
   TBranch        *b_muminus_VCHI2_NDOF;   //!
   TBranch        *b_muminus_VeloClusters;   //!
   TBranch        *b_muminus_Y;   //!
   TBranch        *b_muminus_eta;   //!
   TBranch        *b_muminus_phi;   //!
   TBranch        *b_muminus_OWNPV_X;   //!
   TBranch        *b_muminus_OWNPV_Y;   //!
   TBranch        *b_muminus_OWNPV_Z;   //!
   TBranch        *b_muminus_OWNPV_XERR;   //!
   TBranch        *b_muminus_OWNPV_YERR;   //!
   TBranch        *b_muminus_OWNPV_ZERR;   //!
   TBranch        *b_muminus_OWNPV_CHI2;   //!
   TBranch        *b_muminus_OWNPV_NDOF;   //!
   TBranch        *b_muminus_OWNPV_COV_;   //!
   TBranch        *b_muminus_IP_OWNPV;   //!
   TBranch        *b_muminus_IPCHI2_OWNPV;   //!
   TBranch        *b_muminus_ORIVX_X;   //!
   TBranch        *b_muminus_ORIVX_Y;   //!
   TBranch        *b_muminus_ORIVX_Z;   //!
   TBranch        *b_muminus_ORIVX_XERR;   //!
   TBranch        *b_muminus_ORIVX_YERR;   //!
   TBranch        *b_muminus_ORIVX_ZERR;   //!
   TBranch        *b_muminus_ORIVX_CHI2;   //!
   TBranch        *b_muminus_ORIVX_NDOF;   //!
   TBranch        *b_muminus_ORIVX_COV_;   //!
   TBranch        *b_muminus_P;   //!
   TBranch        *b_muminus_PT;   //!
   TBranch        *b_muminus_PE;   //!
   TBranch        *b_muminus_PX;   //!
   TBranch        *b_muminus_PY;   //!
   TBranch        *b_muminus_PZ;   //!
   TBranch        *b_muminus_M;   //!
   TBranch        *b_muminus_ID;   //!
   TBranch        *b_muminus_PIDe;   //!
   TBranch        *b_muminus_PIDmu;   //!
   TBranch        *b_muminus_PIDK;   //!
   TBranch        *b_muminus_PIDp;   //!
   TBranch        *b_muminus_ProbNNe;   //!
   TBranch        *b_muminus_ProbNNk;   //!
   TBranch        *b_muminus_ProbNNp;   //!
   TBranch        *b_muminus_ProbNNpi;   //!
   TBranch        *b_muminus_ProbNNmu;   //!
   TBranch        *b_muminus_ProbNNghost;   //!
   TBranch        *b_muminus_hasMuon;   //!
   TBranch        *b_muminus_isMuon;   //!
   TBranch        *b_muminus_hasRich;   //!
   TBranch        *b_muminus_UsedRichAerogel;   //!
   TBranch        *b_muminus_UsedRich1Gas;   //!
   TBranch        *b_muminus_UsedRich2Gas;   //!
   TBranch        *b_muminus_RichAboveElThres;   //!
   TBranch        *b_muminus_RichAboveMuThres;   //!
   TBranch        *b_muminus_RichAbovePiThres;   //!
   TBranch        *b_muminus_RichAboveKaThres;   //!
   TBranch        *b_muminus_RichAbovePrThres;   //!
   TBranch        *b_muminus_hasCalo;   //!
   TBranch        *b_muminus_L0Global_Dec;   //!
   TBranch        *b_muminus_L0Global_TIS;   //!
   TBranch        *b_muminus_L0Global_TOS;   //!
   TBranch        *b_muminus_Hlt1Global_Dec;   //!
   TBranch        *b_muminus_Hlt1Global_TIS;   //!
   TBranch        *b_muminus_Hlt1Global_TOS;   //!
   TBranch        *b_muminus_Hlt1Phys_Dec;   //!
   TBranch        *b_muminus_Hlt1Phys_TIS;   //!
   TBranch        *b_muminus_Hlt1Phys_TOS;   //!
   TBranch        *b_muminus_Hlt2Global_Dec;   //!
   TBranch        *b_muminus_Hlt2Global_TIS;   //!
   TBranch        *b_muminus_Hlt2Global_TOS;   //!
   TBranch        *b_muminus_Hlt2Phys_Dec;   //!
   TBranch        *b_muminus_Hlt2Phys_TIS;   //!
   TBranch        *b_muminus_Hlt2Phys_TOS;   //!
   TBranch        *b_muminus_L0MUONDecision_Dec;   //!
   TBranch        *b_muminus_L0MUONDecision_TIS;   //!
   TBranch        *b_muminus_L0MUONDecision_TOS;   //!
   TBranch        *b_muminus_L0SPDDecision_Dec;   //!
   TBranch        *b_muminus_L0SPDDecision_TIS;   //!
   TBranch        *b_muminus_L0SPDDecision_TOS;   //!
   TBranch        *b_muminus_L0PUDecision_Dec;   //!
   TBranch        *b_muminus_L0PUDecision_TIS;   //!
   TBranch        *b_muminus_L0PUDecision_TOS;   //!
   TBranch        *b_muminus_L0SPDLowMultDecision_Dec;   //!
   TBranch        *b_muminus_L0SPDLowMultDecision_TIS;   //!
   TBranch        *b_muminus_L0SPDLowMultDecision_TOS;   //!
   TBranch        *b_muminus_L0CALODecision_Dec;   //!
   TBranch        *b_muminus_L0CALODecision_TIS;   //!
   TBranch        *b_muminus_L0CALODecision_TOS;   //!
   TBranch        *b_muminus_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_muminus_Hlt1BBMicroBiasVeloDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1BBMicroBiasVeloDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1BBMicroBiasVeloDecision_TOS;   //!
   TBranch        *b_muminus_Hlt1LumiLowBeamCrossingDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1LumiLowBeamCrossingDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1LumiLowBeamCrossingDecision_TOS;   //!
   TBranch        *b_muminus_Hlt1VeloClosingMicroBiasDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1VeloClosingMicroBiasDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1VeloClosingMicroBiasDecision_TOS;   //!
   TBranch        *b_muminus_Hlt2BBPassThroughDecision_Dec;   //!
   TBranch        *b_muminus_Hlt2BBPassThroughDecision_TIS;   //!
   TBranch        *b_muminus_Hlt2BBPassThroughDecision_TOS;   //!
   TBranch        *b_muminus_TRACK_Type;   //!
   TBranch        *b_muminus_TRACK_Key;   //!
   TBranch        *b_muminus_TRACK_CHI2NDOF;   //!
   TBranch        *b_muminus_TRACK_PCHI2;   //!
   TBranch        *b_muminus_TRACK_MatchCHI2;   //!
   TBranch        *b_muminus_TRACK_GhostProb;   //!
   TBranch        *b_muminus_TRACK_CloneDist;   //!
   TBranch        *b_muminus_TRACK_Likelihood;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_B00;   //!
   TBranch        *b_B01;   //!
   TBranch        *b_B02;   //!
   TBranch        *b_B03;   //!
   TBranch        *b_B10;   //!
   TBranch        *b_B11;   //!
   TBranch        *b_B12;   //!
   TBranch        *b_B13;   //!
   TBranch        *b_B20;   //!
   TBranch        *b_B21;   //!
   TBranch        *b_B22;   //!
   TBranch        *b_B23;   //!
   TBranch        *b_F10;   //!
   TBranch        *b_F11;   //!
   TBranch        *b_F12;   //!
   TBranch        *b_F13;   //!
   TBranch        *b_F20;   //!
   TBranch        *b_F21;   //!
   TBranch        *b_F22;   //!
   TBranch        *b_F23;   //!
   TBranch        *b_log_hrc_fom_v2;   //!
   TBranch        *b_log_hrc_fom_B_v2;   //!
   TBranch        *b_log_hrc_fom_F_v2;   //!
   TBranch        *b_nchB;   //!
   TBranch        *b_adc_B;   //!
   TBranch        *b_nchF;   //!
   TBranch        *b_adc_F;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!

   makeNewTuple(TTree *tree=0);
   virtual ~makeNewTuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef makeNewTuple_cxx
makeNewTuple::makeNewTuple(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("DecayTree",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("DecayTree","");
      chain->Add("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/0_OriginalSamples/LHCb_diMu_PbPb_2015.root/DecayTree");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

makeNewTuple::~makeNewTuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t makeNewTuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t makeNewTuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void makeNewTuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("J_psi_1S_BPVLTIME", &J_psi_1S_BPVLTIME, &b_J_psi_1S_BPVLTIME);
   fChain->SetBranchAddress("J_psi_1S_DOCA", &J_psi_1S_DOCA, &b_J_psi_1S_DOCA);
   fChain->SetBranchAddress("J_psi_1S_ETA", &J_psi_1S_ETA, &b_J_psi_1S_ETA);
   fChain->SetBranchAddress("J_psi_1S_ITClusters", &J_psi_1S_ITClusters, &b_J_psi_1S_ITClusters);
   fChain->SetBranchAddress("J_psi_1S_OTClusters", &J_psi_1S_OTClusters, &b_J_psi_1S_OTClusters);
   fChain->SetBranchAddress("J_psi_1S_PERR2", &J_psi_1S_PERR2, &b_J_psi_1S_PERR2);
   fChain->SetBranchAddress("J_psi_1S_TRCHI2DOF", &J_psi_1S_TRCHI2DOF, &b_J_psi_1S_TRCHI2DOF);
   fChain->SetBranchAddress("J_psi_1S_TTClusters", &J_psi_1S_TTClusters, &b_J_psi_1S_TTClusters);
   fChain->SetBranchAddress("J_psi_1S_TightPhi", &J_psi_1S_TightPhi, &b_J_psi_1S_TightPhi);
   fChain->SetBranchAddress("J_psi_1S_VCHI2_NDOF", &J_psi_1S_VCHI2_NDOF, &b_J_psi_1S_VCHI2_NDOF);
   fChain->SetBranchAddress("J_psi_1S_VeloClusters", &J_psi_1S_VeloClusters, &b_J_psi_1S_VeloClusters);
   fChain->SetBranchAddress("J_psi_1S_Y", &J_psi_1S_Y, &b_J_psi_1S_Y);
   fChain->SetBranchAddress("J_psi_1S_eta", &J_psi_1S_eta, &b_J_psi_1S_eta);
   fChain->SetBranchAddress("J_psi_1S_phi", &J_psi_1S_phi, &b_J_psi_1S_phi);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_X", &J_psi_1S_ENDVERTEX_X, &b_J_psi_1S_ENDVERTEX_X);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_Y", &J_psi_1S_ENDVERTEX_Y, &b_J_psi_1S_ENDVERTEX_Y);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_Z", &J_psi_1S_ENDVERTEX_Z, &b_J_psi_1S_ENDVERTEX_Z);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_XERR", &J_psi_1S_ENDVERTEX_XERR, &b_J_psi_1S_ENDVERTEX_XERR);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_YERR", &J_psi_1S_ENDVERTEX_YERR, &b_J_psi_1S_ENDVERTEX_YERR);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_ZERR", &J_psi_1S_ENDVERTEX_ZERR, &b_J_psi_1S_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_CHI2", &J_psi_1S_ENDVERTEX_CHI2, &b_J_psi_1S_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_NDOF", &J_psi_1S_ENDVERTEX_NDOF, &b_J_psi_1S_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_COV_", J_psi_1S_ENDVERTEX_COV_, &b_J_psi_1S_ENDVERTEX_COV_);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_X", &J_psi_1S_OWNPV_X, &b_J_psi_1S_OWNPV_X);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_Y", &J_psi_1S_OWNPV_Y, &b_J_psi_1S_OWNPV_Y);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_Z", &J_psi_1S_OWNPV_Z, &b_J_psi_1S_OWNPV_Z);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_XERR", &J_psi_1S_OWNPV_XERR, &b_J_psi_1S_OWNPV_XERR);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_YERR", &J_psi_1S_OWNPV_YERR, &b_J_psi_1S_OWNPV_YERR);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_ZERR", &J_psi_1S_OWNPV_ZERR, &b_J_psi_1S_OWNPV_ZERR);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_CHI2", &J_psi_1S_OWNPV_CHI2, &b_J_psi_1S_OWNPV_CHI2);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_NDOF", &J_psi_1S_OWNPV_NDOF, &b_J_psi_1S_OWNPV_NDOF);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_COV_", J_psi_1S_OWNPV_COV_, &b_J_psi_1S_OWNPV_COV_);
   fChain->SetBranchAddress("J_psi_1S_IP_OWNPV", &J_psi_1S_IP_OWNPV, &b_J_psi_1S_IP_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_IPCHI2_OWNPV", &J_psi_1S_IPCHI2_OWNPV, &b_J_psi_1S_IPCHI2_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_FD_OWNPV", &J_psi_1S_FD_OWNPV, &b_J_psi_1S_FD_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_FDCHI2_OWNPV", &J_psi_1S_FDCHI2_OWNPV, &b_J_psi_1S_FDCHI2_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_DIRA_OWNPV", &J_psi_1S_DIRA_OWNPV, &b_J_psi_1S_DIRA_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_P", &J_psi_1S_P, &b_J_psi_1S_P);
   fChain->SetBranchAddress("J_psi_1S_PT", &J_psi_1S_PT, &b_J_psi_1S_PT);
   fChain->SetBranchAddress("J_psi_1S_PE", &J_psi_1S_PE, &b_J_psi_1S_PE);
   fChain->SetBranchAddress("J_psi_1S_PX", &J_psi_1S_PX, &b_J_psi_1S_PX);
   fChain->SetBranchAddress("J_psi_1S_PY", &J_psi_1S_PY, &b_J_psi_1S_PY);
   fChain->SetBranchAddress("J_psi_1S_PZ", &J_psi_1S_PZ, &b_J_psi_1S_PZ);
   fChain->SetBranchAddress("J_psi_1S_MM", &J_psi_1S_MM, &b_J_psi_1S_MM);
   fChain->SetBranchAddress("J_psi_1S_MMERR", &J_psi_1S_MMERR, &b_J_psi_1S_MMERR);
   fChain->SetBranchAddress("J_psi_1S_M", &J_psi_1S_M, &b_J_psi_1S_M);
   fChain->SetBranchAddress("J_psi_1S_ID", &J_psi_1S_ID, &b_J_psi_1S_ID);
   fChain->SetBranchAddress("J_psi_1S_L0Global_Dec", &J_psi_1S_L0Global_Dec, &b_J_psi_1S_L0Global_Dec);
   fChain->SetBranchAddress("J_psi_1S_L0Global_TIS", &J_psi_1S_L0Global_TIS, &b_J_psi_1S_L0Global_TIS);
   fChain->SetBranchAddress("J_psi_1S_L0Global_TOS", &J_psi_1S_L0Global_TOS, &b_J_psi_1S_L0Global_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1Global_Dec", &J_psi_1S_Hlt1Global_Dec, &b_J_psi_1S_Hlt1Global_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt1Global_TIS", &J_psi_1S_Hlt1Global_TIS, &b_J_psi_1S_Hlt1Global_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1Global_TOS", &J_psi_1S_Hlt1Global_TOS, &b_J_psi_1S_Hlt1Global_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1Phys_Dec", &J_psi_1S_Hlt1Phys_Dec, &b_J_psi_1S_Hlt1Phys_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt1Phys_TIS", &J_psi_1S_Hlt1Phys_TIS, &b_J_psi_1S_Hlt1Phys_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1Phys_TOS", &J_psi_1S_Hlt1Phys_TOS, &b_J_psi_1S_Hlt1Phys_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt2Global_Dec", &J_psi_1S_Hlt2Global_Dec, &b_J_psi_1S_Hlt2Global_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt2Global_TIS", &J_psi_1S_Hlt2Global_TIS, &b_J_psi_1S_Hlt2Global_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt2Global_TOS", &J_psi_1S_Hlt2Global_TOS, &b_J_psi_1S_Hlt2Global_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt2Phys_Dec", &J_psi_1S_Hlt2Phys_Dec, &b_J_psi_1S_Hlt2Phys_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt2Phys_TIS", &J_psi_1S_Hlt2Phys_TIS, &b_J_psi_1S_Hlt2Phys_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt2Phys_TOS", &J_psi_1S_Hlt2Phys_TOS, &b_J_psi_1S_Hlt2Phys_TOS);
   fChain->SetBranchAddress("J_psi_1S_L0MUONDecision_Dec", &J_psi_1S_L0MUONDecision_Dec, &b_J_psi_1S_L0MUONDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_L0MUONDecision_TIS", &J_psi_1S_L0MUONDecision_TIS, &b_J_psi_1S_L0MUONDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_L0MUONDecision_TOS", &J_psi_1S_L0MUONDecision_TOS, &b_J_psi_1S_L0MUONDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_L0SPDDecision_Dec", &J_psi_1S_L0SPDDecision_Dec, &b_J_psi_1S_L0SPDDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_L0SPDDecision_TIS", &J_psi_1S_L0SPDDecision_TIS, &b_J_psi_1S_L0SPDDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_L0SPDDecision_TOS", &J_psi_1S_L0SPDDecision_TOS, &b_J_psi_1S_L0SPDDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_L0PUDecision_Dec", &J_psi_1S_L0PUDecision_Dec, &b_J_psi_1S_L0PUDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_L0PUDecision_TIS", &J_psi_1S_L0PUDecision_TIS, &b_J_psi_1S_L0PUDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_L0PUDecision_TOS", &J_psi_1S_L0PUDecision_TOS, &b_J_psi_1S_L0PUDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_L0SPDLowMultDecision_Dec", &J_psi_1S_L0SPDLowMultDecision_Dec, &b_J_psi_1S_L0SPDLowMultDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_L0SPDLowMultDecision_TIS", &J_psi_1S_L0SPDLowMultDecision_TIS, &b_J_psi_1S_L0SPDLowMultDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_L0SPDLowMultDecision_TOS", &J_psi_1S_L0SPDLowMultDecision_TOS, &b_J_psi_1S_L0SPDLowMultDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_L0CALODecision_Dec", &J_psi_1S_L0CALODecision_Dec, &b_J_psi_1S_L0CALODecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_L0CALODecision_TIS", &J_psi_1S_L0CALODecision_TIS, &b_J_psi_1S_L0CALODecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_L0CALODecision_TOS", &J_psi_1S_L0CALODecision_TOS, &b_J_psi_1S_L0CALODecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1DiMuonHighMassDecision_Dec", &J_psi_1S_Hlt1DiMuonHighMassDecision_Dec, &b_J_psi_1S_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt1DiMuonHighMassDecision_TIS", &J_psi_1S_Hlt1DiMuonHighMassDecision_TIS, &b_J_psi_1S_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1DiMuonHighMassDecision_TOS", &J_psi_1S_Hlt1DiMuonHighMassDecision_TOS, &b_J_psi_1S_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec", &J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec, &b_J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt1BBMicroBiasVeloDecision_TIS", &J_psi_1S_Hlt1BBMicroBiasVeloDecision_TIS, &b_J_psi_1S_Hlt1BBMicroBiasVeloDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1BBMicroBiasVeloDecision_TOS", &J_psi_1S_Hlt1BBMicroBiasVeloDecision_TOS, &b_J_psi_1S_Hlt1BBMicroBiasVeloDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1LumiLowBeamCrossingDecision_Dec", &J_psi_1S_Hlt1LumiLowBeamCrossingDecision_Dec, &b_J_psi_1S_Hlt1LumiLowBeamCrossingDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TIS", &J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TIS, &b_J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TOS", &J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TOS, &b_J_psi_1S_Hlt1LumiLowBeamCrossingDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1VeloClosingMicroBiasDecision_Dec", &J_psi_1S_Hlt1VeloClosingMicroBiasDecision_Dec, &b_J_psi_1S_Hlt1VeloClosingMicroBiasDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TIS", &J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TIS, &b_J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TOS", &J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TOS, &b_J_psi_1S_Hlt1VeloClosingMicroBiasDecision_TOS);
   fChain->SetBranchAddress("J_psi_1S_Hlt2BBPassThroughDecision_Dec", &J_psi_1S_Hlt2BBPassThroughDecision_Dec, &b_J_psi_1S_Hlt2BBPassThroughDecision_Dec);
   fChain->SetBranchAddress("J_psi_1S_Hlt2BBPassThroughDecision_TIS", &J_psi_1S_Hlt2BBPassThroughDecision_TIS, &b_J_psi_1S_Hlt2BBPassThroughDecision_TIS);
   fChain->SetBranchAddress("J_psi_1S_Hlt2BBPassThroughDecision_TOS", &J_psi_1S_Hlt2BBPassThroughDecision_TOS, &b_J_psi_1S_Hlt2BBPassThroughDecision_TOS);
   fChain->SetBranchAddress("muplus_BPVLTIME", &muplus_BPVLTIME, &b_muplus_BPVLTIME);
   fChain->SetBranchAddress("muplus_DOCA", &muplus_DOCA, &b_muplus_DOCA);
   fChain->SetBranchAddress("muplus_ETA", &muplus_ETA, &b_muplus_ETA);
   fChain->SetBranchAddress("muplus_ITClusters", &muplus_ITClusters, &b_muplus_ITClusters);
   fChain->SetBranchAddress("muplus_OTClusters", &muplus_OTClusters, &b_muplus_OTClusters);
   fChain->SetBranchAddress("muplus_PERR2", &muplus_PERR2, &b_muplus_PERR2);
   fChain->SetBranchAddress("muplus_TRCHI2DOF", &muplus_TRCHI2DOF, &b_muplus_TRCHI2DOF);
   fChain->SetBranchAddress("muplus_TTClusters", &muplus_TTClusters, &b_muplus_TTClusters);
   fChain->SetBranchAddress("muplus_TightPhi", &muplus_TightPhi, &b_muplus_TightPhi);
   fChain->SetBranchAddress("muplus_VCHI2_NDOF", &muplus_VCHI2_NDOF, &b_muplus_VCHI2_NDOF);
   fChain->SetBranchAddress("muplus_VeloClusters", &muplus_VeloClusters, &b_muplus_VeloClusters);
   fChain->SetBranchAddress("muplus_Y", &muplus_Y, &b_muplus_Y);
   fChain->SetBranchAddress("muplus_eta", &muplus_eta, &b_muplus_eta);
   fChain->SetBranchAddress("muplus_phi", &muplus_phi, &b_muplus_phi);
   fChain->SetBranchAddress("muplus_OWNPV_X", &muplus_OWNPV_X, &b_muplus_OWNPV_X);
   fChain->SetBranchAddress("muplus_OWNPV_Y", &muplus_OWNPV_Y, &b_muplus_OWNPV_Y);
   fChain->SetBranchAddress("muplus_OWNPV_Z", &muplus_OWNPV_Z, &b_muplus_OWNPV_Z);
   fChain->SetBranchAddress("muplus_OWNPV_XERR", &muplus_OWNPV_XERR, &b_muplus_OWNPV_XERR);
   fChain->SetBranchAddress("muplus_OWNPV_YERR", &muplus_OWNPV_YERR, &b_muplus_OWNPV_YERR);
   fChain->SetBranchAddress("muplus_OWNPV_ZERR", &muplus_OWNPV_ZERR, &b_muplus_OWNPV_ZERR);
   fChain->SetBranchAddress("muplus_OWNPV_CHI2", &muplus_OWNPV_CHI2, &b_muplus_OWNPV_CHI2);
   fChain->SetBranchAddress("muplus_OWNPV_NDOF", &muplus_OWNPV_NDOF, &b_muplus_OWNPV_NDOF);
   fChain->SetBranchAddress("muplus_OWNPV_COV_", muplus_OWNPV_COV_, &b_muplus_OWNPV_COV_);
   fChain->SetBranchAddress("muplus_IP_OWNPV", &muplus_IP_OWNPV, &b_muplus_IP_OWNPV);
   fChain->SetBranchAddress("muplus_IPCHI2_OWNPV", &muplus_IPCHI2_OWNPV, &b_muplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("muplus_ORIVX_X", &muplus_ORIVX_X, &b_muplus_ORIVX_X);
   fChain->SetBranchAddress("muplus_ORIVX_Y", &muplus_ORIVX_Y, &b_muplus_ORIVX_Y);
   fChain->SetBranchAddress("muplus_ORIVX_Z", &muplus_ORIVX_Z, &b_muplus_ORIVX_Z);
   fChain->SetBranchAddress("muplus_ORIVX_XERR", &muplus_ORIVX_XERR, &b_muplus_ORIVX_XERR);
   fChain->SetBranchAddress("muplus_ORIVX_YERR", &muplus_ORIVX_YERR, &b_muplus_ORIVX_YERR);
   fChain->SetBranchAddress("muplus_ORIVX_ZERR", &muplus_ORIVX_ZERR, &b_muplus_ORIVX_ZERR);
   fChain->SetBranchAddress("muplus_ORIVX_CHI2", &muplus_ORIVX_CHI2, &b_muplus_ORIVX_CHI2);
   fChain->SetBranchAddress("muplus_ORIVX_NDOF", &muplus_ORIVX_NDOF, &b_muplus_ORIVX_NDOF);
   fChain->SetBranchAddress("muplus_ORIVX_COV_", muplus_ORIVX_COV_, &b_muplus_ORIVX_COV_);
   fChain->SetBranchAddress("muplus_P", &muplus_P, &b_muplus_P);
   fChain->SetBranchAddress("muplus_PT", &muplus_PT, &b_muplus_PT);
   fChain->SetBranchAddress("muplus_PE", &muplus_PE, &b_muplus_PE);
   fChain->SetBranchAddress("muplus_PX", &muplus_PX, &b_muplus_PX);
   fChain->SetBranchAddress("muplus_PY", &muplus_PY, &b_muplus_PY);
   fChain->SetBranchAddress("muplus_PZ", &muplus_PZ, &b_muplus_PZ);
   fChain->SetBranchAddress("muplus_M", &muplus_M, &b_muplus_M);
   fChain->SetBranchAddress("muplus_ID", &muplus_ID, &b_muplus_ID);
   fChain->SetBranchAddress("muplus_PIDe", &muplus_PIDe, &b_muplus_PIDe);
   fChain->SetBranchAddress("muplus_PIDmu", &muplus_PIDmu, &b_muplus_PIDmu);
   fChain->SetBranchAddress("muplus_PIDK", &muplus_PIDK, &b_muplus_PIDK);
   fChain->SetBranchAddress("muplus_PIDp", &muplus_PIDp, &b_muplus_PIDp);
   fChain->SetBranchAddress("muplus_ProbNNe", &muplus_ProbNNe, &b_muplus_ProbNNe);
   fChain->SetBranchAddress("muplus_ProbNNk", &muplus_ProbNNk, &b_muplus_ProbNNk);
   fChain->SetBranchAddress("muplus_ProbNNp", &muplus_ProbNNp, &b_muplus_ProbNNp);
   fChain->SetBranchAddress("muplus_ProbNNpi", &muplus_ProbNNpi, &b_muplus_ProbNNpi);
   fChain->SetBranchAddress("muplus_ProbNNmu", &muplus_ProbNNmu, &b_muplus_ProbNNmu);
   fChain->SetBranchAddress("muplus_ProbNNghost", &muplus_ProbNNghost, &b_muplus_ProbNNghost);
   fChain->SetBranchAddress("muplus_hasMuon", &muplus_hasMuon, &b_muplus_hasMuon);
   fChain->SetBranchAddress("muplus_isMuon", &muplus_isMuon, &b_muplus_isMuon);
   fChain->SetBranchAddress("muplus_hasRich", &muplus_hasRich, &b_muplus_hasRich);
   fChain->SetBranchAddress("muplus_UsedRichAerogel", &muplus_UsedRichAerogel, &b_muplus_UsedRichAerogel);
   fChain->SetBranchAddress("muplus_UsedRich1Gas", &muplus_UsedRich1Gas, &b_muplus_UsedRich1Gas);
   fChain->SetBranchAddress("muplus_UsedRich2Gas", &muplus_UsedRich2Gas, &b_muplus_UsedRich2Gas);
   fChain->SetBranchAddress("muplus_RichAboveElThres", &muplus_RichAboveElThres, &b_muplus_RichAboveElThres);
   fChain->SetBranchAddress("muplus_RichAboveMuThres", &muplus_RichAboveMuThres, &b_muplus_RichAboveMuThres);
   fChain->SetBranchAddress("muplus_RichAbovePiThres", &muplus_RichAbovePiThres, &b_muplus_RichAbovePiThres);
   fChain->SetBranchAddress("muplus_RichAboveKaThres", &muplus_RichAboveKaThres, &b_muplus_RichAboveKaThres);
   fChain->SetBranchAddress("muplus_RichAbovePrThres", &muplus_RichAbovePrThres, &b_muplus_RichAbovePrThres);
   fChain->SetBranchAddress("muplus_hasCalo", &muplus_hasCalo, &b_muplus_hasCalo);
   fChain->SetBranchAddress("muplus_L0Global_Dec", &muplus_L0Global_Dec, &b_muplus_L0Global_Dec);
   fChain->SetBranchAddress("muplus_L0Global_TIS", &muplus_L0Global_TIS, &b_muplus_L0Global_TIS);
   fChain->SetBranchAddress("muplus_L0Global_TOS", &muplus_L0Global_TOS, &b_muplus_L0Global_TOS);
   fChain->SetBranchAddress("muplus_Hlt1Global_Dec", &muplus_Hlt1Global_Dec, &b_muplus_Hlt1Global_Dec);
   fChain->SetBranchAddress("muplus_Hlt1Global_TIS", &muplus_Hlt1Global_TIS, &b_muplus_Hlt1Global_TIS);
   fChain->SetBranchAddress("muplus_Hlt1Global_TOS", &muplus_Hlt1Global_TOS, &b_muplus_Hlt1Global_TOS);
   fChain->SetBranchAddress("muplus_Hlt1Phys_Dec", &muplus_Hlt1Phys_Dec, &b_muplus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("muplus_Hlt1Phys_TIS", &muplus_Hlt1Phys_TIS, &b_muplus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("muplus_Hlt1Phys_TOS", &muplus_Hlt1Phys_TOS, &b_muplus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("muplus_Hlt2Global_Dec", &muplus_Hlt2Global_Dec, &b_muplus_Hlt2Global_Dec);
   fChain->SetBranchAddress("muplus_Hlt2Global_TIS", &muplus_Hlt2Global_TIS, &b_muplus_Hlt2Global_TIS);
   fChain->SetBranchAddress("muplus_Hlt2Global_TOS", &muplus_Hlt2Global_TOS, &b_muplus_Hlt2Global_TOS);
   fChain->SetBranchAddress("muplus_Hlt2Phys_Dec", &muplus_Hlt2Phys_Dec, &b_muplus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("muplus_Hlt2Phys_TIS", &muplus_Hlt2Phys_TIS, &b_muplus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("muplus_Hlt2Phys_TOS", &muplus_Hlt2Phys_TOS, &b_muplus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("muplus_L0MUONDecision_Dec", &muplus_L0MUONDecision_Dec, &b_muplus_L0MUONDecision_Dec);
   fChain->SetBranchAddress("muplus_L0MUONDecision_TIS", &muplus_L0MUONDecision_TIS, &b_muplus_L0MUONDecision_TIS);
   fChain->SetBranchAddress("muplus_L0MUONDecision_TOS", &muplus_L0MUONDecision_TOS, &b_muplus_L0MUONDecision_TOS);
   fChain->SetBranchAddress("muplus_L0SPDDecision_Dec", &muplus_L0SPDDecision_Dec, &b_muplus_L0SPDDecision_Dec);
   fChain->SetBranchAddress("muplus_L0SPDDecision_TIS", &muplus_L0SPDDecision_TIS, &b_muplus_L0SPDDecision_TIS);
   fChain->SetBranchAddress("muplus_L0SPDDecision_TOS", &muplus_L0SPDDecision_TOS, &b_muplus_L0SPDDecision_TOS);
   fChain->SetBranchAddress("muplus_L0PUDecision_Dec", &muplus_L0PUDecision_Dec, &b_muplus_L0PUDecision_Dec);
   fChain->SetBranchAddress("muplus_L0PUDecision_TIS", &muplus_L0PUDecision_TIS, &b_muplus_L0PUDecision_TIS);
   fChain->SetBranchAddress("muplus_L0PUDecision_TOS", &muplus_L0PUDecision_TOS, &b_muplus_L0PUDecision_TOS);
   fChain->SetBranchAddress("muplus_L0SPDLowMultDecision_Dec", &muplus_L0SPDLowMultDecision_Dec, &b_muplus_L0SPDLowMultDecision_Dec);
   fChain->SetBranchAddress("muplus_L0SPDLowMultDecision_TIS", &muplus_L0SPDLowMultDecision_TIS, &b_muplus_L0SPDLowMultDecision_TIS);
   fChain->SetBranchAddress("muplus_L0SPDLowMultDecision_TOS", &muplus_L0SPDLowMultDecision_TOS, &b_muplus_L0SPDLowMultDecision_TOS);
   fChain->SetBranchAddress("muplus_L0CALODecision_Dec", &muplus_L0CALODecision_Dec, &b_muplus_L0CALODecision_Dec);
   fChain->SetBranchAddress("muplus_L0CALODecision_TIS", &muplus_L0CALODecision_TIS, &b_muplus_L0CALODecision_TIS);
   fChain->SetBranchAddress("muplus_L0CALODecision_TOS", &muplus_L0CALODecision_TOS, &b_muplus_L0CALODecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1DiMuonHighMassDecision_Dec", &muplus_Hlt1DiMuonHighMassDecision_Dec, &b_muplus_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1DiMuonHighMassDecision_TIS", &muplus_Hlt1DiMuonHighMassDecision_TIS, &b_muplus_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1DiMuonHighMassDecision_TOS", &muplus_Hlt1DiMuonHighMassDecision_TOS, &b_muplus_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1BBMicroBiasVeloDecision_Dec", &muplus_Hlt1BBMicroBiasVeloDecision_Dec, &b_muplus_Hlt1BBMicroBiasVeloDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1BBMicroBiasVeloDecision_TIS", &muplus_Hlt1BBMicroBiasVeloDecision_TIS, &b_muplus_Hlt1BBMicroBiasVeloDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1BBMicroBiasVeloDecision_TOS", &muplus_Hlt1BBMicroBiasVeloDecision_TOS, &b_muplus_Hlt1BBMicroBiasVeloDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1LumiLowBeamCrossingDecision_Dec", &muplus_Hlt1LumiLowBeamCrossingDecision_Dec, &b_muplus_Hlt1LumiLowBeamCrossingDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1LumiLowBeamCrossingDecision_TIS", &muplus_Hlt1LumiLowBeamCrossingDecision_TIS, &b_muplus_Hlt1LumiLowBeamCrossingDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1LumiLowBeamCrossingDecision_TOS", &muplus_Hlt1LumiLowBeamCrossingDecision_TOS, &b_muplus_Hlt1LumiLowBeamCrossingDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1VeloClosingMicroBiasDecision_Dec", &muplus_Hlt1VeloClosingMicroBiasDecision_Dec, &b_muplus_Hlt1VeloClosingMicroBiasDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1VeloClosingMicroBiasDecision_TIS", &muplus_Hlt1VeloClosingMicroBiasDecision_TIS, &b_muplus_Hlt1VeloClosingMicroBiasDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1VeloClosingMicroBiasDecision_TOS", &muplus_Hlt1VeloClosingMicroBiasDecision_TOS, &b_muplus_Hlt1VeloClosingMicroBiasDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt2BBPassThroughDecision_Dec", &muplus_Hlt2BBPassThroughDecision_Dec, &b_muplus_Hlt2BBPassThroughDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt2BBPassThroughDecision_TIS", &muplus_Hlt2BBPassThroughDecision_TIS, &b_muplus_Hlt2BBPassThroughDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt2BBPassThroughDecision_TOS", &muplus_Hlt2BBPassThroughDecision_TOS, &b_muplus_Hlt2BBPassThroughDecision_TOS);
   fChain->SetBranchAddress("muplus_TRACK_Type", &muplus_TRACK_Type, &b_muplus_TRACK_Type);
   fChain->SetBranchAddress("muplus_TRACK_Key", &muplus_TRACK_Key, &b_muplus_TRACK_Key);
   fChain->SetBranchAddress("muplus_TRACK_CHI2NDOF", &muplus_TRACK_CHI2NDOF, &b_muplus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("muplus_TRACK_PCHI2", &muplus_TRACK_PCHI2, &b_muplus_TRACK_PCHI2);
   fChain->SetBranchAddress("muplus_TRACK_MatchCHI2", &muplus_TRACK_MatchCHI2, &b_muplus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("muplus_TRACK_GhostProb", &muplus_TRACK_GhostProb, &b_muplus_TRACK_GhostProb);
   fChain->SetBranchAddress("muplus_TRACK_CloneDist", &muplus_TRACK_CloneDist, &b_muplus_TRACK_CloneDist);
   fChain->SetBranchAddress("muplus_TRACK_Likelihood", &muplus_TRACK_Likelihood, &b_muplus_TRACK_Likelihood);
   fChain->SetBranchAddress("muminus_BPVLTIME", &muminus_BPVLTIME, &b_muminus_BPVLTIME);
   fChain->SetBranchAddress("muminus_DOCA", &muminus_DOCA, &b_muminus_DOCA);
   fChain->SetBranchAddress("muminus_ETA", &muminus_ETA, &b_muminus_ETA);
   fChain->SetBranchAddress("muminus_ITClusters", &muminus_ITClusters, &b_muminus_ITClusters);
   fChain->SetBranchAddress("muminus_OTClusters", &muminus_OTClusters, &b_muminus_OTClusters);
   fChain->SetBranchAddress("muminus_PERR2", &muminus_PERR2, &b_muminus_PERR2);
   fChain->SetBranchAddress("muminus_TRCHI2DOF", &muminus_TRCHI2DOF, &b_muminus_TRCHI2DOF);
   fChain->SetBranchAddress("muminus_TTClusters", &muminus_TTClusters, &b_muminus_TTClusters);
   fChain->SetBranchAddress("muminus_TightPhi", &muminus_TightPhi, &b_muminus_TightPhi);
   fChain->SetBranchAddress("muminus_VCHI2_NDOF", &muminus_VCHI2_NDOF, &b_muminus_VCHI2_NDOF);
   fChain->SetBranchAddress("muminus_VeloClusters", &muminus_VeloClusters, &b_muminus_VeloClusters);
   fChain->SetBranchAddress("muminus_Y", &muminus_Y, &b_muminus_Y);
   fChain->SetBranchAddress("muminus_eta", &muminus_eta, &b_muminus_eta);
   fChain->SetBranchAddress("muminus_phi", &muminus_phi, &b_muminus_phi);
   fChain->SetBranchAddress("muminus_OWNPV_X", &muminus_OWNPV_X, &b_muminus_OWNPV_X);
   fChain->SetBranchAddress("muminus_OWNPV_Y", &muminus_OWNPV_Y, &b_muminus_OWNPV_Y);
   fChain->SetBranchAddress("muminus_OWNPV_Z", &muminus_OWNPV_Z, &b_muminus_OWNPV_Z);
   fChain->SetBranchAddress("muminus_OWNPV_XERR", &muminus_OWNPV_XERR, &b_muminus_OWNPV_XERR);
   fChain->SetBranchAddress("muminus_OWNPV_YERR", &muminus_OWNPV_YERR, &b_muminus_OWNPV_YERR);
   fChain->SetBranchAddress("muminus_OWNPV_ZERR", &muminus_OWNPV_ZERR, &b_muminus_OWNPV_ZERR);
   fChain->SetBranchAddress("muminus_OWNPV_CHI2", &muminus_OWNPV_CHI2, &b_muminus_OWNPV_CHI2);
   fChain->SetBranchAddress("muminus_OWNPV_NDOF", &muminus_OWNPV_NDOF, &b_muminus_OWNPV_NDOF);
   fChain->SetBranchAddress("muminus_OWNPV_COV_", muminus_OWNPV_COV_, &b_muminus_OWNPV_COV_);
   fChain->SetBranchAddress("muminus_IP_OWNPV", &muminus_IP_OWNPV, &b_muminus_IP_OWNPV);
   fChain->SetBranchAddress("muminus_IPCHI2_OWNPV", &muminus_IPCHI2_OWNPV, &b_muminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("muminus_ORIVX_X", &muminus_ORIVX_X, &b_muminus_ORIVX_X);
   fChain->SetBranchAddress("muminus_ORIVX_Y", &muminus_ORIVX_Y, &b_muminus_ORIVX_Y);
   fChain->SetBranchAddress("muminus_ORIVX_Z", &muminus_ORIVX_Z, &b_muminus_ORIVX_Z);
   fChain->SetBranchAddress("muminus_ORIVX_XERR", &muminus_ORIVX_XERR, &b_muminus_ORIVX_XERR);
   fChain->SetBranchAddress("muminus_ORIVX_YERR", &muminus_ORIVX_YERR, &b_muminus_ORIVX_YERR);
   fChain->SetBranchAddress("muminus_ORIVX_ZERR", &muminus_ORIVX_ZERR, &b_muminus_ORIVX_ZERR);
   fChain->SetBranchAddress("muminus_ORIVX_CHI2", &muminus_ORIVX_CHI2, &b_muminus_ORIVX_CHI2);
   fChain->SetBranchAddress("muminus_ORIVX_NDOF", &muminus_ORIVX_NDOF, &b_muminus_ORIVX_NDOF);
   fChain->SetBranchAddress("muminus_ORIVX_COV_", muminus_ORIVX_COV_, &b_muminus_ORIVX_COV_);
   fChain->SetBranchAddress("muminus_P", &muminus_P, &b_muminus_P);
   fChain->SetBranchAddress("muminus_PT", &muminus_PT, &b_muminus_PT);
   fChain->SetBranchAddress("muminus_PE", &muminus_PE, &b_muminus_PE);
   fChain->SetBranchAddress("muminus_PX", &muminus_PX, &b_muminus_PX);
   fChain->SetBranchAddress("muminus_PY", &muminus_PY, &b_muminus_PY);
   fChain->SetBranchAddress("muminus_PZ", &muminus_PZ, &b_muminus_PZ);
   fChain->SetBranchAddress("muminus_M", &muminus_M, &b_muminus_M);
   fChain->SetBranchAddress("muminus_ID", &muminus_ID, &b_muminus_ID);
   fChain->SetBranchAddress("muminus_PIDe", &muminus_PIDe, &b_muminus_PIDe);
   fChain->SetBranchAddress("muminus_PIDmu", &muminus_PIDmu, &b_muminus_PIDmu);
   fChain->SetBranchAddress("muminus_PIDK", &muminus_PIDK, &b_muminus_PIDK);
   fChain->SetBranchAddress("muminus_PIDp", &muminus_PIDp, &b_muminus_PIDp);
   fChain->SetBranchAddress("muminus_ProbNNe", &muminus_ProbNNe, &b_muminus_ProbNNe);
   fChain->SetBranchAddress("muminus_ProbNNk", &muminus_ProbNNk, &b_muminus_ProbNNk);
   fChain->SetBranchAddress("muminus_ProbNNp", &muminus_ProbNNp, &b_muminus_ProbNNp);
   fChain->SetBranchAddress("muminus_ProbNNpi", &muminus_ProbNNpi, &b_muminus_ProbNNpi);
   fChain->SetBranchAddress("muminus_ProbNNmu", &muminus_ProbNNmu, &b_muminus_ProbNNmu);
   fChain->SetBranchAddress("muminus_ProbNNghost", &muminus_ProbNNghost, &b_muminus_ProbNNghost);
   fChain->SetBranchAddress("muminus_hasMuon", &muminus_hasMuon, &b_muminus_hasMuon);
   fChain->SetBranchAddress("muminus_isMuon", &muminus_isMuon, &b_muminus_isMuon);
   fChain->SetBranchAddress("muminus_hasRich", &muminus_hasRich, &b_muminus_hasRich);
   fChain->SetBranchAddress("muminus_UsedRichAerogel", &muminus_UsedRichAerogel, &b_muminus_UsedRichAerogel);
   fChain->SetBranchAddress("muminus_UsedRich1Gas", &muminus_UsedRich1Gas, &b_muminus_UsedRich1Gas);
   fChain->SetBranchAddress("muminus_UsedRich2Gas", &muminus_UsedRich2Gas, &b_muminus_UsedRich2Gas);
   fChain->SetBranchAddress("muminus_RichAboveElThres", &muminus_RichAboveElThres, &b_muminus_RichAboveElThres);
   fChain->SetBranchAddress("muminus_RichAboveMuThres", &muminus_RichAboveMuThres, &b_muminus_RichAboveMuThres);
   fChain->SetBranchAddress("muminus_RichAbovePiThres", &muminus_RichAbovePiThres, &b_muminus_RichAbovePiThres);
   fChain->SetBranchAddress("muminus_RichAboveKaThres", &muminus_RichAboveKaThres, &b_muminus_RichAboveKaThres);
   fChain->SetBranchAddress("muminus_RichAbovePrThres", &muminus_RichAbovePrThres, &b_muminus_RichAbovePrThres);
   fChain->SetBranchAddress("muminus_hasCalo", &muminus_hasCalo, &b_muminus_hasCalo);
   fChain->SetBranchAddress("muminus_L0Global_Dec", &muminus_L0Global_Dec, &b_muminus_L0Global_Dec);
   fChain->SetBranchAddress("muminus_L0Global_TIS", &muminus_L0Global_TIS, &b_muminus_L0Global_TIS);
   fChain->SetBranchAddress("muminus_L0Global_TOS", &muminus_L0Global_TOS, &b_muminus_L0Global_TOS);
   fChain->SetBranchAddress("muminus_Hlt1Global_Dec", &muminus_Hlt1Global_Dec, &b_muminus_Hlt1Global_Dec);
   fChain->SetBranchAddress("muminus_Hlt1Global_TIS", &muminus_Hlt1Global_TIS, &b_muminus_Hlt1Global_TIS);
   fChain->SetBranchAddress("muminus_Hlt1Global_TOS", &muminus_Hlt1Global_TOS, &b_muminus_Hlt1Global_TOS);
   fChain->SetBranchAddress("muminus_Hlt1Phys_Dec", &muminus_Hlt1Phys_Dec, &b_muminus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("muminus_Hlt1Phys_TIS", &muminus_Hlt1Phys_TIS, &b_muminus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("muminus_Hlt1Phys_TOS", &muminus_Hlt1Phys_TOS, &b_muminus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("muminus_Hlt2Global_Dec", &muminus_Hlt2Global_Dec, &b_muminus_Hlt2Global_Dec);
   fChain->SetBranchAddress("muminus_Hlt2Global_TIS", &muminus_Hlt2Global_TIS, &b_muminus_Hlt2Global_TIS);
   fChain->SetBranchAddress("muminus_Hlt2Global_TOS", &muminus_Hlt2Global_TOS, &b_muminus_Hlt2Global_TOS);
   fChain->SetBranchAddress("muminus_Hlt2Phys_Dec", &muminus_Hlt2Phys_Dec, &b_muminus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("muminus_Hlt2Phys_TIS", &muminus_Hlt2Phys_TIS, &b_muminus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("muminus_Hlt2Phys_TOS", &muminus_Hlt2Phys_TOS, &b_muminus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("muminus_L0MUONDecision_Dec", &muminus_L0MUONDecision_Dec, &b_muminus_L0MUONDecision_Dec);
   fChain->SetBranchAddress("muminus_L0MUONDecision_TIS", &muminus_L0MUONDecision_TIS, &b_muminus_L0MUONDecision_TIS);
   fChain->SetBranchAddress("muminus_L0MUONDecision_TOS", &muminus_L0MUONDecision_TOS, &b_muminus_L0MUONDecision_TOS);
   fChain->SetBranchAddress("muminus_L0SPDDecision_Dec", &muminus_L0SPDDecision_Dec, &b_muminus_L0SPDDecision_Dec);
   fChain->SetBranchAddress("muminus_L0SPDDecision_TIS", &muminus_L0SPDDecision_TIS, &b_muminus_L0SPDDecision_TIS);
   fChain->SetBranchAddress("muminus_L0SPDDecision_TOS", &muminus_L0SPDDecision_TOS, &b_muminus_L0SPDDecision_TOS);
   fChain->SetBranchAddress("muminus_L0PUDecision_Dec", &muminus_L0PUDecision_Dec, &b_muminus_L0PUDecision_Dec);
   fChain->SetBranchAddress("muminus_L0PUDecision_TIS", &muminus_L0PUDecision_TIS, &b_muminus_L0PUDecision_TIS);
   fChain->SetBranchAddress("muminus_L0PUDecision_TOS", &muminus_L0PUDecision_TOS, &b_muminus_L0PUDecision_TOS);
   fChain->SetBranchAddress("muminus_L0SPDLowMultDecision_Dec", &muminus_L0SPDLowMultDecision_Dec, &b_muminus_L0SPDLowMultDecision_Dec);
   fChain->SetBranchAddress("muminus_L0SPDLowMultDecision_TIS", &muminus_L0SPDLowMultDecision_TIS, &b_muminus_L0SPDLowMultDecision_TIS);
   fChain->SetBranchAddress("muminus_L0SPDLowMultDecision_TOS", &muminus_L0SPDLowMultDecision_TOS, &b_muminus_L0SPDLowMultDecision_TOS);
   fChain->SetBranchAddress("muminus_L0CALODecision_Dec", &muminus_L0CALODecision_Dec, &b_muminus_L0CALODecision_Dec);
   fChain->SetBranchAddress("muminus_L0CALODecision_TIS", &muminus_L0CALODecision_TIS, &b_muminus_L0CALODecision_TIS);
   fChain->SetBranchAddress("muminus_L0CALODecision_TOS", &muminus_L0CALODecision_TOS, &b_muminus_L0CALODecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1DiMuonHighMassDecision_Dec", &muminus_Hlt1DiMuonHighMassDecision_Dec, &b_muminus_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1DiMuonHighMassDecision_TIS", &muminus_Hlt1DiMuonHighMassDecision_TIS, &b_muminus_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1DiMuonHighMassDecision_TOS", &muminus_Hlt1DiMuonHighMassDecision_TOS, &b_muminus_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1BBMicroBiasVeloDecision_Dec", &muminus_Hlt1BBMicroBiasVeloDecision_Dec, &b_muminus_Hlt1BBMicroBiasVeloDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1BBMicroBiasVeloDecision_TIS", &muminus_Hlt1BBMicroBiasVeloDecision_TIS, &b_muminus_Hlt1BBMicroBiasVeloDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1BBMicroBiasVeloDecision_TOS", &muminus_Hlt1BBMicroBiasVeloDecision_TOS, &b_muminus_Hlt1BBMicroBiasVeloDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1LumiLowBeamCrossingDecision_Dec", &muminus_Hlt1LumiLowBeamCrossingDecision_Dec, &b_muminus_Hlt1LumiLowBeamCrossingDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1LumiLowBeamCrossingDecision_TIS", &muminus_Hlt1LumiLowBeamCrossingDecision_TIS, &b_muminus_Hlt1LumiLowBeamCrossingDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1LumiLowBeamCrossingDecision_TOS", &muminus_Hlt1LumiLowBeamCrossingDecision_TOS, &b_muminus_Hlt1LumiLowBeamCrossingDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1VeloClosingMicroBiasDecision_Dec", &muminus_Hlt1VeloClosingMicroBiasDecision_Dec, &b_muminus_Hlt1VeloClosingMicroBiasDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1VeloClosingMicroBiasDecision_TIS", &muminus_Hlt1VeloClosingMicroBiasDecision_TIS, &b_muminus_Hlt1VeloClosingMicroBiasDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1VeloClosingMicroBiasDecision_TOS", &muminus_Hlt1VeloClosingMicroBiasDecision_TOS, &b_muminus_Hlt1VeloClosingMicroBiasDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt2BBPassThroughDecision_Dec", &muminus_Hlt2BBPassThroughDecision_Dec, &b_muminus_Hlt2BBPassThroughDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt2BBPassThroughDecision_TIS", &muminus_Hlt2BBPassThroughDecision_TIS, &b_muminus_Hlt2BBPassThroughDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt2BBPassThroughDecision_TOS", &muminus_Hlt2BBPassThroughDecision_TOS, &b_muminus_Hlt2BBPassThroughDecision_TOS);
   fChain->SetBranchAddress("muminus_TRACK_Type", &muminus_TRACK_Type, &b_muminus_TRACK_Type);
   fChain->SetBranchAddress("muminus_TRACK_Key", &muminus_TRACK_Key, &b_muminus_TRACK_Key);
   fChain->SetBranchAddress("muminus_TRACK_CHI2NDOF", &muminus_TRACK_CHI2NDOF, &b_muminus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("muminus_TRACK_PCHI2", &muminus_TRACK_PCHI2, &b_muminus_TRACK_PCHI2);
   fChain->SetBranchAddress("muminus_TRACK_MatchCHI2", &muminus_TRACK_MatchCHI2, &b_muminus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("muminus_TRACK_GhostProb", &muminus_TRACK_GhostProb, &b_muminus_TRACK_GhostProb);
   fChain->SetBranchAddress("muminus_TRACK_CloneDist", &muminus_TRACK_CloneDist, &b_muminus_TRACK_CloneDist);
   fChain->SetBranchAddress("muminus_TRACK_Likelihood", &muminus_TRACK_Likelihood, &b_muminus_TRACK_Likelihood);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("B00", &B00, &b_B00);
   fChain->SetBranchAddress("B01", &B01, &b_B01);
   fChain->SetBranchAddress("B02", &B02, &b_B02);
   fChain->SetBranchAddress("B03", &B03, &b_B03);
   fChain->SetBranchAddress("B10", &B10, &b_B10);
   fChain->SetBranchAddress("B11", &B11, &b_B11);
   fChain->SetBranchAddress("B12", &B12, &b_B12);
   fChain->SetBranchAddress("B13", &B13, &b_B13);
   fChain->SetBranchAddress("B20", &B20, &b_B20);
   fChain->SetBranchAddress("B21", &B21, &b_B21);
   fChain->SetBranchAddress("B22", &B22, &b_B22);
   fChain->SetBranchAddress("B23", &B23, &b_B23);
   fChain->SetBranchAddress("F10", &F10, &b_F10);
   fChain->SetBranchAddress("F11", &F11, &b_F11);
   fChain->SetBranchAddress("F12", &F12, &b_F12);
   fChain->SetBranchAddress("F13", &F13, &b_F13);
   fChain->SetBranchAddress("F20", &F20, &b_F20);
   fChain->SetBranchAddress("F21", &F21, &b_F21);
   fChain->SetBranchAddress("F22", &F22, &b_F22);
   fChain->SetBranchAddress("F23", &F23, &b_F23);
   fChain->SetBranchAddress("log_hrc_fom_v2", &log_hrc_fom_v2, &b_log_hrc_fom_v2);
   fChain->SetBranchAddress("log_hrc_fom_B_v2", &log_hrc_fom_B_v2, &b_log_hrc_fom_B_v2);
   fChain->SetBranchAddress("log_hrc_fom_F_v2", &log_hrc_fom_F_v2, &b_log_hrc_fom_F_v2);
   fChain->SetBranchAddress("nchB", &nchB, &b_nchB);
   fChain->SetBranchAddress("adc_B", adc_B, &b_adc_B);
   fChain->SetBranchAddress("nchF", &nchF, &b_nchF);
   fChain->SetBranchAddress("adc_F", adc_F, &b_adc_F);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   Notify();
}

Bool_t makeNewTuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void makeNewTuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t makeNewTuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef makeNewTuple_cxx
