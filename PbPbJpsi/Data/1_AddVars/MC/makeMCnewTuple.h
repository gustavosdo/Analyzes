//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct 17 11:47:26 2018 by ROOT version 6.14/04
// from TChain diMuTree/DecayTree/diMuTree/DecayTree
//////////////////////////////////////////////////////////

#ifndef makeMCnewTuple_h
#define makeMCnewTuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class makeMCnewTuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxdiMu_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxdiMu_OWNPV_COV = 1;
   static constexpr Int_t kMaxmuplus_OWNPV_COV = 1;
   static constexpr Int_t kMaxmuplus_ORIVX_COV = 1;
   static constexpr Int_t kMaxmuminus_OWNPV_COV = 1;
   static constexpr Int_t kMaxmuminus_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        diMu_ENDVERTEX_X;
   Double_t        diMu_ENDVERTEX_Y;
   Double_t        diMu_ENDVERTEX_Z;
   Double_t        diMu_ENDVERTEX_XERR;
   Double_t        diMu_ENDVERTEX_YERR;
   Double_t        diMu_ENDVERTEX_ZERR;
   Double_t        diMu_ENDVERTEX_CHI2;
   Int_t           diMu_ENDVERTEX_NDOF;
   Float_t         diMu_ENDVERTEX_COV_[3][3];
   Double_t        diMu_OWNPV_X;
   Double_t        diMu_OWNPV_Y;
   Double_t        diMu_OWNPV_Z;
   Double_t        diMu_OWNPV_XERR;
   Double_t        diMu_OWNPV_YERR;
   Double_t        diMu_OWNPV_ZERR;
   Double_t        diMu_OWNPV_CHI2;
   Int_t           diMu_OWNPV_NDOF;
   Float_t         diMu_OWNPV_COV_[3][3];
   Double_t        diMu_IP_OWNPV;
   Double_t        diMu_IPCHI2_OWNPV;
   Double_t        diMu_FD_OWNPV;
   Double_t        diMu_FDCHI2_OWNPV;
   Double_t        diMu_DIRA_OWNPV;
   Double_t        diMu_P;
   Double_t        diMu_PT;
   Double_t        diMu_PE;
   Double_t        diMu_PX;
   Double_t        diMu_PY;
   Double_t        diMu_PZ;
   Double_t        diMu_MM;
   Double_t        diMu_MMERR;
   Double_t        diMu_M;
   Int_t           diMu_ID;
   Bool_t          diMu_L0Global_Dec;
   Bool_t          diMu_L0Global_TIS;
   Bool_t          diMu_L0Global_TOS;
   Bool_t          diMu_Hlt1Global_Dec;
   Bool_t          diMu_Hlt1Global_TIS;
   Bool_t          diMu_Hlt1Global_TOS;
   Bool_t          diMu_Hlt1Phys_Dec;
   Bool_t          diMu_Hlt1Phys_TIS;
   Bool_t          diMu_Hlt1Phys_TOS;
   Bool_t          diMu_Hlt2Global_Dec;
   Bool_t          diMu_Hlt2Global_TIS;
   Bool_t          diMu_Hlt2Global_TOS;
   Bool_t          diMu_Hlt2Phys_Dec;
   Bool_t          diMu_Hlt2Phys_TIS;
   Bool_t          diMu_Hlt2Phys_TOS;
   Bool_t          diMu_L0DiMuonDecision_Dec;
   Bool_t          diMu_L0DiMuonDecision_TIS;
   Bool_t          diMu_L0DiMuonDecision_TOS;
   Bool_t          diMu_L0DiMuonNoSPDDecision_Dec;
   Bool_t          diMu_L0DiMuonNoSPDDecision_TIS;
   Bool_t          diMu_L0DiMuonNoSPDDecision_TOS;
   Bool_t          diMu_L0MUON_minbiasDecision_Dec;
   Bool_t          diMu_L0MUON_minbiasDecision_TIS;
   Bool_t          diMu_L0MUON_minbiasDecision_TOS;
   Bool_t          diMu_L0MUONDecision_Dec;
   Bool_t          diMu_L0MUONDecision_TIS;
   Bool_t          diMu_L0MUONDecision_TOS;
   Bool_t          diMu_L0MuonDecision_Dec;
   Bool_t          diMu_L0MuonDecision_TIS;
   Bool_t          diMu_L0MuonDecision_TOS;
   Bool_t          diMu_L0MuonNoSPDDecision_Dec;
   Bool_t          diMu_L0MuonNoSPDDecision_TIS;
   Bool_t          diMu_L0MuonNoSPDDecision_TOS;
   Bool_t          diMu_L0Muon_lowMultDecision_Dec;
   Bool_t          diMu_L0Muon_lowMultDecision_TIS;
   Bool_t          diMu_L0Muon_lowMultDecision_TOS;
   Bool_t          diMu_L0DiMuon_lowMultDecision_Dec;
   Bool_t          diMu_L0DiMuon_lowMultDecision_TIS;
   Bool_t          diMu_L0DiMuon_lowMultDecision_TOS;
   Bool_t          diMu_L0CALODecision_Dec;
   Bool_t          diMu_L0CALODecision_TIS;
   Bool_t          diMu_L0CALODecision_TOS;
   Bool_t          diMu_L0SPDDecision_Dec;
   Bool_t          diMu_Hlt1NoPVPassThroughDecision_Dec;
   Bool_t          diMu_Hlt1NoPVPassThroughDecision_TIS;
   Bool_t          diMu_Hlt1NoPVPassThroughDecision_TOS;
   Bool_t          diMu_Hlt1BBMicroBiasVeloDecision_Dec;
   Bool_t          diMu_Hlt1BBMicroBiasVeloDecision_TIS;
   Bool_t          diMu_Hlt1BBMicroBiasVeloDecision_TOS;
   Bool_t          diMu_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          diMu_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          diMu_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          diMu_Hlt2LowMultMuonDecision_Dec;
   Bool_t          diMu_Hlt2LowMultMuonDecision_TIS;
   Bool_t          diMu_Hlt2LowMultMuonDecision_TOS;
   Bool_t          diMu_Hlt2PassThroughDecision_Dec;
   Bool_t          diMu_Hlt2PassThroughDecision_TIS;
   Bool_t          diMu_Hlt2PassThroughDecision_TOS;
   Double_t        muplus_OWNPV_X;
   Double_t        muplus_OWNPV_Y;
   Double_t        muplus_OWNPV_Z;
   Double_t        muplus_OWNPV_XERR;
   Double_t        muplus_OWNPV_YERR;
   Double_t        muplus_OWNPV_ZERR;
   Double_t        muplus_OWNPV_CHI2;
   Int_t           muplus_OWNPV_NDOF;
   Float_t         muplus_OWNPV_COV_[3][3];
   Double_t        muplus_IP_OWNPV;
   Double_t        muplus_IPCHI2_OWNPV;
   Double_t        muplus_ORIVX_X;
   Double_t        muplus_ORIVX_Y;
   Double_t        muplus_ORIVX_Z;
   Double_t        muplus_ORIVX_XERR;
   Double_t        muplus_ORIVX_YERR;
   Double_t        muplus_ORIVX_ZERR;
   Double_t        muplus_ORIVX_CHI2;
   Int_t           muplus_ORIVX_NDOF;
   Float_t         muplus_ORIVX_COV_[3][3];
   Double_t        muplus_P;
   Double_t        muplus_PT;
   Double_t        muplus_PE;
   Double_t        muplus_PX;
   Double_t        muplus_PY;
   Double_t        muplus_PZ;
   Double_t        muplus_M;
   Int_t           muplus_ID;
   Double_t        muplus_PIDe;
   Double_t        muplus_PIDmu;
   Double_t        muplus_PIDK;
   Double_t        muplus_PIDp;
   Double_t        muplus_ProbNNe;
   Double_t        muplus_ProbNNk;
   Double_t        muplus_ProbNNp;
   Double_t        muplus_ProbNNpi;
   Double_t        muplus_ProbNNmu;
   Double_t        muplus_ProbNNghost;
   Bool_t          muplus_hasMuon;
   Bool_t          muplus_isMuon;
   Bool_t          muplus_hasRich;
   Bool_t          muplus_UsedRichAerogel;
   Bool_t          muplus_UsedRich1Gas;
   Bool_t          muplus_UsedRich2Gas;
   Bool_t          muplus_RichAboveElThres;
   Bool_t          muplus_RichAboveMuThres;
   Bool_t          muplus_RichAbovePiThres;
   Bool_t          muplus_RichAboveKaThres;
   Bool_t          muplus_RichAbovePrThres;
   Bool_t          muplus_hasCalo;
   Double_t        muplus_PP_InAccMuon;
   Int_t           muplus_TRACK_Type;
   Int_t           muplus_TRACK_Key;
   Double_t        muplus_TRACK_CHI2NDOF;
   Double_t        muplus_TRACK_PCHI2;
   Double_t        muplus_TRACK_MatchCHI2;
   Double_t        muplus_TRACK_GhostProb;
   Double_t        muplus_TRACK_CloneDist;
   Double_t        muplus_TRACK_Likelihood;
   Bool_t          muplus_L0Global_Dec;
   Bool_t          muplus_L0Global_TIS;
   Bool_t          muplus_L0Global_TOS;
   Bool_t          muplus_Hlt1Global_Dec;
   Bool_t          muplus_Hlt1Global_TIS;
   Bool_t          muplus_Hlt1Global_TOS;
   Bool_t          muplus_Hlt1Phys_Dec;
   Bool_t          muplus_Hlt1Phys_TIS;
   Bool_t          muplus_Hlt1Phys_TOS;
   Bool_t          muplus_Hlt2Global_Dec;
   Bool_t          muplus_Hlt2Global_TIS;
   Bool_t          muplus_Hlt2Global_TOS;
   Bool_t          muplus_Hlt2Phys_Dec;
   Bool_t          muplus_Hlt2Phys_TIS;
   Bool_t          muplus_Hlt2Phys_TOS;
   Bool_t          muplus_L0DiMuonDecision_Dec;
   Bool_t          muplus_L0DiMuonDecision_TIS;
   Bool_t          muplus_L0DiMuonDecision_TOS;
   Bool_t          muplus_L0DiMuonNoSPDDecision_Dec;
   Bool_t          muplus_L0DiMuonNoSPDDecision_TIS;
   Bool_t          muplus_L0DiMuonNoSPDDecision_TOS;
   Bool_t          muplus_L0MUON_minbiasDecision_Dec;
   Bool_t          muplus_L0MUON_minbiasDecision_TIS;
   Bool_t          muplus_L0MUON_minbiasDecision_TOS;
   Bool_t          muplus_L0MUONDecision_Dec;
   Bool_t          muplus_L0MUONDecision_TIS;
   Bool_t          muplus_L0MUONDecision_TOS;
   Bool_t          muplus_L0MuonDecision_Dec;
   Bool_t          muplus_L0MuonDecision_TIS;
   Bool_t          muplus_L0MuonDecision_TOS;
   Bool_t          muplus_L0MuonNoSPDDecision_Dec;
   Bool_t          muplus_L0MuonNoSPDDecision_TIS;
   Bool_t          muplus_L0MuonNoSPDDecision_TOS;
   Bool_t          muplus_L0Muon_lowMultDecision_Dec;
   Bool_t          muplus_L0Muon_lowMultDecision_TIS;
   Bool_t          muplus_L0Muon_lowMultDecision_TOS;
   Bool_t          muplus_L0DiMuon_lowMultDecision_Dec;
   Bool_t          muplus_L0DiMuon_lowMultDecision_TIS;
   Bool_t          muplus_L0DiMuon_lowMultDecision_TOS;
   Bool_t          muplus_L0CALODecision_Dec;
   Bool_t          muplus_L0CALODecision_TIS;
   Bool_t          muplus_L0CALODecision_TOS;
   Bool_t          muplus_Hlt1NoPVPassThroughDecision_Dec;
   Bool_t          muplus_Hlt1NoPVPassThroughDecision_TIS;
   Bool_t          muplus_Hlt1NoPVPassThroughDecision_TOS;
   Bool_t          muplus_Hlt1BBMicroBiasVeloDecision_Dec;
   Bool_t          muplus_Hlt1BBMicroBiasVeloDecision_TIS;
   Bool_t          muplus_Hlt1BBMicroBiasVeloDecision_TOS;
   Bool_t          muplus_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          muplus_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          muplus_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          muplus_Hlt2LowMultMuonDecision_Dec;
   Bool_t          muplus_Hlt2LowMultMuonDecision_TIS;
   Bool_t          muplus_Hlt2LowMultMuonDecision_TOS;
   Bool_t          muplus_Hlt2PassThroughDecision_Dec;
   Bool_t          muplus_Hlt2PassThroughDecision_TIS;
   Bool_t          muplus_Hlt2PassThroughDecision_TOS;
   Double_t        muminus_OWNPV_X;
   Double_t        muminus_OWNPV_Y;
   Double_t        muminus_OWNPV_Z;
   Double_t        muminus_OWNPV_XERR;
   Double_t        muminus_OWNPV_YERR;
   Double_t        muminus_OWNPV_ZERR;
   Double_t        muminus_OWNPV_CHI2;
   Int_t           muminus_OWNPV_NDOF;
   Float_t         muminus_OWNPV_COV_[3][3];
   Double_t        muminus_IP_OWNPV;
   Double_t        muminus_IPCHI2_OWNPV;
   Double_t        muminus_ORIVX_X;
   Double_t        muminus_ORIVX_Y;
   Double_t        muminus_ORIVX_Z;
   Double_t        muminus_ORIVX_XERR;
   Double_t        muminus_ORIVX_YERR;
   Double_t        muminus_ORIVX_ZERR;
   Double_t        muminus_ORIVX_CHI2;
   Int_t           muminus_ORIVX_NDOF;
   Float_t         muminus_ORIVX_COV_[3][3];
   Double_t        muminus_P;
   Double_t        muminus_PT;
   Double_t        muminus_PE;
   Double_t        muminus_PX;
   Double_t        muminus_PY;
   Double_t        muminus_PZ;
   Double_t        muminus_M;
   Int_t           muminus_ID;
   Double_t        muminus_PIDe;
   Double_t        muminus_PIDmu;
   Double_t        muminus_PIDK;
   Double_t        muminus_PIDp;
   Double_t        muminus_ProbNNe;
   Double_t        muminus_ProbNNk;
   Double_t        muminus_ProbNNp;
   Double_t        muminus_ProbNNpi;
   Double_t        muminus_ProbNNmu;
   Double_t        muminus_ProbNNghost;
   Bool_t          muminus_hasMuon;
   Bool_t          muminus_isMuon;
   Bool_t          muminus_hasRich;
   Bool_t          muminus_UsedRichAerogel;
   Bool_t          muminus_UsedRich1Gas;
   Bool_t          muminus_UsedRich2Gas;
   Bool_t          muminus_RichAboveElThres;
   Bool_t          muminus_RichAboveMuThres;
   Bool_t          muminus_RichAbovePiThres;
   Bool_t          muminus_RichAboveKaThres;
   Bool_t          muminus_RichAbovePrThres;
   Bool_t          muminus_hasCalo;
   Double_t        muminus_PP_InAccMuon;
   Int_t           muminus_TRACK_Type;
   Int_t           muminus_TRACK_Key;
   Double_t        muminus_TRACK_CHI2NDOF;
   Double_t        muminus_TRACK_PCHI2;
   Double_t        muminus_TRACK_MatchCHI2;
   Double_t        muminus_TRACK_GhostProb;
   Double_t        muminus_TRACK_CloneDist;
   Double_t        muminus_TRACK_Likelihood;
   Bool_t          muminus_L0Global_Dec;
   Bool_t          muminus_L0Global_TIS;
   Bool_t          muminus_L0Global_TOS;
   Bool_t          muminus_Hlt1Global_Dec;
   Bool_t          muminus_Hlt1Global_TIS;
   Bool_t          muminus_Hlt1Global_TOS;
   Bool_t          muminus_Hlt1Phys_Dec;
   Bool_t          muminus_Hlt1Phys_TIS;
   Bool_t          muminus_Hlt1Phys_TOS;
   Bool_t          muminus_Hlt2Global_Dec;
   Bool_t          muminus_Hlt2Global_TIS;
   Bool_t          muminus_Hlt2Global_TOS;
   Bool_t          muminus_Hlt2Phys_Dec;
   Bool_t          muminus_Hlt2Phys_TIS;
   Bool_t          muminus_Hlt2Phys_TOS;
   Bool_t          muminus_L0DiMuonDecision_Dec;
   Bool_t          muminus_L0DiMuonDecision_TIS;
   Bool_t          muminus_L0DiMuonDecision_TOS;
   Bool_t          muminus_L0DiMuonNoSPDDecision_Dec;
   Bool_t          muminus_L0DiMuonNoSPDDecision_TIS;
   Bool_t          muminus_L0DiMuonNoSPDDecision_TOS;
   Bool_t          muminus_L0MUON_minbiasDecision_Dec;
   Bool_t          muminus_L0MUON_minbiasDecision_TIS;
   Bool_t          muminus_L0MUON_minbiasDecision_TOS;
   Bool_t          muminus_L0MUONDecision_Dec;
   Bool_t          muminus_L0MUONDecision_TIS;
   Bool_t          muminus_L0MUONDecision_TOS;
   Bool_t          muminus_L0MuonDecision_Dec;
   Bool_t          muminus_L0MuonDecision_TIS;
   Bool_t          muminus_L0MuonDecision_TOS;
   Bool_t          muminus_L0MuonNoSPDDecision_Dec;
   Bool_t          muminus_L0MuonNoSPDDecision_TIS;
   Bool_t          muminus_L0MuonNoSPDDecision_TOS;
   Bool_t          muminus_L0Muon_lowMultDecision_Dec;
   Bool_t          muminus_L0Muon_lowMultDecision_TIS;
   Bool_t          muminus_L0Muon_lowMultDecision_TOS;
   Bool_t          muminus_L0DiMuon_lowMultDecision_Dec;
   Bool_t          muminus_L0DiMuon_lowMultDecision_TIS;
   Bool_t          muminus_L0DiMuon_lowMultDecision_TOS;
   Bool_t          muminus_L0CALODecision_Dec;
   Bool_t          muminus_L0CALODecision_TIS;
   Bool_t          muminus_L0CALODecision_TOS;
   Bool_t          muminus_Hlt1NoPVPassThroughDecision_Dec;
   Bool_t          muminus_Hlt1NoPVPassThroughDecision_TIS;
   Bool_t          muminus_Hlt1NoPVPassThroughDecision_TOS;
   Bool_t          muminus_Hlt1BBMicroBiasVeloDecision_Dec;
   Bool_t          muminus_Hlt1BBMicroBiasVeloDecision_TIS;
   Bool_t          muminus_Hlt1BBMicroBiasVeloDecision_TOS;
   Bool_t          muminus_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          muminus_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          muminus_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          muminus_Hlt2LowMultMuonDecision_Dec;
   Bool_t          muminus_Hlt2LowMultMuonDecision_TIS;
   Bool_t          muminus_Hlt2LowMultMuonDecision_TOS;
   Bool_t          muminus_Hlt2PassThroughDecision_Dec;
   Bool_t          muminus_Hlt2PassThroughDecision_TIS;
   Bool_t          muminus_Hlt2PassThroughDecision_TOS;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   Int_t           L0Data_DiMuon_Pt;
   Int_t           L0Data_DiMuonProd_Pt1Pt2;
   Int_t           L0Data_Electron_Et;
   Int_t           L0Data_GlobalPi0_Et;
   Int_t           L0Data_Hadron_Et;
   Int_t           L0Data_LocalPi0_Et;
   Int_t           L0Data_Muon1_Pt;
   Int_t           L0Data_Muon1_Sgn;
   Int_t           L0Data_Muon2_Pt;
   Int_t           L0Data_Muon2_Sgn;
   Int_t           L0Data_Muon3_Pt;
   Int_t           L0Data_Muon3_Sgn;
   Int_t           L0Data_PUHits_Mult;
   Int_t           L0Data_PUPeak1_Cont;
   Int_t           L0Data_PUPeak1_Pos;
   Int_t           L0Data_PUPeak2_Cont;
   Int_t           L0Data_PUPeak2_Pos;
   Int_t           L0Data_Photon_Et;
   Int_t           L0Data_Spd_Mult;
   Int_t           L0Data_Sum_Et;
   Int_t           L0Data_Sum_Et_Next1;
   Int_t           L0Data_Sum_Et_Next2;
   Int_t           L0Data_Sum_Et_Prev1;
   Int_t           L0Data_Sum_Et_Prev2;
   Int_t           NumberOfStdLooseAllPhotons;
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   UInt_t          StrippingLowMultPP2PPMuMuLineDecision;
   UInt_t          StrippingLowMultMuonLineDecision;
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0DiMuonDecision;
   Int_t           L0DiMuonNoSPDDecision;
   Int_t           L0MUON_minbiasDecision;
   Int_t           L0MUONDecision;
   Int_t           L0MuonDecision;
   Int_t           L0MuonNoSPDDecision;
   Int_t           L0Muon_lowMultDecision;
   Int_t           L0DiMuon_lowMultDecision;
   Int_t           L0CALODecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1NoPVPassThroughDecision;
   Int_t           Hlt1BBMicroBiasVeloDecision;
   Int_t           Hlt1DiMuonHighMassDecision;
   UInt_t          Hlt1nSelections;
   Int_t           Hlt2LowMultMuonDecision;
   Int_t           Hlt2PassThroughDecision;
   UInt_t          Hlt2nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]

   // List of branches
   TBranch        *b_diMu_ENDVERTEX_X;   //!
   TBranch        *b_diMu_ENDVERTEX_Y;   //!
   TBranch        *b_diMu_ENDVERTEX_Z;   //!
   TBranch        *b_diMu_ENDVERTEX_XERR;   //!
   TBranch        *b_diMu_ENDVERTEX_YERR;   //!
   TBranch        *b_diMu_ENDVERTEX_ZERR;   //!
   TBranch        *b_diMu_ENDVERTEX_CHI2;   //!
   TBranch        *b_diMu_ENDVERTEX_NDOF;   //!
   TBranch        *b_diMu_ENDVERTEX_COV_;   //!
   TBranch        *b_diMu_OWNPV_X;   //!
   TBranch        *b_diMu_OWNPV_Y;   //!
   TBranch        *b_diMu_OWNPV_Z;   //!
   TBranch        *b_diMu_OWNPV_XERR;   //!
   TBranch        *b_diMu_OWNPV_YERR;   //!
   TBranch        *b_diMu_OWNPV_ZERR;   //!
   TBranch        *b_diMu_OWNPV_CHI2;   //!
   TBranch        *b_diMu_OWNPV_NDOF;   //!
   TBranch        *b_diMu_OWNPV_COV_;   //!
   TBranch        *b_diMu_IP_OWNPV;   //!
   TBranch        *b_diMu_IPCHI2_OWNPV;   //!
   TBranch        *b_diMu_FD_OWNPV;   //!
   TBranch        *b_diMu_FDCHI2_OWNPV;   //!
   TBranch        *b_diMu_DIRA_OWNPV;   //!
   TBranch        *b_diMu_P;   //!
   TBranch        *b_diMu_PT;   //!
   TBranch        *b_diMu_PE;   //!
   TBranch        *b_diMu_PX;   //!
   TBranch        *b_diMu_PY;   //!
   TBranch        *b_diMu_PZ;   //!
   TBranch        *b_diMu_MM;   //!
   TBranch        *b_diMu_MMERR;   //!
   TBranch        *b_diMu_M;   //!
   TBranch        *b_diMu_ID;   //!
   TBranch        *b_diMu_L0Global_Dec;   //!
   TBranch        *b_diMu_L0Global_TIS;   //!
   TBranch        *b_diMu_L0Global_TOS;   //!
   TBranch        *b_diMu_Hlt1Global_Dec;   //!
   TBranch        *b_diMu_Hlt1Global_TIS;   //!
   TBranch        *b_diMu_Hlt1Global_TOS;   //!
   TBranch        *b_diMu_Hlt1Phys_Dec;   //!
   TBranch        *b_diMu_Hlt1Phys_TIS;   //!
   TBranch        *b_diMu_Hlt1Phys_TOS;   //!
   TBranch        *b_diMu_Hlt2Global_Dec;   //!
   TBranch        *b_diMu_Hlt2Global_TIS;   //!
   TBranch        *b_diMu_Hlt2Global_TOS;   //!
   TBranch        *b_diMu_Hlt2Phys_Dec;   //!
   TBranch        *b_diMu_Hlt2Phys_TIS;   //!
   TBranch        *b_diMu_Hlt2Phys_TOS;   //!
   TBranch        *b_diMu_L0DiMuonDecision_Dec;   //!
   TBranch        *b_diMu_L0DiMuonDecision_TIS;   //!
   TBranch        *b_diMu_L0DiMuonDecision_TOS;   //!
   TBranch        *b_diMu_L0DiMuonNoSPDDecision_Dec;   //!
   TBranch        *b_diMu_L0DiMuonNoSPDDecision_TIS;   //!
   TBranch        *b_diMu_L0DiMuonNoSPDDecision_TOS;   //!
   TBranch        *b_diMu_L0MUON_minbiasDecision_Dec;   //!
   TBranch        *b_diMu_L0MUON_minbiasDecision_TIS;   //!
   TBranch        *b_diMu_L0MUON_minbiasDecision_TOS;   //!
   TBranch        *b_diMu_L0MUONDecision_Dec;   //!
   TBranch        *b_diMu_L0MUONDecision_TIS;   //!
   TBranch        *b_diMu_L0MUONDecision_TOS;   //!
   TBranch        *b_diMu_L0MuonDecision_Dec;   //!
   TBranch        *b_diMu_L0MuonDecision_TIS;   //!
   TBranch        *b_diMu_L0MuonDecision_TOS;   //!
   TBranch        *b_diMu_L0MuonNoSPDDecision_Dec;   //!
   TBranch        *b_diMu_L0MuonNoSPDDecision_TIS;   //!
   TBranch        *b_diMu_L0MuonNoSPDDecision_TOS;   //!
   TBranch        *b_diMu_L0Muon_lowMultDecision_Dec;   //!
   TBranch        *b_diMu_L0Muon_lowMultDecision_TIS;   //!
   TBranch        *b_diMu_L0Muon_lowMultDecision_TOS;   //!
   TBranch        *b_diMu_L0DiMuon_lowMultDecision_Dec;   //!
   TBranch        *b_diMu_L0DiMuon_lowMultDecision_TIS;   //!
   TBranch        *b_diMu_L0DiMuon_lowMultDecision_TOS;   //!
   TBranch        *b_diMu_L0CALODecision_Dec;   //!
   TBranch        *b_diMu_L0CALODecision_TIS;   //!
   TBranch        *b_diMu_L0CALODecision_TOS;   //!
   TBranch        *b_diMu_L0SPDDecision_Dec;   //!
   TBranch        *b_diMu_Hlt1NoPVPassThroughDecision_Dec;   //!
   TBranch        *b_diMu_Hlt1NoPVPassThroughDecision_TIS;   //!
   TBranch        *b_diMu_Hlt1NoPVPassThroughDecision_TOS;   //!
   TBranch        *b_diMu_Hlt1BBMicroBiasVeloDecision_Dec;   //!
   TBranch        *b_diMu_Hlt1BBMicroBiasVeloDecision_TIS;   //!
   TBranch        *b_diMu_Hlt1BBMicroBiasVeloDecision_TOS;   //!
   TBranch        *b_diMu_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_diMu_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_diMu_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_diMu_Hlt2LowMultMuonDecision_Dec;   //!
   TBranch        *b_diMu_Hlt2LowMultMuonDecision_TIS;   //!
   TBranch        *b_diMu_Hlt2LowMultMuonDecision_TOS;   //!
   TBranch        *b_diMu_Hlt2PassThroughDecision_Dec;   //!
   TBranch        *b_diMu_Hlt2PassThroughDecision_TIS;   //!
   TBranch        *b_diMu_Hlt2PassThroughDecision_TOS;   //!
   TBranch        *b_muplus_OWNPV_X;   //!
   TBranch        *b_muplus_OWNPV_Y;   //!
   TBranch        *b_muplus_OWNPV_Z;   //!
   TBranch        *b_muplus_OWNPV_XERR;   //!
   TBranch        *b_muplus_OWNPV_YERR;   //!
   TBranch        *b_muplus_OWNPV_ZERR;   //!
   TBranch        *b_muplus_OWNPV_CHI2;   //!
   TBranch        *b_muplus_OWNPV_NDOF;   //!
   TBranch        *b_muplus_OWNPV_COV_;   //!
   TBranch        *b_muplus_IP_OWNPV;   //!
   TBranch        *b_muplus_IPCHI2_OWNPV;   //!
   TBranch        *b_muplus_ORIVX_X;   //!
   TBranch        *b_muplus_ORIVX_Y;   //!
   TBranch        *b_muplus_ORIVX_Z;   //!
   TBranch        *b_muplus_ORIVX_XERR;   //!
   TBranch        *b_muplus_ORIVX_YERR;   //!
   TBranch        *b_muplus_ORIVX_ZERR;   //!
   TBranch        *b_muplus_ORIVX_CHI2;   //!
   TBranch        *b_muplus_ORIVX_NDOF;   //!
   TBranch        *b_muplus_ORIVX_COV_;   //!
   TBranch        *b_muplus_P;   //!
   TBranch        *b_muplus_PT;   //!
   TBranch        *b_muplus_PE;   //!
   TBranch        *b_muplus_PX;   //!
   TBranch        *b_muplus_PY;   //!
   TBranch        *b_muplus_PZ;   //!
   TBranch        *b_muplus_M;   //!
   TBranch        *b_muplus_ID;   //!
   TBranch        *b_muplus_PIDe;   //!
   TBranch        *b_muplus_PIDmu;   //!
   TBranch        *b_muplus_PIDK;   //!
   TBranch        *b_muplus_PIDp;   //!
   TBranch        *b_muplus_ProbNNe;   //!
   TBranch        *b_muplus_ProbNNk;   //!
   TBranch        *b_muplus_ProbNNp;   //!
   TBranch        *b_muplus_ProbNNpi;   //!
   TBranch        *b_muplus_ProbNNmu;   //!
   TBranch        *b_muplus_ProbNNghost;   //!
   TBranch        *b_muplus_hasMuon;   //!
   TBranch        *b_muplus_isMuon;   //!
   TBranch        *b_muplus_hasRich;   //!
   TBranch        *b_muplus_UsedRichAerogel;   //!
   TBranch        *b_muplus_UsedRich1Gas;   //!
   TBranch        *b_muplus_UsedRich2Gas;   //!
   TBranch        *b_muplus_RichAboveElThres;   //!
   TBranch        *b_muplus_RichAboveMuThres;   //!
   TBranch        *b_muplus_RichAbovePiThres;   //!
   TBranch        *b_muplus_RichAboveKaThres;   //!
   TBranch        *b_muplus_RichAbovePrThres;   //!
   TBranch        *b_muplus_hasCalo;   //!
   TBranch        *b_muplus_PP_InAccMuon;   //!
   TBranch        *b_muplus_TRACK_Type;   //!
   TBranch        *b_muplus_TRACK_Key;   //!
   TBranch        *b_muplus_TRACK_CHI2NDOF;   //!
   TBranch        *b_muplus_TRACK_PCHI2;   //!
   TBranch        *b_muplus_TRACK_MatchCHI2;   //!
   TBranch        *b_muplus_TRACK_GhostProb;   //!
   TBranch        *b_muplus_TRACK_CloneDist;   //!
   TBranch        *b_muplus_TRACK_Likelihood;   //!
   TBranch        *b_muplus_L0Global_Dec;   //!
   TBranch        *b_muplus_L0Global_TIS;   //!
   TBranch        *b_muplus_L0Global_TOS;   //!
   TBranch        *b_muplus_Hlt1Global_Dec;   //!
   TBranch        *b_muplus_Hlt1Global_TIS;   //!
   TBranch        *b_muplus_Hlt1Global_TOS;   //!
   TBranch        *b_muplus_Hlt1Phys_Dec;   //!
   TBranch        *b_muplus_Hlt1Phys_TIS;   //!
   TBranch        *b_muplus_Hlt1Phys_TOS;   //!
   TBranch        *b_muplus_Hlt2Global_Dec;   //!
   TBranch        *b_muplus_Hlt2Global_TIS;   //!
   TBranch        *b_muplus_Hlt2Global_TOS;   //!
   TBranch        *b_muplus_Hlt2Phys_Dec;   //!
   TBranch        *b_muplus_Hlt2Phys_TIS;   //!
   TBranch        *b_muplus_Hlt2Phys_TOS;   //!
   TBranch        *b_muplus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_muplus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_muplus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_muplus_L0DiMuonNoSPDDecision_Dec;   //!
   TBranch        *b_muplus_L0DiMuonNoSPDDecision_TIS;   //!
   TBranch        *b_muplus_L0DiMuonNoSPDDecision_TOS;   //!
   TBranch        *b_muplus_L0MUON_minbiasDecision_Dec;   //!
   TBranch        *b_muplus_L0MUON_minbiasDecision_TIS;   //!
   TBranch        *b_muplus_L0MUON_minbiasDecision_TOS;   //!
   TBranch        *b_muplus_L0MUONDecision_Dec;   //!
   TBranch        *b_muplus_L0MUONDecision_TIS;   //!
   TBranch        *b_muplus_L0MUONDecision_TOS;   //!
   TBranch        *b_muplus_L0MuonDecision_Dec;   //!
   TBranch        *b_muplus_L0MuonDecision_TIS;   //!
   TBranch        *b_muplus_L0MuonDecision_TOS;   //!
   TBranch        *b_muplus_L0MuonNoSPDDecision_Dec;   //!
   TBranch        *b_muplus_L0MuonNoSPDDecision_TIS;   //!
   TBranch        *b_muplus_L0MuonNoSPDDecision_TOS;   //!
   TBranch        *b_muplus_L0Muon_lowMultDecision_Dec;   //!
   TBranch        *b_muplus_L0Muon_lowMultDecision_TIS;   //!
   TBranch        *b_muplus_L0Muon_lowMultDecision_TOS;   //!
   TBranch        *b_muplus_L0DiMuon_lowMultDecision_Dec;   //!
   TBranch        *b_muplus_L0DiMuon_lowMultDecision_TIS;   //!
   TBranch        *b_muplus_L0DiMuon_lowMultDecision_TOS;   //!
   TBranch        *b_muplus_L0CALODecision_Dec;   //!
   TBranch        *b_muplus_L0CALODecision_TIS;   //!
   TBranch        *b_muplus_L0CALODecision_TOS;   //!
   TBranch        *b_muplus_Hlt1NoPVPassThroughDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1NoPVPassThroughDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1NoPVPassThroughDecision_TOS;   //!
   TBranch        *b_muplus_Hlt1BBMicroBiasVeloDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1BBMicroBiasVeloDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1BBMicroBiasVeloDecision_TOS;   //!
   TBranch        *b_muplus_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_muplus_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_muplus_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_muplus_Hlt2LowMultMuonDecision_Dec;   //!
   TBranch        *b_muplus_Hlt2LowMultMuonDecision_TIS;   //!
   TBranch        *b_muplus_Hlt2LowMultMuonDecision_TOS;   //!
   TBranch        *b_muplus_Hlt2PassThroughDecision_Dec;   //!
   TBranch        *b_muplus_Hlt2PassThroughDecision_TIS;   //!
   TBranch        *b_muplus_Hlt2PassThroughDecision_TOS;   //!
   TBranch        *b_muminus_OWNPV_X;   //!
   TBranch        *b_muminus_OWNPV_Y;   //!
   TBranch        *b_muminus_OWNPV_Z;   //!
   TBranch        *b_muminus_OWNPV_XERR;   //!
   TBranch        *b_muminus_OWNPV_YERR;   //!
   TBranch        *b_muminus_OWNPV_ZERR;   //!
   TBranch        *b_muminus_OWNPV_CHI2;   //!
   TBranch        *b_muminus_OWNPV_NDOF;   //!
   TBranch        *b_muminus_OWNPV_COV_;   //!
   TBranch        *b_muminus_IP_OWNPV;   //!
   TBranch        *b_muminus_IPCHI2_OWNPV;   //!
   TBranch        *b_muminus_ORIVX_X;   //!
   TBranch        *b_muminus_ORIVX_Y;   //!
   TBranch        *b_muminus_ORIVX_Z;   //!
   TBranch        *b_muminus_ORIVX_XERR;   //!
   TBranch        *b_muminus_ORIVX_YERR;   //!
   TBranch        *b_muminus_ORIVX_ZERR;   //!
   TBranch        *b_muminus_ORIVX_CHI2;   //!
   TBranch        *b_muminus_ORIVX_NDOF;   //!
   TBranch        *b_muminus_ORIVX_COV_;   //!
   TBranch        *b_muminus_P;   //!
   TBranch        *b_muminus_PT;   //!
   TBranch        *b_muminus_PE;   //!
   TBranch        *b_muminus_PX;   //!
   TBranch        *b_muminus_PY;   //!
   TBranch        *b_muminus_PZ;   //!
   TBranch        *b_muminus_M;   //!
   TBranch        *b_muminus_ID;   //!
   TBranch        *b_muminus_PIDe;   //!
   TBranch        *b_muminus_PIDmu;   //!
   TBranch        *b_muminus_PIDK;   //!
   TBranch        *b_muminus_PIDp;   //!
   TBranch        *b_muminus_ProbNNe;   //!
   TBranch        *b_muminus_ProbNNk;   //!
   TBranch        *b_muminus_ProbNNp;   //!
   TBranch        *b_muminus_ProbNNpi;   //!
   TBranch        *b_muminus_ProbNNmu;   //!
   TBranch        *b_muminus_ProbNNghost;   //!
   TBranch        *b_muminus_hasMuon;   //!
   TBranch        *b_muminus_isMuon;   //!
   TBranch        *b_muminus_hasRich;   //!
   TBranch        *b_muminus_UsedRichAerogel;   //!
   TBranch        *b_muminus_UsedRich1Gas;   //!
   TBranch        *b_muminus_UsedRich2Gas;   //!
   TBranch        *b_muminus_RichAboveElThres;   //!
   TBranch        *b_muminus_RichAboveMuThres;   //!
   TBranch        *b_muminus_RichAbovePiThres;   //!
   TBranch        *b_muminus_RichAboveKaThres;   //!
   TBranch        *b_muminus_RichAbovePrThres;   //!
   TBranch        *b_muminus_hasCalo;   //!
   TBranch        *b_muminus_PP_InAccMuon;   //!
   TBranch        *b_muminus_TRACK_Type;   //!
   TBranch        *b_muminus_TRACK_Key;   //!
   TBranch        *b_muminus_TRACK_CHI2NDOF;   //!
   TBranch        *b_muminus_TRACK_PCHI2;   //!
   TBranch        *b_muminus_TRACK_MatchCHI2;   //!
   TBranch        *b_muminus_TRACK_GhostProb;   //!
   TBranch        *b_muminus_TRACK_CloneDist;   //!
   TBranch        *b_muminus_TRACK_Likelihood;   //!
   TBranch        *b_muminus_L0Global_Dec;   //!
   TBranch        *b_muminus_L0Global_TIS;   //!
   TBranch        *b_muminus_L0Global_TOS;   //!
   TBranch        *b_muminus_Hlt1Global_Dec;   //!
   TBranch        *b_muminus_Hlt1Global_TIS;   //!
   TBranch        *b_muminus_Hlt1Global_TOS;   //!
   TBranch        *b_muminus_Hlt1Phys_Dec;   //!
   TBranch        *b_muminus_Hlt1Phys_TIS;   //!
   TBranch        *b_muminus_Hlt1Phys_TOS;   //!
   TBranch        *b_muminus_Hlt2Global_Dec;   //!
   TBranch        *b_muminus_Hlt2Global_TIS;   //!
   TBranch        *b_muminus_Hlt2Global_TOS;   //!
   TBranch        *b_muminus_Hlt2Phys_Dec;   //!
   TBranch        *b_muminus_Hlt2Phys_TIS;   //!
   TBranch        *b_muminus_Hlt2Phys_TOS;   //!
   TBranch        *b_muminus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_muminus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_muminus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_muminus_L0DiMuonNoSPDDecision_Dec;   //!
   TBranch        *b_muminus_L0DiMuonNoSPDDecision_TIS;   //!
   TBranch        *b_muminus_L0DiMuonNoSPDDecision_TOS;   //!
   TBranch        *b_muminus_L0MUON_minbiasDecision_Dec;   //!
   TBranch        *b_muminus_L0MUON_minbiasDecision_TIS;   //!
   TBranch        *b_muminus_L0MUON_minbiasDecision_TOS;   //!
   TBranch        *b_muminus_L0MUONDecision_Dec;   //!
   TBranch        *b_muminus_L0MUONDecision_TIS;   //!
   TBranch        *b_muminus_L0MUONDecision_TOS;   //!
   TBranch        *b_muminus_L0MuonDecision_Dec;   //!
   TBranch        *b_muminus_L0MuonDecision_TIS;   //!
   TBranch        *b_muminus_L0MuonDecision_TOS;   //!
   TBranch        *b_muminus_L0MuonNoSPDDecision_Dec;   //!
   TBranch        *b_muminus_L0MuonNoSPDDecision_TIS;   //!
   TBranch        *b_muminus_L0MuonNoSPDDecision_TOS;   //!
   TBranch        *b_muminus_L0Muon_lowMultDecision_Dec;   //!
   TBranch        *b_muminus_L0Muon_lowMultDecision_TIS;   //!
   TBranch        *b_muminus_L0Muon_lowMultDecision_TOS;   //!
   TBranch        *b_muminus_L0DiMuon_lowMultDecision_Dec;   //!
   TBranch        *b_muminus_L0DiMuon_lowMultDecision_TIS;   //!
   TBranch        *b_muminus_L0DiMuon_lowMultDecision_TOS;   //!
   TBranch        *b_muminus_L0CALODecision_Dec;   //!
   TBranch        *b_muminus_L0CALODecision_TIS;   //!
   TBranch        *b_muminus_L0CALODecision_TOS;   //!
   TBranch        *b_muminus_Hlt1NoPVPassThroughDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1NoPVPassThroughDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1NoPVPassThroughDecision_TOS;   //!
   TBranch        *b_muminus_Hlt1BBMicroBiasVeloDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1BBMicroBiasVeloDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1BBMicroBiasVeloDecision_TOS;   //!
   TBranch        *b_muminus_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_muminus_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_muminus_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_muminus_Hlt2LowMultMuonDecision_Dec;   //!
   TBranch        *b_muminus_Hlt2LowMultMuonDecision_TIS;   //!
   TBranch        *b_muminus_Hlt2LowMultMuonDecision_TOS;   //!
   TBranch        *b_muminus_Hlt2PassThroughDecision_Dec;   //!
   TBranch        *b_muminus_Hlt2PassThroughDecision_TIS;   //!
   TBranch        *b_muminus_Hlt2PassThroughDecision_TOS;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_L0Data_DiMuon_Pt;   //!
   TBranch        *b_L0Data_DiMuonProd_Pt1Pt2;   //!
   TBranch        *b_L0Data_Electron_Et;   //!
   TBranch        *b_L0Data_GlobalPi0_Et;   //!
   TBranch        *b_L0Data_Hadron_Et;   //!
   TBranch        *b_L0Data_LocalPi0_Et;   //!
   TBranch        *b_L0Data_Muon1_Pt;   //!
   TBranch        *b_L0Data_Muon1_Sgn;   //!
   TBranch        *b_L0Data_Muon2_Pt;   //!
   TBranch        *b_L0Data_Muon2_Sgn;   //!
   TBranch        *b_L0Data_Muon3_Pt;   //!
   TBranch        *b_L0Data_Muon3_Sgn;   //!
   TBranch        *b_L0Data_PUHits_Mult;   //!
   TBranch        *b_L0Data_PUPeak1_Cont;   //!
   TBranch        *b_L0Data_PUPeak1_Pos;   //!
   TBranch        *b_L0Data_PUPeak2_Cont;   //!
   TBranch        *b_L0Data_PUPeak2_Pos;   //!
   TBranch        *b_L0Data_Photon_Et;   //!
   TBranch        *b_L0Data_Spd_Mult;   //!
   TBranch        *b_L0Data_Sum_Et;   //!
   TBranch        *b_L0Data_Sum_Et_Next1;   //!
   TBranch        *b_L0Data_Sum_Et_Next2;   //!
   TBranch        *b_L0Data_Sum_Et_Prev1;   //!
   TBranch        *b_L0Data_Sum_Et_Prev2;   //!
   TBranch        *b_NumberOfStdLooseAllPhotons;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_StrippingLowMultPP2PPMuMuLineDecision;   //!
   TBranch        *b_StrippingLowMultMuonLineDecision;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0DiMuonDecision;   //!
   TBranch        *b_L0DiMuonNoSPDDecision;   //!
   TBranch        *b_L0MUON_minbiasDecision;   //!
   TBranch        *b_L0MUONDecision;   //!
   TBranch        *b_L0MuonDecision;   //!
   TBranch        *b_L0MuonNoSPDDecision;   //!
   TBranch        *b_L0Muon_lowMultDecision;   //!
   TBranch        *b_L0DiMuon_lowMultDecision;   //!
   TBranch        *b_L0CALODecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1NoPVPassThroughDecision;   //!
   TBranch        *b_Hlt1BBMicroBiasVeloDecision;   //!
   TBranch        *b_Hlt1DiMuonHighMassDecision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_Hlt2LowMultMuonDecision;   //!
   TBranch        *b_Hlt2PassThroughDecision;   //!
   TBranch        *b_Hlt2nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!

   makeMCnewTuple(TTree *tree=0);
   virtual ~makeMCnewTuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef makeMCnewTuple_cxx
makeMCnewTuple::makeMCnewTuple(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("diMuTree/DecayTree",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("diMuTree/DecayTree","diMuTree/DecayTree");
      chain->Add("/media/gustavo/HDGustavo/CEPdata/MC_NonRes_PbPb_2015.root/diMuTree/DecayTree");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

makeMCnewTuple::~makeMCnewTuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t makeMCnewTuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t makeMCnewTuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void makeMCnewTuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("diMu_ENDVERTEX_X", &diMu_ENDVERTEX_X, &b_diMu_ENDVERTEX_X);
   fChain->SetBranchAddress("diMu_ENDVERTEX_Y", &diMu_ENDVERTEX_Y, &b_diMu_ENDVERTEX_Y);
   fChain->SetBranchAddress("diMu_ENDVERTEX_Z", &diMu_ENDVERTEX_Z, &b_diMu_ENDVERTEX_Z);
   fChain->SetBranchAddress("diMu_ENDVERTEX_XERR", &diMu_ENDVERTEX_XERR, &b_diMu_ENDVERTEX_XERR);
   fChain->SetBranchAddress("diMu_ENDVERTEX_YERR", &diMu_ENDVERTEX_YERR, &b_diMu_ENDVERTEX_YERR);
   fChain->SetBranchAddress("diMu_ENDVERTEX_ZERR", &diMu_ENDVERTEX_ZERR, &b_diMu_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("diMu_ENDVERTEX_CHI2", &diMu_ENDVERTEX_CHI2, &b_diMu_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("diMu_ENDVERTEX_NDOF", &diMu_ENDVERTEX_NDOF, &b_diMu_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("diMu_ENDVERTEX_COV_", diMu_ENDVERTEX_COV_, &b_diMu_ENDVERTEX_COV_);
   fChain->SetBranchAddress("diMu_OWNPV_X", &diMu_OWNPV_X, &b_diMu_OWNPV_X);
   fChain->SetBranchAddress("diMu_OWNPV_Y", &diMu_OWNPV_Y, &b_diMu_OWNPV_Y);
   fChain->SetBranchAddress("diMu_OWNPV_Z", &diMu_OWNPV_Z, &b_diMu_OWNPV_Z);
   fChain->SetBranchAddress("diMu_OWNPV_XERR", &diMu_OWNPV_XERR, &b_diMu_OWNPV_XERR);
   fChain->SetBranchAddress("diMu_OWNPV_YERR", &diMu_OWNPV_YERR, &b_diMu_OWNPV_YERR);
   fChain->SetBranchAddress("diMu_OWNPV_ZERR", &diMu_OWNPV_ZERR, &b_diMu_OWNPV_ZERR);
   fChain->SetBranchAddress("diMu_OWNPV_CHI2", &diMu_OWNPV_CHI2, &b_diMu_OWNPV_CHI2);
   fChain->SetBranchAddress("diMu_OWNPV_NDOF", &diMu_OWNPV_NDOF, &b_diMu_OWNPV_NDOF);
   fChain->SetBranchAddress("diMu_OWNPV_COV_", diMu_OWNPV_COV_, &b_diMu_OWNPV_COV_);
   fChain->SetBranchAddress("diMu_IP_OWNPV", &diMu_IP_OWNPV, &b_diMu_IP_OWNPV);
   fChain->SetBranchAddress("diMu_IPCHI2_OWNPV", &diMu_IPCHI2_OWNPV, &b_diMu_IPCHI2_OWNPV);
   fChain->SetBranchAddress("diMu_FD_OWNPV", &diMu_FD_OWNPV, &b_diMu_FD_OWNPV);
   fChain->SetBranchAddress("diMu_FDCHI2_OWNPV", &diMu_FDCHI2_OWNPV, &b_diMu_FDCHI2_OWNPV);
   fChain->SetBranchAddress("diMu_DIRA_OWNPV", &diMu_DIRA_OWNPV, &b_diMu_DIRA_OWNPV);
   fChain->SetBranchAddress("diMu_P", &diMu_P, &b_diMu_P);
   fChain->SetBranchAddress("diMu_PT", &diMu_PT, &b_diMu_PT);
   fChain->SetBranchAddress("diMu_PE", &diMu_PE, &b_diMu_PE);
   fChain->SetBranchAddress("diMu_PX", &diMu_PX, &b_diMu_PX);
   fChain->SetBranchAddress("diMu_PY", &diMu_PY, &b_diMu_PY);
   fChain->SetBranchAddress("diMu_PZ", &diMu_PZ, &b_diMu_PZ);
   fChain->SetBranchAddress("diMu_MM", &diMu_MM, &b_diMu_MM);
   fChain->SetBranchAddress("diMu_MMERR", &diMu_MMERR, &b_diMu_MMERR);
   fChain->SetBranchAddress("diMu_M", &diMu_M, &b_diMu_M);
   fChain->SetBranchAddress("diMu_ID", &diMu_ID, &b_diMu_ID);
   fChain->SetBranchAddress("diMu_L0Global_Dec", &diMu_L0Global_Dec, &b_diMu_L0Global_Dec);
   fChain->SetBranchAddress("diMu_L0Global_TIS", &diMu_L0Global_TIS, &b_diMu_L0Global_TIS);
   fChain->SetBranchAddress("diMu_L0Global_TOS", &diMu_L0Global_TOS, &b_diMu_L0Global_TOS);
   fChain->SetBranchAddress("diMu_Hlt1Global_Dec", &diMu_Hlt1Global_Dec, &b_diMu_Hlt1Global_Dec);
   fChain->SetBranchAddress("diMu_Hlt1Global_TIS", &diMu_Hlt1Global_TIS, &b_diMu_Hlt1Global_TIS);
   fChain->SetBranchAddress("diMu_Hlt1Global_TOS", &diMu_Hlt1Global_TOS, &b_diMu_Hlt1Global_TOS);
   fChain->SetBranchAddress("diMu_Hlt1Phys_Dec", &diMu_Hlt1Phys_Dec, &b_diMu_Hlt1Phys_Dec);
   fChain->SetBranchAddress("diMu_Hlt1Phys_TIS", &diMu_Hlt1Phys_TIS, &b_diMu_Hlt1Phys_TIS);
   fChain->SetBranchAddress("diMu_Hlt1Phys_TOS", &diMu_Hlt1Phys_TOS, &b_diMu_Hlt1Phys_TOS);
   fChain->SetBranchAddress("diMu_Hlt2Global_Dec", &diMu_Hlt2Global_Dec, &b_diMu_Hlt2Global_Dec);
   fChain->SetBranchAddress("diMu_Hlt2Global_TIS", &diMu_Hlt2Global_TIS, &b_diMu_Hlt2Global_TIS);
   fChain->SetBranchAddress("diMu_Hlt2Global_TOS", &diMu_Hlt2Global_TOS, &b_diMu_Hlt2Global_TOS);
   fChain->SetBranchAddress("diMu_Hlt2Phys_Dec", &diMu_Hlt2Phys_Dec, &b_diMu_Hlt2Phys_Dec);
   fChain->SetBranchAddress("diMu_Hlt2Phys_TIS", &diMu_Hlt2Phys_TIS, &b_diMu_Hlt2Phys_TIS);
   fChain->SetBranchAddress("diMu_Hlt2Phys_TOS", &diMu_Hlt2Phys_TOS, &b_diMu_Hlt2Phys_TOS);
   fChain->SetBranchAddress("diMu_L0DiMuonDecision_Dec", &diMu_L0DiMuonDecision_Dec, &b_diMu_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("diMu_L0DiMuonDecision_TIS", &diMu_L0DiMuonDecision_TIS, &b_diMu_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("diMu_L0DiMuonDecision_TOS", &diMu_L0DiMuonDecision_TOS, &b_diMu_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("diMu_L0DiMuonNoSPDDecision_Dec", &diMu_L0DiMuonNoSPDDecision_Dec, &b_diMu_L0DiMuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("diMu_L0DiMuonNoSPDDecision_TIS", &diMu_L0DiMuonNoSPDDecision_TIS, &b_diMu_L0DiMuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("diMu_L0DiMuonNoSPDDecision_TOS", &diMu_L0DiMuonNoSPDDecision_TOS, &b_diMu_L0DiMuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("diMu_L0MUON,minbiasDecision_Dec", &diMu_L0MUON_minbiasDecision_Dec, &b_diMu_L0MUON_minbiasDecision_Dec);
   fChain->SetBranchAddress("diMu_L0MUON,minbiasDecision_TIS", &diMu_L0MUON_minbiasDecision_TIS, &b_diMu_L0MUON_minbiasDecision_TIS);
   fChain->SetBranchAddress("diMu_L0MUON,minbiasDecision_TOS", &diMu_L0MUON_minbiasDecision_TOS, &b_diMu_L0MUON_minbiasDecision_TOS);
   fChain->SetBranchAddress("diMu_L0MUONDecision_Dec", &diMu_L0MUONDecision_Dec, &b_diMu_L0MUONDecision_Dec);
   fChain->SetBranchAddress("diMu_L0MUONDecision_TIS", &diMu_L0MUONDecision_TIS, &b_diMu_L0MUONDecision_TIS);
   fChain->SetBranchAddress("diMu_L0MUONDecision_TOS", &diMu_L0MUONDecision_TOS, &b_diMu_L0MUONDecision_TOS);
   fChain->SetBranchAddress("diMu_L0MuonDecision_Dec", &diMu_L0MuonDecision_Dec, &b_diMu_L0MuonDecision_Dec);
   fChain->SetBranchAddress("diMu_L0MuonDecision_TIS", &diMu_L0MuonDecision_TIS, &b_diMu_L0MuonDecision_TIS);
   fChain->SetBranchAddress("diMu_L0MuonDecision_TOS", &diMu_L0MuonDecision_TOS, &b_diMu_L0MuonDecision_TOS);
   fChain->SetBranchAddress("diMu_L0MuonNoSPDDecision_Dec", &diMu_L0MuonNoSPDDecision_Dec, &b_diMu_L0MuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("diMu_L0MuonNoSPDDecision_TIS", &diMu_L0MuonNoSPDDecision_TIS, &b_diMu_L0MuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("diMu_L0MuonNoSPDDecision_TOS", &diMu_L0MuonNoSPDDecision_TOS, &b_diMu_L0MuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("diMu_L0Muon,lowMultDecision_Dec", &diMu_L0Muon_lowMultDecision_Dec, &b_diMu_L0Muon_lowMultDecision_Dec);
   fChain->SetBranchAddress("diMu_L0Muon,lowMultDecision_TIS", &diMu_L0Muon_lowMultDecision_TIS, &b_diMu_L0Muon_lowMultDecision_TIS);
   fChain->SetBranchAddress("diMu_L0Muon,lowMultDecision_TOS", &diMu_L0Muon_lowMultDecision_TOS, &b_diMu_L0Muon_lowMultDecision_TOS);
   fChain->SetBranchAddress("diMu_L0DiMuon,lowMultDecision_Dec", &diMu_L0DiMuon_lowMultDecision_Dec, &b_diMu_L0DiMuon_lowMultDecision_Dec);
   fChain->SetBranchAddress("diMu_L0DiMuon,lowMultDecision_TIS", &diMu_L0DiMuon_lowMultDecision_TIS, &b_diMu_L0DiMuon_lowMultDecision_TIS);
   fChain->SetBranchAddress("diMu_L0DiMuon,lowMultDecision_TOS", &diMu_L0DiMuon_lowMultDecision_TOS, &b_diMu_L0DiMuon_lowMultDecision_TOS);
   fChain->SetBranchAddress("diMu_L0CALODecision_Dec", &diMu_L0CALODecision_Dec, &b_diMu_L0CALODecision_Dec);
   fChain->SetBranchAddress("diMu_L0CALODecision_TIS", &diMu_L0CALODecision_TIS, &b_diMu_L0CALODecision_TIS);
   fChain->SetBranchAddress("diMu_L0CALODecision_TOS", &diMu_L0CALODecision_TOS, &b_diMu_L0CALODecision_TOS);
   fChain->SetBranchAddress("diMu_L0SPDDecision_Dec", &diMu_L0SPDDecision_Dec, &b_diMu_L0SPDDecision_Dec);
   fChain->SetBranchAddress("diMu_Hlt1NoPVPassThroughDecision_Dec", &diMu_Hlt1NoPVPassThroughDecision_Dec, &b_diMu_Hlt1NoPVPassThroughDecision_Dec);
   fChain->SetBranchAddress("diMu_Hlt1NoPVPassThroughDecision_TIS", &diMu_Hlt1NoPVPassThroughDecision_TIS, &b_diMu_Hlt1NoPVPassThroughDecision_TIS);
   fChain->SetBranchAddress("diMu_Hlt1NoPVPassThroughDecision_TOS", &diMu_Hlt1NoPVPassThroughDecision_TOS, &b_diMu_Hlt1NoPVPassThroughDecision_TOS);
   fChain->SetBranchAddress("diMu_Hlt1BBMicroBiasVeloDecision_Dec", &diMu_Hlt1BBMicroBiasVeloDecision_Dec, &b_diMu_Hlt1BBMicroBiasVeloDecision_Dec);
   fChain->SetBranchAddress("diMu_Hlt1BBMicroBiasVeloDecision_TIS", &diMu_Hlt1BBMicroBiasVeloDecision_TIS, &b_diMu_Hlt1BBMicroBiasVeloDecision_TIS);
   fChain->SetBranchAddress("diMu_Hlt1BBMicroBiasVeloDecision_TOS", &diMu_Hlt1BBMicroBiasVeloDecision_TOS, &b_diMu_Hlt1BBMicroBiasVeloDecision_TOS);
   fChain->SetBranchAddress("diMu_Hlt1DiMuonHighMassDecision_Dec", &diMu_Hlt1DiMuonHighMassDecision_Dec, &b_diMu_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("diMu_Hlt1DiMuonHighMassDecision_TIS", &diMu_Hlt1DiMuonHighMassDecision_TIS, &b_diMu_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("diMu_Hlt1DiMuonHighMassDecision_TOS", &diMu_Hlt1DiMuonHighMassDecision_TOS, &b_diMu_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("diMu_Hlt2LowMultMuonDecision_Dec", &diMu_Hlt2LowMultMuonDecision_Dec, &b_diMu_Hlt2LowMultMuonDecision_Dec);
   fChain->SetBranchAddress("diMu_Hlt2LowMultMuonDecision_TIS", &diMu_Hlt2LowMultMuonDecision_TIS, &b_diMu_Hlt2LowMultMuonDecision_TIS);
   fChain->SetBranchAddress("diMu_Hlt2LowMultMuonDecision_TOS", &diMu_Hlt2LowMultMuonDecision_TOS, &b_diMu_Hlt2LowMultMuonDecision_TOS);
   fChain->SetBranchAddress("diMu_Hlt2PassThroughDecision_Dec", &diMu_Hlt2PassThroughDecision_Dec, &b_diMu_Hlt2PassThroughDecision_Dec);
   fChain->SetBranchAddress("diMu_Hlt2PassThroughDecision_TIS", &diMu_Hlt2PassThroughDecision_TIS, &b_diMu_Hlt2PassThroughDecision_TIS);
   fChain->SetBranchAddress("diMu_Hlt2PassThroughDecision_TOS", &diMu_Hlt2PassThroughDecision_TOS, &b_diMu_Hlt2PassThroughDecision_TOS);
   fChain->SetBranchAddress("muplus_OWNPV_X", &muplus_OWNPV_X, &b_muplus_OWNPV_X);
   fChain->SetBranchAddress("muplus_OWNPV_Y", &muplus_OWNPV_Y, &b_muplus_OWNPV_Y);
   fChain->SetBranchAddress("muplus_OWNPV_Z", &muplus_OWNPV_Z, &b_muplus_OWNPV_Z);
   fChain->SetBranchAddress("muplus_OWNPV_XERR", &muplus_OWNPV_XERR, &b_muplus_OWNPV_XERR);
   fChain->SetBranchAddress("muplus_OWNPV_YERR", &muplus_OWNPV_YERR, &b_muplus_OWNPV_YERR);
   fChain->SetBranchAddress("muplus_OWNPV_ZERR", &muplus_OWNPV_ZERR, &b_muplus_OWNPV_ZERR);
   fChain->SetBranchAddress("muplus_OWNPV_CHI2", &muplus_OWNPV_CHI2, &b_muplus_OWNPV_CHI2);
   fChain->SetBranchAddress("muplus_OWNPV_NDOF", &muplus_OWNPV_NDOF, &b_muplus_OWNPV_NDOF);
   fChain->SetBranchAddress("muplus_OWNPV_COV_", muplus_OWNPV_COV_, &b_muplus_OWNPV_COV_);
   fChain->SetBranchAddress("muplus_IP_OWNPV", &muplus_IP_OWNPV, &b_muplus_IP_OWNPV);
   fChain->SetBranchAddress("muplus_IPCHI2_OWNPV", &muplus_IPCHI2_OWNPV, &b_muplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("muplus_ORIVX_X", &muplus_ORIVX_X, &b_muplus_ORIVX_X);
   fChain->SetBranchAddress("muplus_ORIVX_Y", &muplus_ORIVX_Y, &b_muplus_ORIVX_Y);
   fChain->SetBranchAddress("muplus_ORIVX_Z", &muplus_ORIVX_Z, &b_muplus_ORIVX_Z);
   fChain->SetBranchAddress("muplus_ORIVX_XERR", &muplus_ORIVX_XERR, &b_muplus_ORIVX_XERR);
   fChain->SetBranchAddress("muplus_ORIVX_YERR", &muplus_ORIVX_YERR, &b_muplus_ORIVX_YERR);
   fChain->SetBranchAddress("muplus_ORIVX_ZERR", &muplus_ORIVX_ZERR, &b_muplus_ORIVX_ZERR);
   fChain->SetBranchAddress("muplus_ORIVX_CHI2", &muplus_ORIVX_CHI2, &b_muplus_ORIVX_CHI2);
   fChain->SetBranchAddress("muplus_ORIVX_NDOF", &muplus_ORIVX_NDOF, &b_muplus_ORIVX_NDOF);
   fChain->SetBranchAddress("muplus_ORIVX_COV_", muplus_ORIVX_COV_, &b_muplus_ORIVX_COV_);
   fChain->SetBranchAddress("muplus_P", &muplus_P, &b_muplus_P);
   fChain->SetBranchAddress("muplus_PT", &muplus_PT, &b_muplus_PT);
   fChain->SetBranchAddress("muplus_PE", &muplus_PE, &b_muplus_PE);
   fChain->SetBranchAddress("muplus_PX", &muplus_PX, &b_muplus_PX);
   fChain->SetBranchAddress("muplus_PY", &muplus_PY, &b_muplus_PY);
   fChain->SetBranchAddress("muplus_PZ", &muplus_PZ, &b_muplus_PZ);
   fChain->SetBranchAddress("muplus_M", &muplus_M, &b_muplus_M);
   fChain->SetBranchAddress("muplus_ID", &muplus_ID, &b_muplus_ID);
   fChain->SetBranchAddress("muplus_PIDe", &muplus_PIDe, &b_muplus_PIDe);
   fChain->SetBranchAddress("muplus_PIDmu", &muplus_PIDmu, &b_muplus_PIDmu);
   fChain->SetBranchAddress("muplus_PIDK", &muplus_PIDK, &b_muplus_PIDK);
   fChain->SetBranchAddress("muplus_PIDp", &muplus_PIDp, &b_muplus_PIDp);
   fChain->SetBranchAddress("muplus_ProbNNe", &muplus_ProbNNe, &b_muplus_ProbNNe);
   fChain->SetBranchAddress("muplus_ProbNNk", &muplus_ProbNNk, &b_muplus_ProbNNk);
   fChain->SetBranchAddress("muplus_ProbNNp", &muplus_ProbNNp, &b_muplus_ProbNNp);
   fChain->SetBranchAddress("muplus_ProbNNpi", &muplus_ProbNNpi, &b_muplus_ProbNNpi);
   fChain->SetBranchAddress("muplus_ProbNNmu", &muplus_ProbNNmu, &b_muplus_ProbNNmu);
   fChain->SetBranchAddress("muplus_ProbNNghost", &muplus_ProbNNghost, &b_muplus_ProbNNghost);
   fChain->SetBranchAddress("muplus_hasMuon", &muplus_hasMuon, &b_muplus_hasMuon);
   fChain->SetBranchAddress("muplus_isMuon", &muplus_isMuon, &b_muplus_isMuon);
   fChain->SetBranchAddress("muplus_hasRich", &muplus_hasRich, &b_muplus_hasRich);
   fChain->SetBranchAddress("muplus_UsedRichAerogel", &muplus_UsedRichAerogel, &b_muplus_UsedRichAerogel);
   fChain->SetBranchAddress("muplus_UsedRich1Gas", &muplus_UsedRich1Gas, &b_muplus_UsedRich1Gas);
   fChain->SetBranchAddress("muplus_UsedRich2Gas", &muplus_UsedRich2Gas, &b_muplus_UsedRich2Gas);
   fChain->SetBranchAddress("muplus_RichAboveElThres", &muplus_RichAboveElThres, &b_muplus_RichAboveElThres);
   fChain->SetBranchAddress("muplus_RichAboveMuThres", &muplus_RichAboveMuThres, &b_muplus_RichAboveMuThres);
   fChain->SetBranchAddress("muplus_RichAbovePiThres", &muplus_RichAbovePiThres, &b_muplus_RichAbovePiThres);
   fChain->SetBranchAddress("muplus_RichAboveKaThres", &muplus_RichAboveKaThres, &b_muplus_RichAboveKaThres);
   fChain->SetBranchAddress("muplus_RichAbovePrThres", &muplus_RichAbovePrThres, &b_muplus_RichAbovePrThres);
   fChain->SetBranchAddress("muplus_hasCalo", &muplus_hasCalo, &b_muplus_hasCalo);
   fChain->SetBranchAddress("muplus_PP_InAccMuon", &muplus_PP_InAccMuon, &b_muplus_PP_InAccMuon);
   fChain->SetBranchAddress("muplus_TRACK_Type", &muplus_TRACK_Type, &b_muplus_TRACK_Type);
   fChain->SetBranchAddress("muplus_TRACK_Key", &muplus_TRACK_Key, &b_muplus_TRACK_Key);
   fChain->SetBranchAddress("muplus_TRACK_CHI2NDOF", &muplus_TRACK_CHI2NDOF, &b_muplus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("muplus_TRACK_PCHI2", &muplus_TRACK_PCHI2, &b_muplus_TRACK_PCHI2);
   fChain->SetBranchAddress("muplus_TRACK_MatchCHI2", &muplus_TRACK_MatchCHI2, &b_muplus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("muplus_TRACK_GhostProb", &muplus_TRACK_GhostProb, &b_muplus_TRACK_GhostProb);
   fChain->SetBranchAddress("muplus_TRACK_CloneDist", &muplus_TRACK_CloneDist, &b_muplus_TRACK_CloneDist);
   fChain->SetBranchAddress("muplus_TRACK_Likelihood", &muplus_TRACK_Likelihood, &b_muplus_TRACK_Likelihood);
   fChain->SetBranchAddress("muplus_L0Global_Dec", &muplus_L0Global_Dec, &b_muplus_L0Global_Dec);
   fChain->SetBranchAddress("muplus_L0Global_TIS", &muplus_L0Global_TIS, &b_muplus_L0Global_TIS);
   fChain->SetBranchAddress("muplus_L0Global_TOS", &muplus_L0Global_TOS, &b_muplus_L0Global_TOS);
   fChain->SetBranchAddress("muplus_Hlt1Global_Dec", &muplus_Hlt1Global_Dec, &b_muplus_Hlt1Global_Dec);
   fChain->SetBranchAddress("muplus_Hlt1Global_TIS", &muplus_Hlt1Global_TIS, &b_muplus_Hlt1Global_TIS);
   fChain->SetBranchAddress("muplus_Hlt1Global_TOS", &muplus_Hlt1Global_TOS, &b_muplus_Hlt1Global_TOS);
   fChain->SetBranchAddress("muplus_Hlt1Phys_Dec", &muplus_Hlt1Phys_Dec, &b_muplus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("muplus_Hlt1Phys_TIS", &muplus_Hlt1Phys_TIS, &b_muplus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("muplus_Hlt1Phys_TOS", &muplus_Hlt1Phys_TOS, &b_muplus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("muplus_Hlt2Global_Dec", &muplus_Hlt2Global_Dec, &b_muplus_Hlt2Global_Dec);
   fChain->SetBranchAddress("muplus_Hlt2Global_TIS", &muplus_Hlt2Global_TIS, &b_muplus_Hlt2Global_TIS);
   fChain->SetBranchAddress("muplus_Hlt2Global_TOS", &muplus_Hlt2Global_TOS, &b_muplus_Hlt2Global_TOS);
   fChain->SetBranchAddress("muplus_Hlt2Phys_Dec", &muplus_Hlt2Phys_Dec, &b_muplus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("muplus_Hlt2Phys_TIS", &muplus_Hlt2Phys_TIS, &b_muplus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("muplus_Hlt2Phys_TOS", &muplus_Hlt2Phys_TOS, &b_muplus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("muplus_L0DiMuonDecision_Dec", &muplus_L0DiMuonDecision_Dec, &b_muplus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("muplus_L0DiMuonDecision_TIS", &muplus_L0DiMuonDecision_TIS, &b_muplus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("muplus_L0DiMuonDecision_TOS", &muplus_L0DiMuonDecision_TOS, &b_muplus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("muplus_L0DiMuonNoSPDDecision_Dec", &muplus_L0DiMuonNoSPDDecision_Dec, &b_muplus_L0DiMuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("muplus_L0DiMuonNoSPDDecision_TIS", &muplus_L0DiMuonNoSPDDecision_TIS, &b_muplus_L0DiMuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("muplus_L0DiMuonNoSPDDecision_TOS", &muplus_L0DiMuonNoSPDDecision_TOS, &b_muplus_L0DiMuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("muplus_L0MUON,minbiasDecision_Dec", &muplus_L0MUON_minbiasDecision_Dec, &b_muplus_L0MUON_minbiasDecision_Dec);
   fChain->SetBranchAddress("muplus_L0MUON,minbiasDecision_TIS", &muplus_L0MUON_minbiasDecision_TIS, &b_muplus_L0MUON_minbiasDecision_TIS);
   fChain->SetBranchAddress("muplus_L0MUON,minbiasDecision_TOS", &muplus_L0MUON_minbiasDecision_TOS, &b_muplus_L0MUON_minbiasDecision_TOS);
   fChain->SetBranchAddress("muplus_L0MUONDecision_Dec", &muplus_L0MUONDecision_Dec, &b_muplus_L0MUONDecision_Dec);
   fChain->SetBranchAddress("muplus_L0MUONDecision_TIS", &muplus_L0MUONDecision_TIS, &b_muplus_L0MUONDecision_TIS);
   fChain->SetBranchAddress("muplus_L0MUONDecision_TOS", &muplus_L0MUONDecision_TOS, &b_muplus_L0MUONDecision_TOS);
   fChain->SetBranchAddress("muplus_L0MuonDecision_Dec", &muplus_L0MuonDecision_Dec, &b_muplus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("muplus_L0MuonDecision_TIS", &muplus_L0MuonDecision_TIS, &b_muplus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("muplus_L0MuonDecision_TOS", &muplus_L0MuonDecision_TOS, &b_muplus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("muplus_L0MuonNoSPDDecision_Dec", &muplus_L0MuonNoSPDDecision_Dec, &b_muplus_L0MuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("muplus_L0MuonNoSPDDecision_TIS", &muplus_L0MuonNoSPDDecision_TIS, &b_muplus_L0MuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("muplus_L0MuonNoSPDDecision_TOS", &muplus_L0MuonNoSPDDecision_TOS, &b_muplus_L0MuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("muplus_L0Muon,lowMultDecision_Dec", &muplus_L0Muon_lowMultDecision_Dec, &b_muplus_L0Muon_lowMultDecision_Dec);
   fChain->SetBranchAddress("muplus_L0Muon,lowMultDecision_TIS", &muplus_L0Muon_lowMultDecision_TIS, &b_muplus_L0Muon_lowMultDecision_TIS);
   fChain->SetBranchAddress("muplus_L0Muon,lowMultDecision_TOS", &muplus_L0Muon_lowMultDecision_TOS, &b_muplus_L0Muon_lowMultDecision_TOS);
   fChain->SetBranchAddress("muplus_L0DiMuon,lowMultDecision_Dec", &muplus_L0DiMuon_lowMultDecision_Dec, &b_muplus_L0DiMuon_lowMultDecision_Dec);
   fChain->SetBranchAddress("muplus_L0DiMuon,lowMultDecision_TIS", &muplus_L0DiMuon_lowMultDecision_TIS, &b_muplus_L0DiMuon_lowMultDecision_TIS);
   fChain->SetBranchAddress("muplus_L0DiMuon,lowMultDecision_TOS", &muplus_L0DiMuon_lowMultDecision_TOS, &b_muplus_L0DiMuon_lowMultDecision_TOS);
   fChain->SetBranchAddress("muplus_L0CALODecision_Dec", &muplus_L0CALODecision_Dec, &b_muplus_L0CALODecision_Dec);
   fChain->SetBranchAddress("muplus_L0CALODecision_TIS", &muplus_L0CALODecision_TIS, &b_muplus_L0CALODecision_TIS);
   fChain->SetBranchAddress("muplus_L0CALODecision_TOS", &muplus_L0CALODecision_TOS, &b_muplus_L0CALODecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1NoPVPassThroughDecision_Dec", &muplus_Hlt1NoPVPassThroughDecision_Dec, &b_muplus_Hlt1NoPVPassThroughDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1NoPVPassThroughDecision_TIS", &muplus_Hlt1NoPVPassThroughDecision_TIS, &b_muplus_Hlt1NoPVPassThroughDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1NoPVPassThroughDecision_TOS", &muplus_Hlt1NoPVPassThroughDecision_TOS, &b_muplus_Hlt1NoPVPassThroughDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1BBMicroBiasVeloDecision_Dec", &muplus_Hlt1BBMicroBiasVeloDecision_Dec, &b_muplus_Hlt1BBMicroBiasVeloDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1BBMicroBiasVeloDecision_TIS", &muplus_Hlt1BBMicroBiasVeloDecision_TIS, &b_muplus_Hlt1BBMicroBiasVeloDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1BBMicroBiasVeloDecision_TOS", &muplus_Hlt1BBMicroBiasVeloDecision_TOS, &b_muplus_Hlt1BBMicroBiasVeloDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt1DiMuonHighMassDecision_Dec", &muplus_Hlt1DiMuonHighMassDecision_Dec, &b_muplus_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt1DiMuonHighMassDecision_TIS", &muplus_Hlt1DiMuonHighMassDecision_TIS, &b_muplus_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt1DiMuonHighMassDecision_TOS", &muplus_Hlt1DiMuonHighMassDecision_TOS, &b_muplus_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt2LowMultMuonDecision_Dec", &muplus_Hlt2LowMultMuonDecision_Dec, &b_muplus_Hlt2LowMultMuonDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt2LowMultMuonDecision_TIS", &muplus_Hlt2LowMultMuonDecision_TIS, &b_muplus_Hlt2LowMultMuonDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt2LowMultMuonDecision_TOS", &muplus_Hlt2LowMultMuonDecision_TOS, &b_muplus_Hlt2LowMultMuonDecision_TOS);
   fChain->SetBranchAddress("muplus_Hlt2PassThroughDecision_Dec", &muplus_Hlt2PassThroughDecision_Dec, &b_muplus_Hlt2PassThroughDecision_Dec);
   fChain->SetBranchAddress("muplus_Hlt2PassThroughDecision_TIS", &muplus_Hlt2PassThroughDecision_TIS, &b_muplus_Hlt2PassThroughDecision_TIS);
   fChain->SetBranchAddress("muplus_Hlt2PassThroughDecision_TOS", &muplus_Hlt2PassThroughDecision_TOS, &b_muplus_Hlt2PassThroughDecision_TOS);
   fChain->SetBranchAddress("muminus_OWNPV_X", &muminus_OWNPV_X, &b_muminus_OWNPV_X);
   fChain->SetBranchAddress("muminus_OWNPV_Y", &muminus_OWNPV_Y, &b_muminus_OWNPV_Y);
   fChain->SetBranchAddress("muminus_OWNPV_Z", &muminus_OWNPV_Z, &b_muminus_OWNPV_Z);
   fChain->SetBranchAddress("muminus_OWNPV_XERR", &muminus_OWNPV_XERR, &b_muminus_OWNPV_XERR);
   fChain->SetBranchAddress("muminus_OWNPV_YERR", &muminus_OWNPV_YERR, &b_muminus_OWNPV_YERR);
   fChain->SetBranchAddress("muminus_OWNPV_ZERR", &muminus_OWNPV_ZERR, &b_muminus_OWNPV_ZERR);
   fChain->SetBranchAddress("muminus_OWNPV_CHI2", &muminus_OWNPV_CHI2, &b_muminus_OWNPV_CHI2);
   fChain->SetBranchAddress("muminus_OWNPV_NDOF", &muminus_OWNPV_NDOF, &b_muminus_OWNPV_NDOF);
   fChain->SetBranchAddress("muminus_OWNPV_COV_", muminus_OWNPV_COV_, &b_muminus_OWNPV_COV_);
   fChain->SetBranchAddress("muminus_IP_OWNPV", &muminus_IP_OWNPV, &b_muminus_IP_OWNPV);
   fChain->SetBranchAddress("muminus_IPCHI2_OWNPV", &muminus_IPCHI2_OWNPV, &b_muminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("muminus_ORIVX_X", &muminus_ORIVX_X, &b_muminus_ORIVX_X);
   fChain->SetBranchAddress("muminus_ORIVX_Y", &muminus_ORIVX_Y, &b_muminus_ORIVX_Y);
   fChain->SetBranchAddress("muminus_ORIVX_Z", &muminus_ORIVX_Z, &b_muminus_ORIVX_Z);
   fChain->SetBranchAddress("muminus_ORIVX_XERR", &muminus_ORIVX_XERR, &b_muminus_ORIVX_XERR);
   fChain->SetBranchAddress("muminus_ORIVX_YERR", &muminus_ORIVX_YERR, &b_muminus_ORIVX_YERR);
   fChain->SetBranchAddress("muminus_ORIVX_ZERR", &muminus_ORIVX_ZERR, &b_muminus_ORIVX_ZERR);
   fChain->SetBranchAddress("muminus_ORIVX_CHI2", &muminus_ORIVX_CHI2, &b_muminus_ORIVX_CHI2);
   fChain->SetBranchAddress("muminus_ORIVX_NDOF", &muminus_ORIVX_NDOF, &b_muminus_ORIVX_NDOF);
   fChain->SetBranchAddress("muminus_ORIVX_COV_", muminus_ORIVX_COV_, &b_muminus_ORIVX_COV_);
   fChain->SetBranchAddress("muminus_P", &muminus_P, &b_muminus_P);
   fChain->SetBranchAddress("muminus_PT", &muminus_PT, &b_muminus_PT);
   fChain->SetBranchAddress("muminus_PE", &muminus_PE, &b_muminus_PE);
   fChain->SetBranchAddress("muminus_PX", &muminus_PX, &b_muminus_PX);
   fChain->SetBranchAddress("muminus_PY", &muminus_PY, &b_muminus_PY);
   fChain->SetBranchAddress("muminus_PZ", &muminus_PZ, &b_muminus_PZ);
   fChain->SetBranchAddress("muminus_M", &muminus_M, &b_muminus_M);
   fChain->SetBranchAddress("muminus_ID", &muminus_ID, &b_muminus_ID);
   fChain->SetBranchAddress("muminus_PIDe", &muminus_PIDe, &b_muminus_PIDe);
   fChain->SetBranchAddress("muminus_PIDmu", &muminus_PIDmu, &b_muminus_PIDmu);
   fChain->SetBranchAddress("muminus_PIDK", &muminus_PIDK, &b_muminus_PIDK);
   fChain->SetBranchAddress("muminus_PIDp", &muminus_PIDp, &b_muminus_PIDp);
   fChain->SetBranchAddress("muminus_ProbNNe", &muminus_ProbNNe, &b_muminus_ProbNNe);
   fChain->SetBranchAddress("muminus_ProbNNk", &muminus_ProbNNk, &b_muminus_ProbNNk);
   fChain->SetBranchAddress("muminus_ProbNNp", &muminus_ProbNNp, &b_muminus_ProbNNp);
   fChain->SetBranchAddress("muminus_ProbNNpi", &muminus_ProbNNpi, &b_muminus_ProbNNpi);
   fChain->SetBranchAddress("muminus_ProbNNmu", &muminus_ProbNNmu, &b_muminus_ProbNNmu);
   fChain->SetBranchAddress("muminus_ProbNNghost", &muminus_ProbNNghost, &b_muminus_ProbNNghost);
   fChain->SetBranchAddress("muminus_hasMuon", &muminus_hasMuon, &b_muminus_hasMuon);
   fChain->SetBranchAddress("muminus_isMuon", &muminus_isMuon, &b_muminus_isMuon);
   fChain->SetBranchAddress("muminus_hasRich", &muminus_hasRich, &b_muminus_hasRich);
   fChain->SetBranchAddress("muminus_UsedRichAerogel", &muminus_UsedRichAerogel, &b_muminus_UsedRichAerogel);
   fChain->SetBranchAddress("muminus_UsedRich1Gas", &muminus_UsedRich1Gas, &b_muminus_UsedRich1Gas);
   fChain->SetBranchAddress("muminus_UsedRich2Gas", &muminus_UsedRich2Gas, &b_muminus_UsedRich2Gas);
   fChain->SetBranchAddress("muminus_RichAboveElThres", &muminus_RichAboveElThres, &b_muminus_RichAboveElThres);
   fChain->SetBranchAddress("muminus_RichAboveMuThres", &muminus_RichAboveMuThres, &b_muminus_RichAboveMuThres);
   fChain->SetBranchAddress("muminus_RichAbovePiThres", &muminus_RichAbovePiThres, &b_muminus_RichAbovePiThres);
   fChain->SetBranchAddress("muminus_RichAboveKaThres", &muminus_RichAboveKaThres, &b_muminus_RichAboveKaThres);
   fChain->SetBranchAddress("muminus_RichAbovePrThres", &muminus_RichAbovePrThres, &b_muminus_RichAbovePrThres);
   fChain->SetBranchAddress("muminus_hasCalo", &muminus_hasCalo, &b_muminus_hasCalo);
   fChain->SetBranchAddress("muminus_PP_InAccMuon", &muminus_PP_InAccMuon, &b_muminus_PP_InAccMuon);
   fChain->SetBranchAddress("muminus_TRACK_Type", &muminus_TRACK_Type, &b_muminus_TRACK_Type);
   fChain->SetBranchAddress("muminus_TRACK_Key", &muminus_TRACK_Key, &b_muminus_TRACK_Key);
   fChain->SetBranchAddress("muminus_TRACK_CHI2NDOF", &muminus_TRACK_CHI2NDOF, &b_muminus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("muminus_TRACK_PCHI2", &muminus_TRACK_PCHI2, &b_muminus_TRACK_PCHI2);
   fChain->SetBranchAddress("muminus_TRACK_MatchCHI2", &muminus_TRACK_MatchCHI2, &b_muminus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("muminus_TRACK_GhostProb", &muminus_TRACK_GhostProb, &b_muminus_TRACK_GhostProb);
   fChain->SetBranchAddress("muminus_TRACK_CloneDist", &muminus_TRACK_CloneDist, &b_muminus_TRACK_CloneDist);
   fChain->SetBranchAddress("muminus_TRACK_Likelihood", &muminus_TRACK_Likelihood, &b_muminus_TRACK_Likelihood);
   fChain->SetBranchAddress("muminus_L0Global_Dec", &muminus_L0Global_Dec, &b_muminus_L0Global_Dec);
   fChain->SetBranchAddress("muminus_L0Global_TIS", &muminus_L0Global_TIS, &b_muminus_L0Global_TIS);
   fChain->SetBranchAddress("muminus_L0Global_TOS", &muminus_L0Global_TOS, &b_muminus_L0Global_TOS);
   fChain->SetBranchAddress("muminus_Hlt1Global_Dec", &muminus_Hlt1Global_Dec, &b_muminus_Hlt1Global_Dec);
   fChain->SetBranchAddress("muminus_Hlt1Global_TIS", &muminus_Hlt1Global_TIS, &b_muminus_Hlt1Global_TIS);
   fChain->SetBranchAddress("muminus_Hlt1Global_TOS", &muminus_Hlt1Global_TOS, &b_muminus_Hlt1Global_TOS);
   fChain->SetBranchAddress("muminus_Hlt1Phys_Dec", &muminus_Hlt1Phys_Dec, &b_muminus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("muminus_Hlt1Phys_TIS", &muminus_Hlt1Phys_TIS, &b_muminus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("muminus_Hlt1Phys_TOS", &muminus_Hlt1Phys_TOS, &b_muminus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("muminus_Hlt2Global_Dec", &muminus_Hlt2Global_Dec, &b_muminus_Hlt2Global_Dec);
   fChain->SetBranchAddress("muminus_Hlt2Global_TIS", &muminus_Hlt2Global_TIS, &b_muminus_Hlt2Global_TIS);
   fChain->SetBranchAddress("muminus_Hlt2Global_TOS", &muminus_Hlt2Global_TOS, &b_muminus_Hlt2Global_TOS);
   fChain->SetBranchAddress("muminus_Hlt2Phys_Dec", &muminus_Hlt2Phys_Dec, &b_muminus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("muminus_Hlt2Phys_TIS", &muminus_Hlt2Phys_TIS, &b_muminus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("muminus_Hlt2Phys_TOS", &muminus_Hlt2Phys_TOS, &b_muminus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("muminus_L0DiMuonDecision_Dec", &muminus_L0DiMuonDecision_Dec, &b_muminus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("muminus_L0DiMuonDecision_TIS", &muminus_L0DiMuonDecision_TIS, &b_muminus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("muminus_L0DiMuonDecision_TOS", &muminus_L0DiMuonDecision_TOS, &b_muminus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("muminus_L0DiMuonNoSPDDecision_Dec", &muminus_L0DiMuonNoSPDDecision_Dec, &b_muminus_L0DiMuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("muminus_L0DiMuonNoSPDDecision_TIS", &muminus_L0DiMuonNoSPDDecision_TIS, &b_muminus_L0DiMuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("muminus_L0DiMuonNoSPDDecision_TOS", &muminus_L0DiMuonNoSPDDecision_TOS, &b_muminus_L0DiMuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("muminus_L0MUON,minbiasDecision_Dec", &muminus_L0MUON_minbiasDecision_Dec, &b_muminus_L0MUON_minbiasDecision_Dec);
   fChain->SetBranchAddress("muminus_L0MUON,minbiasDecision_TIS", &muminus_L0MUON_minbiasDecision_TIS, &b_muminus_L0MUON_minbiasDecision_TIS);
   fChain->SetBranchAddress("muminus_L0MUON,minbiasDecision_TOS", &muminus_L0MUON_minbiasDecision_TOS, &b_muminus_L0MUON_minbiasDecision_TOS);
   fChain->SetBranchAddress("muminus_L0MUONDecision_Dec", &muminus_L0MUONDecision_Dec, &b_muminus_L0MUONDecision_Dec);
   fChain->SetBranchAddress("muminus_L0MUONDecision_TIS", &muminus_L0MUONDecision_TIS, &b_muminus_L0MUONDecision_TIS);
   fChain->SetBranchAddress("muminus_L0MUONDecision_TOS", &muminus_L0MUONDecision_TOS, &b_muminus_L0MUONDecision_TOS);
   fChain->SetBranchAddress("muminus_L0MuonDecision_Dec", &muminus_L0MuonDecision_Dec, &b_muminus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("muminus_L0MuonDecision_TIS", &muminus_L0MuonDecision_TIS, &b_muminus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("muminus_L0MuonDecision_TOS", &muminus_L0MuonDecision_TOS, &b_muminus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("muminus_L0MuonNoSPDDecision_Dec", &muminus_L0MuonNoSPDDecision_Dec, &b_muminus_L0MuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("muminus_L0MuonNoSPDDecision_TIS", &muminus_L0MuonNoSPDDecision_TIS, &b_muminus_L0MuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("muminus_L0MuonNoSPDDecision_TOS", &muminus_L0MuonNoSPDDecision_TOS, &b_muminus_L0MuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("muminus_L0Muon,lowMultDecision_Dec", &muminus_L0Muon_lowMultDecision_Dec, &b_muminus_L0Muon_lowMultDecision_Dec);
   fChain->SetBranchAddress("muminus_L0Muon,lowMultDecision_TIS", &muminus_L0Muon_lowMultDecision_TIS, &b_muminus_L0Muon_lowMultDecision_TIS);
   fChain->SetBranchAddress("muminus_L0Muon,lowMultDecision_TOS", &muminus_L0Muon_lowMultDecision_TOS, &b_muminus_L0Muon_lowMultDecision_TOS);
   fChain->SetBranchAddress("muminus_L0DiMuon,lowMultDecision_Dec", &muminus_L0DiMuon_lowMultDecision_Dec, &b_muminus_L0DiMuon_lowMultDecision_Dec);
   fChain->SetBranchAddress("muminus_L0DiMuon,lowMultDecision_TIS", &muminus_L0DiMuon_lowMultDecision_TIS, &b_muminus_L0DiMuon_lowMultDecision_TIS);
   fChain->SetBranchAddress("muminus_L0DiMuon,lowMultDecision_TOS", &muminus_L0DiMuon_lowMultDecision_TOS, &b_muminus_L0DiMuon_lowMultDecision_TOS);
   fChain->SetBranchAddress("muminus_L0CALODecision_Dec", &muminus_L0CALODecision_Dec, &b_muminus_L0CALODecision_Dec);
   fChain->SetBranchAddress("muminus_L0CALODecision_TIS", &muminus_L0CALODecision_TIS, &b_muminus_L0CALODecision_TIS);
   fChain->SetBranchAddress("muminus_L0CALODecision_TOS", &muminus_L0CALODecision_TOS, &b_muminus_L0CALODecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1NoPVPassThroughDecision_Dec", &muminus_Hlt1NoPVPassThroughDecision_Dec, &b_muminus_Hlt1NoPVPassThroughDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1NoPVPassThroughDecision_TIS", &muminus_Hlt1NoPVPassThroughDecision_TIS, &b_muminus_Hlt1NoPVPassThroughDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1NoPVPassThroughDecision_TOS", &muminus_Hlt1NoPVPassThroughDecision_TOS, &b_muminus_Hlt1NoPVPassThroughDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1BBMicroBiasVeloDecision_Dec", &muminus_Hlt1BBMicroBiasVeloDecision_Dec, &b_muminus_Hlt1BBMicroBiasVeloDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1BBMicroBiasVeloDecision_TIS", &muminus_Hlt1BBMicroBiasVeloDecision_TIS, &b_muminus_Hlt1BBMicroBiasVeloDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1BBMicroBiasVeloDecision_TOS", &muminus_Hlt1BBMicroBiasVeloDecision_TOS, &b_muminus_Hlt1BBMicroBiasVeloDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt1DiMuonHighMassDecision_Dec", &muminus_Hlt1DiMuonHighMassDecision_Dec, &b_muminus_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt1DiMuonHighMassDecision_TIS", &muminus_Hlt1DiMuonHighMassDecision_TIS, &b_muminus_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt1DiMuonHighMassDecision_TOS", &muminus_Hlt1DiMuonHighMassDecision_TOS, &b_muminus_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt2LowMultMuonDecision_Dec", &muminus_Hlt2LowMultMuonDecision_Dec, &b_muminus_Hlt2LowMultMuonDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt2LowMultMuonDecision_TIS", &muminus_Hlt2LowMultMuonDecision_TIS, &b_muminus_Hlt2LowMultMuonDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt2LowMultMuonDecision_TOS", &muminus_Hlt2LowMultMuonDecision_TOS, &b_muminus_Hlt2LowMultMuonDecision_TOS);
   fChain->SetBranchAddress("muminus_Hlt2PassThroughDecision_Dec", &muminus_Hlt2PassThroughDecision_Dec, &b_muminus_Hlt2PassThroughDecision_Dec);
   fChain->SetBranchAddress("muminus_Hlt2PassThroughDecision_TIS", &muminus_Hlt2PassThroughDecision_TIS, &b_muminus_Hlt2PassThroughDecision_TIS);
   fChain->SetBranchAddress("muminus_Hlt2PassThroughDecision_TOS", &muminus_Hlt2PassThroughDecision_TOS, &b_muminus_Hlt2PassThroughDecision_TOS);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("L0Data_DiMuon_Pt", &L0Data_DiMuon_Pt, &b_L0Data_DiMuon_Pt);
   fChain->SetBranchAddress("L0Data_DiMuonProd_Pt1Pt2", &L0Data_DiMuonProd_Pt1Pt2, &b_L0Data_DiMuonProd_Pt1Pt2);
   fChain->SetBranchAddress("L0Data_Electron_Et", &L0Data_Electron_Et, &b_L0Data_Electron_Et);
   fChain->SetBranchAddress("L0Data_GlobalPi0_Et", &L0Data_GlobalPi0_Et, &b_L0Data_GlobalPi0_Et);
   fChain->SetBranchAddress("L0Data_Hadron_Et", &L0Data_Hadron_Et, &b_L0Data_Hadron_Et);
   fChain->SetBranchAddress("L0Data_LocalPi0_Et", &L0Data_LocalPi0_Et, &b_L0Data_LocalPi0_Et);
   fChain->SetBranchAddress("L0Data_Muon1_Pt", &L0Data_Muon1_Pt, &b_L0Data_Muon1_Pt);
   fChain->SetBranchAddress("L0Data_Muon1_Sgn", &L0Data_Muon1_Sgn, &b_L0Data_Muon1_Sgn);
   fChain->SetBranchAddress("L0Data_Muon2_Pt", &L0Data_Muon2_Pt, &b_L0Data_Muon2_Pt);
   fChain->SetBranchAddress("L0Data_Muon2_Sgn", &L0Data_Muon2_Sgn, &b_L0Data_Muon2_Sgn);
   fChain->SetBranchAddress("L0Data_Muon3_Pt", &L0Data_Muon3_Pt, &b_L0Data_Muon3_Pt);
   fChain->SetBranchAddress("L0Data_Muon3_Sgn", &L0Data_Muon3_Sgn, &b_L0Data_Muon3_Sgn);
   fChain->SetBranchAddress("L0Data_PUHits_Mult", &L0Data_PUHits_Mult, &b_L0Data_PUHits_Mult);
   fChain->SetBranchAddress("L0Data_PUPeak1_Cont", &L0Data_PUPeak1_Cont, &b_L0Data_PUPeak1_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak1_Pos", &L0Data_PUPeak1_Pos, &b_L0Data_PUPeak1_Pos);
   fChain->SetBranchAddress("L0Data_PUPeak2_Cont", &L0Data_PUPeak2_Cont, &b_L0Data_PUPeak2_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak2_Pos", &L0Data_PUPeak2_Pos, &b_L0Data_PUPeak2_Pos);
   fChain->SetBranchAddress("L0Data_Photon_Et", &L0Data_Photon_Et, &b_L0Data_Photon_Et);
   fChain->SetBranchAddress("L0Data_Spd_Mult", &L0Data_Spd_Mult, &b_L0Data_Spd_Mult);
   fChain->SetBranchAddress("L0Data_Sum_Et", &L0Data_Sum_Et, &b_L0Data_Sum_Et);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next1", &L0Data_Sum_Et_Next1, &b_L0Data_Sum_Et_Next1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next2", &L0Data_Sum_Et_Next2, &b_L0Data_Sum_Et_Next2);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev1", &L0Data_Sum_Et_Prev1, &b_L0Data_Sum_Et_Prev1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev2", &L0Data_Sum_Et_Prev2, &b_L0Data_Sum_Et_Prev2);
   fChain->SetBranchAddress("NumberOfStdLooseAllPhotons", &NumberOfStdLooseAllPhotons, &b_NumberOfStdLooseAllPhotons);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("StrippingLowMultPP2PPMuMuLineDecision", &StrippingLowMultPP2PPMuMuLineDecision, &b_StrippingLowMultPP2PPMuMuLineDecision);
   fChain->SetBranchAddress("StrippingLowMultMuonLineDecision", &StrippingLowMultMuonLineDecision, &b_StrippingLowMultMuonLineDecision);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0DiMuonDecision", &L0DiMuonDecision, &b_L0DiMuonDecision);
   fChain->SetBranchAddress("L0DiMuonNoSPDDecision", &L0DiMuonNoSPDDecision, &b_L0DiMuonNoSPDDecision);
   fChain->SetBranchAddress("L0MUON,minbiasDecision", &L0MUON_minbiasDecision, &b_L0MUON_minbiasDecision);
   fChain->SetBranchAddress("L0MUONDecision", &L0MUONDecision, &b_L0MUONDecision);
   fChain->SetBranchAddress("L0MuonDecision", &L0MuonDecision, &b_L0MuonDecision);
   fChain->SetBranchAddress("L0MuonNoSPDDecision", &L0MuonNoSPDDecision, &b_L0MuonNoSPDDecision);
   fChain->SetBranchAddress("L0Muon,lowMultDecision", &L0Muon_lowMultDecision, &b_L0Muon_lowMultDecision);
   fChain->SetBranchAddress("L0DiMuon,lowMultDecision", &L0DiMuon_lowMultDecision, &b_L0DiMuon_lowMultDecision);
   fChain->SetBranchAddress("L0CALODecision", &L0CALODecision, &b_L0CALODecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1NoPVPassThroughDecision", &Hlt1NoPVPassThroughDecision, &b_Hlt1NoPVPassThroughDecision);
   fChain->SetBranchAddress("Hlt1BBMicroBiasVeloDecision", &Hlt1BBMicroBiasVeloDecision, &b_Hlt1BBMicroBiasVeloDecision);
   fChain->SetBranchAddress("Hlt1DiMuonHighMassDecision", &Hlt1DiMuonHighMassDecision, &b_Hlt1DiMuonHighMassDecision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("Hlt2LowMultMuonDecision", &Hlt2LowMultMuonDecision, &b_Hlt2LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt2PassThroughDecision", &Hlt2PassThroughDecision, &b_Hlt2PassThroughDecision);
   fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   Notify();
}

Bool_t makeMCnewTuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void makeMCnewTuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t makeMCnewTuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef makeMCnewTuple_cxx
