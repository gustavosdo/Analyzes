import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *

# function to obtain the name of variables:
def namestr(obj, namespace):
    return [name for name in namespace if namespace[name] is obj]

# Importing selection strings
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerLHCbSelections import * #lstMinbiasTriggerLHCb
from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb
from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# Data samples
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCbDataSample
MC_diMu_PbPb_2015 = TChain('',''); MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
MC_diMu_PbPb_2015Pre = TChain('',''); MC_diMu_PbPb_2015Pre.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars_prescaled.root/DecayTree') # MC Data Sample (prescaled)
MC_Jpsi_PbPb_2015_Coh = TChain('',''); MC_Jpsi_PbPb_2015_Coh.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_Jpsi_PbPb_Coherent_2015_withNewVars.root/DecayTree')
MC_Jpsi_PbPb_2015_Inc = TChain('',''); MC_Jpsi_PbPb_2015_Inc.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_Jpsi_PbPb_Incoherent_2015_withNewVars.root/DecayTree')
MC_Jpsi_PbPb_2015_Psi = TChain('',''); MC_Jpsi_PbPb_2015_Psi.Add('/home/gustavo/LHCb_Psi2S_PbPb_2015_withNewVars_SPD_LongTr_muEta_muPT_diMuPT_muIsMu.root/DecayTree')

####################################################
# New samples
LHCb_NominalNonResonant                         = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_NominalNonResonant.root", "recreate")
LHCb_NominalNonResonantDeltaPhi                 = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_NominalNonResonantDeltaPhi.root", "recreate")
LHCb_NominalNonResonantHerschel                 = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_NominalNonResonantHerschel.root", "recreate")
LHCb_NominalNonResonantHerschelDeltaPhi         = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_NominalNonResonantHerschelDeltaPhi.root", "recreate")
LHCb_NominalNonResonantInvertedHerschel         = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_NominalNonResonantInvertedHerschel.root", "recreate")
LHCb_NominalNonResonantInvertedHerschelDeltaPhi = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_NominalNonResonantInvertedHerschelDeltaPhi.root", "recreate")
MC_NominalNonResonant                           = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NominalNonResonant.root", "recreate")
MC_NominalNonResonantDeltaPhi                   = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NominalNonResonantDeltaPhi.root", "recreate")
MCprescaled_NominalNonResonant                  = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MCprescaled_NominalNonResonant.root", "recreate")
MCprescaled_NominalNonResonantDeltaPhi          = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MCprescaled_NominalNonResonantDeltaPhi.root", "recreate")
LHCb_MinbiasNonResonant                         = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_MinbiasNonResonant.root", "recreate")
LHCb_MinbiasNonResonantDeltaPhi                 = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_MinbiasNonResonantDeltaPhi.root", "recreate")
LHCb_MinbiasNonResonantHerschel                 = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_MinbiasNonResonantHerschel.root", "recreate")
LHCb_MinbiasNonResonantHerschelDeltaPhi         = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_MinbiasNonResonantHerschelDeltaPhi.root", "recreate")
LHCb_MinbiasNonResonantInvertedHerschel         = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_MinbiasNonResonantInvertedHerschel.root", "recreate")
LHCb_MinbiasNonResonantInvertedHerschelDeltaPhi = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_MinbiasNonResonantInvertedHerschelDeltaPhi.root", "recreate")
MC_MinbiasNonResonant                           = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_MinbiasNonResonant.root", "recreate")
MC_MinbiasNonResonantDeltaPhi                   = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_MinbiasNonResonantDeltaPhi.root", "recreate")
MCprescaled_MinbiasNonResonant                  = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MCprescaled_MinbiasNonResonant.root", "recreate")
MCprescaled_MinbiasNonResonantDeltaPhi          = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MCprescaled_MinbiasNonResonantDeltaPhi.root", "recreate")
#
LHCb_JpsiFullSelection		= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_JpsiFullSelection.root", "recreate") 
LHCb_JpsiFullSelDeltaPhi	= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_JpsiFullSelDeltaPhi.root", "recreate")
MC_JpsiCohFullSelection		= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_JpsiCohFullSelection.root", "recreate")
MC_JpsiCohFullSelDeltaPhi	= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_JpsiCohFullSelDeltaPhi.root", "recreate")
MC_JpsiIncFullSelection		= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_JpsiIncFullSelection.root", "recreate")
MC_JpsiIncFullSelDeltaPhi	= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_JpsiIncFullSelDeltaPhi.root", "recreate")
MC_PsiFullSelection			= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_PsiFullSelection.root", "recreate")
MC_PsiFullSelDeltaPhi		= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_PsiFullSelDeltaPhi.root", "recreate")
MC_NonResFullSelection		= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NonResFullSelection.root", "recreate")
MC_NonResFullSelDeltaPhi	= TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NonResFullSelDeltaPhi.root", "recreate")
#
LHCb_JpsiFullSelDPhiNoMass = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_JpsiFullSelDPhiNoMass.root", "recreate")
MC_NonResFullSelDPhiNoMass = TFile("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NonResFullSelDPhiNoMass.root", "recreate")
# list with all new files
# A CHANGE HERE MUST BE DONE ON THE LIST BELOW TOO !
newFiles = [\
LHCb_NominalNonResonant,\
LHCb_NominalNonResonantDeltaPhi,\
LHCb_NominalNonResonantHerschel,\
LHCb_NominalNonResonantHerschelDeltaPhi,\
LHCb_NominalNonResonantInvertedHerschel,\
LHCb_NominalNonResonantInvertedHerschelDeltaPhi,\
MC_NominalNonResonant,\
MC_NominalNonResonantDeltaPhi,\
MCprescaled_NominalNonResonant,\
MCprescaled_NominalNonResonantDeltaPhi,\
LHCb_MinbiasNonResonant,\
LHCb_MinbiasNonResonantDeltaPhi,\
LHCb_MinbiasNonResonantHerschel,\
LHCb_MinbiasNonResonantHerschelDeltaPhi,\
LHCb_MinbiasNonResonantInvertedHerschel,\
LHCb_MinbiasNonResonantInvertedHerschelDeltaPhi,\
MC_MinbiasNonResonant,\
MC_MinbiasNonResonantDeltaPhi,\
MCprescaled_MinbiasNonResonant,\
MCprescaled_MinbiasNonResonantDeltaPhi,\
\
LHCb_JpsiFullSelection,\
LHCb_JpsiFullSelDeltaPhi,\
MC_JpsiCohFullSelection,\
MC_JpsiCohFullSelDeltaPhi,\
MC_JpsiIncFullSelection,\
MC_JpsiIncFullSelDeltaPhi,\
MC_PsiFullSelection,\
MC_PsiFullSelDeltaPhi,\
MC_NonResFullSelection,\
MC_NonResFullSelDeltaPhi,\
\
LHCb_JpsiFullSelDPhiNoMass,\
MC_NonResFullSelDPhiNoMass]
# THIS LIST IS DEEPLY CONNECTED TO THE LIST ABOVE
cutsList = [\
cutNonResonant,\
cutNonResPhi,\
cutNonResHRC,\
cutNonResHRCPhi,\
cutNonResInvHRC,\
cutNonResInvHRCPhi,\
cutMCNonResonant,\
cutMCNonResPhi,\
cutMCNonResonant,\
cutMCNonResPhi,\
cutMBNonResonant,\
cutMBNonResPhi,\
cutMBNonResHRC,\
cutMBNonResHRCPhi,\
cutMBNonResInvHRC,\
cutMBNonResInvHRCPhi,\
cutMCMBNonResonant,\
cutMCMBNonResPhi,\
cutMCMBNonResonant,\
cutMCMBNonResPhi,\
cutFullSelHRC,\
cutFullSelHRCPhi,\
cutMCFullSelNoTrigger,\
cutMCFullSelNoTrigPhi,\
cutMCFullSelNoTrigger,\
cutMCFullSelNoTrigPhi,\
cutMCFullSelNoTrigger,\
cutMCFullSelNoTrigPhi,\
cutMCFullSelNoTrigger,\
cutMCFullSelNoTrigPhi,\
\
cutFullSelNoMassHRCdeltaPhi,\
cutMCFullSelNoMassHRCdeltaPhi]
# THIS LIST IS DEEPLY CONNECTED TO THE LIST ABOVE
typeList = [\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'MC',\
'MC',\
'MCpre',\
'MCpre',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'MC',\
'MC',\
'MCpre',\
'MCpre',\
'LHCbJpsi',\
'LHCbJpsi',\
'MCcoh',\
'MCcoh',\
'MCinc',\
'MCinc',\
'MCpsi',\
'MCpsi',\
'MC',\
'MC',\
'LHCb',\
'MC']
##################################################

for i in range(0,32):
	fileOut = newFiles[i]
	applyCut = cutsList[i]
	dataType = typeList[i]
	fileOut.cd()
	if dataType == 'LHCbJpsi':
		print('LHCb')
		treeOut = LHCb_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MC':
		print('MC')
		treeOut = MC_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCpre':
		print('prescaled')
		treeOut = MC_diMu_PbPb_2015Pre.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'LHCb':
		print('LHCb')
		treeOut = LHCb_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCcoh':
		treeOut = MC_Jpsi_PbPb_2015_Coh.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCinc':
		treeOut = MC_Jpsi_PbPb_2015_Inc.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCpsi':
		treeOut = MC_Jpsi_PbPb_2015_Psi.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
