from ROOT import TFile, TTree, TDirectory
runNumbers = "!((runNumber > 169024) && (runNumber < 169091))" # not bad lumi runs
L0       = "(J_psi_1S_L0MUONDecision_TOS)" # L0 line muon_pt gt(greater than) 900 mev
Hlt1     = "J_psi_1S_Hlt1BBMicroBiasVeloDecision_Dec"
SPD      = "nSPDHits < 20" # CEP-like event
LongTr   = "nLongTracks == 2" # muons tracks
muEta    = "muplus_eta > 2 && muplus_eta < 4.5 && muminus_eta > 2 && muminus_eta < 4.5" # lhcb acceptance
muPT     = "muplus_PT > 500 && muminus_PT > 500" # both muons pt gt 500 mev 
lowMass  = "J_psi_1S_M < 2700" # nonresonant mass
HERSCHEL = "log_hrc_fom_v2 > 7" # enrich NonResonant contribution
# * * * * * #
lstCutNonRes   = [runNumbers, L0, Hlt1, SPD, LongTr, muEta, muPT, lowMass, HERSCHEL ] # no HLT1, low mass, low pt
# Joining cuts and saving in strings
NonRes  = " && ".join([x for x in lstCutNonRes])

fileIn = TFile("diMuonTuple_newVars.root", "read")
fileOut = TFile("InvertedHRC.root", "recreate")

#dirIn = fileIn.Get("diMuonTuple")
treeIn = fileIn.Get("DecayTree")
treeOut = treeIn.CopyTree(NonRes)

fileOut.cd()
treeOut.Write()

fileIn.Close()
fileOut.Close()
