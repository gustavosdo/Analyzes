from ROOT import TFile, TTree, TDirectory

fileIn = TFile("/media/gustavo/HDGustavo/CEPdata/PbPbJpsi2015.root", "read")
fileOut = TFile("LHCb_diMu_PbPb_2015.root", "recreate")

dirIn = fileIn.Get("diMuonTuple")
treeIn = dirIn.Get("DecayTree")
treeOut = treeIn.CloneTree()

fileOut.cd()
treeOut.Write()

fileIn.Close()
fileOut.Close()
