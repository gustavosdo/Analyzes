import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *

# Defining selection strings
bin1 = "( (J_psi_1S_Y >= 2.0) && (J_psi_1S_Y < 2.5) )"
bin2 = "( (J_psi_1S_Y >= 2.5) && (J_psi_1S_Y < 3.0) )"
bin3 = "( (J_psi_1S_Y >= 3.0) && (J_psi_1S_Y < 3.5) )"
bin4 = "( (J_psi_1S_Y >= 3.5) && (J_psi_1S_Y < 4.0) )"
bin5 = "( (J_psi_1S_Y >= 4.0) && (J_psi_1S_Y <= 4.5) )"

# Data samples
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_JpsiFullSelDPhiNoMass.root/DecayTree') # LHCbDataSample
MC_diMu_PbPb_2015 = TChain('',''); MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NonResFullSelDPhiNoMass.root/DecayTree') # MC Data Sample

####################################################
# New samples
folder = "/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/"
LHCb_JpsiFullSelDphiNoMass_bin1 = TFile(folder+"LHCb_JpsiFullSelDphiNoMass_bin1.root", "recreate")
LHCb_JpsiFullSelDphiNoMass_bin2 = TFile(folder+"LHCb_JpsiFullSelDphiNoMass_bin2.root", "recreate")
LHCb_JpsiFullSelDphiNoMass_bin3 = TFile(folder+"LHCb_JpsiFullSelDphiNoMass_bin3.root", "recreate")
LHCb_JpsiFullSelDphiNoMass_bin4 = TFile(folder+"LHCb_JpsiFullSelDphiNoMass_bin4.root", "recreate")
LHCb_JpsiFullSelDphiNoMass_bin5 = TFile(folder+"LHCb_JpsiFullSelDphiNoMass_bin5.root", "recreate")
MC_NonResFullSelDphiNoMass_bin1 = TFile(folder+"MC_NonResFullSelDphiNoMass_bin1.root", "recreate")
MC_NonResFullSelDphiNoMass_bin2 = TFile(folder+"MC_NonResFullSelDphiNoMass_bin2.root", "recreate")
MC_NonResFullSelDphiNoMass_bin3 = TFile(folder+"MC_NonResFullSelDphiNoMass_bin3.root", "recreate")
MC_NonResFullSelDphiNoMass_bin4 = TFile(folder+"MC_NonResFullSelDphiNoMass_bin4.root", "recreate")
MC_NonResFullSelDphiNoMass_bin5 = TFile(folder+"MC_NonResFullSelDphiNoMass_bin5.root", "recreate")

# list with all new files
# A CHANGE HERE MUST BE DONE ON THE LIST BELOW TOO !
newFiles = [\
LHCb_JpsiFullSelDphiNoMass_bin1,\
LHCb_JpsiFullSelDphiNoMass_bin2,\
LHCb_JpsiFullSelDphiNoMass_bin3,\
LHCb_JpsiFullSelDphiNoMass_bin4,\
LHCb_JpsiFullSelDphiNoMass_bin5,\
MC_NonResFullSelDphiNoMass_bin1,\
MC_NonResFullSelDphiNoMass_bin2,\
MC_NonResFullSelDphiNoMass_bin3,\
MC_NonResFullSelDphiNoMass_bin4,\
MC_NonResFullSelDphiNoMass_bin5]
# List of cuts to be applied in each sample
cutsList = [\
bin1,\
bin2,\
bin3,\
bin4,\
bin5,\
bin1,\
bin2,\
bin3,\
bin4,\
bin5]
# THIS LIST IS DEEPLY CONNECTED TO THE LIST ABOVE
typeList = [\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'MC',\
'MC',\
'MC',\
'MC',\
'MC']
##################################################

for i in range(0,10):
	fileOut = newFiles[i]
	applyCut = cutsList[i]
	dataType = typeList[i]
	fileOut.cd()
	if dataType == 'LHCb':
		print('LHCb')
		treeOut = LHCb_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MC':
		print('MC')
		treeOut = MC_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
