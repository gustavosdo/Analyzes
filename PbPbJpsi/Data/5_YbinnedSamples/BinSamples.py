import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *

# Defining selection strings
bin1 = "( (J_psi_1S_Y >= 2.0) && (J_psi_1S_Y < 2.5) )"
bin2 = "( (J_psi_1S_Y >= 2.5) && (J_psi_1S_Y < 3.0) )"
bin3 = "( (J_psi_1S_Y >= 3.0) && (J_psi_1S_Y < 3.5) )"
bin4 = "( (J_psi_1S_Y >= 3.5) && (J_psi_1S_Y < 4.0) )"
bin5 = "( (J_psi_1S_Y >= 4.0) && (J_psi_1S_Y <= 4.5) )"

# Data samples
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_JpsiFullSelDeltaPhi.root/DecayTree') # LHCbDataSample
MC_diMu_PbPb_2015 = TChain('',''); MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_NonResFullSelDeltaPhi.root/DecayTree') # MC Data Sample
MC_JpsiCoh_PbPb_2015 = TChain('',''); MC_JpsiCoh_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_JpsiCohFullSelDeltaPhi.root/DecayTree') # MC Data Sample
MC_JpsiInc_PbPb_2015 = TChain('',''); MC_JpsiInc_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_JpsiIncFullSelDeltaPhi.root/DecayTree') # MC Data Sample
MC_Psi2S_PbPb_2015 = TChain('',''); MC_Psi2S_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/MC_PsiFullSelDeltaPhi.root/DecayTree') # MC Data Sample

####################################################
# New samples
folder = "/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/"
LHCb_JpsiFullSelDphi_bin1 = TFile(folder+"LHCb_JpsiFullSelDphi_bin1.root", "recreate")
LHCb_JpsiFullSelDphi_bin2 = TFile(folder+"LHCb_JpsiFullSelDphi_bin2.root", "recreate")
LHCb_JpsiFullSelDphi_bin3 = TFile(folder+"LHCb_JpsiFullSelDphi_bin3.root", "recreate")
LHCb_JpsiFullSelDphi_bin4 = TFile(folder+"LHCb_JpsiFullSelDphi_bin4.root", "recreate")
LHCb_JpsiFullSelDphi_bin5 = TFile(folder+"LHCb_JpsiFullSelDphi_bin5.root", "recreate")
MC_NonResFullSelDphi_bin1 = TFile(folder+"MC_NonResFullSelDphi_bin1.root", "recreate")
MC_NonResFullSelDphi_bin2 = TFile(folder+"MC_NonResFullSelDphi_bin2.root", "recreate")
MC_NonResFullSelDphi_bin3 = TFile(folder+"MC_NonResFullSelDphi_bin3.root", "recreate")
MC_NonResFullSelDphi_bin4 = TFile(folder+"MC_NonResFullSelDphi_bin4.root", "recreate")
MC_NonResFullSelDphi_bin5 = TFile(folder+"MC_NonResFullSelDphi_bin5.root", "recreate")
MC_JpsiCohFullSelDphi_bin1 = TFile(folder+"MC_JpsiCohFullSelDphi_bin1.root", "recreate")
MC_JpsiCohFullSelDphi_bin2 = TFile(folder+"MC_JpsiCohFullSelDphi_bin2.root", "recreate")
MC_JpsiCohFullSelDphi_bin3 = TFile(folder+"MC_JpsiCohFullSelDphi_bin3.root", "recreate")
MC_JpsiCohFullSelDphi_bin4 = TFile(folder+"MC_JpsiCohFullSelDphi_bin4.root", "recreate")
MC_JpsiCohFullSelDphi_bin5 = TFile(folder+"MC_JpsiCohFullSelDphi_bin5.root", "recreate")
MC_JpsiIncFullSelDphi_bin1 = TFile(folder+"MC_JpsiIncFullSelDphi_bin1.root", "recreate")
MC_JpsiIncFullSelDphi_bin2 = TFile(folder+"MC_JpsiIncFullSelDphi_bin2.root", "recreate")
MC_JpsiIncFullSelDphi_bin3 = TFile(folder+"MC_JpsiIncFullSelDphi_bin3.root", "recreate")
MC_JpsiIncFullSelDphi_bin4 = TFile(folder+"MC_JpsiIncFullSelDphi_bin4.root", "recreate")
MC_JpsiIncFullSelDphi_bin5 = TFile(folder+"MC_JpsiIncFullSelDphi_bin5.root", "recreate")
MC_Psi2SFullSelDphi_bin1 = TFile(folder+"MC_Psi2SFullSelDphi_bin1.root", "recreate")
MC_Psi2SFullSelDphi_bin2 = TFile(folder+"MC_Psi2SFullSelDphi_bin2.root", "recreate")
MC_Psi2SFullSelDphi_bin3 = TFile(folder+"MC_Psi2SFullSelDphi_bin3.root", "recreate")
MC_Psi2SFullSelDphi_bin4 = TFile(folder+"MC_Psi2SFullSelDphi_bin4.root", "recreate")
MC_Psi2SFullSelDphi_bin5 = TFile(folder+"MC_Psi2SFullSelDphi_bin5.root", "recreate")

# list with all new files
# A CHANGE HERE MUST BE DONE ON THE LIST BELOW TOO !
newFiles = [\
LHCb_JpsiFullSelDphi_bin1,\
LHCb_JpsiFullSelDphi_bin2,\
LHCb_JpsiFullSelDphi_bin3,\
LHCb_JpsiFullSelDphi_bin4,\
LHCb_JpsiFullSelDphi_bin5,\
MC_NonResFullSelDphi_bin1,\
MC_NonResFullSelDphi_bin2,\
MC_NonResFullSelDphi_bin3,\
MC_NonResFullSelDphi_bin4,\
MC_NonResFullSelDphi_bin5,\
MC_JpsiCohFullSelDphi_bin1,\
MC_JpsiCohFullSelDphi_bin2,\
MC_JpsiCohFullSelDphi_bin3,\
MC_JpsiCohFullSelDphi_bin4,\
MC_JpsiCohFullSelDphi_bin5,\
MC_JpsiIncFullSelDphi_bin1,\
MC_JpsiIncFullSelDphi_bin2,\
MC_JpsiIncFullSelDphi_bin3,\
MC_JpsiIncFullSelDphi_bin4,\
MC_JpsiIncFullSelDphi_bin5,\
MC_Psi2SFullSelDphi_bin1,\
MC_Psi2SFullSelDphi_bin2,\
MC_Psi2SFullSelDphi_bin3,\
MC_Psi2SFullSelDphi_bin4,\
MC_Psi2SFullSelDphi_bin5]

# List of cuts to be applied in each sample
cutsList = [\
bin1,\
bin2,\
bin3,\
bin4,\
bin5,\
bin1,\
bin2,\
bin3,\
bin4,\
bin5,\
bin1,\
bin2,\
bin3,\
bin4,\
bin5,\
bin1,\
bin2,\
bin3,\
bin4,\
bin5,\
bin1,\
bin2,\
bin3,\
bin4,\
bin5]
# THIS LIST IS DEEPLY CONNECTED TO THE LIST ABOVE
typeList = [\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'LHCb',\
'MC',\
'MC',\
'MC',\
'MC',\
'MC',\
'MCCoh',\
'MCCoh',\
'MCCoh',\
'MCCoh',\
'MCCoh',\
'MCInc',\
'MCInc',\
'MCInc',\
'MCInc',\
'MCInc',\
'MCPsi',\
'MCPsi',\
'MCPsi',\
'MCPsi',\
'MCPsi']
##################################################

for i in range(0,25):
	fileOut = newFiles[i]
	applyCut = cutsList[i]
	dataType = typeList[i]
	fileOut.cd()
	if dataType == 'LHCb':
		print('LHCb')
		treeOut = LHCb_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MC':
		print('MC')
		treeOut = MC_diMu_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCCoh':
		print('MCCoh')
		treeOut = MC_JpsiCoh_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCInc':
		print('MCInc')
		treeOut = MC_JpsiInc_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
	elif dataType == 'MCPsi':
		print('MCPsi')
		treeOut = MC_Psi2S_PbPb_2015.CopyTree(applyCut)
		treeOut.Write()
		fileOut.Close()
		del fileOut
		continue
