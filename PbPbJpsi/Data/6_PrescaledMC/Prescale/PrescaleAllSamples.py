import ROOT, math, os
from ROOT import *
from math import *

# Maximum number of events
Nentries = 10000

# # # # # # ORIGINAL SAMPLES # # # # # #
originalSamples = []
PrunedSamplesFolder = '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/'
BinnedSamplesFolder = '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/'
MC_JpsiCohFullSelDeltaPhi       = TChain('',''); MC_JpsiCohFullSelDeltaPhi.Add(PrunedSamplesFolder+'MC_JpsiCohFullSelDeltaPhi.root/DecayTree')             ; originalSamples.append( MC_JpsiCohFullSelDeltaPhi      ) 
MC_JpsiIncFullSelDeltaPhi       = TChain('',''); MC_JpsiIncFullSelDeltaPhi.Add(PrunedSamplesFolder+'MC_JpsiIncFullSelDeltaPhi.root/DecayTree')             ; originalSamples.append( MC_JpsiIncFullSelDeltaPhi      ) 
MC_NonResFullSelDeltaPhi        = TChain('',''); MC_NonResFullSelDeltaPhi.Add(PrunedSamplesFolder+'MC_NonResFullSelDeltaPhi.root/DecayTree')               ; originalSamples.append( MC_NonResFullSelDeltaPhi       ) 
MC_NonResFullSelDPhiNoMass      = TChain('',''); MC_NonResFullSelDPhiNoMass.Add(PrunedSamplesFolder+'MC_NonResFullSelDPhiNoMass.root/DecayTree')           ; originalSamples.append( MC_NonResFullSelDPhiNoMass     ) 
MC_PsiFullSelDeltaPhi           = TChain('',''); MC_PsiFullSelDeltaPhi.Add(PrunedSamplesFolder+'MC_PsiFullSelDeltaPhi.root/DecayTree')                     ; originalSamples.append( MC_PsiFullSelDeltaPhi          ) 
MC_JpsiCohFullSelDphi_bin1      = TChain('',''); MC_JpsiCohFullSelDphi_bin1.Add(BinnedSamplesFolder+'MC_JpsiCohFullSelDphi_bin1.root/DecayTree')           ; originalSamples.append( MC_JpsiCohFullSelDphi_bin1     ) 
MC_JpsiCohFullSelDphi_bin2      = TChain('',''); MC_JpsiCohFullSelDphi_bin2.Add(BinnedSamplesFolder+'MC_JpsiCohFullSelDphi_bin2.root/DecayTree')           ; originalSamples.append( MC_JpsiCohFullSelDphi_bin2     ) 
MC_JpsiCohFullSelDphi_bin3      = TChain('',''); MC_JpsiCohFullSelDphi_bin3.Add(BinnedSamplesFolder+'MC_JpsiCohFullSelDphi_bin3.root/DecayTree')           ; originalSamples.append( MC_JpsiCohFullSelDphi_bin3     ) 
MC_JpsiCohFullSelDphi_bin4      = TChain('',''); MC_JpsiCohFullSelDphi_bin4.Add(BinnedSamplesFolder+'MC_JpsiCohFullSelDphi_bin4.root/DecayTree')           ; originalSamples.append( MC_JpsiCohFullSelDphi_bin4     ) 
MC_JpsiCohFullSelDphi_bin5      = TChain('',''); MC_JpsiCohFullSelDphi_bin5.Add(BinnedSamplesFolder+'MC_JpsiCohFullSelDphi_bin5.root/DecayTree')           ; originalSamples.append( MC_JpsiCohFullSelDphi_bin5     ) 
MC_JpsiIncFullSelDphi_bin1      = TChain('',''); MC_JpsiIncFullSelDphi_bin1.Add(BinnedSamplesFolder+'MC_JpsiIncFullSelDphi_bin1.root/DecayTree')           ; originalSamples.append( MC_JpsiIncFullSelDphi_bin1     ) 
MC_JpsiIncFullSelDphi_bin2      = TChain('',''); MC_JpsiIncFullSelDphi_bin2.Add(BinnedSamplesFolder+'MC_JpsiIncFullSelDphi_bin2.root/DecayTree')           ; originalSamples.append( MC_JpsiIncFullSelDphi_bin2     ) 
MC_JpsiIncFullSelDphi_bin3      = TChain('',''); MC_JpsiIncFullSelDphi_bin3.Add(BinnedSamplesFolder+'MC_JpsiIncFullSelDphi_bin3.root/DecayTree')           ; originalSamples.append( MC_JpsiIncFullSelDphi_bin3     ) 
MC_JpsiIncFullSelDphi_bin4      = TChain('',''); MC_JpsiIncFullSelDphi_bin4.Add(BinnedSamplesFolder+'MC_JpsiIncFullSelDphi_bin4.root/DecayTree')           ; originalSamples.append( MC_JpsiIncFullSelDphi_bin4     ) 
MC_JpsiIncFullSelDphi_bin5      = TChain('',''); MC_JpsiIncFullSelDphi_bin5.Add(BinnedSamplesFolder+'MC_JpsiIncFullSelDphi_bin5.root/DecayTree')           ; originalSamples.append( MC_JpsiIncFullSelDphi_bin5     ) 
MC_NonResFullSelDphi_bin1       = TChain('',''); MC_NonResFullSelDphi_bin1.Add(BinnedSamplesFolder+'MC_NonResFullSelDphi_bin1.root/DecayTree')             ; originalSamples.append( MC_NonResFullSelDphi_bin1      ) 
MC_NonResFullSelDphi_bin2       = TChain('',''); MC_NonResFullSelDphi_bin2.Add(BinnedSamplesFolder+'MC_NonResFullSelDphi_bin2.root/DecayTree')             ; originalSamples.append( MC_NonResFullSelDphi_bin2      ) 
MC_NonResFullSelDphi_bin3       = TChain('',''); MC_NonResFullSelDphi_bin3.Add(BinnedSamplesFolder+'MC_NonResFullSelDphi_bin3.root/DecayTree')             ; originalSamples.append( MC_NonResFullSelDphi_bin3      ) 
MC_NonResFullSelDphi_bin4       = TChain('',''); MC_NonResFullSelDphi_bin4.Add(BinnedSamplesFolder+'MC_NonResFullSelDphi_bin4.root/DecayTree')             ; originalSamples.append( MC_NonResFullSelDphi_bin4      ) 
MC_NonResFullSelDphi_bin5       = TChain('',''); MC_NonResFullSelDphi_bin5.Add(BinnedSamplesFolder+'MC_NonResFullSelDphi_bin5.root/DecayTree')             ; originalSamples.append( MC_NonResFullSelDphi_bin5      ) 
MC_NonResFullSelDphiNoMass_bin1 = TChain('',''); MC_NonResFullSelDphiNoMass_bin1.Add(BinnedSamplesFolder+'MC_NonResFullSelDphiNoMass_bin1.root/DecayTree') ; originalSamples.append( MC_NonResFullSelDphiNoMass_bin1) 
MC_NonResFullSelDphiNoMass_bin2 = TChain('',''); MC_NonResFullSelDphiNoMass_bin2.Add(BinnedSamplesFolder+'MC_NonResFullSelDphiNoMass_bin2.root/DecayTree') ; originalSamples.append( MC_NonResFullSelDphiNoMass_bin2) 
MC_NonResFullSelDphiNoMass_bin3 = TChain('',''); MC_NonResFullSelDphiNoMass_bin3.Add(BinnedSamplesFolder+'MC_NonResFullSelDphiNoMass_bin3.root/DecayTree') ; originalSamples.append( MC_NonResFullSelDphiNoMass_bin3) 
MC_NonResFullSelDphiNoMass_bin4 = TChain('',''); MC_NonResFullSelDphiNoMass_bin4.Add(BinnedSamplesFolder+'MC_NonResFullSelDphiNoMass_bin4.root/DecayTree') ; originalSamples.append( MC_NonResFullSelDphiNoMass_bin4) 
MC_NonResFullSelDphiNoMass_bin5 = TChain('',''); MC_NonResFullSelDphiNoMass_bin5.Add(BinnedSamplesFolder+'MC_NonResFullSelDphiNoMass_bin5.root/DecayTree') ; originalSamples.append( MC_NonResFullSelDphiNoMass_bin5) 
MC_Psi2SFullSelDphi_bin1        = TChain('',''); MC_Psi2SFullSelDphi_bin1.Add(BinnedSamplesFolder+'MC_Psi2SFullSelDphi_bin1.root/DecayTree')               ; originalSamples.append( MC_Psi2SFullSelDphi_bin1       ) 
MC_Psi2SFullSelDphi_bin2        = TChain('',''); MC_Psi2SFullSelDphi_bin2.Add(BinnedSamplesFolder+'MC_Psi2SFullSelDphi_bin2.root/DecayTree')               ; originalSamples.append( MC_Psi2SFullSelDphi_bin2       ) 
MC_Psi2SFullSelDphi_bin3        = TChain('',''); MC_Psi2SFullSelDphi_bin3.Add(BinnedSamplesFolder+'MC_Psi2SFullSelDphi_bin3.root/DecayTree')               ; originalSamples.append( MC_Psi2SFullSelDphi_bin3       ) 
MC_Psi2SFullSelDphi_bin4        = TChain('',''); MC_Psi2SFullSelDphi_bin4.Add(BinnedSamplesFolder+'MC_Psi2SFullSelDphi_bin4.root/DecayTree')               ; originalSamples.append( MC_Psi2SFullSelDphi_bin4       ) 
MC_Psi2SFullSelDphi_bin5        = TChain('',''); MC_Psi2SFullSelDphi_bin5.Add(BinnedSamplesFolder+'MC_Psi2SFullSelDphi_bin5.root/DecayTree')               ; originalSamples.append( MC_Psi2SFullSelDphi_bin5       ) 

# # # # # # PRESCALE SAMPLES # # # # # #
prescaledFiles = []
MC_JpsiCohFullSelDeltaPhi_prescaled       = TFile('MC_JpsiCohFullSelDeltaPhi_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_JpsiCohFullSelDeltaPhi_prescaled      )  
MC_JpsiIncFullSelDeltaPhi_prescaled       = TFile('MC_JpsiIncFullSelDeltaPhi_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_JpsiIncFullSelDeltaPhi_prescaled      )  
MC_NonResFullSelDeltaPhi_prescaled        = TFile('MC_NonResFullSelDeltaPhi_prescaled.root'       ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDeltaPhi_prescaled       )  
MC_NonResFullSelDPhiNoMass_prescaled      = TFile('MC_NonResFullSelDPhiNoMass_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDPhiNoMass_prescaled     )  
MC_PsiFullSelDeltaPhi_prescaled           = TFile('MC_PsiFullSelDeltaPhi_prescaled.root'          ,'recreate') ; prescaledFiles.append(MC_PsiFullSelDeltaPhi_prescaled          )  
MC_JpsiCohFullSelDphi_bin1_prescaled      = TFile('MC_JpsiCohFullSelDphi_bin1_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiCohFullSelDphi_bin1_prescaled     )  
MC_JpsiCohFullSelDphi_bin2_prescaled      = TFile('MC_JpsiCohFullSelDphi_bin2_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiCohFullSelDphi_bin2_prescaled     )  
MC_JpsiCohFullSelDphi_bin3_prescaled      = TFile('MC_JpsiCohFullSelDphi_bin3_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiCohFullSelDphi_bin3_prescaled     )  
MC_JpsiCohFullSelDphi_bin4_prescaled      = TFile('MC_JpsiCohFullSelDphi_bin4_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiCohFullSelDphi_bin4_prescaled     )  
MC_JpsiCohFullSelDphi_bin5_prescaled      = TFile('MC_JpsiCohFullSelDphi_bin5_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiCohFullSelDphi_bin5_prescaled     )  
MC_JpsiIncFullSelDphi_bin1_prescaled      = TFile('MC_JpsiIncFullSelDphi_bin1_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiIncFullSelDphi_bin1_prescaled     )  
MC_JpsiIncFullSelDphi_bin2_prescaled      = TFile('MC_JpsiIncFullSelDphi_bin2_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiIncFullSelDphi_bin2_prescaled     )  
MC_JpsiIncFullSelDphi_bin3_prescaled      = TFile('MC_JpsiIncFullSelDphi_bin3_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiIncFullSelDphi_bin3_prescaled     )  
MC_JpsiIncFullSelDphi_bin4_prescaled      = TFile('MC_JpsiIncFullSelDphi_bin4_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiIncFullSelDphi_bin4_prescaled     )  
MC_JpsiIncFullSelDphi_bin5_prescaled      = TFile('MC_JpsiIncFullSelDphi_bin5_prescaled.root'     ,'recreate') ; prescaledFiles.append(MC_JpsiIncFullSelDphi_bin5_prescaled     )  
MC_NonResFullSelDphi_bin1_prescaled       = TFile('MC_NonResFullSelDphi_bin1_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDphi_bin1_prescaled      )  
MC_NonResFullSelDphi_bin2_prescaled       = TFile('MC_NonResFullSelDphi_bin2_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDphi_bin2_prescaled      )  
MC_NonResFullSelDphi_bin3_prescaled       = TFile('MC_NonResFullSelDphi_bin3_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDphi_bin3_prescaled      )  
MC_NonResFullSelDphi_bin4_prescaled       = TFile('MC_NonResFullSelDphi_bin4_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDphi_bin4_prescaled      )  
MC_NonResFullSelDphi_bin5_prescaled       = TFile('MC_NonResFullSelDphi_bin5_prescaled.root'      ,'recreate') ; prescaledFiles.append(MC_NonResFullSelDphi_bin5_prescaled      )  
MC_NonResFullSelDphiNoMass_bin1_prescaled = TFile('MC_NonResFullSelDphiNoMass_bin1_prescaled.root','recreate') ; prescaledFiles.append(MC_NonResFullSelDphiNoMass_bin1_prescaled) 
MC_NonResFullSelDphiNoMass_bin2_prescaled = TFile('MC_NonResFullSelDphiNoMass_bin2_prescaled.root','recreate') ; prescaledFiles.append(MC_NonResFullSelDphiNoMass_bin2_prescaled) 
MC_NonResFullSelDphiNoMass_bin3_prescaled = TFile('MC_NonResFullSelDphiNoMass_bin3_prescaled.root','recreate') ; prescaledFiles.append(MC_NonResFullSelDphiNoMass_bin3_prescaled) 
MC_NonResFullSelDphiNoMass_bin4_prescaled = TFile('MC_NonResFullSelDphiNoMass_bin4_prescaled.root','recreate') ; prescaledFiles.append(MC_NonResFullSelDphiNoMass_bin4_prescaled) 
MC_NonResFullSelDphiNoMass_bin5_prescaled = TFile('MC_NonResFullSelDphiNoMass_bin5_prescaled.root','recreate') ; prescaledFiles.append(MC_NonResFullSelDphiNoMass_bin5_prescaled) 
MC_Psi2SFullSelDphi_bin1_prescaled        = TFile('MC_Psi2SFullSelDphi_bin1_prescaled.root'       ,'recreate') ; prescaledFiles.append(MC_Psi2SFullSelDphi_bin1_prescaled       )  
MC_Psi2SFullSelDphi_bin2_prescaled        = TFile('MC_Psi2SFullSelDphi_bin2_prescaled.root'       ,'recreate') ; prescaledFiles.append(MC_Psi2SFullSelDphi_bin2_prescaled       )  
MC_Psi2SFullSelDphi_bin3_prescaled        = TFile('MC_Psi2SFullSelDphi_bin3_prescaled.root'       ,'recreate') ; prescaledFiles.append(MC_Psi2SFullSelDphi_bin3_prescaled       )  
MC_Psi2SFullSelDphi_bin4_prescaled        = TFile('MC_Psi2SFullSelDphi_bin4_prescaled.root'       ,'recreate') ; prescaledFiles.append(MC_Psi2SFullSelDphi_bin4_prescaled       )  
MC_Psi2SFullSelDphi_bin5_prescaled        = TFile('MC_Psi2SFullSelDphi_bin5_prescaled.root'       ,'recreate') ; prescaledFiles.append(MC_Psi2SFullSelDphi_bin5_prescaled       )  

# # # # # # COPYING  SAMPLES # # # # # #
for i in range(len(prescaledFiles)):
	treeIn = originalSamples[i]
	fileOut = prescaledFiles[i]
	fileOut.cd()
	treeOut = treeIn.CopyTree("", "", Nentries, 0)
	treeOut.Write()
	fileOut.Close()
