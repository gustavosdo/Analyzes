Processing pt2Fit.C(1)
    Floating Parameter    FinalValue +/-  Error
  --------------------  --------------------------
                   Coh    6.1599e+01 +/-  8.48e+00
                   Inc    1.6690e-06 +/-  1.63e+00
                   Non    6.5240e+00 +/-  1.97e+00
                   Psi    8.3283e+00 +/-  3.69e+00 

Processing pt2Fit.C(2)                                                                     
    Floating Parameter    FinalValue +/-  Error
  -------------------- --------------------------
                   Coh    1.7028e+02 +/-  1.44e+01
                   Inc    1.9161e+01 +/-  9.29e+00
                   Non    2.1400e+01 +/-  3.25e+00
                   Psi    6.5118e+00 +/-  1.08e+01

Processing pt2Fit.C(3)                                              
    Floating Parameter    FinalValue +/-  Error
  --------------------  --------------------------                     
                   Coh    2.0070e+02 +/-  1.56e+01
                   Inc    1.1348e+01 +/-  7.19e+00
                   Non    2.2744e+01 +/-  1.88e+00
                   Psi    1.5451e+01 +/-  1.02e+01


Processing pt2Fit.C(4)
    Floating Parameter    FinalValue +/-  Error
  --------------------  --------------------------
                   Coh    1.1960e+02 +/-  1.19e+01
                   Inc    8.5429e+00 +/-  6.53e+00
                   Non    1.0059e+01 +/-  2.77e+00
                   Psi    5.6015e+00 +/-  8.76e+0


Processing pt2Fit.C(5)
    Floating Parameter    FinalValue +/-  Error   
  --------------------  --------------------------
                   Coh    2.7298e+01 +/-  5.80e+00
                   Inc    2.0424e-06 +/-  9.65e-01
                   Non    1.6173e+00 +/-  6.58e-01
                   Psi    3.9565e+00 +/-  2.97e+00

