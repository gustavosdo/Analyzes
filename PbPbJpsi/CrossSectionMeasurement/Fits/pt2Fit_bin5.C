/*
Perform all necessary fits
of cross-section UPC Jpsi 
using LHCb 2015 PbPb data
gustavo.if.ufrj@gmail.com
*/

#include<stdio.h>

void pt2Fit_bin5()
{
    // REset variables and Set full LHCb style
	gROOT->Reset();
	gROOT->ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C"); // plot style
	
	// RooFit libraries
	gSystem->Load("libRooFit.so");
	using namespace RooFit;
	using namespace RooStats;
	
	// Data definition
    TChain * LHCb15 = new TChain("","");
    TChain * MC_Coh = new TChain("","");
    //TChain * MC_Inc = new TChain("","");
    TChain * MC_Psi = new TChain("","");
    TChain * MC_Non = new TChain("","");

	// Adding tuples
	LHCb15->Add("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/LHCb_JpsiFullSelDphi_bin5.root/DecayTree");
	MC_Coh->Add("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/MC_JpsiCohFullSelDphi_bin5.root/DecayTree");
	//MC_Inc->Add("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/MC_JpsiIncFullSelDphi_bin5.root/DecayTree");
	MC_Psi->Add("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/MC_Psi2SFullSelDphi_bin5.root/DecayTree");
	MC_Non->Add("/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/5_YbinnedSamples/MC_NonResFullSelDphi_bin5.root/DecayTree");

	// Range and binning variables
	Int_t  nBin = 24; Float_t lnpt2_min = -12; Float_t lnpt2_max = 0;
	TString binning = "Candidates/(0.5)";

	// Plot legend
	TLegend leg(0.7, 0.5, 1.0, 1.0);
	leg.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}");

	// PDF variables
	Double_t N  = 1.023179463;
	Double_t eN = 1.024200959;
	RooRealVar J_psi_1S_LNPT2("J_psi_1S_LNPT2","ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}]", lnpt2_min, lnpt2_max); // Variable to perform fit
	RooRealVar    Coh("Coh", "Coh",  2e2,  0e0,  1e3); // Jpsi coherent signal
	//RooRealVar    Inc("Inc", "Inc",  3e1,  0e0,  1e3); // Jpsi incoherent background
	RooRealVar    Psi("Psi", "Psi",  3e1,  0e0,  1e3); // Psi2S Feed-down coherent background
	RooRealVar    Non("Non", "Non",    N, N-eN, N+eN); // Non-resonant coherent background

	// Unbinned Datasets
	RooDataSet unbinLHCb("unbinLHCb", "unbinLHCb", LHCb15, J_psi_1S_LNPT2); // Unbinned LHCb 15 Data ln(pt2) distribution
	RooDataSet  unbinCoh( "unbinCoh",  "unbinCoh", MC_Coh, J_psi_1S_LNPT2); // Unbinned  MC Coherent ln(pt2) distribution
	//RooDataSet  unbinInc( "unbinInc",  "unbinInc", MC_Inc, J_psi_1S_LNPT2); // Unbinned  MC Incoherent ln(pt2) distribution
	RooDataSet  unbinPsi( "unbinPsi",  "unbinPsi", MC_Psi, J_psi_1S_LNPT2); // Unbinned  MC Psi2S Feed-down coherent ln(pt2) distribution
	RooDataSet  unbinNon( "unbinNon",  "unbinNon", MC_Non, J_psi_1S_LNPT2); // Unbinned  MC Non-resonant coherent ln(pt2) distribution

	// Kernel estimation template using MC samples
	RooKeysPdf templateCoh("templateCoh","templateCoh", J_psi_1S_LNPT2, unbinCoh, RooKeysPdf::MirrorBoth, 2);
	//RooKeysPdf templateInc("templateInc","templateInc", J_psi_1S_LNPT2, unbinInc, RooKeysPdf::MirrorBoth, 2);
	RooKeysPdf templatePsi("templatePsi","templatePsi", J_psi_1S_LNPT2, unbinPsi, RooKeysPdf::MirrorBoth, 2);
	RooKeysPdf templateNon("templateNon","templateNon", J_psi_1S_LNPT2, unbinNon, RooKeysPdf::MirrorBoth, 2);

	// PDFs from kernel estimation templates
	RooAbsPdf * cohPDF = new RooExtendPdf("cohPDF", "cohPDF", templateCoh, Coh);
	//RooAbsPdf * incPDF = new RooExtendPdf("incPDF", "incPDF", templateInc, Inc);
	RooAbsPdf * psiPDF = new RooExtendPdf("psiPDF", "psiPDF", templatePsi, Psi);
	RooAbsPdf * nonPDF = new RooExtendPdf("nonPDF", "nonPDF", templateNon, Non);

	// total PDF = signalPDF + backgroundPDF
	//RooAbsPdf * convolutedPDF = new RooAddPdf("convolutedPDF", "convolutedPDF", RooArgList(*cohPDF, *incPDF, *psiPDF, *nonPDF));
	RooAbsPdf * convolutedPDF = new RooAddPdf("convolutedPDF", "convolutedPDF", RooArgList(*cohPDF, *psiPDF, *nonPDF));

	// Roofit fit to data and save in a RooFitResult object the parameters
	RooFitResult * res = convolutedPDF->fitTo(unbinLHCb,Hesse(true),Strategy(2), Save(true), Verbose(0), PrintLevel(-1), Warnings(0), PrintEvalErrors(-1));

	//  Data binned (to draw)
	TH1F * lnpt2Histo = new TH1F("lnpt2Histo", "ln(p_{T}^{2})", nBin, lnpt2_min, lnpt2_max);
	lnpt2Histo->SetMarkerStyle(20) ; lnpt2Histo->SetLineWidth(3);
	LHCb15->Project("lnpt2Histo", "J_psi_1S_LNPT2", "");
	RooDataHist binLHCbData("binLHCbData", "binLHCbData", J_psi_1S_LNPT2, lnpt2Histo);

	// lnpt2 Frame
	RooPlot* lnpt2Frame = J_psi_1S_LNPT2.frame();
	lnpt2Frame->GetYaxis()->SetTitle(binning);

    // Draw
	binLHCbData.plotOn(lnpt2Frame, MarkerSize(1), RooFit::Name("dataPoints"));
	convolutedPDF->plotOn(lnpt2Frame,                         LineWidth(8), LineStyle(1),  LineColor(kBlack),    RooFit::Name("totalPDF"));  
	convolutedPDF->plotOn(lnpt2Frame,    Components(*cohPDF), LineWidth(7), LineStyle(4),   LineColor(kBlue), RooFit::Name("templateCoh"));
	//convolutedPDF->plotOn(lnpt2Frame,    Components(*incPDF), LineWidth(7), LineStyle(9),    LineColor(kRed), RooFit::Name("templateInc"));
	convolutedPDF->plotOn(lnpt2Frame,    Components(*psiPDF), LineWidth(7), LineStyle(9), LineColor(kViolet), RooFit::Name("templatePsi"));
	convolutedPDF->plotOn(lnpt2Frame,    Components(*nonPDF), LineWidth(7), LineStyle(9), LineColor(kOrange), RooFit::Name("templateNon"));
	binLHCbData.plotOn(lnpt2Frame, MarkerSize(1));

	// Adding to legend
	leg.AddEntry(lnpt2Frame->findObject("dataPoints"), "Data", "lp");
	TLegendEntry * templateCohText = leg.AddEntry(lnpt2Frame->findObject("templateCoh"), "Coherent J/#psi"          , "l");
	//TLegendEntry * templateIncText = leg.AddEntry(lnpt2Frame->findObject("templateInc"), "Incoherent J/#psi"        , "l");
	TLegendEntry * templatePsiText = leg.AddEntry(lnpt2Frame->findObject("templatePsi"), "#psi(2S) Feed-down"       , "l");
	TLegendEntry * templateNonText = leg.AddEntry(lnpt2Frame->findObject("templateNon"), "Non-Resonant #gamma#gamma", "l");
	templateCohText->SetTextColor(kBlue);
	//templateIncText->SetTextColor(kRed);
	templatePsiText->SetTextColor(kViolet);
	templateNonText->SetTextColor(kOrange);

	// TCanvas to draw
	TCanvas * canvas = new TCanvas("canvas", "canvas", 0, 0, 1604, 1228);
	canvas->cd(); lnpt2Frame->Draw();
	
	// Signal yield
	Double_t signalYield  = Coh.getVal();
	Double_t sYieldError  = Coh.getError();
	leg.AddEntry("", Form("Signal = %.0f +/- %.0f", signalYield, sYieldError) , "");
	
	// Chi2
	Double_t chi2 = lnpt2Frame->chiSquare("totalPDF", "dataPoints", 3);
	leg.AddEntry("", Form("#chi^{2}/ndf = %f", chi2) , "");

	// printing legend on plot
	leg.Draw();
	res->Print();

	// save file
	canvas->Print("figs/fitJpsiPT2_bin5JpsiY.png");
}
