[12:23, 3/5/2019] Albert Bursche: The normalisation is saved. But for the fit over the 2700 - 4000 MeV range. In a later commit I added the integral ofer the 65 MeV mass window
[12:24, 3/5/2019] Albert Bursche: https://gitlab.cern.ch/bursche/upc-jpsi/commit/d9ee76953d2604e113028ba5e3b574cdd6465efb
[12:24, 3/5/2019] Gustavo Silva: Oh thank you
[12:24, 3/5/2019] Gustavo Silva: I was trting to open these db files with the shelve in python2
[12:25, 3/5/2019] Gustavo Silva: is this the correct way?
[12:25, 3/5/2019] Albert Bursche: The DBs are in here. The code to run it is only complete with a commit later.
[12:25, 3/5/2019] Gustavo Silva: Ok
[12:25, 3/5/2019] Albert Bursche: If you get the RooFitResult objects it is correct.
[12:26, 3/5/2019] Gustavo Silva: Ahh I see
[12:26, 3/5/2019] Gustavo Silva: Thank you
[12:26, 3/5/2019] Gustavo Silva: I will dig into it
[12:26, 3/5/2019] Albert Bursche: saving is done here. https://gitlab.cern.ch/bursche/upc-jpsi/commit/d9ee76953d2604e113028ba5e3b574cdd6465efb#5cb16adff0ae58c78fbc206cc5feee7135fc41c0_104_110
[12:27, 3/5/2019] Albert Bursche: This is a Gaudi::Math::ValueWithError object. Aka an Ostap.Core.VE
