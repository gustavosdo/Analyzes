# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCb Data Sample
MC_Jpsi_PbPb_2015_Coh = TChain('',''); MC_Jpsi_PbPb_2015_Coh.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_Jpsi_PbPb_Coherent_2015_withNewVars.root/DecayTree')
MC_Jpsi_PbPb_2015_Inc = TChain('',''); MC_Jpsi_PbPb_2015_Inc.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_Jpsi_PbPb_Incoherent_2015_withNewVars.root/DecayTree')
MC_Jpsi_PbPb_2015_Psi = TChain('',''); MC_Jpsi_PbPb_2015_Psi.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_Psi2S_PbPb_Coherent_2015_withNewVars.root/DecayTree')
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb
from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_LHCb_Jpsi_Dphi_FullSelection   = TH1D("h_LHCb_Jpsi_Dphi_FullSelection"  ,  "J/#psi Full Selection;#Delta#varphi;Arbitrary Units", Nbins, 0.75, 1)
h_MC_Jpsi_Dphi_FullSelection_Coh = TH1D("h_MC_Jpsi_Dphi_FullSelection_Coh", "MC Coh. Full Selection;#Delta#varphi;Arbitrary Units", Nbins, 0.75, 1)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #
h_MC_Jpsi_Dphi_FullSelection_Coh.SetLineColor(blue) ; h_MC_Jpsi_Dphi_FullSelection_Coh.SetLineWidth(4) ; h_MC_Jpsi_Dphi_FullSelection_Coh.SetMarkerColor(blue) ; h_MC_Jpsi_Dphi_FullSelection_Coh.SetLineStyle(7)

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
LHCb_diMu_PbPb_2015.Project(  "h_LHCb_Jpsi_Dphi_FullSelection"  , "deltaPhi_muplus_muminus", cutFullSelection     )
MC_Jpsi_PbPb_2015_Coh.Project("h_MC_Jpsi_Dphi_FullSelection_Coh", "deltaPhi_muplus_muminus", cutMCFullSelNoTrigger)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
canvas.SetLogy(1)
h_MC_Jpsi_Dphi_FullSelection_Coh.DrawNormalized("HISTO E1")
h_LHCb_Jpsi_Dphi_FullSelection.DrawNormalized("HISTO E1 SAMES")
legend = canvas.BuildLegend(0.15, 0.7, 0.55, .95) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
# selection line ant text
cut = TLine(0.9, 6e-7, 0.9, 3e-1); cut.SetLineWidth(5); cut.SetLineColor(1); cut.Draw("SAMES");
text = TText(0.92, 7e-2, "Selection"); text.SetTextColor(1); text.Draw("SAMES");
selection = TArrow(0.9, 2e-2, 0.98, 2e-2, 0.05, "|>"); selection.SetLineColor(1); selection.SetFillColor(1); selection.Draw();

canvas.Print("../figs/h_LHCb_Jpsi_Dphi_FullSelectionAndh_MC_Jpsi_Dphi_FullSelection_Coh.png")
