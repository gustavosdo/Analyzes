import os, math, ROOT, csv
from ROOT import *
from math import *

def crossSection(Nexc, eff, lumi, fsi, errNexc, errlumi, errfsi, erreff, year):
	Sigma = Nexc / ( eff * lumi * fsi)
	errSigma = Sigma * errNexc / Nexc
	#errSigma = Sigma * ( ( errNexc / Nexc )**2. + ( errlumi / lumi )**2. + ( errfsi / fsi )**2. + ( erreff / eff )**2.)**(0.5)
	return( [Sigma, errSigma] )

def sumLumiByPol(lumiMD, lumiMU, errlumiMD, errlumiMU, lumiWoutHlt2, year):
	lumi = lumiMD + lumiMU - lumiWoutHlt2
	errlumi = sqrt( errlumiMD*errlumiMD + errlumiMU*errlumiMU + 0.035*0.035*lumiWoutHlt2*lumiWoutHlt2)
	return( [lumi,errlumi] )
