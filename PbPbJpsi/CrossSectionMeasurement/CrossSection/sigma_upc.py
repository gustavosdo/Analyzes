import os, math, ROOT
from ROOT import *
from math import *
from functions_upc import *

# Folder with data
Folder = '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/CrossSectionMeasurement/LumiAndEffs/'

lumi = 0. ; errlumi = 0.; # lumi from file
with open(Folder+'luminosity.txt', 'r') as lumiFile:
	for line in lumiFile:
		values = line.split('\t')
		lumi = float(values[0])
		errlumi = float(values[1])

B_ratio = 0.; errB_ratio = 0.; # Branching ratio from file
with open(Folder+'Bratio.txt', 'r') as BratioFile:
	for line in BratioFile:
		values = line.split('\t')
		B_ratio = float(values[0])
		errB_ratio = float(values[1])

effsFiles = ['L0', 'acc', 'muacc', 'frec', 'muon', 'track', 'hlt1', 'sel', 'hrc'] # Files to get the effs
effs = [1.,1.,1.,1.,1.,1.] ; erreffs = [0.,0.,0.,0.,0.,0.]; # eff = [total, bin1, bin2, ... , bin5]
Ybin = 0
for nameFile in effsFiles:
	with open(Folder+nameFile+'.txt', 'r') as effFile:
		for line in effFile:
			values = line.split('\t')
			# Filling efficiencies and its errors
			if (Ybin == 0): effs[0] =  (float(values[0])*effs[0]); erreffs[0] +=  (float(values[1])/float(values[0]))**2.
			if (Ybin == 1): effs[1] =  (float(values[0])*effs[1]); erreffs[1] +=  (float(values[1])/float(values[0]))**2.
			if (Ybin == 2): effs[2] =  (float(values[0])*effs[2]); erreffs[2] +=  (float(values[1])/float(values[0]))**2.
			if (Ybin == 3): effs[3] =  (float(values[0])*effs[3]); erreffs[3] +=  (float(values[1])/float(values[0]))**2.
			if (Ybin == 4): effs[4] =  (float(values[0])*effs[4]); erreffs[4] +=  (float(values[1])/float(values[0]))**2.
			if (Ybin == 5): effs[5] =  (float(values[0])*effs[5]); erreffs[5] +=  (float(values[1])/float(values[0]))**2.
			Ybin += 1
	Ybin = 0
# Propagate errors
for i in range(len(erreffs)):
	erreffs[i] = effs[i]*sqrt(erreffs[i])

# Get yields by bin
Ybin = 0
Nexc = [0.*x for x in range(0,6)] ; errNexc = [0.*x for x in range(0,6)];
with open(Folder+'Nexc.txt', 'r') as Nfile:
	for line in Nfile:
		values = line.split('\t')
		if (Ybin == 0): Nexc[0] = float(values[0]); errNexc[0] = float(values[1])
		if (Ybin == 1): Nexc[1] = float(values[0]); errNexc[1] = float(values[1])
		if (Ybin == 2): Nexc[2] = float(values[0]); errNexc[2] = float(values[1])
		if (Ybin == 3): Nexc[3] = float(values[0]); errNexc[3] = float(values[1])
		if (Ybin == 4): Nexc[4] = float(values[0]); errNexc[4] = float(values[1])
		if (Ybin == 5): Nexc[5] = float(values[0]); errNexc[5] = float(values[1])
		Ybin += 1

crossSection(Nexc, effs, lumi, B_ratio, errNexc, erreffs, errlumi, errB_ratio)
