import os, math, ROOT, csv
from ROOT import *
from math import *

def crossSection(Nexc, effs, lumi, B_ratio, errNexc, erreffs, errlumi, errB_ratio):
	sigma = [0 for x in range(0,6)]
	errSigma = [0 for x in range(0,6)]
	for i in range(1,len(sigma)):
		sigma[i] = Nexc[i] / (0.5 * effs[i] * lumi * B_ratio)
		#errSigma[i] = sigma[i] * ( ( errNexc[i] / Nexc[i] )**2. + ( errlumi / lumi )**2. + ( errB_ratio / B_ratio )**2. + ( erreffs[i] / effs[i] )**2.)**(0.5)
		errSigma[i] = sigma[i] * errNexc[i] / Nexc[i]
		print('Sigma(bin '+str(i)+') = ('+str(sigma[i])+' +- '+str(errSigma[i])+') mb\n')
	#
	SigmaTotal = sum(sigma)/2
	errSigmaTotal = sqrt( sum ( [ x**2./2. for x in errSigma ] ) ) 
	print('Sigma(all) = ('+str(SigmaTotal)+' +- '+str(errSigmaTotal)+') mb\n')
