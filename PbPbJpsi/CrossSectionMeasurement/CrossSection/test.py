from math import *

acc = [0.211,0.539,0.727,0.557,0.212]
tr = [0.742, 0.84, 0.886, 0.919, 0.91]
muacc = [0.848,0.902,0.880,0.838,0.78]
muid = [0.980,.958,.939,.928,.902]
l0 = [0.861, 0.854, .841, .876, .853]
hlt = [.836,.904,.925,.925,.905]
off = [.954,.954,.954,.954,.954]
frec = [1.025,0.97,.929,0.914,0.910]
effs = [acc,tr,muacc,muid,l0,hlt,off,frec]

n = [76, 217,249,141,29]
xsec = []
xsec_ana = [1515.21,1301.26,1139.49,864.80,551.58]
xsec_ana = [2*x for x in xsec_ana]

bin_eff = [1.,1.,1.,1.,1.]

for eff in effs:
	for i in range(0,5):
		bin_eff[i] = bin_eff[i] * eff[i]

for i in range(0,5):
	xsec.append(n[i]/(bin_eff[i]*0.05961*0.5*10.12))
	

print(bin_eff)
print(acc[0]*tr[0]*muacc[0]*muid[0]*l0[0]*hlt[0]*off[0]*frec[0])
print(xsec)
print(xsec_ana)

frac = [x/y for (x,y) in zip(xsec , xsec_ana)]
subt = [x-y for (y,x) in zip(xsec , xsec_ana)]
print(frac)
print(subt)


# Adapt
acc = [0.211,0.539,0.727,0.557,0.212]
tr = [0.742, 0.84, 0.886, 0.919, 0.91]
muacc = [0.848,0.902,0.880,0.838,0.78]
muacc = [v*eps for (v,eps) in zip(muacc, frac)]
muid = [0.980,.958,.939,.928,.902]
l0 = [0.861, 0.854, .841, .876, .853]
hlt = [.836,.904,.925,.925,.905]
off = [.954,.954,.954,.954,.954]
frec = [1.025,0.97,.929,0.914,0.910]
effs = [acc,tr,muacc,muid,l0,hlt,off,frec]

xsec = []
bin_eff = [1.,1.,1.,1.,1.]

for eff in effs:
    for i in range(0,5):
        bin_eff[i] = bin_eff[i] * eff[i]

for i in range(0,5):
    xsec.append(n[i]/(bin_eff[i]*0.05961*0.5*10.12))

print(bin_eff)
print(acc[0]*tr[0]*muacc[0]*muid[0]*l0[0]*hlt[0]*off[0]*frec[0])
print(xsec)
print(muacc)
