# Python2/3 code to determine
# the efficiency using the yields obtained
# by the fits (must be performed obviously before)
# AUTHOR: gustavo.if.ufrj@gmail.com
# DATA: LHCb 2015 PbPb UPC Jpsi(mumu) data

from math import *
import csv

effFile = open('efficiencies.csv', 'a') # file to save the efficiencies
effFile.write('Selection,dataType,Efficiency,ErrorEff\n') # file header

# method to determine the efficiency using the yields after cut (nCut) and after inverted cut (nFail)
# eff = nCut/(nCut + nFail) (uncertainty obtained by derivative error propagation)
def calculateHRCefficiency(yieldAfterCut, yieldFailedCut, errorYieldAfterCut, errorYieldFailedCut):
	nCut = yieldAfterCut ; nFail = yieldFailedCut ; sCut = errorYieldAfterCut ; sFail = errorYieldFailedCut
	dEffdnC = 1./(nCut+nFail) - nCut/((nCut+nFail)**2.)
	dEffdnF = - nCut/((nCut+nFail)**2.)
	Eff = nCut/(nCut + nFail)
	sEff = sqrt( (sCut*dEffdnC)**2. + (sFail*dEffdnF)**2. )
	return [100.*Eff, 100.*sEff]

listData = [] # all parameters fitted
listEfficiencyTypes = [ ['Nominal', 'noPhiCut', 'noExpFix'], ['Nominal', '_PhiCut', 'noExpFix'], ['Nominal', '_PhiCut', '_ExpFix'],\
						['MinBias', 'noPhiCut', 'noExpFix'], ['MinBias', '_PhiCut', 'noExpFix'], ['MinBias', '_PhiCut', '_ExpFix'] ] # the types I want to calculate the HRC eff for

with open('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Work/Fits/yields.csv', 'r') as yieldsFile: # reading the table with all params fitted
	allYields = csv.reader(yieldsFile, delimiter=',') # using cvs reader to import all for a single variable
	for line in allYields: # entry = line from file
		if line[0] != 'fitType': # we're interested in the lines with values, not the header
			name = line[0][5:].replace('.png','') # defining the key for the dictionary of data
			# and getting (float) values from the file
			S    = float(line[1]) 
			errS = float(line[2]) 
			B    = float(line[3])
			errB = float(line[4])
			b    = float(line[5])
			errb = float(line[6])
			dictData = {name:[S,errS,B,errB,b,errb]} # define the dictionary for this line of data
			listData.append(dictData) # append to a BIG list

for listFeatures in listEfficiencyTypes: # for each type of data I want to calculate the efficiency...
	nCutList = [] ; sCutList = [] ; nFailList = [] ; sFailList = [] # initialize variables
	for dictData in listData: # ...i walk all data available...
		for dataType in dictData.keys(): # ...looking for the type availables
			if ( (listFeatures[0] in dataType) and (listFeatures[1] in dataType) and (listFeatures[2] in dataType) ): # ...if the type matches my data Type specified in listFeatures...
				if ('_HRCcut' in dataType): # ...I get the numbers after the cut...
					nCutList.append(dictData[dataType][0]) ; nCutList.append(dictData[dataType][2])
					sCutList.append(dictData[dataType][1]) ; sCutList.append(dictData[dataType][3])
				if ('InvertedHRCcut' in dataType): # ...and after the inverted cut!
					nFailList.append(dictData[dataType][0]) ; nFailList.append(dictData[dataType][2])
					sFailList.append(dictData[dataType][1]) ; sFailList.append(dictData[dataType][3])
	# calculate the efficiencies for both signal (coherent) and background (incoherent)
	signalEff = calculateHRCefficiency(nCutList[0],nFailList[0],sCutList[0],sFailList[0])
	incoheEff = calculateHRCefficiency(nCutList[1],nFailList[1],sCutList[1],sFailList[1])
	# save on a .csv file
	effFile.write(''.join(listFeatures)+',Coherent,'+str(signalEff[0])+','+str(signalEff[1])+'\n')
	effFile.write(''.join(listFeatures)+',Incoherent,'+str(incoheEff[0])+','+str(incoheEff[1])+'\n')
