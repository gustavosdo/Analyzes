# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCb Data Sample
#MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
DphiPlotslog = open("../logs/individualDphiPlots.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerLHCbSelections import * #lstMinbiasTriggerLHCb
#from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb
#from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
# # LHCb Data Histograms # #
# PreSelection
h_LHCb_diMuon_Dphi_runNumber  = TH1D("h_LHCb_diMuon_Dphi_runNumber" , "PreSelection;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# Trigger paths
h_LHCb_diMuon_Dphi_muTrigger  = TH1D("h_LHCb_diMuon_Dphi_muTrigger" , "Muon Trigger;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_MBTrigger  = TH1D("h_LHCb_diMuon_Dphi_MBTrigger" , "mBias Trigger;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_L0muHlt1MB = TH1D("h_LHCb_diMuon_Dphi_L0muHlt1MB", "L0mu + Hlt1MB;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_muonMBTCK  = TH1D("h_LHCb_diMuon_Dphi_muonMBTCK" , "L0mu+HLMB+TCK;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# NonResonant and Enriched non-resonant (w w/o HRC and Dphi)
h_LHCb_diMuon_Dphi_NonResonant  = TH1D("h_LHCb_diMuon_Dphi_NonResonant" , "NonResonant;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_EnrichNonRes = TH1D("h_LHCb_diMuon_Dphi_EnrichNonRes", "Enr. NonRes;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_NonResHRC    = TH1D("h_LHCb_diMuon_Dphi_NonResHRC"   , "NonResHRC;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_EnrNonResHRC = TH1D("h_LHCb_diMuon_Dphi_EnrNonResHRC", "EnrNonResHRC;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_NonResPhi    = TH1D("h_LHCb_diMuon_Dphi_NonResPhi"   , "NonResPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_EnrNonResPhi = TH1D("h_LHCb_diMuon_Dphi_EnrNonResPhi", "EnrNonResPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# NonResonant and Enriched non-resonant with HRC and Dphi
h_LHCb_diMuon_Dphi_NonResHRCPhi    = TH1D("h_LHCb_diMuon_Dphi_NonResHRCPhi"   , "NonResHRCPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_EnrNonResHRCPhi = TH1D("h_LHCb_diMuon_Dphi_EnrNonResHRCPhi", "EnrNonResHRCPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# Enriched incoherent (w w/o HRC and Dphi)
lowestMassGeV = lowestMass/1000 ; highestMassGeV = highestMass/1000
h_LHCb_diMuon_Dphi_EnrichIncoh = TH1D("h_LHCb_diMuon_Dphi_EnrichIncoh", "Enr. Incoh;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_EnrIncohHRC = TH1D("h_LHCb_diMuon_Dphi_EnrIncohHRC", "EnrIncohHRC;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_EnrIncohPhi = TH1D("h_LHCb_diMuon_Dphi_EnrIncohPhi", "EnrIncohPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# Enriched incoherent with HRC and Dphi
h_LHCb_diMuon_Dphi_EnrIncohHRCPhi = TH1D("h_LHCb_diMuon_Dphi_EnrIncohHRCPhi", "EnrIncohHRCPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# Full signal selectin (w w/o HRC and Dphi)
h_LHCb_diMuon_Dphi_Offline    = TH1D("h_LHCb_diMuon_Dphi_Offline"   , "Offline;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_OfflineHRC = TH1D("h_LHCb_diMuon_Dphi_OfflineHRC", "OfflineHRC;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_LHCb_diMuon_Dphi_OfflinePhi = TH1D("h_LHCb_diMuon_Dphi_OfflinePhi", "OfflinePhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
# Full signal selection with HRC and Dphi
h_LHCb_diMuon_Dphi_OfflineHRCPhi = TH1D("h_LHCb_diMuon_Dphi_OfflineHRCPhi", "OfflineHRCPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
rN = runNumbers
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_runNumber"      , "deltaPhi_muplus_muminus", runNumbers            )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_muTrigger"      , "deltaPhi_muplus_muminus", rN+"&&"+triggerDimuon )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_MBTrigger"      , "deltaPhi_muplus_muminus", rN+"&&"+triggerMinBias)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_L0muHlt1MB"     , "deltaPhi_muplus_muminus", rN+"&&"+L0MUHlt1MBias )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_muonMBTCK"      , "deltaPhi_muplus_muminus", rN+"&&"+L0MUHlt1MBTCKS)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_NonResonant"    , "deltaPhi_muplus_muminus", cutNonResonant        )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrichNonRes"   , "deltaPhi_muplus_muminus", cutEnrNonRes          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_NonResHRC"      , "deltaPhi_muplus_muminus", cutNonResHRC          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrNonResHRC"   , "deltaPhi_muplus_muminus", cutEnrNonResHRC       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_NonResPhi"      , "deltaPhi_muplus_muminus", cutNonResPhi          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrNonResPhi"   , "deltaPhi_muplus_muminus", cutEnrNonResPhi       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_NonResHRCPhi"   , "deltaPhi_muplus_muminus", cutNonResHRCPhi       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrNonResHRCPhi", "deltaPhi_muplus_muminus", cutEnrNonResHRCPhi    )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrichIncoh"    , "deltaPhi_muplus_muminus", cutEnrIncJpsi         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrIncohHRC"    , "deltaPhi_muplus_muminus", cutEnrIncJpsiHRC      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrIncohPhi"    , "deltaPhi_muplus_muminus", cutEnrIncJpsiPhi      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_EnrIncohHRCPhi" , "deltaPhi_muplus_muminus", cutEnrIncJpsiHRCPhi   )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_Offline"        , "deltaPhi_muplus_muminus", cutFullSelection      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_OfflineHRC"     , "deltaPhi_muplus_muminus", cutFullSelHRC         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_OfflinePhi"     , "deltaPhi_muplus_muminus", cutFullSelPhi         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_OfflineHRCPhi"  , "deltaPhi_muplus_muminus", cutFullSelHRCPhi      )
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
DphiHistos = [h_LHCb_diMuon_Dphi_runNumber,\
			  h_LHCb_diMuon_Dphi_muTrigger,\
			  h_LHCb_diMuon_Dphi_MBTrigger,\
			  h_LHCb_diMuon_Dphi_L0muHlt1MB,\
			  h_LHCb_diMuon_Dphi_muonMBTCK,\
			  h_LHCb_diMuon_Dphi_NonResonant,\
			  h_LHCb_diMuon_Dphi_EnrichNonRes,\
			  h_LHCb_diMuon_Dphi_NonResHRC,\
			  h_LHCb_diMuon_Dphi_EnrNonResHRC,\
			  h_LHCb_diMuon_Dphi_NonResPhi,\
			  h_LHCb_diMuon_Dphi_EnrNonResPhi,\
			  h_LHCb_diMuon_Dphi_NonResHRCPhi,\
			  h_LHCb_diMuon_Dphi_EnrNonResHRCPhi,\
			  h_LHCb_diMuon_Dphi_EnrichIncoh,\
			  h_LHCb_diMuon_Dphi_EnrIncohHRC,\
			  h_LHCb_diMuon_Dphi_EnrIncohPhi,\
			  h_LHCb_diMuon_Dphi_EnrIncohHRCPhi,\
			  h_LHCb_diMuon_Dphi_Offline,\
			  h_LHCb_diMuon_Dphi_OfflineHRC,\
			  h_LHCb_diMuon_Dphi_OfflinePhi,\
			  h_LHCb_diMuon_Dphi_OfflineHRCPhi]
for histo in DphiHistos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("HISTO E1")
	legend = canvas.BuildLegend(0.3, 0.77, 0.7, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
	# Printing yields
	DphiPlotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
DphiPlotslog.close()
