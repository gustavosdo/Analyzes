# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCb Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerLHCbSelections import * #lstMinbiasTriggerLHCb
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_LHCb_diMuon_HRC_EnrichNonRes = TH1D("h_LHCb_diMuon_HRC_EnrichNonRes", "Enr. NonRes;ln(#chi^{2}_{HRC});Arbitrary Units", Nbins, 6, 11)
h_LHCb_diMuon_HRC_EnrichIncoh  = TH1D("h_LHCb_diMuon_HRC_EnrichIncoh" , "Enr. Incoh;ln(#chi^{2}_{HRC});Arbitrary Units" , Nbins, 6, 11)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #
h_LHCb_diMuon_HRC_EnrichNonRes.SetLineColor(blue) ; h_LHCb_diMuon_HRC_EnrichNonRes.SetMarkerColor(blue) ; h_LHCb_diMuon_HRC_EnrichNonRes.SetLineStyle(1) ; h_LHCb_diMuon_HRC_EnrichNonRes.SetMarkerStyle(20)
h_LHCb_diMuon_HRC_EnrichIncoh.SetLineColor(red) ; h_LHCb_diMuon_HRC_EnrichIncoh.SetMarkerColor(red) ; h_LHCb_diMuon_HRC_EnrichIncoh.SetLineStyle(7) ; h_LHCb_diMuon_HRC_EnrichIncoh.SetLineWidth(4) ; h_LHCb_diMuon_HRC_EnrichIncoh.SetMarkerStyle(45)

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrichNonRes", "log_hrc_fom_v2", cutEnrNonRes )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrichIncoh" , "log_hrc_fom_v2", cutEnrIncJpsi)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
h_LHCb_diMuon_HRC_EnrichNonRes.DrawNormalized("E1")
h_LHCb_diMuon_HRC_EnrichIncoh.DrawNormalized("E1 SAMES")
legend = canvas.BuildLegend(0.53, 0.65, 0.93, 0.9) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
# selection line ant text
cut = TLine(7, 0, 7, 0.32); cut.SetLineWidth(5); cut.SetLineColor(1); cut.Draw("SAMES");
text = TText(7.2, 0.15, "Selection"); text.SetTextColor(1); text.Draw("SAMES");
selection = TArrow(7, 0.15, 6.7, 0.15, 0.05, "|>"); selection.SetLineColor(1); selection.SetFillColor(1);	selection.Draw();

canvas.Print("../figs/h_LHCb_diMuon_HRC_EnrichNonResAndh_LHCb_diMuon_HRC_EnrichIncoh.png")
