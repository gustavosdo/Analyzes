# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCb Data Sample
#MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
massPlotslog = open("../logs/individualMassPlots.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerLHCbSelections import * #lstMinbiasTriggerLHCb
#from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb
#from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
# # LHCb Data Histograms # #
# PreSelection
h_LHCb_diMuon_Mass_runNumber  = TH1D("h_LHCb_diMuon_Mass_runNumber" , "PreSelection;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
# Trigger paths
h_LHCb_diMuon_Mass_muTrigger  = TH1D("h_LHCb_diMuon_Mass_muTrigger" , "Muon Trigger;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
h_LHCb_diMuon_Mass_MBTrigger  = TH1D("h_LHCb_diMuon_Mass_MBTrigger" , "mBias Trigger;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
h_LHCb_diMuon_Mass_L0muHlt1MB = TH1D("h_LHCb_diMuon_Mass_L0muHlt1MB", "L0mu + Hlt1MB;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
h_LHCb_diMuon_Mass_muonMBTCK  = TH1D("h_LHCb_diMuon_Mass_muonMBTCK" , "L0mu+HLMB+TCK;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
# NonResonant and Enriched non-resonant (w w/o HRC and Dphi)
h_LHCb_diMuon_Mass_NonResonant  = TH1D("h_LHCb_diMuon_Mass_NonResonant" , "NonResonant;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_LHCb_diMuon_Mass_EnrichNonRes = TH1D("h_LHCb_diMuon_Mass_EnrichNonRes", "Enr. NonRes;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_LHCb_diMuon_Mass_NonResHRC    = TH1D("h_LHCb_diMuon_Mass_NonResHRC"   , "NonResHRC;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_LHCb_diMuon_Mass_EnrNonResHRC = TH1D("h_LHCb_diMuon_Mass_EnrNonResHRC", "EnrNonResHRC;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_LHCb_diMuon_Mass_NonResPhi    = TH1D("h_LHCb_diMuon_Mass_NonResPhi"   , "NonResPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_LHCb_diMuon_Mass_EnrNonResPhi = TH1D("h_LHCb_diMuon_Mass_EnrNonResPhi", "EnrNonResPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
# NonResonant and Enriched non-resonant with HRC and Dphi
h_LHCb_diMuon_Mass_NonResHRCPhi    = TH1D("h_LHCb_diMuon_Mass_NonResHRCPhi"   , "NonResHRCPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_LHCb_diMuon_Mass_EnrNonResHRCPhi = TH1D("h_LHCb_diMuon_Mass_EnrNonResHRCPhi", "EnrNonResHRCPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
# Enriched incoherent (w w/o HRC and Dphi)
lowestMassGeV = lowestMass/1000 ; highestMassGeV = highestMass/1000
h_LHCb_diMuon_Mass_EnrichIncoh = TH1D("h_LHCb_diMuon_Mass_EnrichIncoh", "Enr. Incoh;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_LHCb_diMuon_Mass_EnrIncohHRC = TH1D("h_LHCb_diMuon_Mass_EnrIncohHRC", "EnrIncohHRC;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_LHCb_diMuon_Mass_EnrIncohPhi = TH1D("h_LHCb_diMuon_Mass_EnrIncohPhi", "EnrIncohPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
# Enriched incoherent with HRC and Dphi
h_LHCb_diMuon_Mass_EnrIncohHRCPhi = TH1D("h_LHCb_diMuon_Mass_EnrIncohHRCPhi", "EnrIncohHRCPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
# Full signal selectin (w w/o HRC and Dphi)
h_LHCb_diMuon_Mass_Offline    = TH1D("h_LHCb_diMuon_Mass_Offline"   , "Offline;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_LHCb_diMuon_Mass_OfflineHRC = TH1D("h_LHCb_diMuon_Mass_OfflineHRC", "OfflineHRC;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_LHCb_diMuon_Mass_OfflinePhi = TH1D("h_LHCb_diMuon_Mass_OfflinePhi", "OfflinePhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
# Full signal selection with HRC and Dphi
h_LHCb_diMuon_Mass_OfflineHRCPhi = TH1D("h_LHCb_diMuon_Mass_OfflineHRCPhi", "OfflineHRCPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
rN = runNumbers
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_runNumber"      , "J_psi_1S_M/1e3", rN                    )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_muTrigger"      , "J_psi_1S_M/1e3", rN+"&&"+triggerDimuon )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_MBTrigger"      , "J_psi_1S_M/1e3", rN+"&&"+triggerMinBias)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_L0muHlt1MB"     , "J_psi_1S_M/1e3", rN+"&&"+L0MUHlt1MBias )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_muonMBTCK"      , "J_psi_1S_M/1e3", rN+"&&"+L0MUHlt1MBTCKS)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_NonResonant"    , "J_psi_1S_M/1e3", cutNonResonant        )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrichNonRes"   , "J_psi_1S_M/1e3", cutEnrNonRes          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_NonResHRC"      , "J_psi_1S_M/1e3", cutNonResHRC          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrNonResHRC"   , "J_psi_1S_M/1e3", cutEnrNonResHRC       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_NonResPhi"      , "J_psi_1S_M/1e3", cutNonResPhi          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrNonResPhi"   , "J_psi_1S_M/1e3", cutEnrNonResPhi       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_NonResHRCPhi"   , "J_psi_1S_M/1e3", cutNonResHRCPhi       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrNonResHRCPhi", "J_psi_1S_M/1e3", cutEnrNonResHRCPhi    )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrichIncoh"    , "J_psi_1S_M/1e3", cutEnrIncJpsi         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrIncohHRC"    , "J_psi_1S_M/1e3", cutEnrIncJpsiHRC      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrIncohPhi"    , "J_psi_1S_M/1e3", cutEnrIncJpsiPhi      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_EnrIncohHRCPhi" , "J_psi_1S_M/1e3", cutEnrIncJpsiHRCPhi   )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_Offline"        , "J_psi_1S_M/1e3", cutFullSelection      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_OfflineHRC"     , "J_psi_1S_M/1e3", cutFullSelHRC         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_OfflinePhi"     , "J_psi_1S_M/1e3", cutFullSelPhi         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Mass_OfflineHRCPhi"  , "J_psi_1S_M/1e3", cutFullSelHRCPhi      )
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
massHistos = [h_LHCb_diMuon_Mass_runNumber,\
			  h_LHCb_diMuon_Mass_muTrigger,\
			  h_LHCb_diMuon_Mass_MBTrigger,\
			  h_LHCb_diMuon_Mass_L0muHlt1MB,\
			  h_LHCb_diMuon_Mass_muonMBTCK,\
			  h_LHCb_diMuon_Mass_NonResonant,\
			  h_LHCb_diMuon_Mass_EnrichNonRes,\
			  h_LHCb_diMuon_Mass_NonResHRC,\
			  h_LHCb_diMuon_Mass_EnrNonResHRC,\
			  h_LHCb_diMuon_Mass_NonResPhi,\
			  h_LHCb_diMuon_Mass_EnrNonResPhi,\
			  h_LHCb_diMuon_Mass_NonResHRCPhi,\
			  h_LHCb_diMuon_Mass_EnrNonResHRCPhi,\
			  h_LHCb_diMuon_Mass_EnrichIncoh,\
			  h_LHCb_diMuon_Mass_EnrIncohHRC,\
			  h_LHCb_diMuon_Mass_EnrIncohPhi,\
			  h_LHCb_diMuon_Mass_EnrIncohHRCPhi,\
			  h_LHCb_diMuon_Mass_Offline,\
			  h_LHCb_diMuon_Mass_OfflineHRC,\
			  h_LHCb_diMuon_Mass_OfflinePhi,\
			  h_LHCb_diMuon_Mass_OfflineHRCPhi]
canvas.SetLogy(1)
for histo in massHistos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("HISTO E1")
	if 'NonRes' in name:
		legend = canvas.BuildLegend(0.2, 0.27, 0.6, 0.5) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	else:
		legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
	# Printing yields
	massPlotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
massPlotslog.close()

#############################  BACKUP  ###########################
## lnPT2 histograms
#h_diMu_PT2_PreSelection  = TH1D("h_diMu_PT2_PreSelection" , "PreSelection;ln[p_{T}^{2}(#mu^{+}#mu^{-})/GeV^{2}];Candidates", 180, -15, 3)
#h_diMu_PT2_NonRes        = TH1D("h_diMu_PT2_NonRes"		  , "Non-Resonant;ln(p_{T}^{2}(#mu^{+}#mu^{-})/GeV^{2});Candidates", 100, -13, 3)
#h_diMu_PT2_IncJpsi       = TH1D("h_diMu_PT2_IncJpsi"	  , "Incoherent;ln(p_{T}^{2}(#mu^{+}#mu^{-})/GeV^{2});Candidates", 100, -13, 3)
#h_diMu_PT2_OffHRC        = TH1D("h_diMu_PT2_OffHRC" 	  , "J/#psi, HRC < 7;ln(p_{T}^{2}(#mu^{+}#mu^{-})/GeV^{2});Candidates", 50, -13, 3)
#h_diMu_PT2_Offline       = TH1D("h_diMu_PT2_Offline"	  , "J/#psi;ln(p_{T}^{2}(#mu^{+}#mu^{-})/GeV^{2});Candidates", 50, -13, 3)
## lnChi2HRC histograms
#h_diMu_HRC_runNumber = TH1D("h_diMu_HRC_runNumber", "PreSelection;ln(#chi^{2}_{HRC});Candidates", 100, 5, 11)
#h_diMu_HRC_Trigger   = TH1D("h_diMu_HRC_Trigger"  , "Trigger;ln(#chi^{2}_{HRC});Candidates", 100, 5, 11)
#h_diMu_HRC_Offline   = TH1D("h_diMu_HRC_Offline"  , "J/#psi;ln(#chi^{2}_{HRC});Candidates", 100, 5, 11)
#h_diMu_HRC_EnrNR     = TH1D("h_diMu_HRC_EnrNR"    , "Enriched Non-Res.;ln(#chi^{2}_{HRC});Candidates", 100, 5, 11)
#h_diMu_HRC_EnrInc    = TH1D("h_diMu_HRC_EnrInc"   , "Enriched Incoh.;ln(#chi^{2}_{HRC});Candidates", 100, 5, 11)
## lnChi2HRC B and F
#h_diMu_HRCb_Offline = TH1D("h_diMu_HRCb_Offline", "J/#psi;ln(#chi^{2}_{HRC}B);Candidates", 100, 5, 11)
#h_diMu_HRCf_Offline = TH1D("h_diMu_HRCf_Offline", "J/#psi;ln(#chi^{2}_{HRC}F);Candidates", 100, 5, 11)
#h2_diMu_HRCb_HRCf_Offline = TH2D("h2_diMu_HRCb_HRCf_Offline", "HRC_{B}xHRC_{F} J/#psi;HRC_{B};HRC_{F}", 50, 5, 12, 50, 5, 12)
#h_diMu_HRCb_NonRes = TH1D("h_diMu_HRCb_NonRes", "NonRes;ln(#chi^{2}_{HRC}B);Candidates", 100, 5, 11)
#h_diMu_HRCf_NonRes = TH1D("h_diMu_HRCf_NonRes", "NonRes;ln(#chi^{2}_{HRC}F);Candidates", 100, 5, 11)
#h2_diMu_HRCb_HRCf_NonRes = TH2D("h2_diMu_HRCb_HRCf_NonRes", "HRC_{B}xHRC_{F} NonRes;HRC_{B};HRC_{F}", 50, 5, 12, 50, 5, 12)
#h_diMu_HRCb_EnrNR = TH1D("h_diMu_HRCb_EnrNR", "EnrNR;ln(#chi^{2}_{HRC}B);Candidates", 100, 5, 11)
#h_diMu_HRCf_EnrNR = TH1D("h_diMu_HRCf_EnrNR", "EnrNR;ln(#chi^{2}_{HRC}F);Candidates", 100, 5, 11)
#h2_diMu_HRCb_HRCf_EnrNR = TH2D("h2_diMu_HRCb_HRCf_EnrNR", "HRC_{B}xHRC_{F} EnrNR;HRC_{B};HRC_{F}", 50, 5, 12, 50, 5, 12)
#h_diMu_HRCb_EnrNRhrcF = TH1D("h_diMu_HRCb_EnrNRhrcF", "EnrNRhrcF;ln(#chi^{2}_{HRC}B);Candidates", 100, 5, 11)
#h_diMu_HRCf_EnrNRhrcB = TH1D("h_diMu_HRCf_EnrNRhrcB", "EnrNRhrcB;ln(#chi^{2}_{HRC}F);Candidates", 100, 5, 11)
## L0Calo plots (L0CALO: SPDmult > 2 && Hadron E_T < 240 MeV)
#h_diMu_L0Calo_TOS             = TH1D("h_diMu_L0Calo_TOS"            , "EnrNR;L0CaloTOS(J/#psi);Candidates", 2, 0, 2) 
#h_muplus_L0Calo_TOS           = TH1D("h_muplus_L0Calo_TOS"          , "EnrNR;L0CaloTOS(#mu^{+});Candidates", 2, 0, 2)
#h_muminus_L0Calo_TOS          = TH1D("h_muminus_L0Calo_TOS"         , "EnrNR;L0CaloTOS(#mu^{-});Candidates", 2, 0, 2)
#h_muplusORmuminus_L0Calo_TOS  = TH1D("h_muplusORmuminus_L0Calo_TOS" , "EnrNR;L0CaloTOS(#mu^{+} or #mu^{-});Candidates", 2, 0, 2)
#h2_muplusVSmuminus_L0Calo_TOS = TH2D("h2_muplusVSmuminus_L0Calo_TOS", "EnrNR;L0CaloTOS(#mu^{+});L0CaloTOS(#mu^{-})", 2, 0, 2, 2, 0 ,2)
## histogram to determine efficiency
#h_muplus_isMuon = TH1D("h_muplus_isMuon", "MuPlusIsMuon;isMuon(#mu^{+});Candidates", 3, 1, 0)
## Cosmetics
#h_diMu_M_EnrNR.SetLineColor(blue) ; h_diMu_M_EnrNR.SetMarkerColor(blue)
#h_diMu_M_IncJpsi.SetLineColor(red) ; h_diMu_M_IncJpsi.SetMarkerColor(red)
#h_diMu_M_runNumber.SetLineColor(red) ; h_diMu_M_runNumber.SetMarkerColor(red)
#h_diMu_HRC_runNumber.SetLineColor(red) ; h_diMu_HRC_runNumber.SetMarkerColor(red)
#h_diMu_M_Trigger.SetLineColor(blue) ; h_diMu_M_Trigger.SetMarkerColor(blue)
#h_diMu_HRC_Trigger.SetLineColor(blue) ; h_diMu_HRC_Trigger.SetMarkerColor(blue)
#h_diMu_PT2_NonRes.SetLineColor(blue) ; h_diMu_PT2_NonRes.SetMarkerColor(blue)
#h_diMu_PT2_IncJpsi.SetLineColor(red) ; h_diMu_PT2_IncJpsi.SetMarkerColor(red)
#h_diMu_PT2_OffHRC.SetLineColor(green) ; h_diMu_PT2_OffHRC.SetMarkerColor(green)
#h_diMu_HRC_EnrNR.SetLineColor(blue) ; h_diMu_HRC_EnrNR.SetMarkerColor(blue)
#h_diMu_HRC_EnrInc.SetLineColor(red) ; h_diMu_HRC_EnrInc.SetMarkerColor(red)
#h_diMu_HRCb_NonRes.SetMarkerColor(green) ;h_diMu_HRCb_NonRes.SetLineColor(green)
#h_diMu_HRCf_NonRes.SetMarkerColor(blue) ;h_diMu_HRCf_NonRes.SetLineColor(blue)  
#h_diMu_HRCb_EnrNR.SetMarkerColor(green) ;h_diMu_HRCb_EnrNR.SetLineColor(green)
#h_diMu_HRCf_EnrNR.SetMarkerColor(blue) ;h_diMu_HRCf_EnrNR.SetLineColor(blue)
#h_diMu_HRCb_EnrNRhrcF.SetMarkerColor(green) ;h_diMu_HRCb_EnrNRhrcF.SetLineColor(green)
#h_diMu_HRCf_EnrNRhrcB.SetMarkerColor(blue) ;h_diMu_HRCf_EnrNRhrcB.SetLineColor(blue)
#
## # # # # # # # # # 6.PROJECT DATA ON HISTOS # # # # # # # # # #
#data.Project("h_diMu_M_PreSelection", "J_psi_1S_M/1e3", runNumbers)
#data.Project("h_diMu_M_runNumber", "J_psi_1S_M/1e3", runNumbers)
#data.Project("h_diMu_M_Trigger", "J_psi_1S_M/1e3", Trigger)
#data.Project("h_diMu_M_Offline", "J_psi_1S_M/1e3", Offline)
#data.Project("h_diMu_M_NRforFit", "J_psi_1S_M/1e3", NRforFit)
#data.Project("h_diMu_M_NRFFPhi", "J_psi_1S_M/1e3", NRFFPhi)
#data.Project("h_diMu_M_EnrNR" , "J_psi_1S_M/1e3", EnrNR)
#data.Project("h_diMu_M_IncJpsi", "J_psi_1S_M/1e3", IncJpsi)
#data.Project("h_diMu_M_Jpsi"   , "J_psi_1S_M/1e3", Offline)
#data.Project("h_diMu_PT2_PreSelection", "J_psi_1S_LNPT2", runNumbers)
#data.Project("h_diMu_PT2_NonRes", "J_psi_1S_LNPT2", NonRes)
#data.Project("h_diMu_PT2_IncJpsi", "J_psi_1S_LNPT2", IncJpsi)
#data.Project("h_diMu_PT2_OffHRC" , "J_psi_1S_LNPT2", OffHRC)
#data.Project("h_diMu_PT2_Offline" , "J_psi_1S_LNPT2", Offline)
#data.Project("h_diMu_HRC_runNumber", "log_hrc_fom_v2", runNumbers)
#data.Project("h_diMu_HRC_Trigger", "log_hrc_fom_v2", Trigger)
#data.Project("h_diMu_HRC_Offline", "log_hrc_fom_v2", Offline)
#data.Project("h_diMu_HRC_EnrNR", "log_hrc_fom_v2", EnrNR)
#data.Project("h_diMu_HRC_EnrInc", "log_hrc_fom_v2", EnrInc)
#data.Project("h_diMu_HRCb_Offline", "log_hrc_fom_B_v2", Offline)
#data.Project("h_diMu_HRCf_Offline", "log_hrc_fom_F_v2", Offline)
#data.Project("h2_diMu_HRCb_HRCf_Offline","log_hrc_fom_F_v2:log_hrc_fom_B_v2", Offline)
#data.Project("h_diMu_HRCb_NonRes", "log_hrc_fom_B_v2", NonRes)
#data.Project("h_diMu_HRCf_NonRes", "log_hrc_fom_F_v2", NonRes)
#data.Project("h2_diMu_HRCb_HRCf_NonRes","log_hrc_fom_F_v2:log_hrc_fom_B_v2", NonRes)
#data.Project("h_diMu_HRCb_EnrNR", "log_hrc_fom_B_v2", EnrNR)
#data.Project("h_diMu_HRCf_EnrNR", "log_hrc_fom_F_v2", EnrNR)
#data.Project("h2_diMu_HRCb_HRCf_EnrNR","log_hrc_fom_F_v2:log_hrc_fom_B_v2", EnrNR)
#data.Project("h_diMu_HRCb_EnrNRhrcF", "log_hrc_fom_B_v2", EnrNRhrcF)
#data.Project("h_diMu_HRCf_EnrNRhrcB", "log_hrc_fom_F_v2", EnrNRhrcB)
#data.Project("h_diMu_L0Calo_TOS"            , "J_psi_1S_L0CALODecision_TOS", EnrNR)
#data.Project("h_muplus_L0Calo_TOS"          , "muplus_L0CALODecision_TOS"  , EnrNR)
#data.Project("h_muminus_L0Calo_TOS"         , "muminus_L0CALODecision_TOS" , EnrNR)
#data.Project("h_muplusORmuminus_L0Calo_TOS" , "(muplus_L0CALODecision_TOS || muminus_L0CALODecision_TOS)", EnrNR)
#data.Project("h2_muplusVSmuminus_L0Calo_TOS", "muminus_L0CALODecision_TOS:muplus_L0CALODecision_TOS", EnrNR)
#
## # # # # # # # # # 7.DRAW AND SAVE # # # # # # # # # #
#massHistos = [h_diMu_M_runNumber, h_diMu_M_Trigger, h_diMu_M_Offline, h_diMu_M_NRforFit, h_diMu_M_NRFFPhi, h_diMu_M_PreSelection]
#herschelHistos = [h_diMu_HRC_runNumber, h_diMu_HRC_Trigger, h_diMu_HRC_Offline, h_diMu_HRC_EnrNR, h_diMu_HRC_EnrInc]
#pt2Histos = [h_diMu_PT2_NonRes, h_diMu_PT2_IncJpsi, h_diMu_PT2_OffHRC, h_diMu_PT2_Offline, h_diMu_PT2_PreSelection]
#fomHistos = [h_diMu_HRCb_Offline, h_diMu_HRCf_Offline, h_diMu_HRCf_NonRes, h_diMu_HRCb_NonRes, h_diMu_HRCb_EnrNR, h_diMu_HRCf_EnrNR, h_diMu_HRCb_EnrNRhrcF, h_diMu_HRCf_EnrNRhrcB]
#L0CaloHistos = [h_diMu_L0Calo_TOS, h_muplus_L0Calo_TOS, h_muminus_L0Calo_TOS, h_muplusORmuminus_L0Calo_TOS]
## diMu_M plots
#canvas.SetLogy(1)
#for histo in massHistos:
#	histo.Draw("HISTO E1")
#	if histo.GetName() in ["h_diMu_M_NRforFit", "h_diMu_M_NRFFPhi"]:
#		legend = canvas.BuildLegend(0.68, 0.77, 1.08, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#	else:
#		legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#	canvas.Print("figs/"+histo.GetName()+".png")
## dimu_M runNumber and trigger
#h_diMu_M_runNumber.Draw("HISTO E1") ; h_diMu_M_Trigger.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_M_runNumber_Trigger.png")
## all dimu_M
#h_diMu_M_runNumber.Draw("HISTO E1") ; h_diMu_M_Trigger.Draw("HISTO E1 SAMES") ; h_diMu_M_Offline.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.7, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_M_runNumber_Trigger_Offline.png")
## dimu_M for main selections
#h_diMu_M_IncJpsi.Draw("HISTO E1") ; h_diMu_M_EnrNR.Draw("HISTO E1 SAMES") ; h_diMu_M_Jpsi.Draw("HISTO E1 SAMES") 
#legend = canvas.BuildLegend(0.15, 0.7, 0.55, 1.0) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_M_Jpsi_IncJpsi_EnrNR.png")
#
## diMu_HRC plots
#canvas.SetLogy(0)
#for histo in herschelHistos:
#	histo.Draw("HISTO E1")
#	if histo.GetName() == "h_diMu_HRC_EnrInc":
#		legend = canvas.BuildLegend(0.15, 0.72, 0.55, 0.95) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#	else:
#		legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#	canvas.Print("figs/"+histo.GetName()+".png")
## dimu_HRC runNumber and trigger
#h_diMu_HRC_runNumber.Draw("HISTO E1") ; h_diMu_HRC_Trigger.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRC_runNumber_Trigger.png")
## dimu_HRC runNumber, trigger, offline
#h_diMu_HRC_runNumber.Draw("HISTO E1") ; h_diMu_HRC_Trigger.Draw("HISTO E1 SAMES") ; h_diMu_HRC_Offline.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRC_runNumber_Trigger_Offline.png")
## dimu_HRC Enriched NonRes and Enriched Incoh.
#h_diMu_HRC_EnrNR.Draw("HISTO E1") ; h_diMu_HRC_EnrInc.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRC_EnrNR_EnrInc.png")
## dimu_HRC EnrNR, EnrInc, Offline
#h_diMu_HRC_EnrNR.Draw("HISTO E1") ; h_diMu_HRC_Offline.Draw("HISTO E1 SAMES") ; h_diMu_HRC_EnrInc.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.7, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRC_Offline_EnrNR_EnrInc.png")
#
## diMu_PT2 plots
#for histo in pt2Histos:
#	histo.Draw("HISTO E1")
#	legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#	canvas.Print("figs/"+histo.GetName()+".png")
## diMu_logPT2 Offline and OffHRC
#h_diMu_PT2_Offline.Draw("HISTO E1")
#h_diMu_PT2_OffHRC.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_PT2_Offline_OffHRC.png")
## diMu_logPT2 NonRes and IncJpsi
#h_diMu_PT2_NonRes.Draw("HISTO E1") ; h_diMu_PT2_IncJpsi.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_PT2_NonRes_IncJpsi.png")
#
## fom B or F plots
#for histo in fomHistos:
#    histo.Draw("HISTO E1")
#    legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#    canvas.Print("figs/"+histo.GetName()+".png")
## diMu_HRC F and B Offline
#h_diMu_HRCb_Offline.Draw("HISTO E1") ; h_diMu_HRCf_Offline.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRCb_HRCf_Offline.png")
## 2D HRCbxHRCf Offline
#h2_diMu_HRCb_HRCf_Offline.Draw("COL")
#legend = TLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.AddEntry("", "J/#psi", "") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_diMu_HRCb_HRCf_Offline.png")
#h2_diMu_HRCb_HRCf_Offline.Draw("TEXT20")
#legend = TLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.AddEntry("", "J/#psi", "") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_diMu_HRCb_HRCf_Offline_Text.png")
## diMu_HRC F and B NonRes
#h_diMu_HRCb_NonRes.Draw("HISTO E1") ; h_diMu_HRCf_NonRes.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRCb_HRCf_NonRes.png")
## 2D HRCbxHRCf NonRes
#h2_diMu_HRCb_HRCf_NonRes.Draw("COL")
#legend = TLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.AddEntry("", "NonRes", "") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_diMu_HRCb_HRCf_NonRes.png")
#h2_diMu_HRCb_HRCf_NonRes.Draw("TEXT20")
#legend = TLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.AddEntry("", "NonRes", "") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_diMu_HRCb_HRCf_NonRes_Text.png")
## diMu_HRC F and B EnrNR
#h_diMu_HRCb_EnrNR.Draw("HISTO E1") ; h_diMu_HRCf_EnrNR.Draw("HISTO E1 SAMES")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h_diMu_HRCb_HRCf_EnrNR.png")
## 2D HRCbxHRCf EnrNR
#h2_diMu_HRCb_HRCf_EnrNR.Draw("COL")
#legend = TLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.AddEntry("", "EnrNR", "") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_diMu_HRCb_HRCf_EnrNR.png")
## L0Calo plots
#canvas.SetLogy(0)
#for histo in L0CaloHistos:
#	histo.Draw("HISTO E1")
#	legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#	canvas.Print("figs/"+histo.GetName()+".png")
#h2_muplusVSmuminus_L0Calo_TOS.Draw("COL")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_muplusVSmuminus_L0Calo_TOS.png")
#h2_muplusVSmuminus_L0Calo_TOS.Draw("TEXT20")
#legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
#canvas.Print("figs/h2_muplusVSmuminus_L0Calo_TOS_Text.png")
#
## Efficiency
#B = data.Project("h_muplus_isMuon", "muplus_isMuon", EnrNR)
#A = data.Project("h_muplus_isMuon", "muplus_isMuon", EnrNRhrc)
## print yields
#print("NonRes candidates = %d; LogCutBoth candidates = %d" %(B, A))
## efficiency
#eff = 1.*A/B
#err = (1./B)*(A*(1.-eff))**(1/2.)
#eff = 100*eff
#err = 100*err
## print eff +- err
#print("eff(FOM_BOTH) = (%f +/- %f)" %(eff, err))
#
## Efficiency
#B = data.Project("h_muplus_isMuon", "muplus_isMuon", EnrNR)
#A = data.Project("h_muplus_isMuon", "muplus_isMuon", EnrNRhrcB)
## print yields
#print("NonRes candidates = %d; LogCutOnlyB candidates = %d" %(B, A))
## efficiency
#eff = 1.*A/B
#err = (1./B)*(A*(1.-eff))**(1/2.)
#eff = 100*eff
#err = 100*err
## print eff +- err
#print("eff(FOM_B) = (%f +/- %f)" %(eff, err))
#
## Efficiency
#B = data.Project("h_muplus_isMuon", "muplus_isMuon", EnrNR)
#A = data.Project("h_muplus_isMuon", "muplus_isMuon", EnrNRhrcF)
## print yields
#print("NonRes candidates = %d; LogCutOnlyF candidates = %d" %(B, A))
## efficiency
#eff = 1.*A/B
#err = (1./B)*(A*(1.-eff))**(1/2.)
#eff = 100*eff
#err = 100*err
## print eff +- err
#print("eff(FOM_F) = (%f +/- %f)" %(eff, err))
