# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys, ROOT # System definitions (as PATH)
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/4_PrunedSamples/LHCb_JpsiFullSelDeltaPhi.root/DecayTree') # LHCb Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
massPlotslog = open("../logs/individualMassPlots.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerLHCbSelections import * #lstMinbiasTriggerLHCb
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
# # LHCb Data Histograms # #
# Enriched incoherent (w w/o HRC and Dphi)
lowestMassGeV = lowestMass/1000 ; highestMassGeV = highestMass/1000
h_LHCb_diMuon_Mass_OfflineHRCPhi  = TH1D( "h_LHCb_diMuon_Mass_OfflineHRCPhi", "LHCb Data;M(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates/[1.3 MeV/#it{c}^{2}]", Nbins, lowestMassGeV, highestMassGeV)
h_LHCb_diMuon_LNPT2_OfflineHRCPhi = TH1D("h_LHCb_diMuon_LNPT2_OfflineHRCPhi",     "LHCb Data;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates/0.12", Nbins,           -12,              0)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
rN = runNumbers
LHCb_diMu_PbPb_2015.Project( "h_LHCb_diMuon_Mass_OfflineHRCPhi", "J_psi_1S_M/1e3", cutFullSelHRCPhi)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_LNPT2_OfflineHRCPhi", "J_psi_1S_LNPT2", cutFullSelHRCPhi)
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
massHistos = [h_LHCb_diMuon_Mass_OfflineHRCPhi, h_LHCb_diMuon_LNPT2_OfflineHRCPhi]
canvas.SetLogy(0)
for histo in massHistos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("E1")
	#if 'NonRes' in name:
	legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	#else:
	#	legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+"_thesis.png")
	# Printing yields
	massPlotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
massPlotslog.close()
