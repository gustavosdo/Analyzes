# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCb Data Sample
#MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
HRCPlotslog = open("../logs/individualHRCPlots.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerLHCbSelections import * #lstMinbiasTriggerLHCb
#from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb
#from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
# # LHCb Data Histograms # #
# PreSelection
h_LHCb_diMuon_HRC_runNumber  = TH1D("h_LHCb_diMuon_HRC_runNumber" , "PreSelection;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# Trigger paths
h_LHCb_diMuon_HRC_muTrigger  = TH1D("h_LHCb_diMuon_HRC_muTrigger" , "Muon Trigger;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_MBTrigger  = TH1D("h_LHCb_diMuon_HRC_MBTrigger" , "mBias Trigger;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_L0muHlt1MB = TH1D("h_LHCb_diMuon_HRC_L0muHlt1MB", "L0mu + Hlt1MB;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_muonMBTCK  = TH1D("h_LHCb_diMuon_HRC_muonMBTCK" , "L0mu+HLMB+TCK;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# NonResonant and Enriched non-resonant (w w/o HRC and Dphi)
h_LHCb_diMuon_HRC_NonResonant  = TH1D("h_LHCb_diMuon_HRC_NonResonant" , "NonResonant;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_EnrichNonRes = TH1D("h_LHCb_diMuon_HRC_EnrichNonRes", "Enr. NonRes;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_NonResHRC    = TH1D("h_LHCb_diMuon_HRC_NonResHRC"   , "NonResHRC;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_EnrNonResHRC = TH1D("h_LHCb_diMuon_HRC_EnrNonResHRC", "EnrNonResHRC;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_NonResPhi    = TH1D("h_LHCb_diMuon_HRC_NonResPhi"   , "NonResPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_EnrNonResPhi = TH1D("h_LHCb_diMuon_HRC_EnrNonResPhi", "EnrNonResPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# NonResonant and Enriched non-resonant with HRC and Dphi
h_LHCb_diMuon_HRC_NonResHRCPhi    = TH1D("h_LHCb_diMuon_HRC_NonResHRCPhi"   , "NonResHRCPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_EnrNonResHRCPhi = TH1D("h_LHCb_diMuon_HRC_EnrNonResHRCPhi", "EnrNonResHRCPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# Enriched incoherent (w w/o HRC and Dphi)
lowestMassGeV = lowestMass/1000 ; highestMassGeV = highestMass/1000
h_LHCb_diMuon_HRC_EnrichIncoh = TH1D("h_LHCb_diMuon_HRC_EnrichIncoh", "Enr. Incoh;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_EnrIncohHRC = TH1D("h_LHCb_diMuon_HRC_EnrIncohHRC", "EnrIncohHRC;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_EnrIncohPhi = TH1D("h_LHCb_diMuon_HRC_EnrIncohPhi", "EnrIncohPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# Enriched incoherent with HRC and Dphi
h_LHCb_diMuon_HRC_EnrIncohHRCPhi = TH1D("h_LHCb_diMuon_HRC_EnrIncohHRCPhi", "EnrIncohHRCPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# Full signal selectin (w w/o HRC and Dphi)
h_LHCb_diMuon_HRC_Offline    = TH1D("h_LHCb_diMuon_HRC_Offline"   , "Offline;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_OfflineHRC = TH1D("h_LHCb_diMuon_HRC_OfflineHRC", "OfflineHRC;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
h_LHCb_diMuon_HRC_OfflinePhi = TH1D("h_LHCb_diMuon_HRC_OfflinePhi", "OfflinePhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)
# Full signal selection with HRC and Dphi
h_LHCb_diMuon_HRC_OfflineHRCPhi = TH1D("h_LHCb_diMuon_HRC_OfflineHRCPhi", "OfflineHRCPhi;ln(#chi^{2}_{HRC});Candidates", Nbins, 4, 11)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #


# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
rN = runNumbers
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_runNumber"      , "log_hrc_fom_v2", runNumbers            )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_muTrigger"      , "log_hrc_fom_v2", rN+"&&"+triggerDimuon )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_MBTrigger"      , "log_hrc_fom_v2", rN+"&&"+triggerMinBias)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_L0muHlt1MB"     , "log_hrc_fom_v2", rN+"&&"+L0MUHlt1MBias )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_muonMBTCK"      , "log_hrc_fom_v2", rN+"&&"+L0MUHlt1MBTCKS)
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_NonResonant"    , "log_hrc_fom_v2", cutNonResonant        )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrichNonRes"   , "log_hrc_fom_v2", cutEnrNonRes          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_NonResHRC"      , "log_hrc_fom_v2", cutNonResHRC          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrNonResHRC"   , "log_hrc_fom_v2", cutEnrNonResHRC       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_NonResPhi"      , "log_hrc_fom_v2", cutNonResPhi          )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrNonResPhi"   , "log_hrc_fom_v2", cutEnrNonResPhi       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_NonResHRCPhi"   , "log_hrc_fom_v2", cutNonResHRCPhi       )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrNonResHRCPhi", "log_hrc_fom_v2", cutEnrNonResHRCPhi    )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrichIncoh"    , "log_hrc_fom_v2", cutEnrIncJpsi         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrIncohHRC"    , "log_hrc_fom_v2", cutEnrIncJpsiHRC      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrIncohPhi"    , "log_hrc_fom_v2", cutEnrIncJpsiPhi      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_EnrIncohHRCPhi" , "log_hrc_fom_v2", cutEnrIncJpsiHRCPhi   )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_Offline"        , "log_hrc_fom_v2", cutFullSelection      )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_OfflineHRC"     , "log_hrc_fom_v2", cutFullSelHRC         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_OfflinePhi"     , "log_hrc_fom_v2", cutFullSelPhi         )
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_HRC_OfflineHRCPhi"  , "log_hrc_fom_v2", cutFullSelHRCPhi      )
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
HRCHistos = [h_LHCb_diMuon_HRC_runNumber,\
			 h_LHCb_diMuon_HRC_muTrigger,\
			 h_LHCb_diMuon_HRC_MBTrigger,\
			 h_LHCb_diMuon_HRC_L0muHlt1MB,\
			 h_LHCb_diMuon_HRC_muonMBTCK,\
			 h_LHCb_diMuon_HRC_NonResonant,\
			 h_LHCb_diMuon_HRC_EnrichNonRes,\
			 h_LHCb_diMuon_HRC_NonResHRC,\
			 h_LHCb_diMuon_HRC_EnrNonResHRC,\
			 h_LHCb_diMuon_HRC_NonResPhi,\
			 h_LHCb_diMuon_HRC_EnrNonResPhi,\
			 h_LHCb_diMuon_HRC_NonResHRCPhi,\
			 h_LHCb_diMuon_HRC_EnrNonResHRCPhi,\
			 h_LHCb_diMuon_HRC_EnrichIncoh,\
			 h_LHCb_diMuon_HRC_EnrIncohHRC,\
			 h_LHCb_diMuon_HRC_EnrIncohPhi,\
			 h_LHCb_diMuon_HRC_EnrIncohHRCPhi,\
			 h_LHCb_diMuon_HRC_Offline,\
			 h_LHCb_diMuon_HRC_OfflineHRC,\
			 h_LHCb_diMuon_HRC_OfflinePhi,\
			 h_LHCb_diMuon_HRC_OfflineHRCPhi]
for histo in HRCHistos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("HISTO E1")
	if name in ['h_LHCb_diMuon_HRC_EnrichIncoh', 'h_LHCb_diMuon_HRC_EnrIncohPhi']:
		legend = canvas.BuildLegend(0.2, 0.77, 0.6, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	else:
		legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
	# Printing yields
	HRCPlotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
HRCPlotslog.close()
