# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_PbPb_2015 = TChain('',''); LHCb_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/LHCb_diMu_PbPb_2015_withNewVars.root/DecayTree') # LHCb Data Sample
MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from nominalTriggerLHCbSelections import * #lstNominalTriggerLHCb
from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_LHCb_diMuon_Dphi_NonResonant = TH1D("h_LHCb_diMuon_Dphi_NonResonant", "NonResonant;#Delta#varphi;Arbitrary Units"   , Nbins, 0, 1)
h_MC_diMuon_Dphi_NonResonant   = TH1D("h_MC_diMuon_Dphi_NonResonant"  , "MC NonResonant;#Delta#varphi;Arbitrary Units", Nbins, 0, 1)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #
h_MC_diMuon_Dphi_NonResonant.SetLineColor(blue) ; h_MC_diMuon_Dphi_NonResonant.SetLineWidth(4) ; h_MC_diMuon_Dphi_NonResonant.SetMarkerColor(blue) ; h_MC_diMuon_Dphi_NonResonant.SetLineStyle(7) ; h_MC_diMuon_Dphi_NonResonant.SetMarkerStyle(20)
h_LHCb_diMuon_Dphi_NonResonant.SetMarkerStyle(45)

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
LHCb_diMu_PbPb_2015.Project("h_LHCb_diMuon_Dphi_NonResonant", "deltaPhi_muplus_muminus", cutNonResonant  )
MC_diMu_PbPb_2015.Project(  "h_MC_diMuon_Dphi_NonResonant"  , "deltaPhi_muplus_muminus", cutMCNonResonant)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
canvas.SetLogy(1)
h_MC_diMuon_Dphi_NonResonant.DrawNormalized("E1")
h_LHCb_diMuon_Dphi_NonResonant.DrawNormalized("E1 SAMES")
legend = canvas.BuildLegend(0.3, 0.65, 0.7, 0.9) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
# selection line ant text
cut = TLine(0.9, 2.4e-5, 0.9, 1.2); cut.SetLineWidth(5); cut.SetLineColor(1); cut.Draw("SAMES");
text = TText(0.7, 7e-3, "Selection"); text.SetTextColor(1); text.Draw("SAMES");
selection = TArrow(0.9, 5e-1, 0.98, 5e-1, 0.05, "|>"); selection.SetLineColor(1); selection.SetFillColor(1);	selection.Draw();

canvas.Print("../figs/h_LHCb_diMuon_Dphi_NonResonantAndh_MC_diMuon_Dphi_NonResonant.png")
