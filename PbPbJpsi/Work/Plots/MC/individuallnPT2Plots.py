# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
lnPT2Plotslog = open("../logs/individualMClnPT2Plots.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_MC_diMuon_LNPT2_muTrigger    = TH1D("h_MC_diMuon_LNPT2_muTrigger"   , "Muon Trigger;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_MBTrigger    = TH1D("h_MC_diMuon_LNPT2_MBTrigger"   , "mBias Trigger;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_L0muHlt1MB   = TH1D("h_MC_diMuon_LNPT2_L0muHlt1MB"  , "L0mu + Hlt1MB;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_NonResonant  = TH1D("h_MC_diMuon_LNPT2_NonResonant" , "NonResonant;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_EnrichNonRes = TH1D("h_MC_diMuon_LNPT2_EnrichNonRes", "Enr. NonRes;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_NonResPhi    = TH1D("h_MC_diMuon_LNPT2_NonResPhi"   , "NonResPhi;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_EnrNonResPhi = TH1D("h_MC_diMuon_LNPT2_EnrNonResPhi", "EnrNonResPhi;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_EnrichIncoh  = TH1D("h_MC_diMuon_LNPT2_EnrichIncoh" , "Enr. Incoh;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_EnrIncohPhi  = TH1D("h_MC_diMuon_LNPT2_EnrIncohPhi" , "EnrIncohPhi;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_Offline      = TH1D("h_MC_diMuon_LNPT2_Offline"     , "Offline;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)
h_MC_diMuon_LNPT2_OfflinePhi   = TH1D("h_MC_diMuon_LNPT2_OfflinePhi"  , "OfflinePhi;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c}^{2})];Candidates", Nbins, -19, 5)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_muTrigger"      , "J_psi_1S_LNPT2", triggerDimuon     )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_MBTrigger"      , "J_psi_1S_LNPT2", triggerMinBias    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_L0muHlt1MB"     , "J_psi_1S_LNPT2", L0MUHlt1MBias     )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_NonResonant"    , "J_psi_1S_LNPT2", cutMCNonResonant  )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_EnrichNonRes"   , "J_psi_1S_LNPT2", cutMCEnrNonRes    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_NonResPhi"      , "J_psi_1S_LNPT2", cutMCNonResPhi    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_EnrNonResPhi"   , "J_psi_1S_LNPT2", cutMCEnrNonResPhi )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_EnrichIncoh"    , "J_psi_1S_LNPT2", cutMCEnrIncJpsi   )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_EnrIncohPhi"    , "J_psi_1S_LNPT2", cutMCEnrIncJpsiPhi)
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_Offline"        , "J_psi_1S_LNPT2", cutMCFullSelection)
MC_diMu_PbPb_2015.Project("h_MC_diMuon_LNPT2_OfflinePhi"     , "J_psi_1S_LNPT2", cutMCFullSelPhi   )
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
lnPT2Histos = [h_MC_diMuon_LNPT2_muTrigger,\
			   h_MC_diMuon_LNPT2_MBTrigger,\
			   h_MC_diMuon_LNPT2_L0muHlt1MB,\
			   h_MC_diMuon_LNPT2_NonResonant,\
			   h_MC_diMuon_LNPT2_EnrichNonRes,\
			   h_MC_diMuon_LNPT2_NonResPhi,\
			   h_MC_diMuon_LNPT2_EnrNonResPhi,\
			   h_MC_diMuon_LNPT2_EnrichIncoh,\
			   h_MC_diMuon_LNPT2_EnrIncohPhi,\
			   h_MC_diMuon_LNPT2_Offline,\
			   h_MC_diMuon_LNPT2_OfflinePhi]
for histo in lnPT2Histos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("HISTO E1")
	#if 'Incoh' in name:
	#	legend = canvas.BuildLegend(0.2, 0.77, 0.6, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	#else:
	legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
	# Printing yields
	lnPT2Plotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
lnPT2Plotslog.close()
