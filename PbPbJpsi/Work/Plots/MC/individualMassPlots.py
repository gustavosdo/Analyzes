# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
massPlotslog = open("../logs/individualMCMassPlots.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_MC_diMuon_Mass_muTrigger  = TH1D("h_MC_diMuon_Mass_muTrigger" , "Muon Trigger;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
h_MC_diMuon_Mass_MBTrigger  = TH1D("h_MC_diMuon_Mass_MBTrigger" , "mBias Trigger;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
h_MC_diMuon_Mass_L0muHlt1MB = TH1D("h_MC_diMuon_Mass_L0muHlt1MB", "L0mu + Hlt1MB;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 15)
h_MC_diMuon_Mass_NonResonant  = TH1D("h_MC_diMuon_Mass_NonResonant" , "NonResonant;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_MC_diMuon_Mass_EnrichNonRes = TH1D("h_MC_diMuon_Mass_EnrichNonRes", "Enr. NonRes;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_MC_diMuon_Mass_NonResPhi    = TH1D("h_MC_diMuon_Mass_NonResPhi"   , "NonResPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
h_MC_diMuon_Mass_EnrNonResPhi = TH1D("h_MC_diMuon_Mass_EnrNonResPhi", "EnrNonResPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 1, 2.7)
lowestMassGeV = lowestMass/1000 ; highestMassGeV = highestMass/1000
h_MC_diMuon_Mass_EnrichIncoh = TH1D("h_MC_diMuon_Mass_EnrichIncoh", "Enr. Incoh;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_MC_diMuon_Mass_EnrIncohPhi = TH1D("h_MC_diMuon_Mass_EnrIncohPhi", "EnrIncohPhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_MC_diMuon_Mass_Offline    = TH1D("h_MC_diMuon_Mass_Offline"   , "Offline;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)
h_MC_diMuon_Mass_OfflinePhi = TH1D("h_MC_diMuon_Mass_OfflinePhi", "OfflinePhi;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, lowestMassGeV, highestMassGeV)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_muTrigger"      , "J_psi_1S_M/1e3", triggerDimuon     )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_MBTrigger"      , "J_psi_1S_M/1e3", triggerMinBias    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_L0muHlt1MB"     , "J_psi_1S_M/1e3", L0MUHlt1MBias     )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_NonResonant"    , "J_psi_1S_M/1e3", cutMCNonResonant  )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_EnrichNonRes"   , "J_psi_1S_M/1e3", cutMCEnrNonRes    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_NonResPhi"      , "J_psi_1S_M/1e3", cutMCNonResPhi    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_EnrNonResPhi"   , "J_psi_1S_M/1e3", cutMCEnrNonResPhi )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_EnrichIncoh"    , "J_psi_1S_M/1e3", cutMCEnrIncJpsi   )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_EnrIncohPhi"    , "J_psi_1S_M/1e3", cutMCEnrIncJpsiPhi)
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_Offline"        , "J_psi_1S_M/1e3", cutMCFullSelection)
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Mass_OfflinePhi"     , "J_psi_1S_M/1e3", cutMCFullSelPhi   )
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
massHistos = [h_MC_diMuon_Mass_muTrigger,\
			  h_MC_diMuon_Mass_MBTrigger,\
			  h_MC_diMuon_Mass_L0muHlt1MB,\
			  h_MC_diMuon_Mass_NonResonant,\
			  h_MC_diMuon_Mass_EnrichNonRes,\
			  h_MC_diMuon_Mass_NonResPhi,\
			  h_MC_diMuon_Mass_EnrNonResPhi,\
			  h_MC_diMuon_Mass_EnrichIncoh,\
			  h_MC_diMuon_Mass_EnrIncohPhi,\
			  h_MC_diMuon_Mass_Offline,\
			  h_MC_diMuon_Mass_OfflinePhi]
canvas.SetLogy(1)
for histo in massHistos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("HISTO E1")
	#if 'NonRes' in name:
	#	legend = canvas.BuildLegend(0.2, 0.27, 0.6, 0.5) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	#else:
	legend = canvas.BuildLegend(0.6, 0.77, 1, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
	# Printing yields
	massPlotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
massPlotslog.close()
