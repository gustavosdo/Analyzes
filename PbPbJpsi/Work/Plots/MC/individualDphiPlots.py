# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for HERSCHEL selection efficiency
# for Jpsi on PbPb LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
MC_diMu_PbPb_2015   = TChain('','');   MC_diMu_PbPb_2015.Add('/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/Data/2_TuplesWithNewVars/MC_diMu_PbPb_2015_withNewVars.root/DecayTree') # MC Data Sample
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
DphiPlotslog = open("../logs/individualMCDphiPlotsMC.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/PbPbJpsi/StyleAndSelection/')
from minbiasTriggerMCSelections   import * #lstMinbiasTriggerMC
from nominalTriggerMCSelections   import * #lstNominalTriggerMC

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_MC_diMuon_Dphi_muTrigger  = TH1D("h_MC_diMuon_Dphi_muTrigger" , "Muon Trigger;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_MBTrigger  = TH1D("h_MC_diMuon_Dphi_MBTrigger" , "mBias Trigger;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_L0muHlt1MB = TH1D("h_MC_diMuon_Dphi_L0muHlt1MB", "L0mu + Hlt1MB;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_NonResonant  = TH1D("h_MC_diMuon_Dphi_NonResonant" , "NonResonant;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_EnrichNonRes = TH1D("h_MC_diMuon_Dphi_EnrichNonRes", "Enr. NonRes;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_NonResPhi    = TH1D("h_MC_diMuon_Dphi_NonResPhi"   , "NonResPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_EnrNonResPhi = TH1D("h_MC_diMuon_Dphi_EnrNonResPhi", "EnrNonResPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_EnrichIncoh = TH1D("h_MC_diMuon_Dphi_EnrichIncoh", "Enr. Incoh;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_EnrIncohPhi = TH1D("h_MC_diMuon_Dphi_EnrIncohPhi", "EnrIncohPhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_Offline    = TH1D("h_MC_diMuon_Dphi_Offline"   , "Offline;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)
h_MC_diMuon_Dphi_OfflinePhi = TH1D("h_MC_diMuon_Dphi_OfflinePhi", "OfflinePhi;|#varphi(#mu^{+}) - #varphi(#mu^{-})|/#pi;Candidates", Nbins, 0, 1)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_muTrigger"      , "deltaPhi_muplus_muminus", triggerDimuon     )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_MBTrigger"      , "deltaPhi_muplus_muminus", triggerMinBias    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_L0muHlt1MB"     , "deltaPhi_muplus_muminus", L0MUHlt1MBias     )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_NonResonant"    , "deltaPhi_muplus_muminus", cutMCNonResonant  )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_EnrichNonRes"   , "deltaPhi_muplus_muminus", cutMCEnrNonRes    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_NonResPhi"      , "deltaPhi_muplus_muminus", cutMCNonResPhi    )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_EnrNonResPhi"   , "deltaPhi_muplus_muminus", cutMCEnrNonResPhi )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_EnrichIncoh"    , "deltaPhi_muplus_muminus", cutMCEnrIncJpsi   )
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_EnrIncohPhi"    , "deltaPhi_muplus_muminus", cutMCEnrIncJpsiPhi)
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_Offline"        , "deltaPhi_muplus_muminus", cutMCFullSelection)
MC_diMu_PbPb_2015.Project("h_MC_diMuon_Dphi_OfflinePhi"     , "deltaPhi_muplus_muminus", cutMCFullSelPhi   )
# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
DphiHistos = [h_MC_diMuon_Dphi_muTrigger,\
			  h_MC_diMuon_Dphi_MBTrigger,\
			  h_MC_diMuon_Dphi_L0muHlt1MB,\
			  h_MC_diMuon_Dphi_NonResonant,\
			  h_MC_diMuon_Dphi_EnrichNonRes,\
			  h_MC_diMuon_Dphi_NonResPhi,\
			  h_MC_diMuon_Dphi_EnrNonResPhi,\
			  h_MC_diMuon_Dphi_EnrichIncoh,\
			  h_MC_diMuon_Dphi_EnrIncohPhi,\
			  h_MC_diMuon_Dphi_Offline,\
			  h_MC_diMuon_Dphi_OfflinePhi]
for histo in DphiHistos:
	# histo name and integral
	name     = histo.GetName()
	integral = histo.GetEntries()
	# saving png plot
	histo.Draw("HISTO E1")
	legend = canvas.BuildLegend(0.3, 0.77, 0.7, 1) ; legend.SetHeader("#splitline{LHCb Unofficial}{Pb-Pb #sqrt{s_{NN}} = 5 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
	# Printing yields
	DphiPlotslog.write("Yield("+name+") = "+str(integral)+"\n")
# close log file
DphiPlotslog.close()
