\babel@toc {english}{}
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Data $\&$ Selection}{5}{0}{2}
\beamer@sectionintoc {3}{HERSCHEL Variables}{8}{0}{3}
\beamer@sectionintoc {4}{HERSCHEL Selection Efficiency}{12}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{17}{0}{5}
