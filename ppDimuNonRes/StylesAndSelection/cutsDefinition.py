# mass windows rejections
#lowJpsiM  = 2.796
#highJpsiM = 3.196
#low2SM    = 3.586
#high2SM   = 3.786
#lowUpsM   = 9.0
#highUpsM  = 10.6

# individual cuts
L0Muon     = "(DiMu_L0Muon_lowMultDecision_TOS)" # at least one muon pt gt(greater than) 200 mev and SPDmult lt 10
Hlt2dimu   = "(DiMu_Hlt2diPhotonDiMuonDecision_TOS)" # both muons pt gt 400 mev
Stripping  = "(StrippingLowMultPP2PPMuMuLineDecision)"
runNumbers = "(runNumber >= 91000)" # removes runs without HLT2 line implemented
nLongTr    = "(nLongTracks == 2)" # long track = created in VELO, detected even on muon chambers
nMuonTr    = "(nMuonTracks == 2)" # both muon tracks must be recognized as muion tracks
muonsEta   = "((muplus_eta >= 2.0) && (muplus_eta <= 4.5) && (muminus_eta >= 2.0) && (muminus_eta <= 4.5))" # lhcb acceptance
muonsPT    = "((muplus_PT >= 500.0) && (muminus_PT >= 500.0))" # minimum transverse momentum of both muons
muonsIsMu  = "((muplus_isMuon == 1) && (muminus_isMuon == 1))" # muon Identification variable (boolean)
diMuonPT2  = "(DiMu_PT2 <= 2.0)" # CEP event: product have low pt
diMuonM    = "(DiMu_M <= 20000)" # low mass CEPs
RemoveRes  = "( (DiMu_M >= 1500 && DiMu_M <= 2796) || ((DiMu_M >= 3196) && (DiMu_M <= 3586)) || ((DiMu_M >= 3786) && (DiMu_M <= 9000)) || (DiMu_M >= 10600) )" #nonres mass
#deltaPhi   = "(deltaPhi_muplus_muminus >= 0.9)" # selects more than 90 percent of MC events for both years
deltaPhi   = "(deltaPhi_muplus_muminus >= 0)" # selects more than 90 percent of MC events for both years

# Selection Chains #
Trigger       = L0Muon+" && "+Hlt2dimu
OfflineRes    = nLongTr+" && "+nMuonTr+" && "+muonsEta+" && "+muonsPT+" && "+muonsIsMu+" && "+diMuonPT2+" && "+diMuonM+" && "+deltaPhi
Offline       = OfflineRes+" && "+RemoveRes
TriggerStrip  = Trigger+" && "+Stripping
FullSelRes    = TriggerStrip+" && "+OfflineRes
FullSelection = TriggerStrip+" && "+Offline
