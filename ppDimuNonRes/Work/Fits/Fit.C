/*
Perform all necessary fits
of cross section of nonresonant
dimuon using LHCb RunI data
gustavo.if.ufrj@gmail.com
*/

#include<stdio.h>
#include<math.h>

void Fit(int year)
{
	gROOT->Reset(); // removes previous definitions
	gROOT->ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/lhcbStyle.C"); // plot style
	
	// RooFit libraries
	gSystem->Load("libRooFit.so");
	using namespace RooFit;
	using namespace RooStats;
	
	// Data definition
    TChain * Data = new TChain("","");
    TChain * MC   = new TChain("","");
	// file name
	TString plotName = "figs/";

	// Plot legend
	TLegend leg(0.6, 0.7, 1.0, 1.0);

	if (year == 2011)
	{
		Data->Add("/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/LHCb11_OFFLINE.root/DecayTree");
		MC->Add("/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/MC11_OFFLINE.root/DecayTree");
		plotName += "keysFromMCsignal_expFreeBackground_2011.png";
		leg.SetHeader("#splitline{LHCb Unofficial}{pp #sqrt{s} = 7 TeV}");
	}
	else if (year == 2012)
	{
		Data->Add("/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/LHCb12_OFFLINE.root/DecayTree");
        MC->Add("/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/MC12_OFFLINE.root/DecayTree");
		plotName += "keysFromMCsignal_expFreeBackground_2012.png";
		leg.SetHeader("#splitline{LHCb Unofficial}{pp #sqrt{s} = 8 TeV}");
	}

	// Range and binning variables
	Int_t  nBin = 100; Float_t pt2_min = 0.0; Float_t pt2_max = 1.0;
	TString binning = "Candidates/[0.01 (GeV/#it{c})^{2}]";

	// PDF variables
	RooRealVar DiMu_PT2("DiMu_PT2","p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2}", pt2_min, pt2_max); // Variable to perform fit
	RooDataSet unbinLHCbData("unbinLHCbData", "unbinLHCbData", Data, DiMu_PT2); // Unbinned LHCb Data pt2 distribution
	RooDataSet unbinDimuonMC("unbinDimuonMC", "unbinDimuonMC",   MC, DiMu_PT2); // Unbinned dimuon MC pt2 distribution
	RooRealVar S("S", "S",  1e4,  1e0,  3e4); // non-resonant exclusive signal
	RooRealVar B("B", "B",  2e4,  1e3,  4e4); // double-diss dimuon background
	RooRealVar C("C", "C",  1e4,  1e3,  4e4); // single-diss dimuon background
	RooRealVar b("b", "b", -8e0, -1e1, -3e0); // double-diss exponential parameter
	RooRealVar c("c", "c", -1e0, -3e0,  0e0); // single-diss exponential parameter

	// Kernel estimation template using dimuon coherent non-resonant MC
	RooKeysPdf templateMC("templateMC","templateMC", DiMu_PT2, unbinDimuonMC, RooKeysPdf::MirrorBoth, 1);
	RooAbsPdf * signalPDF = new RooExtendPdf("signalPDF","signalPDF", templateMC, S);

	// e(-b*pt2) PDF background
	RooGenericPdf expB("expB", "expB", "exp(b*DiMu_PT2)", RooArgSet(b, DiMu_PT2));
	RooAbsPdf * backgroundPDF_b = new RooExtendPdf("backgroundPDF_b", "backgroundPDF_b", expB, B);
	// e(-c*pt2) PDF background
	RooGenericPdf expC("expC", "expC", "exp(c*DiMu_PT2)", RooArgSet(c, DiMu_PT2));
	RooAbsPdf * backgroundPDF_c = new RooExtendPdf("backgroundPDF_c", "backgroundPDF_c", expC, C);
	//
	// double exp
	//RooGenericPdf doubleExp("doubleExp", "doubleExp", "exp(b*DiMu_PT2) + exp(c*DiMu_PT2)", RooArgSet(b, DiMu_PT2,c));
	//RooAbsPdf * inelastic = new RooExtendPdf("inelastic", "inelastic", doubleExp, B); 

	// total PDF = signalPDF + backgroundPDF
	RooAbsPdf * convolutedPDF = new RooAddPdf("convolutedPDF", "convolutedPDF", RooArgList(*signalPDF, *backgroundPDF_b, *backgroundPDF_c));
	//RooAbsPdf * convolutedPDF = new RooAddPdf("convolutedPDF", "convolutedPDF", RooArgList(*signalPDF, *inelastic));

	// Roofit fit to data and save in a RooFitResult object the parameters
	RooFitResult * res = convolutedPDF->fitTo(unbinLHCbData,Hesse(true),Strategy(2), Save(true), Verbose(0), PrintLevel(-1), Warnings(0), PrintEvalErrors(-1));

	//  Data binned (to draw)
	TH1F * pt2Histo = new TH1F("pt2Histo", "p_{T}^{2}", nBin, pt2_min, pt2_max);
	pt2Histo->SetMarkerStyle(20) ; pt2Histo->SetLineWidth(3); pt2Histo->SetMinimum(1);
	Data->Project("pt2Histo", "DiMu_PT2", "");
	RooDataHist binLHCbData("binLHCbData", "binLHCbData", DiMu_PT2, pt2Histo);

	// pt2 Frame
	RooPlot* pt2Frame = DiMu_PT2.frame();
	pt2Frame->GetYaxis()->SetTitle(binning);

	// Draw
	binLHCbData.plotOn(pt2Frame, MarkerSize(1), RooFit::Name("dataPoints"));
	convolutedPDF->plotOn(pt2Frame,                 LineWidth(4), LineStyle(1), LineColor(kBlack), RooFit::Name("totalPDF"));  
	convolutedPDF->plotOn(pt2Frame,       Components(*signalPDF), LineWidth(4),      LineStyle(2),          LineColor(kBlue),      RooFit::Name("templateMC"));  
	convolutedPDF->plotOn(pt2Frame, Components(*backgroundPDF_b), LineWidth(4),      LineStyle(3),           LineColor(kRed), RooFit::Name("expBackground_b"));  
	convolutedPDF->plotOn(pt2Frame, Components(*backgroundPDF_c), LineWidth(4),      LineStyle(4),        LineColor(kViolet), RooFit::Name("expBackground_c"));  
	//convolutedPDF->plotOn(pt2Frame, Components(*inelastic), LineWidth(4),      LineStyle(4),        LineColor(kRed), RooFit::Name("expBackground_c"));  
	binLHCbData.plotOn(pt2Frame, MarkerSize(1));

	// Adding to legend
	leg.AddEntry(pt2Frame->findObject("dataPoints"), "Data", "lp");
	//TLegendEntry * expBackgroundText_b = leg.AddEntry(pt2Frame->findObject("expBackground_b"), "e^{bp_{T}^{2}}", "l");
	//TLegendEntry * expBackgroundText_c = leg.AddEntry(pt2Frame->findObject("expBackground_c"), "e^{cp_{T}^{2}}", "l");
	//TLegendEntry * templateSignalText  = leg.AddEntry(     pt2Frame->findObject("templateMC"),     "RooKeysPdf", "l");
	//expBackgroundText_b->SetTextColor(   kRed);
	//expBackgroundText_c->SetTextColor(kViolet);
	//templateSignalText->SetTextColor(   kBlue);

	// TCanvas to draw
	TCanvas * canvas = new TCanvas("canvas", "canvas", 0, 0, 1604, 1228);
	canvas->SetLogy(1); canvas->cd(); pt2Frame->Draw();

	// chi2/ndof (nearest to 1 the better)
	Double_t chi2;
	chi2 = pt2Frame->chiSquare("totalPDF", "dataPoints", 5);
	leg.AddEntry("", Form("#chi^{2}/ndf = %f", chi2) , "");

	// Getting fitted parameters to print on plot
	Double_t signalYield  = S.getVal();
	Double_t sYieldError  = S.getError();
	Double_t backgrYield  = B.getVal();
	Double_t bYieldError  = B.getError();
	Double_t backgrYieldc = C.getVal();
	Double_t cYieldError  = C.getError();
	Double_t bExpParam    = b.getVal();
	Double_t bExpParError = b.getError();
	Double_t cExpParam    = c.getVal();
	Double_t cExpParError = c.getError();
	leg.AddEntry("", Form("CEP = %.0f +/- %.0f", signalYield , sYieldError) , "");
	//leg.AddEntry("", Form("Bpartial = %.0f +/- %.0f", backgrYield , bYieldError) , "");
	//leg.AddEntry("", Form("Bfull = %.0f +/- %.0f", backgrYieldc, cYieldError) , "");
	//leg.AddEntry("", Form("b = %.3f +/- %.3f", bExpParam   , bExpParError), "");
	//leg.AddEntry("", Form("c = %.3f +/- %.3f", cExpParam   , cExpParError), "");

	// printing legend on plot
	leg.Draw();
	res->Print();

	// save file
	canvas->Print(plotName);

	// save yields
	FILE *yields = NULL;
	yields = fopen("yields.csv", "a");
	if (year == 2011)
	{
		fprintf(yields, "fitType,Signal,errSignal,Background,errBackground,Background,errBackground,b,errb,c,errc\n");
		//fprintf(yields, "fitType,Signal,errSignal,Background,errBackground,b,errb\n");
	}
	fprintf(yields, plotName);
	fprintf(yields, ",");
	fprintf(yields, "%.9f,%.9f,", signalYield , sYieldError);
	fprintf(yields, "%.9f,%.9f,", backgrYield , bYieldError);
	fprintf(yields, "%.9f,%.9f,", backgrYieldc, cYieldError);
	fprintf(yields, "%.9f,%.9f", bExpParam  , bExpParError);
	fprintf(yields, "%.9f,%.9f\n", cExpParam  , cExpParError);
}
