import os, math, ROOT, csv
from ROOT import *
from math import *
from functions import *

# int. lumi. per pol. per year
lumiMD11 = 0.; lumiMU11 = 0.; lumiMD12 = 0.; lumiMU12 = 0.;
lumiWithoutHlt2 = 0.;
errlumiMD11 = 0.; errlumiMU11 = 0.; errlumiMD12 = 0.; errlumiMU12 = 0.;

# store values from files
lumi11 = 0.; lumi12 = 0.; errlumi11 = 0.; errlumi12 = 0.
fsi11 = 0.; fsi12 = 0.; errfsi11 = 0.; errfsi12 = 0.
Nexc11 = 0.; errNexc11 = 0.; Nexc12 = 0.; errNexc12 = 0.
eff11 = 0.; eff12 = 0.; erreff11 = 0.; erreff12 = 0.

with open('/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/Work/Lumi/LumiByYearByPol.txt', 'r') as LumiFile:
	for line in LumiFile:
		if ('Hlt2 line' in line):
			lumiWithoutHlt2 = float(line[33:41])
		if (('2011' in line) and ('MD' in line)):
			lumiMD11 = float(line[37:50])
			errlumiMD11 = float(line[55:68])
		if (('2011' in line) and ('MU' in line)):
			lumiMU11 = float(line[37:50])
			errlumiMU11 = float(line[55:68])
		if (('2012' in line) and ('MD' in line)):
			lumiMD12 = float(line[37:50])
			errlumiMD12 = float(line[55:68])
		if (('2012' in line) and ('MU' in line)):
			lumiMU12 = float(line[37:50])
			errlumiMU12 = float(line[55:68])

with open('/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/Work/Fsi_CorrectSamplesOnLxplus.txt', 'r') as FsiFile:
	for line in FsiFile:
		if('f(SI) 2011' in line):
			fsi11 = float(line[13:26])/100.
			errfsi11 = float(line[30:45])/100.
		if('f(SI) 2012' in line):
			fsi12 = float(line[13:26])/100.
			errfsi12 = float(line[30:45])/100.

with open('/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/Work/Fits/yields.csv', 'r') as NexcFile:
	allYields = csv.reader(NexcFile, delimiter=',') # using cvs reader to import all for a single variable
	for line in allYields: # entry = line from file
		if ( (line[0] != 'fitType') and ('2011' in str(line[0]) ) ):
			Nexc11    = float(line[1]) 
			errNexc11 = float(line[2])
		if ( (line[0] != 'fitType') and ('2012' in str(line[0]) ) ):
			Nexc12    = float(line[1]) 
			errNexc12 = float(line[2])

with open('/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/Work/Efficiencies/usingMC/Selections/Efficiency.log', 'r') as EffFile:
	for line in EffFile:
		if('Full Selection 2011' in line):
			eff11 = float(line[28:41])/100.
			erreff11 = float(line[46:59])/100.
		if('Full Selection 2012' in line):
			eff12 = float(line[28:41])/100.
			erreff12 = float(line[46:59])/100.

[lumi11,errlumi11] = sumLumiByPol(lumiMD11, lumiMU11, errlumiMD11, errlumiMU11, lumiWithoutHlt2, 2011)
[lumi12,errlumi12] = sumLumiByPol(lumiMD12, lumiMU12, errlumiMD12, errlumiMU12,              0., 2012)
[Sigma11, errSigma11] = crossSection(Nexc11, eff11, lumi11, fsi11, errNexc11, errlumi11, errfsi11, erreff11, 2011)
[Sigma12, errSigma12] = crossSection(Nexc12, eff12, lumi12, fsi12, errNexc12, errlumi12, errfsi12, erreff12, 2012)

with open('CrossSection_DiMu_RunI_LHCb.txt', 'w') as sigma:
	sigma.write("Sigma(pp -> pmumup ; 7 TeV) = ( "+str(Sigma11)+" +/- "+str(errSigma11)+" ) pb\n")
	sigma.write("Sigma(pp -> pmumup ; 8 TeV) = ( "+str(Sigma12)+" +/- "+str(errSigma12)+" ) pb\n")
with open('Luminosity_RunI_LHCb.txt', 'w') as lumiFile:
	lumiFile.write("Lumi(7 TeV) = ( "+str(lumi11)+" +/- "+str(errlumi11)+" ) 1/pb\n")
	lumiFile.write("Lumi(8 TeV) = ( "+str(lumi12)+" +/- "+str(errlumi12)+" ) 1/pb\n")
