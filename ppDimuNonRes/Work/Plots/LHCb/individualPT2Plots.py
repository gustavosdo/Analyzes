# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb pp data
# Objective: Produce plots for cross section
# for non-resonant dimuon on pp LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
LHCb_diMu_pp_2011 = TChain('','');
LHCb_diMu_pp_2012 = TChain('','');
LHCb_diMu_pp_2011.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/LHCb11MU.root/DecayTree'); LHCb_diMu_pp_2011.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/LHCb11MD.root/DecayTree')
LHCb_diMu_pp_2012.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/LHCb12MU.root/DecayTree'); LHCb_diMu_pp_2012.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/LHCb12MD.root/DecayTree')
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS AND CODE TO CALC EFF # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/')
from cutsDefinition import *

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_LHCb_2011_diMuon_PT2_L0Muon    = TH1D(   "h_LHCb_2011_diMuon_PT2_L0Muon",           "L0;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2011_diMuon_PT2_Trigger   = TH1D(  "h_LHCb_2011_diMuon_PT2_Trigger",      "Trigger;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2011_diMuon_PT2_Stripping = TH1D("h_LHCb_2011_diMuon_PT2_Stripping", "PreSelection;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2011_diMuon_PT2_Offline   = TH1D(  "h_LHCb_2011_diMuon_PT2_Offline",      "Offline;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2012_diMuon_PT2_L0Muon    = TH1D(   "h_LHCb_2012_diMuon_PT2_L0Muon",           "L0;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2012_diMuon_PT2_Trigger   = TH1D(  "h_LHCb_2012_diMuon_PT2_Trigger",      "Trigger;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2012_diMuon_PT2_Stripping = TH1D("h_LHCb_2012_diMuon_PT2_Stripping", "PreSelection;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)
h_LHCb_2012_diMuon_PT2_Offline   = TH1D(  "h_LHCb_2012_diMuon_PT2_Offline",      "Offline;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Candidates", Nbins, 0, 2)

# # # # # # # # # # 6.PROJECTIONS # # # # # # # # # #
LHCb_diMu_pp_2011.Project(   "h_LHCb_2011_diMuon_PT2_L0Muon", "DiMu_PT2",        L0Muon)
LHCb_diMu_pp_2011.Project(  "h_LHCb_2011_diMuon_PT2_Trigger", "DiMu_PT2",       Trigger)
LHCb_diMu_pp_2011.Project("h_LHCb_2011_diMuon_PT2_Stripping", "DiMu_PT2",  TriggerStrip)
LHCb_diMu_pp_2011.Project(  "h_LHCb_2011_diMuon_PT2_Offline", "DiMu_PT2", FullSelection)
LHCb_diMu_pp_2012.Project(   "h_LHCb_2012_diMuon_PT2_L0Muon", "DiMu_PT2",        L0Muon)
LHCb_diMu_pp_2012.Project(  "h_LHCb_2012_diMuon_PT2_Trigger", "DiMu_PT2",       Trigger)
LHCb_diMu_pp_2012.Project("h_LHCb_2012_diMuon_PT2_Stripping", "DiMu_PT2",  TriggerStrip)
LHCb_diMu_pp_2012.Project(  "h_LHCb_2012_diMuon_PT2_Offline", "DiMu_PT2", FullSelection)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
pt2Histos = [\
h_LHCb_2011_diMuon_PT2_L0Muon,\
h_LHCb_2011_diMuon_PT2_Trigger,\
h_LHCb_2011_diMuon_PT2_Stripping,\
h_LHCb_2011_diMuon_PT2_Offline,\
h_LHCb_2012_diMuon_PT2_L0Muon,\
h_LHCb_2012_diMuon_PT2_Trigger,\
h_LHCb_2012_diMuon_PT2_Stripping,\
h_LHCb_2012_diMuon_PT2_Offline]

canvas.SetLogy(1)
for histo in pt2Histos:
	# histo name 
	name = histo.GetName()
	# saving png plot
	histo.Draw("HISTO E1")
	legend = canvas.BuildLegend(0.15, 0.7, 0.55, 0.93)
	if '2011' in name:
		legend.SetHeader("#splitline{LHCb Unofficial}{pp #sqrt{s} = 7 TeV}") ; legend.Draw("SAMES")
	else:
		legend.SetHeader("#splitline{LHCb Unofficial}{pp #sqrt{s} = 8 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
