# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb pp data
# Objective: Produce plots for cross section
# for non-resonant dimuon on pp LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
MC_diMu_pp_2011 = TChain('',''); MC_diMu_pp_2011.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/MC11Lpair.root/DecayTree')
MC_diMu_pp_2012 = TChain('',''); MC_diMu_pp_2012.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/MC12Lpair.root/DecayTree')
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots
massPlotslog = open("../logs/individualMCMassPlots.log", "w")
effsByMC = open("/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/Work/Efficiencies/usingMC/Selections/Efficiency.log", "w")

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS AND CODE TO CALC EFF # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/Work/Efficiencies')
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/')
from cutsDefinition import *
from CalcEff import *

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_MC_2011_diMuon_Mass_NoCut     = TH1D(    "h_MC_2011_diMuon_Mass_NoCut",       "No Cut;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2011_diMuon_Mass_L0Muon    = TH1D(   "h_MC_2011_diMuon_Mass_L0Muon",           "L0;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2011_diMuon_Mass_Trigger   = TH1D(  "h_MC_2011_diMuon_Mass_Trigger",      "Trigger;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2011_diMuon_Mass_Stripping = TH1D("h_MC_2011_diMuon_Mass_Stripping", "PreSelection;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2011_diMuon_Mass_Offline   = TH1D(  "h_MC_2011_diMuon_Mass_Offline",      "Offline;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2012_diMuon_Mass_NoCut     = TH1D(    "h_MC_2012_diMuon_Mass_NoCut",       "No Cut;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2012_diMuon_Mass_L0Muon    = TH1D(   "h_MC_2012_diMuon_Mass_L0Muon",           "L0;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2012_diMuon_Mass_Trigger   = TH1D(  "h_MC_2012_diMuon_Mass_Trigger",      "Trigger;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2012_diMuon_Mass_Stripping = TH1D("h_MC_2012_diMuon_Mass_Stripping", "PreSelection;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)
h_MC_2012_diMuon_Mass_Offline   = TH1D(  "h_MC_2012_diMuon_Mass_Offline",      "Offline;m(#mu^{+}#mu^{-}) (GeV/#it{c}^{2});Candidates", Nbins, 0, 20)

# # # # # # # # # # 6.INITIAL YIELDS # # # # # # # # # #
N_noCut_2011 = float(MC_diMu_pp_2011.Project("h_MC_2011_diMuon_Mass_NoCut", "DiMu_M/1e3", ""))
N_noCut_2012 = float(MC_diMu_pp_2012.Project("h_MC_2012_diMuon_Mass_NoCut", "DiMu_M/1e3", ""))
yields2011 = [N_noCut_2011]
yields2012 = [N_noCut_2012]

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
MC_diMu_pp_2011.Project(   "h_MC_2011_diMuon_Mass_L0Muon", "DiMu_M/1e3",        L0Muon)
MC_diMu_pp_2011.Project(  "h_MC_2011_diMuon_Mass_Trigger", "DiMu_M/1e3",       Trigger)
MC_diMu_pp_2011.Project("h_MC_2011_diMuon_Mass_Stripping", "DiMu_M/1e3",  TriggerStrip)
MC_diMu_pp_2011.Project(  "h_MC_2011_diMuon_Mass_Offline", "DiMu_M/1e3", FullSelection)
MC_diMu_pp_2012.Project(   "h_MC_2012_diMuon_Mass_L0Muon", "DiMu_M/1e3",        L0Muon)
MC_diMu_pp_2012.Project(  "h_MC_2012_diMuon_Mass_Trigger", "DiMu_M/1e3",       Trigger)
MC_diMu_pp_2012.Project("h_MC_2012_diMuon_Mass_Stripping", "DiMu_M/1e3",  TriggerStrip)
MC_diMu_pp_2012.Project(  "h_MC_2012_diMuon_Mass_Offline", "DiMu_M/1e3", FullSelection)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
massHistos = [\
h_MC_2011_diMuon_Mass_NoCut,\
h_MC_2011_diMuon_Mass_L0Muon,\
h_MC_2011_diMuon_Mass_Trigger,\
h_MC_2011_diMuon_Mass_Stripping,\
h_MC_2011_diMuon_Mass_Offline,\
h_MC_2012_diMuon_Mass_NoCut,\
h_MC_2012_diMuon_Mass_L0Muon,\
h_MC_2012_diMuon_Mass_Trigger,\
h_MC_2012_diMuon_Mass_Stripping,\
h_MC_2012_diMuon_Mass_Offline]

canvas.SetLogy(1)
for histo in massHistos:
	# histo name and integral
	n = histo.GetEntries()
	name = histo.GetName()
	selection = name[5:10]+name[22:]
	# saving png plot
	histo.Draw("HISTO E1")
	legend = canvas.BuildLegend(0.5, 0.67, 0.85, 0.9)
	if '2011' in name:
		legend.SetHeader("#splitline{MC LPair}{pp #sqrt{s} = 7 TeV}") ; legend.Draw("SAMES")
		yields2011.append(n)
		N = yields2011[yields2011.index(n) - 1]
	else:
		legend.SetHeader("#splitline{MC LPair}{pp #sqrt{s} = 8 TeV}") ; legend.Draw("SAMES")
		yields2012.append(n)
		N = yields2012[yields2012.index(n) - 1]
	canvas.Print("../figs/"+name+".png")
	# Selections efficiencies
	eff = EffWithBinomialErr(N,n)[0]; err = EffWithBinomialErr(N,n)[1]
	effsByMC.write("Eff("+str(selection)+") = ("+str(100.*eff)+" +/- "+str(100.*err)+") %\n")
	# Printing yields
	massPlotslog.write("Yield("+name+") = "+str(n)+"\n")
# Full selection chain efficiency
n = yields2011[-1]; N = yields2011[0]
eff = EffWithBinomialErr(N,n)[0]; err = EffWithBinomialErr(N,n)[1]
effsByMC.write("Eff(Full Selection 2011) = ("+str(100.*eff)+" +/- "+str(100.*err)+") %\n")
n = yields2012[-1]; N = yields2012[0]
eff = EffWithBinomialErr(N,n)[0]; err = EffWithBinomialErr(N,n)[1]
effsByMC.write("Eff(Full Selection 2012) = ("+str(100.*eff)+" +/- "+str(100.*err)+") %\n")
# close log file
massPlotslog.close()
effsByMC.close()
