# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb pp data
# Objective: Produce plots for cross section
# for non-resonant dimuon on pp LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(False) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
MC_diMu_pp_2011 = TChain('',''); MC_diMu_pp_2011.Add('/home/gustavo/Drive/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/MC11Lpair.root/DecayTree')
MC_diMu_pp_2012 = TChain('',''); MC_diMu_pp_2012.Add('/home/gustavo/Drive/ppDimuNonRes_LHCb_Run1_Data/1_TuplesWithNewVars/MC12Lpair.root/DecayTree')
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS AND CODE TO CALC EFF # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/')
from cutsDefinition import *

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_MC_2011_diMuon_LNPT2_NoCut    = TH1D(     "h_MC_2011_diMuon_LNPT2_NoCut",           "L0;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2011_diMuon_LNPT2_Trigger   = TH1D(  "h_MC_2011_diMuon_LNPT2_Trigger",      "Trigger;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2011_diMuon_LNPT2_Stripping = TH1D("h_MC_2011_diMuon_LNPT2_Stripping", "PreSelection;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2011_diMuon_LNPT2_Offline   = TH1D(  "h_MC_2011_diMuon_LNPT2_Offline",      "Offline;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2012_diMuon_LNPT2_NoCut    = TH1D(     "h_MC_2012_diMuon_LNPT2_NoCut",           "L0;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2012_diMuon_LNPT2_Trigger   = TH1D(  "h_MC_2012_diMuon_LNPT2_Trigger",      "Trigger;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2012_diMuon_LNPT2_Stripping = TH1D("h_MC_2012_diMuon_LNPT2_Stripping", "PreSelection;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)
h_MC_2012_diMuon_LNPT2_Offline   = TH1D(  "h_MC_2012_diMuon_LNPT2_Offline",      "Offline;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];Candidates", Nbins, -20, 3)

# # # # # # # # # # 6.PROJECTIONS # # # # # # # # # #
MC_diMu_pp_2011.Project(    "h_MC_2011_diMuon_LNPT2_NoCut", "DiMu_LNPT2",            "")
MC_diMu_pp_2011.Project(  "h_MC_2011_diMuon_LNPT2_Trigger", "DiMu_LNPT2",       Trigger)
MC_diMu_pp_2011.Project("h_MC_2011_diMuon_LNPT2_Stripping", "DiMu_LNPT2",  TriggerStrip)
MC_diMu_pp_2011.Project(  "h_MC_2011_diMuon_LNPT2_Offline", "DiMu_LNPT2", FullSelection)
MC_diMu_pp_2012.Project(    "h_MC_2012_diMuon_LNPT2_NoCut", "DiMu_LNPT2",            "")
MC_diMu_pp_2012.Project(  "h_MC_2012_diMuon_LNPT2_Trigger", "DiMu_LNPT2",       Trigger)
MC_diMu_pp_2012.Project("h_MC_2012_diMuon_LNPT2_Stripping", "DiMu_LNPT2",  TriggerStrip)
MC_diMu_pp_2012.Project(  "h_MC_2012_diMuon_LNPT2_Offline", "DiMu_LNPT2", FullSelection)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
e_MC_2011_diMuon_LNPT2_Trigger   = TEfficiency(h_MC_2011_diMuon_LNPT2_Trigger  ,      h_MC_2011_diMuon_LNPT2_NoCut)
e_MC_2011_diMuon_LNPT2_Stripping = TEfficiency(h_MC_2011_diMuon_LNPT2_Stripping,    h_MC_2011_diMuon_LNPT2_Trigger)
e_MC_2011_diMuon_LNPT2_Offline   = TEfficiency(h_MC_2011_diMuon_LNPT2_Offline  ,  h_MC_2011_diMuon_LNPT2_Stripping)
e_MC_2012_diMuon_LNPT2_Trigger   = TEfficiency(h_MC_2012_diMuon_LNPT2_Trigger  ,      h_MC_2012_diMuon_LNPT2_NoCut)
e_MC_2012_diMuon_LNPT2_Stripping = TEfficiency(h_MC_2012_diMuon_LNPT2_Stripping,    h_MC_2012_diMuon_LNPT2_Trigger)
e_MC_2012_diMuon_LNPT2_Offline   = TEfficiency(h_MC_2012_diMuon_LNPT2_Offline  ,  h_MC_2012_diMuon_LNPT2_Stripping)

pt2Effs = [\
e_MC_2011_diMuon_LNPT2_Trigger,\
e_MC_2011_diMuon_LNPT2_Stripping,\
e_MC_2011_diMuon_LNPT2_Offline,\
e_MC_2012_diMuon_LNPT2_Trigger,\
e_MC_2012_diMuon_LNPT2_Stripping,\
e_MC_2012_diMuon_LNPT2_Offline]

for eff in pt2Effs:
	canvas.Clear()
	name = eff.GetName()[:-6]
	selection = name[23:]
	selection = selection+" Efficiency"
	eff.SetTitle("Efficiency;ln[p_{T}^{2}(#mu^{+}#mu^{-})/(GeV/#it{c})^{2}];"+selection)
	eff.Draw("ap")
	if '2011' in name:
		legend = canvas.BuildLegend(0.45, 0.17, 0.85, 0.4) ; legend.SetHeader("#splitline{LHCb Unofficial}{pp #sqrt{s} = 7 TeV}") ; legend.Draw("SAMES")
	else:
		legend = canvas.BuildLegend(0.45, 0.17, 0.85, 0.4) ; legend.SetHeader("#splitline{LHCb Unofficial}{pp #sqrt{s} = 8 TeV}") ; legend.Draw("SAMES")
	canvas.Print("../figs/"+name+".png")
