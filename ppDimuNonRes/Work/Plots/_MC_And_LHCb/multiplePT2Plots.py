# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Sample: LHCb PbPb data
# Objective: Produce plots for cross section
# measurement of nonresonant dimuon on pp LHCb data

# # # # # # # # # # 2.IMPORTS AND DEFINITIONS # # # # # # # # # #
import sys # System definitions (as PATH)
import ROOT # ROOT CERN Python Library
from ROOT import *
from math import log, sqrt, exp # natural log, square root, Euler number exponential
ROOT.TH1.SetDefaultSumw2(True) # Correct sum of errors
gROOT.ProcessLine(".X /home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/lhcbStyle.C") # lhcb style for plots
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;") # Ignoring print alert output
TGaxis.SetMaxDigits(3) # sets a maximum of 3 numbers on ticks on axis
gStyle.SetPalette(kRainBow) # sets nice colors for COLZ plot(s)

# # # # # # # # # # 3.BASICS: SAMPLE, CANVAS TO DRAW, LOG # # # # # # # # # #
MC_diMu_pp_2011 = TChain('',''); MC_diMu_pp_2011.Add(    '/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/MC11_OFFLINE.root/DecayTree')
MC_diMu_pp_2012 = TChain('',''); MC_diMu_pp_2012.Add(    '/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/MC12_OFFLINE.root/DecayTree')
LHCb_diMu_pp_2011 = TChain('',''); LHCb_diMu_pp_2011.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/LHCb11_OFFLINE.root/DecayTree'); 
LHCb_diMu_pp_2012 = TChain('',''); LHCb_diMu_pp_2012.Add('/media/gustavo/Backups/ppDimuNonRes_LHCb_Run1_Data/3_PrunedTrees/LHCb12_OFFLINE.root/DecayTree'); 
canvas = TCanvas("canvas", "canvas", 0, 0, 1604, 1228); canvas.SetBorderSize(0); # 1600 x 1200 plots

# # # # # # # # # # 4.IMPORTING ALL SELECTIONS # # # # # # # # # #
sys.path.insert(0, '/home/gustavo/Dropbox/GitLab/Analyzes/ppDimuNonRes/StylesAndSelection/')
from cutsDefinition import *

# # # # # # # # # # 5.HISTOGRAMS # # # # # # # # # #
Nbins = 100
h_LHCb_2011_PT2_Offline = TH1D("h_LHCb_2011_PT2_Offline", "LHCb Data;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Arbitrary Units", Nbins, 0, 2)
h_MC_2011_PT2_Offline   = TH1D(  "h_MC_2011_PT2_Offline",  "MC LPair;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Arbitrary Units", Nbins, 0, 2)
h_LHCb_2012_PT2_Offline = TH1D("h_LHCb_2012_PT2_Offline", "LHCb Data;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Arbitrary Units", Nbins, 0, 2)
h_MC_2012_PT2_Offline   = TH1D(  "h_MC_2012_PT2_Offline",  "MC LPair;p_{T}^{2}(#mu^{+}#mu^{-}) (GeV/#it{c})^{2};Arbitrary Units", Nbins, 0, 2)

# # # # # # # # # # 6.COSMETICS # # # # # # # # # #
h_MC_2011_PT2_Offline.SetLineColor(blue) ; h_MC_2011_PT2_Offline.SetLineWidth(4) ; h_MC_2011_PT2_Offline.SetMarkerColor(blue) ; h_MC_2011_PT2_Offline.SetLineStyle(7) ; h_MC_2011_PT2_Offline.SetMarkerStyle(20);
h_MC_2012_PT2_Offline.SetLineColor(blue) ; h_MC_2012_PT2_Offline.SetLineWidth(4) ; h_MC_2012_PT2_Offline.SetMarkerColor(blue) ; h_MC_2012_PT2_Offline.SetLineStyle(7) ; h_MC_2012_PT2_Offline.SetMarkerStyle(20);
#
h_LHCb_2011_PT2_Offline.SetMarkerStyle(33)
h_LHCb_2012_PT2_Offline.SetMarkerStyle(33)

# # # # # # # # # # 7.PROJECTIONS # # # # # # # # # #
LHCb_diMu_pp_2011.Project("h_LHCb_2011_PT2_Offline", "DiMu_PT2", FullSelection)
LHCb_diMu_pp_2012.Project("h_LHCb_2012_PT2_Offline", "DiMu_PT2", FullSelection)
MC_diMu_pp_2011.Project("h_MC_2011_PT2_Offline", "DiMu_PT2", FullSelection)
MC_diMu_pp_2012.Project("h_MC_2012_PT2_Offline", "DiMu_PT2", FullSelection)

# # # # # # # # # # 8.PRINT AND SAVE # # # # # # # # # #
canvas.SetLogy(1)
h_MC_2011_PT2_Offline.DrawNormalized("E1")
h_LHCb_2011_PT2_Offline.DrawNormalized("E1 SAMES")
legend = canvas.BuildLegend(0.5, 0.7, 0.9, 0.9) ; legend.SetHeader("pp #sqrt{s} = 7 TeV") ; legend.Draw("SAMES")
canvas.Print("../figs/_MC_and_LHCb_PT2_2011.png")

h_MC_2012_PT2_Offline.DrawNormalized("E1")
h_LHCb_2012_PT2_Offline.DrawNormalized("E1 SAMES")
legend = canvas.BuildLegend(0.5, 0.7, 0.9, 0.9) ; legend.SetHeader("pp #sqrt{s} = 8 TeV") ; legend.Draw("SAMES")
canvas.Print("../figs/_MC_and_LHCb_PT2_2012.png")
