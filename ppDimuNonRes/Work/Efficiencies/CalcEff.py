# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.CODE DESCRIPTION # # # # # # # # # #
# Author: gustavo.if.ufrj at gmail dot com
# Objective: Calculate the efficiency of a selection applied
# on a sample with N signal events before selection and n
# events after selection

from math import *

def EffWithBinomialErr(N, n):
	efficiency  = n/N
	binomialErr = (1./N)*sqrt(n*(1.-n/N))
	results = [efficiency, binomialErr]
	return(results)
