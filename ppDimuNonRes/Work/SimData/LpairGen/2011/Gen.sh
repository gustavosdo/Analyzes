#!/bin/bash
# Shell script to:
# 1.Generate a 2011.cards file to generate Lpair MC in LHCb 2011 configuration
# 2.Generate 1000 events with referenced 2011.cards file (events of dimuon continuum CEP)
# 3.Select only events with M(dimuon) > 1 GeV/c2
# 4.Save these events in a hepevt format
# 5.Convert these hepevt data in a ROOT tuple and on a hepmc file

rm lpair.out
#rm lpair.hepevt
declare n=0 # yield of events with mass > 1GeV
declare ITVG=4
declare NCVG=990
while [ "$n" -lt "1" ]
do
 ########################################
 # Generate a .cards file  with different 
 rm 2011.cards # remove old file
 ITVG=$[$ITVG+1]
 NCVG=$[$NCVG+10]
 # Save these random numbers and all others important parameters
 echo "ITVG	$ITVG" >> 2011.cards	
 echo "NCVG	$NCVG" >> 2011.cards
 echo "IEND	3" >> 2011.cards	
 echo "INPP	3500." >> 2011.cards
 echo "PMOD	2" >> 2011.cards
 echo "INPE	3500." >> 2011.cards
 echo "EMOD	2" >> 2011.cards
 echo "PAIR	13" >> 2011.cards
 echo "MCUT	0" >> 2011.cards
 #echo "THMN	0.7" >> 2011.cards
 #echo "THMX	40." >> 2011.cards
 #############################################
 # Generate events with the card defined above
 rm sample10k.out
 time ./LpairExample -PHOTOS 100000 < 2011.cards > sample10k.out
 ################################################
 # Select only events with M(dimuon) > 1GeV/c2
 n=$(python analyse10k.py $n) 
 echo "$n"
done 
