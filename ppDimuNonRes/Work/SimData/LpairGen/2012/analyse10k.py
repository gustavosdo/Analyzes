# Header
import os
import sys
import ROOT
from ROOT import *
from math import *
import fileinput
# Integers
N = int(sys.argv[1]) # Number of events
j = 0 # iteration
n = 0 # sums number of evets in sample10k.out
# Constants
M_MUON   = 0.1057
M_PROTON = 0.9353
# List of lpair outputs
lpair_outs = ['sample10k.out']
output = open('lpair.out', 'a')
# Muons/Protons 1 and 2 quadri-momenta components
P_Mu1_x = []; P_Mu1_y = []; P_Mu1_z = []; E_Mu1 = 0.
P_Mu2_x = []; P_Mu2_y = []; P_Mu2_z = []; E_Mu2 = 0.
P_P1_x = []; P_P1_y = []; P_P1_z = []; E_P1 = []
P_P2_x = []; P_P2_y = []; P_P2_z = []; E_P2 = []
# Loop to get the number of events and simulated momenta
for line in fileinput.input(lpair_outs):
  splitLine=line.split()
  if line.find("11: KF")!=-1:
    n = n + 1
    #if ( n%1 == 0): print N;
    P_Mu1_x.append(float(splitLine[6]))
    P_Mu1_y.append(float(splitLine[7]))
    P_Mu1_z.append(float(splitLine[8]))
    #E_Mu1.append(float(splitLine[10]))
  if line.find("12: KF")!=-1:
    P_Mu2_x.append(float(splitLine[6]))
    P_Mu2_y.append(float(splitLine[7]))
    P_Mu2_z.append(float(splitLine[8]))
    #E_Mu2.append(float(splitLine[10]))
  if line.find("5: KF")!=-1:
    P_P1_x.append(float(splitLine[6]))
    P_P1_y.append(float(splitLine[7]))
    P_P1_z.append(float(splitLine[8]))
    E_P1.append(float(splitLine[10]))
  if line.find("9: KF")!=-1:
    P_P2_x.append(float(splitLine[6]))
    P_P2_y.append(float(splitLine[7]))
    P_P2_z.append(float(splitLine[8]))
    E_P2.append(float(splitLine[10]))
for j in range(0,n-1):
  p1 = (P_Mu1_x[j]**2. + P_Mu1_y[j]**2. + P_Mu1_z[j]**2.)**.5
  p2 = (P_Mu2_x[j]**2. + P_Mu2_y[j]**2. + P_Mu2_z[j]**2.)**.5
  E_Mu1 = (p1**2. + M_MUON**2.)**.5 ; E_Mu2 = (p2**2. + M_MUON**2.)**.5
  E = E_Mu1 + E_Mu2
  #
  px = P_Mu1_x[j] + P_Mu2_x[j]
  py = P_Mu1_y[j] + P_Mu2_y[j]
  pz = P_Mu1_z[j] + P_Mu2_z[j]
  p = (px**2. + py**2. + pz**2.)**.5
  DiMu_M = (E*E - p*p)**.5
  #
  pt1 = ( P_Mu1_x[j]**2.+ P_Mu1_y[j]**2. )**.5                                
  pt2 = ( P_Mu2_x[j]**2.+ P_Mu2_y[j]**2. )**.5                                
  if(pt1 != 0. and pt2 != 0.):                           
    rap1 = -log( tan( 0.5*atan2(pt1,P_Mu1_z[j]) ) )                       
    rap2 = -log( tan( 0.5*atan2(pt2,P_Mu2_z[j]) ) )                       
    if ( DiMu_M >= 1. and rap1 >= 1.5 and rap1 <= 5. and rap2 >= 1.5 and rap2 <= 5. and pt1 >= 0.375 and pt2 >= 0.375):
      N = N + 1
      output.write('event '+str(N)+'\n')
      output.write('5: KF 2212 name p+ momentum '+str(P_P1_x[j])+' '+str(P_P1_y[j])+' '+str(P_P1_z[j])+' E '+str(E_P1[j])+' M '+str(M_PROTON)+'\n')
      output.write('9: KF 2212 name p- momentum '+str(P_P2_x[j])+' '+str(P_P2_y[j])+' '+str(P_P2_z[j])+' E '+str(E_P2[j])+' M '+str(M_PROTON)+'\n')
      output.write('11: KF -13 name mu+ momentum '+str(P_Mu1_x[j])+' '+str(P_Mu1_y[j])+' '+str(P_Mu1_z[j])+' E '+str(E_Mu1)+' M '+str(M_MUON)+'\n')
      output.write('12: KF 13 name mu- momentum '+str(P_Mu2_x[j])+' '+str(P_Mu2_y[j])+' '+str(P_Mu2_z[j])+' E '+str(E_Mu2)+' M '+str(M_MUON)+'\n')
print N
