#!/bin/bash
# Shell script to:
# 1.Generate a 2011.cards file to generate Lpair MC in LHCb 2011 configuration
# 2.Generate 1000 events with referenced 2011.cards file (events of dimuon continuum CEP)
# 3.Select only events with M(dimuon) > 1 GeV/c2
# 4.Save these events in a hepevt format
# 5.Convert these hepevt data in a ROOT tuple and on a hepmc file

rm lpair.out
rm lpair.hepevt
declare n="0" # yield of events with mass > 1GeV
while [ "$n" -lt "20000" ]
do
 ########################################
 # Generate a .cards file  with different 
 rm 2011.cards # remove old file
 declare ITVG=5 # define number of VEGAS iterations
 declare NCVG=1000 # define maximum number of calculations in VEGAS
 ITVG=$(shuf -i 5-10 -n 1) # pick a number randomly in the 1-5M range
 NCVG=$(shuf -i 1000-5000 -n 1) # pick a number randomly in the 100-10M range
 # Save these random numbers and all others important parameters
 echo "ITVG	$ITVG" >> 2011.cards	
 echo "NCVG	$NCVG" >> 2011.cards
 echo "IEND	3" >> 2011.cards	
 echo "INPP	3500." >> 2011.cards
 echo "PMOD	2" >> 2011.cards
 echo "INPE	3500." >> 2011.cards
 echo "EMOD	2" >> 2011.cards
 echo "PAIR	13" >> 2011.cards
 echo "MCUT	2" >> 2011.cards
 echo "PTCT	0.35" >> 2011.cards
 echo "THMN	0.7" >> 2011.cards
 echo "THMX	40." >> 2011.cards
 #############################################
 # Generate 1K events with the card defined above
 #./LpairExample -PHOTOS 1000 < 2011.cards > samples.txt
 rm samples.txt # remove old file
 ./LpairExample -PHOTOS 1 < 2011.cards >> samples.txt
 ################################################
 # Select only events with M(dimuon) > 1GeV/c2
 # Strings to determine charge of muons
 declare proton1="5:"
 declare proton2="9:"
 declare proton1_PX=0 
 declare proton1_PY=0 
 declare proton1_PZ=0 
 declare proton1_P2=0 
 declare proton1_E=0 
 declare proton2_PX=0 
 declare proton2_PY=0 
 declare proton2_PZ=0 
 declare proton2_P2=0 
 declare proton2_E=0 
 declare muplus="11:"
 declare muminus="12:"
 declare muplus_PX=0 
 declare muplus_PY=0 
 declare muplus_PZ=0 
 declare muplus_P2=0 
 declare muplus_PT=0 
 declare muplus_E=0 
 declare muminus_PX=0 
 declare muminus_PY=0 
 declare muminus_PZ=0 
 declare muminus_P2=0 
 declare muminus_PT=0 
 declare muminus_E=0 
 declare mu_M=$(echo "0.1057"|bc)
 declare DiMu_PX=0
 declare DiMu_PY=0
 declare DiMu_PZ=0
 declare DiMu_P2=0
 declare DiMu_E=0
 declare DiMu_M=0
 declare p_M=$(echo "0.9353"|bc)
 while read line
 do
   if [[ "$line" == *"$muplus"* ]]
     then
       splitLine=( $line )
       muplus_PX=$(echo "${splitLine[6]}"|bc)
       muplus_PY=$(echo "${splitLine[7]}"|bc)
       muplus_PZ=$(echo "${splitLine[8]}"|bc)
   fi
   if [[ "$line" == *"$muminus"* ]]
     then
       splitLine=( $line )
       muminus_PX=$(echo "${splitLine[6]}"|bc)
       muminus_PY=$(echo "${splitLine[7]}"|bc)
       muminus_PZ=$(echo "${splitLine[8]}"|bc)
   fi
   if [[ "$line" == *"$proton1"* ]]
     then
       splitLine=( $line )
       proton1_PX=$(echo "${splitLine[6]}"|bc)
       proton1_PY=$(echo "${splitLine[7]}"|bc)
       proton1_PZ=$(echo "${splitLine[8]}"|bc)
   fi
   if [[ "$line" == *"$proton2"* ]]
     then
       splitLine=( $line )
       proton2_PX=$(echo "${splitLine[6]}"|bc)
       proton2_PY=$(echo "${splitLine[7]}"|bc)
       proton2_PZ=$(echo "${splitLine[8]}"|bc)
   fi
 done < samples.txt
 proton1_P2=$(echo "$proton1_PX*$proton1_PX + $proton1_PY*$proton1_PY + $proton1_PZ*$proton1_PZ"|bc)
 proton1_PE=$(echo "sqrt($p_M*$p_M + $proton1_P2)"|bc)
 proton2_P2=$(echo "$proton2_PX*$proton2_PX + $proton2_PY*$proton2_PY + $proton2_PZ*$proton2_PZ"|bc)
 proton2_PE=$(echo "sqrt($p_M*$p_M + $proton2_P2)"|bc)
 muplus_P2=$(echo "$muplus_PX*$muplus_PX + $muplus_PY*$muplus_PY + $muplus_PZ*$muplus_PZ"|bc)
 muplus_PT=$(echo "sqrt($muplus_PX*$muplus_PX + $muplus_PY*$muplus_PY)"|bc)
 muplus_PE=$(echo "sqrt($mu_M*$mu_M + $muplus_P2)"|bc)
 muplus_Eta=$(python -c "import math; from math import *; -math.log( math.tan( 0.5*math.atan2($muplus_PT,$muplus_PZ) ) )") 
 muminus_P2=$(echo "$muminus_PX*$muminus_PX + $muminus_PY*$muminus_PY + $muminus_PZ*$muminus_PZ"|bc)
 muminus_PT=$(echo "sqrt($muminus_PX*$muminus_PX + $muminus_PY*$muminus_PY)"|bc)
 muminus_PE=$(echo "sqrt($mu_M*$mu_M + $muminus_P2)"|bc)
 muminus_Eta=$(python -c "import math; from math import *; -math.log( math.tan( 0.5*math.atan2($muminus_PT,$muminus_PZ) ) )")
 DiMu_PX=$(echo "$muplus_PX + $muminus_PX"|bc)
 DiMu_PY=$(echo "$muplus_PY + $muminus_PY"|bc)
 DiMu_PZ=$(echo "$muplus_PZ + $muminus_PZ"|bc)
 DiMu_P2=$(echo "$DiMu_PX*$DiMu_PX + $DiMu_PY*$DiMu_PY + $DiMu_PZ*$DiMu_PZ"|bc)
 DiMu_E=$(echo "$muplus_PE + $muminus_PE"|bc)
 DiMu_M=$(echo "sqrt($DiMu_E*$DiMu_E - $DiMu_P2)"|bc)
 if [[ $(echo $DiMu_M'>='1 | bc -l) -eq 1 ]] ; then
   #[ $(echo $muplus_Eta'>='1 | bc -l) -eq 1 ] && [[ $(echo $muplus_Eta'<='5 | bc -l) -eq 1 ]] && [[ $(echo $muminus_Eta'>='1 | bc -l) -eq 1 ]] && [[ $(echo $muminus_Eta'<='5 | bc -l) -eq 1 ]]
   n=$[$n+1]
   #echo "$muplus_Eta"
   #echo "$muminus_Eta"
   echo "event $n" >> lpair.out
   echo "5: KF 2212 name p+ momentum $proton1_PX $proton1_PY $proton1_PZ E $proton1_PE M $p_M" >> lpair.out
   echo "9: KF 2212 name p+ momentum $proton2_PX $proton2_PY $proton2_PZ E $proton2_PE M $p_M" >> lpair.out
   echo "11: KF -13 name mu+ momentum $muplus_PX $muplus_PY $muplus_PZ E $muplus_PE M $mu_M" >> lpair.out
   echo "12: KF 13 name mu- momentum $muminus_PX $muminus_PY $muminus_PZ E $muminus_PE M $mu_M" >> lpair.out
 fi
done 
