import os
import fileinput
import sys
import math
import ROOT
from ROOT import *
from math import *
ROOT.TH1.SetDefaultSumw2(True)

#jobType = sys.argv[1]
jobType = 2011
DataSets = []
if str(jobType) == '2011':
  DataSets.append('lpair_1.out')
  DataSets.append('lpair_2.out')
  Output = open('lpair.out','w')
else: sys.exit()
# Constants
M_MUON   = 0.1057
M_PROTON = 0.9353

# Reading from LPAIR #########################################################
a = 0 # Number of events within LHCb                                          #
j = 0 # iterator                                                              #
# Muons 1 and 2 momenta components                                            #
px1 = []; py1 = []; pz1 = []; e1 = []; px2 = []; py2 = []; pz2 = []; e2 = []  #
# Protons 1 and 2 momenta components                                          #
Px1 = []; Py1 = []; Pz1 = []; E1 = []; Px2 = []; Py2 = []; Pz2 = []; E2 = []  #
for line in fileinput.input(DataSets):                                        #
  splitLine=line.split()                                                      #
  if line.find("11: KF")!=-1:                                                 #
    a = a + 1;                                                                #
    px1.append(float(splitLine[6]))                                           #
    py1.append(float(splitLine[7]))                                           #
    pz1.append(float(splitLine[8]))                                           #
  elif line.find("12: KF")!=-1:                                               #
    px2.append(float(splitLine[6]))                                           #
    py2.append(float(splitLine[7]))                                           #
    pz2.append(float(splitLine[8]))                                           #
  if line.find("5: KF")!=-1:                                                  #
    Px1.append(float(splitLine[6]))                                           #
    Py1.append(float(splitLine[7]))                                           #
    Pz1.append(float(splitLine[8]))                                           #
    E1.append(float(splitLine[10]))                                           #
  elif line.find("9: KF")!=-1:                                                #
    Px2.append(float(splitLine[6]))                                           #
    Py2.append(float(splitLine[7]))                                           #
    Pz2.append(float(splitLine[8]))                                           #
    E2.append(float(splitLine[10]))                                           #
# Loop to get distributions                                                   #
for j in range(0,a):                                                          #
  pt1 = ( px1[j]**2.+ py1[j]**2. )**.5                                        #
  pt2 = ( px2[j]**2.+ py2[j]**2. )**.5                                        #
  p1 = (px1[j]**2. + py1[j]**2. + pz1[j]**2.)**.5                             #
  p2 = (px2[j]**2. + py2[j]**2. + pz2[j]**2.)**.5                             #
  e1 = (p1**2. + M_MUON**2.)**.5 ; e2 = (p2**2. + M_MUON**2.)**.5             #
  Output.write('\t'+str(j+1)+'\t6\n')
  Output.write('\t1\t2212\t-1\t0\t0\t0\t0\t0.\t0.\t3500.\t3500.\t'+str(M_PROTON)+'\t0.\t0.\t0.\t0.\n')
  Output.write('\t2\t2212\t-1\t0\t0\t0\t0\t0.\t0.\t-3500.\t3500.\t'+str(M_PROTON)+'\t0.\t0.\t0.\t0.\n')
  Output.write('\t3\t2212\t1\t1\t1\t0\t0\t'+str(Px1[j])+'\t'+str(Py1[j])+'\t'+str(Pz1[j])+'\t'+str(E1[j])+'\t'+str(M_PROTON)+'\t0.\t0.\t0.\t0.\n')
  Output.write('\t4\t2212\t1\t2\t2\t0\t0\t'+str(Px2[j])+'\t'+str(Py2[j])+'\t'+str(Pz2[j])+'\t'+str(E2[j])+'\t'+str(M_PROTON)+'\t0.\t0.\t0.\t0.\n')
  Output.write('\t5\t13\t1\t1\t2\t0\t0\t'+str(px1[j])+'\t'+str(py1[j])+'\t'+str(pz1[j])+'\t'+str(e1)+'\t'+str(M_MUON)+'\t0.\t0.\t0.\t0.\n')
  Output.write('\t6\t-13\t1\t1\t2\t0\t0\t'+str(px2[j])+'\t'+str(py2[j])+'\t'+str(pz2[j])+'\t'+str(e2)+'\t'+str(M_MUON)+'\t0.\t0.\t0.\t0.\n')
Output.close()
